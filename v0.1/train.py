#!/bin/python
# normalizzo dati e label

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input, Conv2D, Dropout, Flatten, Concatenate, Reshape, BatchNormalization
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, Callback
import numpy as np
import pandas as pd
import os, sys, time
import glob
import argparse
import matplotlib.ticker as ticker
#from keras import keras.utils
#from keras.utils import plot_model
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


########################################################
parser = argparse.ArgumentParser(description='DNN trainer')
parser.add_argument( '-i', '--filelistname', default="v0.1/data_preparation/training/" )
parser.add_argument( '-n', '--name',  default="standard_train" )
 
args         = parser.parse_args()
filelistname = args.filelistname
name = args.name


paths = [filelistname + 'models', filelistname + 'history', filelistname + 'images']

# Check whether the specified path exists or not
for var in paths:
    isExist = os.path.exists(var)

    if not isExist:
    
        # Create a new directory because it does not exist 
        os.makedirs(var)
        print("The " + var + " directory is created!")


def getDataLabels(data):
    data.fillna(0, inplace = True)
    labels = data.pop(0)
    return data, labels

def norm(data, mean, std):
    normed_data = (data - mean)/std
    return normed_data

def snorm(normed_data, mean, std):
    snorm = normed_data*std + mean
    return snorm

class CustomCallback(Callback):
    def on_epoch_end(self, epoch, logs=None):
        y_pred = model.predict(X_val)
        df = pd.DataFrame(y_pred, columns=['Prediction'])
        df['Truth'] = y_val
        print('values: {} at epoch: {}'.format(df.head(20), epoch))

#Find the training dataset in the specified folder
input_raw_data = glob.glob(os.path.join(filelistname, 'training*_uniform_flip_norm.csv'))
#input_raw_data = glob.glob(os.path.join(filelistname, 'train_uniform_normed_6000000.csv'))

raw_train_data = pd.read_csv(input_raw_data[0] , sep=",", header=None)
normed_train_data, normed_train_labels = getDataLabels(raw_train_data)

title = 'size_' + str(len(normed_train_data))   

inputLayer = Input(shape=(25,))
#x = BatchNormalization()(inputLayer)
####
x = Dense(100, activation='relu')(inputLayer)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
outputLayer = Dense(1, activation='linear')(x)
####
model = Model(inputs=inputLayer, outputs=outputLayer)

model.compile(optimizer = tf.keras.optimizers.Adam(lr=0.0001),                     
             loss = 'mae',
             metrics = ['mse'])
model.summary()

X_train, X_val, y_train, y_val = train_test_split(normed_train_data, normed_train_labels, test_size=0.2)

start = time.time()
print("Start training")
history = model.fit(normed_train_data, normed_train_labels, 
                    epochs = 100, validation_split = 0.2,                                            
                    batch_size = 50, verbose = 2, 
                    callbacks = [
                    EarlyStopping(monitor='val_loss', patience=10, verbose=1), 
                    CustomCallback()])
                    # ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1),
                    # TerminateOnNaN()]) 
end = time.time()
print('Time spent on training: ' + str(int((end - start)/60)) + ' minutes' ) 

model.save(filelistname + '/models/' + title + "_" + name + ".h5")

hist_df = pd.DataFrame(history.history) 
hist_df.insert(loc=0, column='epoch', value=hist_df.index + 1)
hist_df.to_csv(filelistname + '/history/' + title + '.csv', index=False, header=True)

fig = plt.figure(figsize=(8,4))

# loss
plt.subplot(121)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Loss: Mean Absolute Error')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')
axes = plt.gca()
axes.set_ylim([0.34,0.42]) 

# metrics
plt.subplot(122)
plt.plot(history.history['mse'])
plt.plot(history.history['val_mse'])
plt.title('Metrics: Mean Squared Error')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')
axes = plt.gca()
axes.set_ylim([0.24,0.40])
plt.tight_layout()
fig.savefig(filelistname + '/images/' + title + "_" + name + '.png')

print(title + "_" + name + ".h5")
