[![build status](https://gitlab.cern.ch/gguerrie/ttbar-res-2l-nn/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/gguerrie/ttbar-res-2l-nn/commits/master)

**[Technical Overview](#technical-overview)** |
**[Repo structure](#repo-structure)** |
**[Training the model](#training-the-model)** |

Neural Networks to reconstruct the ttbar invariant mass

## Technical Overview

This project deploys a DNN to predict the value of the ttbar resonance invariant mass; the model can be trained on any analysis-top compatible output; the training features are are shown below as actually implemented in the script:
```
    vector<char> *jet_isbtagged_DL1r_77 = 0; t->SetBranchAddress("jet_isbtagged_DL1r_77",&jet_isbtagged_DL1r_77);
    vector<float> *el_pt = 0; t->SetBranchAddress("el_pt",&el_pt);
    vector<float> *el_eta = 0; t->SetBranchAddress("el_eta",&el_eta);
    vector<float> *el_phi = 0; t->SetBranchAddress("el_phi",&el_phi);
    vector<float> *el_e = 0; t->SetBranchAddress("el_e",&el_e);
    
    vector<float> *mu_pt = 0; t->SetBranchAddress("mu_pt",&mu_pt);
    vector<float> *mu_eta = 0; t->SetBranchAddress("mu_eta",&mu_eta);
    vector<float> *mu_phi = 0; t->SetBranchAddress("mu_phi",&mu_phi);
    vector<float> *mu_e = 0; t->SetBranchAddress("mu_e",&mu_e);
    
    vector<float> *jet_pt = 0; t->SetBranchAddress("jet_pt",&jet_pt);
    vector<float> *jet_eta = 0; t->SetBranchAddress("jet_eta",&jet_eta);
    vector<float> *jet_phi = 0; t->SetBranchAddress("jet_phi",&jet_phi);
    vector<float> *jet_e = 0; t->SetBranchAddress("jet_e",&jet_e);

    Float_t MC_t_pt = 0; t->SetBranchAddress("MC_t_pt",&MC_t_pt);
    Float_t MC_t_eta = 0; t->SetBranchAddress("MC_t_eta",&MC_t_eta);
    Float_t MC_t_phi = 0; t->SetBranchAddress("MC_t_phi",&MC_t_phi);
    Float_t MC_t_m = 0; t->SetBranchAddress("MC_t_m",&MC_t_m);

    Float_t MC_tbar_pt = 0; t->SetBranchAddress("MC_tbar_pt",&MC_tbar_pt);
    Float_t MC_tbar_eta = 0; t->SetBranchAddress("MC_tbar_eta",&MC_tbar_eta);
    Float_t MC_tbar_phi = 0; t->SetBranchAddress("MC_tbar_phi",&MC_tbar_phi);
    Float_t MC_tbar_m = 0; t->SetBranchAddress("MC_tbar_m",&MC_tbar_m);
    
    Float_t met_met; t->SetBranchAddress("met_met",&met_met);
    Float_t met_phi; t->SetBranchAddress("met_phi",&met_phi);
```

The ntuples are converted into a `.csv` file and then fed to a simple DNN, which performs a regression task and returns the prediction for mttbar.

## Repo structure

The project is divided into a data preparation step, followed by the trianing step. 

### Data Preparation
To produce a trianing sample, open the `data_preparation` folder and check out the `config.ini` file; it is necessary to set up 3 parameters, which are
- `input_folder`: directory in which the MC files are stored
- `sample_name`: the name of the sub-campaign of interest (mca/mcd/mce, etc)
- `output_folder`: the directory in which to store the `.csv` triaing file
Once the parameters are set-up, it is just necessary to run the execution script:
```
./execute.sh
```

## Training the model
To train the model, first setup the python environment, and then launch the setup script:

```
setup auxiliary/setup_python.sh
./train.py -i <training_dataset_directory> -n <task_name>
```
where the options are expressed as the following:
- `-i` or `--filelistname`: the path to the folder **that contains** the training dataset; just up the folder, nothing more please :D
- `-n` or `--name`: the name of the task: the final output will contain the name mentioned.

In the specified input folder, three new folders will appear: `models`, `images`, `history`; there will be respectively the trained model (`.h5` format), the images of the loss curves, and the values for such losses.






