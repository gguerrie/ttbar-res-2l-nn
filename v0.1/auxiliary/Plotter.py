#!/usr/bin/env python3

"""
This module introduces a class handling the conversion of root files.
"""

from shutil import copyfile
from PlotterModules.CtrlPlotHandler import CtrlPlotHandler
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.Directories import Directories
from HelperModules.Settings import Settings
import HelperModules.HelperFunctions as hf
import uproot
import awkward as ak
import numpy as np
import pandas as pd
import glob
import os
import ROOT
import pickle
pickle.HIGHEST_PROTOCOL = 4


class ConversionHandler(object):
    """
    This class handles the conversion for all models.
    """

    def __init__(self, args):
        ROOT.gErrorIgnoreLevel = ROOT.kWarning
        cfg_Settings = Settings(args.configfile, option="all")
        self.m_args = args
        self.m_cfg_Settings = cfg_Settings
        self.m_Variables = cfg_Settings.get_Variables()
        self.m_VarNames = [var.get("Name") for var in self.m_Variables] + [self.m_cfg_Settings.get_General().get("FoldSplitVariable")]
        self.m_Model = cfg_Settings.get_Model()
        self.m_Samples = cfg_Settings.get_Samples()
        self.m_df_X_all = None
        self.m_df_Y_all = None
        self.m_df_W_all = None
        self.m_df_N_all = None
        self.m_df_T_all = None
        self.m_df_isNominal_all = None

        self.m_SampleTreeName = None
        self.m_SampleSelection = None
        self.m_SampleMCWeight = None
        self.m_SanitizedVariables = None
        self.m_SampleFilePaths = []
        self.m_nEmptyFrames = 0
        self.m_nEmptySamples = 0

        #self.m_NTUPLELOCATION = os.environ["NTUPLE_LOCATION"] + os.environ["NTUPLE_VERSION"] + "/"
        print(cfg_Settings.get_General().get("NtuplePath"))
        self.m_NTUPLELOCATION = cfg_Settings.get_General().get("NtuplePath")
        self.m_Dirs = Directories(cfg_Settings.get_General().get("Job"))
        self.m_DoCtrlPlots = cfg_Settings.get_General().do_CtrlPlots()
        # Copy the config file to the job directory
        copyfile(args.configfile,
                 hf.ensure_trailing_slash(self.m_Dirs.ConfigDir()) + os.path.basename(args.configfile).replace(".cfg", "_conversion_step.cfg"))


    def Check_LogicStrings(self):
        """
        Method to check whether the logic operators in string are valid,
        whether parenthesis are matched and whether string is python conform.
        """
        # Checking for matched brackets
        if not hf.matched(self.m_SampleSelection):
            ErrorMessage("Sample selection has unmatched brackets! %s" % (self.m_SampleSelection))
        if not hf.matched(self.m_SampleMCWeight):
            ErrorMessage("Sample selection has unmatched brackets! %s" % (self.m_SampleMCWeight))
        if any([item in self.m_SampleSelection for item in ["@", "->", "^"]]):
            ErrorMessage("Found c++ style character in selection! %s" % (self.m_SampleSelection))
        if any([item in self.m_cfg_Settings.get_General().get("MCWeight") for item in ["@", "->", "^"]]):
            ErrorMessage("Found c++ style character in weight!" % (self.m_cfg_Settings.get_General().get("MCWeight")))
        if "&&" in self.m_SampleSelection:
            ErrorMessage("Found c++ style '&&' in combined selection string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_SampleSelection:
            ErrorMessage("Found c++ style '||' in combined selection string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_SampleSelection.find("!")
        if self.m_SampleSelection[idx+1]!="=" and idx!=-1:
            ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_SampleSelection.find("!", idx + 1)
            if self.m_SampleSelection[idx+1]!="=" and idx!=-1:
                ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        if "&&" in self.m_SampleMCWeight:
            ErrorMessage("Found c++ style '&&' in combined MCWeight string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_SampleMCWeight:
            ErrorMessage("Found c++ style '||' in combined MCWeight string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_SampleMCWeight.find("!")
        if self.m_SampleMCWeight[idx+1]!="=" and idx!=-1:
            ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_SampleMCWeight.find("!", idx + 1)
            if self.m_SampleMCWeight[idx+1]!="=":
                ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")


    def PerformConversion(self):
        '''
        Main function to perform the conversion. It checks the type of the model and subsequently
        calls the corresponding conversion method to perform the conversion.
        Afterwards it merges the created .h5 datasets and (optionally) creates control plots.
        '''
        if not self.m_args.plots_only:
            self.do_Regression_Conversion()
            self.MergeDatasets()
        if self.m_DoCtrlPlots or self.m_args.plots_only:
            CtrlPlotHandler(self.m_cfg_Settings).do_CtrlPlots(plots=self.m_args.plots)

    def FinaliseConversion(self, Sample):
        '''
        Function to finalise the conversion by concatenating the pandas dataframes corresponding to
        individual folds. The user is notified at the end.

        Keyword arguments:
        Sample -- Sample object
        '''
        ConverterMessage("Finalising conversion for %s (%s)" % (Sample.get("Name"), Sample.get("Type")))
        # Adding all temporary lists together
        self.m_df_X_all = self.m_df_X_all.reset_index(drop=True)
        self.m_df_Y_all = self.m_df_Y_all.reset_index(drop=True)
        self.m_df_W_all = self.m_df_W_all.reset_index(drop=True)
        self.m_df_N_all = self.m_df_N_all.reset_index(drop=True)
        self.m_df_T_all = self.m_df_T_all.reset_index(drop=True)
        self.m_df_isNominal_all = self.m_df_isNominal_all.reset_index(drop=True)
        df_all = pd.concat([self.m_df_X_all,
                            self.m_df_Y_all,
                            self.m_df_W_all,
                            self.m_df_N_all,
                            self.m_df_T_all,
                            self.m_df_isNominal_all],
                           axis=1,
                           sort=False)
        df_all.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + Sample.get("Name") + ".h5", key="df", mode="w")
        ConverterDoneMessage("Processing " + Sample.get("Name") + " (" + Sample.get("Type") + ") DONE!")

    def MergeDatasets(self):
        '''
        Simple function to merge the .h5 datasets which were created together into one big dataset containing everything.
        '''
        if self.m_nEmptySamples == len(self.m_Samples):
            ErrorMessage("None of the given Samples has events passing the respective selection! You need to check this!")
        ConverterMessage("Merging datasets...")
        dfs = [pd.read_hdf(fname) for fname in [hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + Sample.get("Name") + ".h5" for Sample in self.m_Samples]]
        merged = pd.concat(dfs)
        # Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
        seed = np.random.randint(1000, size=1)[0]
        merged = merged.sample(frac=1, random_state=seed).reset_index(drop=True)
        merged.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged.h5", key="df", mode="w")
        ConverterDoneMessage("Merged hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged.h5")

    def get_X(self, Filepath, Treename, Variables, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        '''
        try:
            X_Uncut = uproot.open(Filepath + ":" + Treename)
        except BaseException:
            ErrorMessage("Could not open %s tree in %s" % (Treename, Filepath))

        try:
            foldsplitvar = X_Uncut.arrays(self.m_cfg_Settings.get_General().get("FoldSplitVariable"), library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
        except KeyError as e:
            ErrorMessage("Could not find 'FoldSplitVariable' (%s) in %s : %s" % (self.m_cfg_Settings.get_General().get("FoldSplitVariable"), Filepath + ":" + Treename, e.args[0]))

        try:

            mttbar_arrays = X_Uncut.arrays(filter_name="MC_t*")
            t_vec = ak.zip({
                "pt": mttbar_arrays.MC_t_pt,
                "phi": mttbar_arrays.MC_t_phi,
                "eta": mttbar_arrays.MC_t_eta,
                "energy": mttbar_arrays.MC_t_m,
            }, with_name="t_vec")

            tbar_vec = ak.zip({
                "pt": mttbar_arrays.MC_tbar_pt,
                "phi": mttbar_arrays.MC_tbar_phi,
                "eta": mttbar_arrays.MC_tbar_eta,
                "energy": mttbar_arrays.MC_tbar_m,
            }, with_name="tbar_vec")

            ttMass = (t_vec+tbar_vec).Mag()

        except uproot.exceptions.KeyInFileError as e:
            ErrorMessage("The variable '%s' is missing in tree '%s' in file '%s'. Consider using the 'IgnoreTreenames' option." % (e.args[0], Treename, Filepath))
        except Exception as e:
            ErrorMessage("Could not convert %s. %s!" % (Filepath, e))
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        if len(X) == 0:
            WarningMessage("No events added from %s" % Filepath)
            self.m_nEmptyFrames += 1
            return None
        elif len(X) < 50:
            WarningMessage("Less than 50 events added from %s" % Filepath)
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        for variable in Variables:
            if X[variable].isnull().values.any():
                ErrorMessage("Variable %s has nan entries!" % variable)
        return X



    def do_Regression_Conversion(self):
        """
        Performing the conversion for a regression model.
        """
        for Sample in self.m_Samples:
            self.InitialiseConversion(Sample)
            # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.m_SampleFilePaths:
                df_X = self.get_X(Filepath=Filepath,
                                  Treename=self.m_SampleTreename,
                                  Variables=self.m_SanitizedVariables,
                                  Selection=self.m_SampleSelection)
                if df_X is None:
                    continue
                # for Y we use get_X but just ask for other variables in the truth tree

                df_Y = self.get_X(Filepath=Filepath,
                                Treename="truth",
                                Variables=[self.m_cfg_Settings.get_Model().get("RegressionTarget"), self.m_cfg_Settings.get_General().get("FoldSplitVariable")],
                                Selection="")
                df_Y[self.m_cfg_Settings.get_Model().get("RegressionTarget")] *= self.m_cfg_Settings.get_Model().get("RegressionScale")
                df_Y = df_Y.rename(columns={self.m_cfg_Settings.get_Model().get("RegressionTarget"): "Label"})
                df_X_merged = pd.merge(df_X, df_Y, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                DefaultLength = len(df_X_merged)

                df_X_list.append(df_X_merged[self.m_VarNames])
                df_Y_list.append(df_X_merged["Label"])
            self.m_df_X_all = pd.concat(df_X_list)
            self.m_df_Y_all = pd.concat(df_Y_list)
            self.FinaliseConversion(Sample)

    def get_FullSampleMCWeight(self, Sample):
        '''
        Function combine Sample and general MCWeight in case a sample-specific MCWeight is given

        Keyword arguments:
        Sample --- Sample object to be checked for MCWeight
        '''
        if Sample.get("MCWeight") != "":
            return "%s*%s" % (self.m_cfg_Settings.get_General().get("MCWeight"), Sample.get("MCWeight"))
        return self.m_cfg_Settings.get_General().get("MCWeight")

    def get_FullSampleSelection(self, Sample):
        '''
        Function combine Sample and general selection in case a sample-specific selection is given

        Keyword arguments:
        Selection --- Selection string to be checked
        '''
        if Sample.get("Selection") != "":
            return "(%s)&(%s)" % (self.m_cfg_Settings.get_General().get("Selection"), Sample.get("Selection"))
        return self.m_cfg_Settings.get_General().get("Selection")
