#!/bin/python
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import numpy as np
import pandas as pd
import time
import matplotlib.ticker as ticker
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import os

def Uniform(name3, name2, input_txt):
    print('')
    print('############################')
    print('INFO: Making sample uniform')
    print('############################')
    counts = np.zeros(21)
    #uniform_events = 21000
    uniform_events = 840000
    #max_count = 31579
    #max_count = 1000
    max_count = 40000
    rows_list = []
    m = 1
    bin = pd.DataFrame(np.arange(350000,2350000,100000))
    bin = bin.append([99999999], ignore_index=True)

    # 4M events per run
    print('INFO: Loading the .txt file')
    #Template
    #ee = pd.read_csv('/gpfs/atlas/pinamont/TRExFitter_v4_master_new/ttbar_dilep_ee.txt', skiprows=4000000 ,nrows= 4000000, sep=" ", header=None)
    #ee = pd.read_csv('/eos/infnts/atlas/gguerrie/DNN/training/training_mca_dil.txt', sep=" ", header=None)
    var= name3 + '/training_' + name2 +'_uniform.csv'
    ee = pd.read_csv(input_txt, sep=" ", header=None)
    print(ee.shape)

    label = ee[0]
    print('INFO: File loaded!')

    for event_index, value in ee.iterrows():
        actual_events =  counts.sum()
        if actual_events != 0: 
            if actual_events%(m*8000) == 0:
                print('INFO: I collected ' + str(int(actual_events)) + ' events (' + str(int(actual_events/uniform_events*100)) + '%) \n')
                #print(bin)
                #print(counts)
                m+=1
        
        if event_index%(m*10000) == 0:
            print('INFO: Event: ' + str(int(event_index)))
        
        
        if actual_events == uniform_events:
            print('INFO: I collected all ' + str(int(actual_events)) + ' events')
            print('INFO: Event index ' + str(int(event_index)))      
            break
        for bin_index in np.arange(len(bin)-1):
            if label.iat[event_index] > bin.iat[bin_index,0]:
                if label.iat[event_index] < bin.iat[bin_index + 1,0]:
                    if counts[bin_index] < max_count:
                        rows_list.append(value)
                        counts[bin_index] += 1  
                        break
    print('INFO: I collected ' +  str(int(actual_events)) + ' events (' + str(int(actual_events/uniform_events*100)) + '%)')
    #print('INFO: Counts: '+ str(counts))
    df = pd.DataFrame(rows_list) 
    #print(var)
    df = df.sample(frac=1).reset_index(drop=True)
    print("INFO: Saving to " + var)

    df.to_csv(var, sep=',', header=False, index=False)
    return var

############################################################################################################
############################################################################################################
############################################################################################################

def FlipAngle(name3, name2, name):
    print('')
    print('############################')
    print('INFO: Data augmentation step')
    print('############################')
    #df1 = pd.read_csv(name3 + '/training_' + name2 +'_uniform.csv', sep=",", header=None)
    df1 = pd.read_csv(name, sep=",", header=None)

    var=name3 + '/training_' + name2 +'_uniform_flip.csv'

    print('INFO: Length before flipping angles: ' + str(len(df1)))
    df2 = df1.copy(deep=True)

    df2[3] = -df2[3]    # lepton1 phi
    df2[7] = -df2[7]    # lepton2 phi
    df2[11] = -df2[11]  # jet1 phi
    df2[15] = -df2[16]  # jet2 phi
    df2[19] = -df2[21]  # jet3 phi
    df2[22] = -df2[25]  # met phi

    df = pd.concat([df1,df2], ignore_index=True)

    print('INFO: Total length: ' + str(len(df)))

    df = df.sample(frac=1).reset_index(drop=True)

    #print('Total length: ' + str(len(df)))
    df.to_csv(var, index=False, header=False)
    return var

############################################################################################################
############################################################################################################
############################################################################################################

def MeanStdCreator(name3, name2, name):
    def getDataLabels(data):
        data.fillna(0, inplace = True)
        labels = data.pop(0)
        return data, labels
    
    def norm(data, mean, std):
        normed_data = (data - mean)/std
        return normed_data
    print('')
    print('############################')
    print('INFO: Creating mean and std')
    print('############################')
    #raw_train_data = pd.read_csv(name3 + '/training_' + name2 +'_uniform_flip.csv', sep=",", header=None)
    raw_train_data = pd.read_csv(name, sep=",", header=None)      

    var=name3 + '/training_' + name2 +'_uniform_flip_norm.csv'
        #  <--------------
    #raw_train_data = pd.read_csv('/gpfs/atlas/tilotta/uniform/data/train_uniform_non_normed_6000000.csv', sep=",", header=None)          #  <--------------

    #print(raw_train_data)

    #train_data, train_labels = getDataLabels(raw_train_data)

    data_mean = raw_train_data.mean()
    data_std = raw_train_data.std()

    #print(data_mean)

    data_mean = data_mean.to_frame().T
    data_std = data_std.to_frame().T


    #print(data_mean)
    #print(data_std)

    normed_train_data = (raw_train_data - data_mean.iloc[0,:]) / data_std.iloc[0,:]

    #print(normed_train_data)

    normed_train_data.to_csv(var, index=False, header=False)
    data_mean.to_csv(name3 + '/training_' + name2 +'_uniform_flip_mean_norm.csv', index=False, header=False)
    data_std.to_csv(name3 + '/training_' + name2 +'_uniform_flip_std_norm.csv', index=False, header=False)
    return var



