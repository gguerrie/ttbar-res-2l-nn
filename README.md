[![build status](https://gitlab.cern.ch/gguerrie/ttbar-res-2l-nn/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/gguerrie/ttbar-res-2l-nn/commits/master)

**[Technical Overview](#technical-overview)** |
**[Repo structure](#repo-structure)** |
**[Training the model](#training-the-model)** |

Neural Networks to reconstruct the ttbar invariant mass

## Technical Overview

This project deploys a DNN to predict the value of the ttbar resonance invariant mass; the model can be trained on any analysis-top compatible output; the training features and all the network settings are stu up in the `config.cfg` file.

The ntuples are converted into a `.h5` file and then fed to a simple DNN, which performs a regression task and returns the prediction for mttbar.

### Repo structure

The project is divided into an old benchmark version of the analysis, stored into the `v0.1` folder, and a newer one. 

This work was inspired to Steffen Korn's [MVA-trainer](https://gitlab.cern.ch/skorn/mva-trainer); thanks a lot Steffen!

- In the `scripts` folder are stored all the libraries and executables used to run the data preparation and the training. 
- The configuration file is stored in the `configs` directory.

### Get the code
Just execute `git clone https://gitlab.cern.ch/gguerrie/ttbar-res-2l-nn.git`

### Data Preparation

#### Setup with LCG release (cvmfs))
At the beginning of each (each!) session, _**please execute**_ 
```
source setup.sh
```

this will set up the LCG stack with all the necessary packages. Since some of the libraries are not updated, a python virtual environment will be created as well to incoprorate what's missing. Once the session is over, the envionment will remain stored in the `env/` folder. **To deactivate the virtual environment and get rid of it**, just execute:
```
deactivate
rm -rf env/
```

#### Setup via python virtual environment (always work)
IF you want to fully and permanently setup the environment on your machine, profit from the `requirements.txt` file and execute:
```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```
To deactivate / delete the environment, see above. _Notice that the env creation could take a while_.

### Configuration file

The configuration file is composed of 4 different blocks: 
- `GENERAL`
- `VARIABLE`
- `SAMPLE`
- `DNNMODEL`
Click on the drop-down below to check all the possible settings.

<details closed>
<summary> Summary of settings </summary>
<br>

#### `GENERAL` block options
| **Option** | **Effect** |
| ------ | ------ |
| **Inputs** | |
| Job | The name of the `Job`. An output directory (using this string given to this option as the name of the directory) is automatically created and all scripts will store their output in it.|
| DoNOnly | Integer value. Can be set to only read the first `N` entries of a file during conversion. Useful for debugging.|
| NtuplePath | Path to the **folder** containing the ntuples.|
| DileptonMerge | Default value: `True`: merge the 2 electrons and the 2 muons into two generic lepton 1 and 2 |
| CutMttbar | Default value: `True`: make the mttbar distribution uniform by setting a ceiling on each bin content| 
| PhiMirroring | Default value: `False`: mirrors the phi angle of each event to double up statistics| 
| FillNaN | Default value: `mean`: fills the NaN values in the ntuples with different values. Accepted parameters: `mean`, `median`, `zeros`|
| Selection | Python style selection string. Pass a string of (boolean) criteria: *(nJets>=2)&(nEl==2) ...* . This can not be a c++-style string because `uproot` is used for reading the `.root` files.|
| Treename | Name of ROOT `TTree` from which the events are taken in the conversion step. |
| **Preprocessing** | |
| InputScaling | Determine procedure to scale inputs. Possible options are [minmax](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) minmax_symmetric or [standard](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html) and `none`. If `minmax` is chosen inputs are scaled into a range between 0 and 1. If `standard` is chosen the input features are scaled by removing the mean and scaling to unit variance. If `none` is chosen no scaling is applied. Default is `minmax`. |

#### `VARIABLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the variable. |
| Label | Label of the variable. This label will be used in all plots. |
| Binning | Default ROOT-style binning, i.e. 10,0,1 is 10 bins from 0 to 1 |
| CustomBinning | Custom binning, i.e. user-defined edges: e.g. 0.1,0.4,0.6,0.7,0.8,1.0 |

#### `SAMPLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the sample.|
| Type | Type of the sample. Can be `Signal`, `Background`.|
| NtupleFiles | Comma-separated list of ntuple files. Remember that the parent path is already given in the setup script.|

#### Model Options

| **Option** | **Effect** |
| ------ | ------ |
| **Generic options** | |
| Name | Custom name of the model. This name needs to be unique. I.e. if you plan to inject several models you need to make sure the names of the models are unambiguous.|
| Type | Type of the model. Currently supported: `Regression-DNN`:D |
| Loss | Loss function which is used in the training. Currently supported for `DNNMODEL`: `mean_squared_error`, `mean_absolute_error`, `mean_absolute_percentage_error`, `mean_squared_logarithmic_error`, `cosine_similarity`, `huber_loss`, `log_cosh`.|
| Optimiser | String to defined the optimiser used during the training of a deep neural network. Allowed optimisers are: `Adadelta`, `Adagrad`, `Adam`, `Adamax`, `Ftrl`, `Nadam`(Default), `RMSprop` and `SGD`.|
| ValidationSize | Relative size of the validation set used during training. Default if 0.2.|
| Verbosity | Can be 0, 1 or 2. Defines the verbosity level of the training output. 0 represents the most silent option. For configs, that are supposed to run with HTCondor and are produced using the `Optimise` script the verbosity will be set to 1 to reduce the size of the output files.|
| OutputLabels | Custom classifier labels. Pass a comma-separated list of labels for multi-class classification.|
| ModelBinning | Custom binning using a fixed bin with. Pass a comma-separated list of values following the formar `nbins`, `x_low`, `x_high`. Default is 20,0,1. This binning will be used for all appropriate plots. In case of a multi-class classifier simply append different binnings (e.g. 20,0,1|10,0,1|5,0,1 for fixed binning with 20,10 and 5 bins respectively).|
| **Architecture related options** | |
| Nodes | Comma-separated list of neurons for each layer.|
| MaxNodes | Integer defining the maximum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 100.|
| MinNodes | Integer defining the minimum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 10.|
| StepNodes | Integer defining the stepsize between `MaxNodes` and `MinNodes` of a DNN for hyperparameter optimisation. Default 10.|
| MaxLayers | Integer defining the maximum number of layers of a DNN for hyperparameter optimisation. Default 5.|
| MinLayers | Integer defining the minimum number of layers of a DNN for hyperparameter optimisation. Default 1.|
| nEstimators | Number of boosting stages to be performed during the training of a BDT.|
| MaxDepth | Maximum Depth of the individual estimators of a BDT.|
| Criterion | Function to measure the quality of a split in a BDT.| 
| DropoutIndece | Layer indece at which [Dropout](https://keras.io/api/layers/regularization_layers/dropout/) layers are added. Only supported in a DNN.|
| DropoutProb | Probability of dropout. Default is 0.1.|
| BatchNormIndece | Layer indece at which [BatchNormalisation](https://keras.io/api/layers/normalization_layers/batch_normalization/) layers are added. Only supported in a DNN.|
| OutputSize | Number of neurons in the output layer of a DNN.|
| KernelInitialiser | [KernelInitialiser](https://keras.io/api/layers/initializers/) in the hidden layers. Pass a string defining an initializers to define the way to set the initial random weights of Keras layers. Used in a DNN.|
| KernelRegulariser | [KernelRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's kernel. Allowed options are `l1`, `l2` and `l1_l2`. UUsed in a DNN.|
| BiasRegulariser | [BiasRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's bias. Allowed options are `l1`, `l2` and `l1_l2`. Used in a DNN.
| ActivitylRegulariser | [ActivityRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's output. Allowed options are `l1`, `l2` and `l1_l2`. Used in a DNN.|
| SetActivationFunctions | List of activation functions from which possible activation functions for the layers for hyperparameter optimisation are randomly drawn. The default only contains `ReLU`.|
| ActivationFunctions | [Activation function](https://keras.io/api/layers/activations/) in the hidden layers. Pass a list of activation functions with a length that corresponds to the number of hidden layers defined in 'Nodes'. Used in a DNN.|
| OutputActivation | [Activation function](https://keras.io/api/layers/activations/) in the output layer of a DNN.|
| **Learning related options** | |
| Epochs | Number of training epochs for a DNN. Default is 100.|
| LearningRate | Initial learning rate for the training of a DNN or BDT. Default is 0.001.|
| BatchSize | Batch size used in training. Default is 32.|
| Patience | Number of epochs with no improvement after which training will be stopped.|
| MinDelta | Minimum change in the monitored quantity to qualify as an improvement.|
| Metrics | Comma-separated list of metrics to be evaluated during training. Supported for a DNNM`|
| SmoothingAlpha | Float between 0 and 0.5 to be applied to smooth labels according using Y=Y(1-alpha)+alpha/K see [arXiv:1512.00567](https://arxiv.org/abs/1512.00567) for an introduction to label smoothing.|
| UseTensorBoard | Can be True or False. If set to True [TensorBoard](https://www.tensorflow.org/tensorboard) log files are automatically written to the model directory. These can then be studied using TensorBoard. Used in `DNNMODEL`.|
| RegressionTarget | Name of the variable in your Ntuples to use as the target for regression. The variable is read from the truth tree by default.|
| RegressionTargetLabel | Custom label for the regression target variable to be used in the plots.|
| RegressionScale | Custom scale to be applied to the regression target variable on an event by event basis.|
| ReweightTargetDist | Option for a regression model. Can be True (default) or False. When set to True the target distribution is binned using 'ModelBinning'. Afterwards weights are calculated to flatten the distribution and hence provide a flat training distribution of the target variable.|

</details>

Remember to create the shared libraries for the `c++` part of the scripts, using the `Event.C` file in the `scripts/ROOT_Macros` folder: 
```
root -l
.L Event.C++
```
To produce a training sample, simply run 
```
python scripts/Converter.py -c configs/config.cfg
```

### Parallel conversion with HTCondor
In order to speed up the process (could last several hours), it is possible to submit a set of parallel jobs that will convert singularly all the `.root` files present into a folder, without aggregating them into a single dataframe.
To do so, just execute
```
source scripts/HTCondor/ParallelConversion.sh <path_to_config_file>
```
A set of jobs will be released, and a file named `parent_procell_list.txt` will be created. This file will contain the single command lines submitted in condor. This file is drammatically important should some of the jobs fail. In ths case it is possible to re-submit only the failed jobs by running:
```
source scripts/HTCondor/ParallelResubmitter.sh <path_to_config_file>
```
A the end of this process, in order to get a single `.h5` file it is necessary to execute
```
python scripts/CondorMerger.py -c <path_to_config_file>
```
And in the folder `Data/` will appear a `merged.h5` file.

## Training the model
To train the produced model, execute

```
python scripts/Trainer.py -c configs/config.cfg
```

## Evaluating the model and producing plots
To test the model predictions and produce control plots, run:
```
python scripts/Evaluate.py -c configs/config.cfg
```

***
***
***

# Old framework


This project deploys a DNN to predict the value of the ttbar resonance invariant mass; the model can be trained on any analysis-top compatible output; the training features are are shown below as actually implemented in the script:
```
    vector<char> *jet_isbtagged_DL1r_77 = 0; t->SetBranchAddress("jet_isbtagged_DL1r_77",&jet_isbtagged_DL1r_77);
    vector<float> *el_pt = 0; t->SetBranchAddress("el_pt",&el_pt);
    vector<float> *el_eta = 0; t->SetBranchAddress("el_eta",&el_eta);
    vector<float> *el_phi = 0; t->SetBranchAddress("el_phi",&el_phi);
    vector<float> *el_e = 0; t->SetBranchAddress("el_e",&el_e);
    
    vector<float> *mu_pt = 0; t->SetBranchAddress("mu_pt",&mu_pt);
    vector<float> *mu_eta = 0; t->SetBranchAddress("mu_eta",&mu_eta);
    vector<float> *mu_phi = 0; t->SetBranchAddress("mu_phi",&mu_phi);
    vector<float> *mu_e = 0; t->SetBranchAddress("mu_e",&mu_e);
    
    vector<float> *jet_pt = 0; t->SetBranchAddress("jet_pt",&jet_pt);
    vector<float> *jet_eta = 0; t->SetBranchAddress("jet_eta",&jet_eta);
    vector<float> *jet_phi = 0; t->SetBranchAddress("jet_phi",&jet_phi);
    vector<float> *jet_e = 0; t->SetBranchAddress("jet_e",&jet_e);

    Float_t MC_t_pt = 0; t->SetBranchAddress("MC_t_pt",&MC_t_pt);
    Float_t MC_t_eta = 0; t->SetBranchAddress("MC_t_eta",&MC_t_eta);
    Float_t MC_t_phi = 0; t->SetBranchAddress("MC_t_phi",&MC_t_phi);
    Float_t MC_t_m = 0; t->SetBranchAddress("MC_t_m",&MC_t_m);

    Float_t MC_tbar_pt = 0; t->SetBranchAddress("MC_tbar_pt",&MC_tbar_pt);
    Float_t MC_tbar_eta = 0; t->SetBranchAddress("MC_tbar_eta",&MC_tbar_eta);
    Float_t MC_tbar_phi = 0; t->SetBranchAddress("MC_tbar_phi",&MC_tbar_phi);
    Float_t MC_tbar_m = 0; t->SetBranchAddress("MC_tbar_m",&MC_tbar_m);
    
    Float_t met_met; t->SetBranchAddress("met_met",&met_met);
    Float_t met_phi; t->SetBranchAddress("met_phi",&met_phi);
```

The ntuples are converted into a `.csv` file and then fed to a simple DNN, which performs a regression task and returns the prediction for mttbar.

## Overview

The project is divided into a data preparation step, followed by the trianing step. 

### Data Preparation
To produce a trianing sample, open the `data_preparation` folder and check out the `config.ini` file; it is necessary to set up 3 parameters, which are
- `input_folder`: directory in which the MC files are stored
- `sample_name`: the name of the sub-campaign of interest (mca/mcd/mce, etc)
- `output_folder`: the directory in which to store the `.csv` triaing file
Once the parameters are set-up, it is just necessary to run the execution script:
```
./execute.sh
```

## Training
To train the model, first setup the python environment, and then launch the setup script:

```
setup auxiliary/setup_python.sh
./train.py -i <training_dataset_directory> -n <task_name>
```
where the options are expressed as the following:
- `-i` or `--filelistname`: the path to the folder **that contains** the training dataset; just up the folder, nothing more please :D
- `-n` or `--name`: the name of the task: the final output will contain the name mentioned.

In the specified input folder, three new folders will appear: `models`, `images`, `history`; there will be respectively the trained model (`.h5` format), the images of the loss curves, and the values for such losses.



