FROM rootproject/root:latest
RUN useradd --create-home --shell /bin/bash dnn-trainer-user
WORKDIR /home/dnn-trainer-user

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y python3 python3-dev git curl python3-pip graphviz nano

RUN pip3 install --no-cache-dir --upgrade  \
    tensorflow \
    uproot \
    pandas \
    scikit-learn \
    tables \
    matplotlib \
    pydot \
    awkward==1.2.0

RUN pip3 install --no-cache-dir vector

RUN pip3 install --no-cache-dir --extra-index-url https://download.pytorch.org/whl/cpu torch torchvision torchaudio

RUN pip3 install --no-cache-dir -f https://data.pyg.org/whl/torch-1.12.0+cpu.html torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric
    
USER dnn-trainer-user
COPY . .
CMD ["bash"]