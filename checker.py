#!/bin/python
import pandas as pd
import numpy as np
import ROOT
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from tensorflow.keras.models import load_model
from sklearn.preprocessing import StandardScaler


#input_name = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_regression_nodilep/Data/merged.h5"                         # Dataset produced with new framework
#input_name = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_mce_dilepmerge_zeros_parallel/Data/merged.h5"         # Dataset produced with new framework
#input_name = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_daniele/Data/merged_uniform.h5"                            # Daniele's dataset converted in h5 for the new framework
#input_name = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_full_mttbar_mca_nodilepmerge/Data/temp_merged_uniform.h5"   # Dataset produced with new framework
input_name = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_mcd_dilepmerge_chel_parallel_101022/Data/ttbar_dil_mcd.410472.e6348_s3126_r10201_p4514.mc16d.nom._000720output.root.h5"
#bsm_name = ["/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_full_mttbar_mca_dilepmerge_BSM/Data/merged.h5"]#, "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_full_mttbar_mca_dilepmerge_2BSM/Data/merged.h5"]

#model_path = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_daniele/Model/"                                            # Daniele's dataset converted and trained in new framework
#model_path = "/eos/infnts/atlas/gguerrie/DNN/daniele/models/"                                                          # Daniele's dataset converted and trained in old framework
#model_path = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/TEST_fullCampaign_mirrored/Model/"
model_path = "/gpfs/atlas/gguerrie/DNN/ttbar-res-2l-nn/test_mcd_dilepmerge_chel_parallel_101022/Model/"

#model_name = ["size_5999066_uniform_zeros_22f_6M_100_100_100_b50.h5"]
model_name = ["size_1164878_standard_train.h5"]
#model_name = ["size_10763890_mca_dilepton_zeroes_peak_MAE.h5"]#, "size_10763890_mca_dilepton_zeroes_peak_MSE.h5"]#, "CI_test_Regressor_complete_100nodes_1layer_20_6_22_Regression-DNN.h5"]
#model_name = ["size_10763890_peak_train_zeros_22_batchNorm_128.h5"]

#BSM_label_truth = ["TRUTH $t \overline{t}$ mass 1TeV", "TRUTH $t \overline{t}$ mass 2TeV", "TRUTH $t \overline{t}$ mass 3TeV"]
#BSM_label_pred  = ["PRED $t \overline{t}$ mass 1TeV", "PRED $t \overline{t}$ mass 2TeV", "PRED $t \overline{t}$ mass 3TeV"]
print("Opening input file")
SM_data=pd.read_hdf(input_name)
#SM_input = SM_data.drop(columns=['eventNumber', 'Label'])
print("Opening BSM file")
SM_input = SM_data.drop(columns=['Label', 'Label_chel'])



###########################
###########################
# Uniform
mean = pd.read_hdf(model_path + "mean_" + model_name[0])
std = pd.read_hdf(model_path + "std_" + model_name[0])
###########################
print(mean.drop(columns=['Label','Label_chel']))
print(std.drop(columns=['Label','Label_chel']))
###########################

def normalize(input):
    X = (input - mean.drop(columns=['Label', 'Label_chel']).iloc[0]).divide(std.drop(columns=['Label', 'Label_chel']).iloc[0]) # Normalize to the training sample mean and std
    return X

def normalize_truth(input):
    X = (input - mean['Label'].iloc[0]).divide(std['Label'].iloc[0]) # Normalize to the training sample mean and std
    return X

def normalize_chel(input):
    X = (input - mean['Label_chel'].iloc[0]).divide(std['Label_chel'].iloc[0]) # Normalize to the training sample mean and std
    return X

def snormalize(input):
    X = input.mul(std.drop(columns=['Label', 'Label_chel']).iloc[0]) + mean.drop(columns=['Label', 'Label_chel']).iloc[0] # Snormalize to the training sample mean and std
    return X

def snormalize_truth(input):
    X = input * std['Label'].iloc[0] + mean['Label'].iloc[0] # Normalize to the training sample mean and std
    return X

def snormalize_chel(input):
    X = input * std['Label_chel'].iloc[0] + mean['Label_chel'].iloc[0] # Normalize to the training sample mean and std
    return X

# BSM_data=pd.read_hdf(bsm_name)
# #BSM_input = BSM_data.drop(columns=['eventNumber', 'Label'])
# BSM_input = BSM_data.drop(columns=['Label'])

prediction = []
prediction_BSM = []
model_prediction_BSM = []

scaler_x = StandardScaler(with_mean=mean, with_std=std)
scaler_y = StandardScaler()
#X = scaler_x.fit_transform(SM_input)
X = normalize(SM_input)
#print(X)
#X = SM_input
#truth = SM_data['Label'].values.reshape(-1, 1)
#truth = scaler_y.fit_transform(truth)

truth = normalize_truth(SM_data['Label']).values.reshape(-1, 1) # Normalize to the training sample mean and std
truth = snormalize_truth(truth) / 1e6

# truth = scaler_y.inverse_transform(truth) / 1e6
print(truth)

X_bsm = []
truth_BSM = []
scaler_y_BSM = []
for idx, sample in enumerate(bsm_name):
    BSM_data = pd.read_hdf(sample)
    #BSM_input = BSM_data.drop(columns=['eventNumber', 'Label'])
    BSM_input = BSM_data.drop(columns=['Label'])
    # scaler_x_BSM = StandardScaler()
    # scaler_y_BSM.append(StandardScaler())
    #X_bsm.append(scaler_x_BSM.fit_transform(BSM_input))
    #X_bsm.append(scaler_x.transform(BSM_input))
    #print(BSM_input)
    X_bsm.append(normalize(BSM_input))
    #X_bsm = BSM_input
    #truth_bsm = scaler_y_BSM[idx].fit_transform(BSM_data['Label'].values.reshape(-1, 1))
    truth_bsm = normalize_truth(BSM_data['Label']).values.reshape(-1, 1)
    truth_bsm = snormalize_truth(truth_bsm) / 1e6
    #print(truth_bsm)
    truth_BSM.append(truth_bsm)
    # truth_BSM.append(scaler_y_BSM[idx].inverse_transform(truth_bsm) / 1e6)

########################################################
#for model in model_name:
loaded_model = load_model(model_path + str(model_name[0]), compile = False)

prediction_loop = loaded_model.predict(X)
# prediction_loop = scaler_y.inverse_transform(prediction_loop) / 1e6
# prediction_loop = (prediction_loop*std + mean) / 1e6

prediction_loop = snormalize_truth(prediction_loop) / 1e6
prediction.append(prediction_loop)
print(prediction_loop)

for idx, sample in enumerate(bsm_name):
    prediction_loop_BSM = loaded_model.predict(X_bsm[idx])
    # prediction_loop_BSM = scaler_y_BSM[idx].inverse_transform(prediction_loop_BSM) / 1e6
    # prediction_loop_BSM = (prediction_loop_BSM*std + mean) / 1e6

    prediction_loop_BSM = snormalize_truth(prediction_loop_BSM) / 1e6
    prediction_BSM.append(prediction_loop_BSM)

print(len(truth))
########################################################


#bin=np.histogram(np.hstack((prediction[0],truth)), bins=100, range=[np.hstack((prediction[0],truth)).min(), 2])[1] #get the bin edges
bin=np.histogram(np.hstack((prediction[0],truth)), bins=100, range=[0, 2])[1] #get the bin edges
bin_BSM=np.histogram(np.hstack((prediction_BSM[0],truth_BSM[0])), bins=100, range=[0, 2])[1] #get the bin edges
print(len(bin_BSM))
inf = 0.2
sup = 1.8

ratio = []
up = []
down = []
ratio_BSM = []
up_BSM = []
down_BSM = []
colors = ['b', 'g', 'm', 'c']
colors_BSM = ['r', 'g', 'm', 'c']

print("Plotting image at " + str(model_path + model_name[0]) + '.png')
fig = plt.figure(figsize=(16, 16), constrained_layout=True)
grid = gs.GridSpec(3, 1, height_ratios=[5, 1, 1])
a0 = plt.subplot(grid[0])
for idx, item in enumerate(prediction):
    n0, bin0, patches0 = a0.hist(item,  bins=bin, alpha=1, label=model_name[idx], stacked=True, histtype='step', color =colors[idx], density=True)

for idx, item in enumerate(prediction_BSM):
    n0_BSM, bin0_BSM, patches0_BSM = a0.hist(item,  bins=bin_BSM, alpha=1, label=BSM_label_pred[idx], stacked=True, histtype='step', color =colors_BSM[idx], density=True)
    n1_BSM, bin1_BSM, patches1_BSM = a0.hist(truth_BSM[idx],  bins=bin_BSM, alpha=1, label=BSM_label_truth[idx], stacked=True, histtype='step', color = colors_BSM[idx], linestyle='dashed', density=True)

n1, bin1, patches1 = a0.hist(truth,  bins=bin, alpha=1, label="TRUTH $t \overline{t}$ mass", stacked=True, histtype='step', color ='b', linestyle='dashed', density=True)


plt.yticks(fontsize=20)
plt.ylabel('Counts', fontsize=20)
a0.legend(fontsize='x-large')


# for i in range(n0.shape[0]):
#     if n1[i] != 0.:
#         if n0[i] / n1[i] >= sup:
#             up.append(sup)
#             down.append(inf-1)
#             ratio.append(inf-1)
#         elif n0[i] / n1[i] <= inf:
#             up.append(sup+1)
#             down.append(inf)
#             ratio.append(inf-1)
#         elif n0[i] / n1[i] > inf and n0[i] / n1[i] < sup:
#             ratio.append(n0[i] / n1[i])
#             up.append(sup+1)
#             down.append(inf-1)
#     else: 
#         ratio.append(0.)
#         up.append(sup+1)
#         down.append(inf-1)


# for i in range(n0_BSM.shape[0]):
#     if n1_BSM[i] != 0.:
#         if n0_BSM[i] / n1_BSM[i] >= sup:
#             up_BSM.append(sup)
#             down_BSM.append(inf-1)
#             ratio_BSM.append(inf-1)
#         elif n0_BSM[i] / n1_BSM[i] <= inf:
#             up_BSM.append(sup+1)
#             down_BSM.append(inf)
#             ratio_BSM.append(inf-1)
#         elif n0_BSM[i] / n1_BSM[i] > inf and n0_BSM[i] / n1_BSM[i] < sup:
#             ratio_BSM.append(n0_BSM[i] / n1_BSM[i])
#             up_BSM.append(sup+1)
#             down_BSM.append(inf-1)
#     else: 
#         ratio_BSM.append(0.)
#         up_BSM.append(sup+1)
#         down_BSM.append(inf-1)
# ##############################################################################################################
# ##############################################################################################################
# a1 = plt.subplot(grid[1])
# plt.ylim(inf-0.05*(sup-inf), sup+0.05*(sup-inf))
# plt.axhline(y = 1., color = 'k', linestyle = 'dashed')
# a1.scatter(bin0[:-1],     # this is what makes it comparable
#         ratio, # maybe check for div-by-zero!
#         alpha=1,
#         marker='o', 
#         color='k')
# a1.scatter(bin0[:-1],     # this is what makes it comparable
#         up, # maybe check for div-by-zero!
#         alpha=1,
#         marker='^',
#         s=180, 
#         facecolors='none', edgecolors='royalblue')
# a1.scatter(bin0[:-1],     # this is what makes it comparable
#         down, # maybe check for div-by-zero!
#         alpha=1,
#         marker='v',
#         s=180, 
#         facecolors='none', edgecolors='royalblue')
# #a1.hist(xaxis, ratio,  range=(0, 5), alpha=1, label="DNN $t \overline{t}$ SM", stacked=True, histtype='step')
# plt.xlabel('$t \overline{t}$ mass [TeV]', fontsize=20)
# plt.ylabel('Ratio SM', fontsize=20)
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# ##############################################################################################################
# ##############################################################################################################
# a2 = plt.subplot(grid[2])
# plt.ylim(inf-0.05*(sup-inf), sup+0.05*(sup-inf))
# plt.axhline(y = 1., color = 'k', linestyle = 'dashed')
# a2.scatter(bin0_BSM[:-1],     # this is what makes it comparable
#         ratio_BSM, # maybe check for div-by-zero!
#         alpha=1,
#         marker='o', 
#         color='k')
# a2.scatter(bin0_BSM[:-1],     # this is what makes it comparable
#         up_BSM, # maybe check for div-by-zero!
#         alpha=1,
#         marker='^',
#         s=180, 
#         facecolors='none', edgecolors='royalblue')
# a2.scatter(bin0_BSM[:-1],     # this is what makes it comparable
#         down_BSM, # maybe check for div-by-zero!
#         alpha=1,
#         marker='v',
#         s=180, 
#         facecolors='none', edgecolors='royalblue')
# #a1.hist(xaxis, ratio,  range=(0, 5), alpha=1, label="DNN $t \overline{t}$ SM", stacked=True, histtype='step')
# plt.xlabel('$t \overline{t}$ mass [TeV]', fontsize=20)
# plt.ylabel('Ratio BSM', fontsize=20)
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# fig.subplots_adjust(hspace=0)
fig.savefig(model_path + model_name[0] + '.png')
