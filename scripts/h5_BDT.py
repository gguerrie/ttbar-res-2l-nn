#!/bin/python
# normalizzo dati e label

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input, Conv2D, Dropout, Flatten, Concatenate, Reshape, BatchNormalization, Normalization, Rescaling
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, Callback
import numpy as np
import pandas as pd
import os, sys, time
import glob
import argparse
import matplotlib.ticker as ticker
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from sklearn.ensemble import GradientBoostingRegressor



########################################################
parser = argparse.ArgumentParser(description='DNN trainer')
parser.add_argument( '-i', '--filelistname', default="v0.1/data_preparation/training/" )
parser.add_argument( '-n', '--name',  default="standard_train" )
 
args         = parser.parse_args()
filelistname = args.filelistname
name = args.name


paths = [filelistname + 'Model/', filelistname + 'Plots/', filelistname + 'Data/']

# Check whether the specified path exists or not
for var in paths:
    isExist = os.path.exists(var)

    if not isExist:
    
        # Create a new directory because it does not exist 
        os.makedirs(var)
        print("The " + var + " directory is created!")


#Find the training dataset in the specified folder
# input_raw_data = glob.glob(os.path.join(filelistname, 'training*_uniform_flip_norm.csv'))
# input_raw_data = glob.glob(os.path.join(filelistname, 'train_uniform_normed_6000000.csv'))
input_raw_data = glob.glob(os.path.join(paths[2], 'merged_uniform.h5'))
#input_raw_data = glob.glob(os.path.join(paths[2], 'merged.h5'))

print(input_raw_data)

raw_train_data = pd.read_hdf(input_raw_data[0])

y_train = raw_train_data['Label'].values.reshape(-1,1)

#normed_train_data = raw_train_data.drop(columns=['Label', 'eventNumber'])
X_train = raw_train_data.drop(columns=['Label'])


title = 'size_' + str(len(X_train))

print(y_train)

scaler_x = StandardScaler()
scaler_y = StandardScaler()

X_train = scaler_x.fit_transform(X_train)
y_train = scaler_y.fit_transform(y_train)


###########################################################################
# norm_layer = Normalization()
# norm_layer.adapt(X_train)
# print(norm_layer(X_train))
# print(scaler_x.fit_transform(X_train))

# print(y_train.std())
# print(y_train.mean())

# rescaling_layer = Rescaling(scale=y_train.std(), offset=y_train.mean())
# y_train = scaler_y.fit_transform(y_train)
# print(rescaling_layer(y_train))
# print(scaler_y.inverse_transform(y_train))
###########################################################################

model = GradientBoostingRegressor(n_estimators=100,
                                    learning_rate=0.0001,
                                    #loss='absolute_error',
                                    #criterion=self.get("Criterion"),
                                    #min_samples_split=self.get("MinSamplesSplit"),
                                    #min_samples_leaf=self.get("MinSamplesLeaf"),
                                    max_depth=3,
                                    #max_features=self.get("MaxFeatures"),
                                    n_iter_no_change=10,
                                    validation_fraction=0.2,
                                    #tol=self.get("MinDelta"),
                                    verbose=1)


#model.summary()
print(X_train)
print(y_train)

start = time.time()
print("Start training")
history = model.fit(X_train, y_train)

end = time.time()
print('Time spent on training: ' + str(int((end - start)/60)) + ' minutes' ) 

model.save(paths[0] + title + "_" + name + ".h5")
model.save(paths[0] + title + "_" + name )

hist_df = pd.DataFrame(history.history) 
hist_df.insert(loc=0, column='epoch', value=hist_df.index + 1)
#hist_df.to_csv(filelistname + '/history/' + title + '.csv', index=False, header=True)

fig = plt.figure(figsize=(8,4))

# loss
plt.subplot(121)
plt.plot(history.history['val_loss'])
plt.plot(history.history['loss'])
plt.title('Loss: Mean Absolute Error')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.34,0.42]) 

# metrics
plt.subplot(122)
plt.plot(history.history['val_mse'])
plt.plot(history.history['mse'])
plt.title('Metrics: Mean Squared Error')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.24,0.40])
plt.tight_layout()
fig.savefig(paths[1] + title + "_" + name + '.png')

print(title + "_" + name + ".h5")
