#!/bin/bash

#RESET THE LAST OPTION PROCESSED
OPTIND=1

if [ -z $MVA_TRAINER_BASE_DIR ]
then
    echo -e "ERROR\t Please run the main setup before using this script."
    return 1
fi

if [ -z $1 ]
then
    echo -e "ERROR\t Please provide the config name."
    return 1
fi

config=${MVA_TRAINER_BASE_DIR}/$1

if [[ ! -f $config ]]
then
    echo -e "ERROR\t Couldn't find the config file named \"$1\". Try again."
    return 1
fi

# Grab the NtuplePath variable
filepath=$(grep "NtuplePath" $config | sed 's/NtuplePath//g' |  sed 's/=//g' |  sed 's/"//g')
filepath=${filepath//[[:blank:]]/}

# Grab the Job variable
outfolder=$(grep "Job" $config | sed 's/Job//g' |  sed 's/=//g' |  sed 's/"//g')
outfolder=${outfolder//[[:blank:]]/}
outfolder="${MVA_TRAINER_BASE_DIR}/${outfolder}/Data"


echo -e "INFO\t Preliminary cleanup.."
if [ -f parent_process_list.txt ]
then
    rm -rf parent_process_list.txt
elif [ -f child_process_list.txt ]
then
    rm -rf child_process_list.txt
fi

# Initialize parent Jobs (conversion)
###################################################################################################################################################
today=$(date +'%d_%m_%Y')
for file in ${filepath}*.root
do
    IFS='/' read -r -a filename <<< "$file"
    echo "${filename[-1]}_${today},source setup.sh && ${MVA_TRAINER_BASE_DIR}/scripts/Converter.py -c $config -n ${filename[-1]}" >> parent_process_list.txt
done

# Submit the jobs
#csub -n ${today}_parallel_conversion -m -p -j parent_process_list.txt
echo "Submitting this: csub -n parallel_conversion -m -j parent_process_list.txt"
csub -n parallel_conversion -m -j parent_process_list.txt

# Save path to the sub file
PARENT_SUBFILE=${PARALLEL_SUBFILE}

# Initialize child jobs (uniformity and merging)
###################################################################################################################################################

# for file in ${outfolder}/*.root
# do
#     IFS='/' read -r -a filename <<< "$file"
#     echo "${filename[-1]},source setup.sh && ${MVA_TRAINER_BASE_DIR}/scripts/Uniform_cutter.py -m -c $config -n ${filename[-1]}" >> child_process_list.txt
# done

# today=$(date +'%d_%m_%Y')

# # Submit the jobs
# #csub -n ${today}_parallel_merging -m -p -j child_process_list.txt

# # Save path to the sub file
# CHILD_SUBFILE=${PARALLEL_SUBFILE}

# echo -e "INFO\t Creating DAG file.." 
# echo "JOB A $PARENT_SUBFILE" > parallel_submission.dag
# echo "JOB B $CHILD_SUBFILE" >> parallel_submission.dag
# echo "PARENT A CHILD B"     >> parallel_submission.dag

# echo -e "INFO\t Submitting DAG file.."
# export _condor_SCHEDD_HOST=sn.ts.infn.it
# #condor_submit_dag -f parallel_submission.dag

# echo -e "INFO\t Cleaning up.."
# if [ -f parent_process_list.txt ]
# then
#     rm -rf parent_process_list.txt
# elif [ -f child_process_list.txt ]
# then
#     rm -rf child_process_list.txt
# fi