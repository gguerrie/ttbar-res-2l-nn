#!/bin/python
# normalizzo dati e label
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input, Conv2D, Dropout, Flatten, Concatenate, Reshape, BatchNormalization, Normalization, Rescaling
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, Callback, TensorBoard
import numpy as np
import pandas as pd
import os, sys, time
import glob
import argparse
import matplotlib.ticker as ticker
#from keras import keras.utils
#from keras.utils import plot_model
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import HelperModules.HelperFunctions as hf
from HelperModules.Settings import Settings
from HelperModules.Directories import Directories

########################################################
parser = argparse.ArgumentParser(description='DNN trainer')
parser.add_argument( '-i', '--filelistname', default="v0.1/data_preparation/training/" )
parser.add_argument( '-n', '--name',  default="standard_train" )
parser.add_argument("-c", "--configfile", help="Config file", required=True)

args         = parser.parse_args()
filelistname = args.filelistname
name = args.name

CFG_SETTINGS = Settings(args.configfile, option="all")

###################################################################
####################### Settings variables ########################

m_LABELNAME = "Label"

###################################################################
m_Samples = CFG_SETTINGS.get_Samples() # List of all samples
m_NominalSamples = CFG_SETTINGS.get_NominalSamples() # List of nominal samples
m_OutputPath = CFG_SETTINGS.get_General().get("Job") # Path to the 'JOB' directory
m_nFolds = CFG_SETTINGS.get_General().get("Folds") # Number of folds
m_ModelSettings = CFG_SETTINGS.get_Model() # Object holding general Model information
m_GeneralSettings = CFG_SETTINGS.get_General() # Object holding general information
m_OutputLabels = m_ModelSettings.get("OutputLabels") # List of output labels
m_Dirs = Directories("%s"%CFG_SETTINGS.get_General().get("Job")) # Directory object
m_SampleFrac = m_ModelSettings.get("SampleFrac")
m_ScaleOutput = m_GeneralSettings.get("OutputScaling")
DileptonMerge = CFG_SETTINGS.get_General().get("DileptonMerge")
m_VarNames = CFG_SETTINGS.get_VarNames() # List of variable names
m_VarObjects = CFG_SETTINGS.get_Variables() # List of variable objects
m_VarLabels = CFG_SETTINGS.get_VarLabels() # List of variable labels


class CustomCallback(Callback):
    def on_epoch_end(self, epoch, logs=None):
        y_pred = model.predict(X_val)
        df = pd.DataFrame(y_pred, columns=['Prediction'])
        df['Truth'] = y_val
        print('values: {} at epoch: {}'.format(df.head(20), epoch))


raw_train_data = pd.read_hdf(hf.ensure_trailing_slash(m_Dirs.DataDir()) + "merged.h5")

W_tmp = raw_train_data['Weight'].values

if m_ModelSettings.get("ReweightTargetDistribution"):
    nbins = int(m_ModelSettings.get("ModelBinning")[0][0])
    xlow = m_ModelSettings.get("ModelBinning")[0][1]
    xhigh = m_ModelSettings.get("ModelBinning")[0][2]
    hist, bin_edges = np.histogram(raw_train_data[m_LABELNAME].values[~np.isnan(raw_train_data[m_LABELNAME].values)],
                                    bins=nbins,
                                    range=(xlow, xhigh),
                                    weights=W_tmp[~np.isnan(raw_train_data[m_LABELNAME].values)])
    SF = np.array([np.max(hist)/h if h>0 else 1 for h in hist])
    indece = [ind - 1 for ind in np.digitize(raw_train_data[m_LABELNAME].values, bin_edges)]
    indece = [ind if ind != nbins else nbins - 1 for ind in indece]
    W_corr = SF[indece]
    W_tmp *= W_corr
    W_tmp = W_tmp / np.mean(W_tmp)

#print(W_tmp.shape)
#print(np.where(W_tmp == W_tmp.max()))

plt.hist(raw_train_data[m_LABELNAME],bins=nbins,weights=W_tmp)
plt.savefig(m_Dirs.ModelDir() + "/test_distr_" + name + '.png')
plt.clf()
plt.hist(raw_train_data[m_LABELNAME],bins=nbins)
plt.savefig(m_Dirs.ModelDir() + "/test_distr_noweights_" + name + '.png')
plt.clf()
print("saved weighted distribution")

raw_train_data = raw_train_data.drop(['eventNumber','Weight','Sample_Name','Sample_Type','isNominal'], axis=1)

#print(raw_train_data)

title = 'size_' + str(len(raw_train_data))   

std = pd.DataFrame(raw_train_data.std()).T
std.to_hdf(m_Dirs.ModelDir() + "/std_" + title + "_" + name + ".h5", key="df", mode="w")
mean = pd.DataFrame(raw_train_data.mean()).T
mean.to_hdf(m_Dirs.ModelDir() + "/mean_" + title + "_" + name + ".h5", key="df", mode="w")

scaler_x = StandardScaler()
scaler_y = StandardScaler()

scaled_data = scaler_x.fit_transform(raw_train_data)
print("StandardScaler Data")
print(scaled_data)

# Normalize inputs and outputs
raw_train_data = (raw_train_data - mean.values[0,:]) / std.values[0,:]
print("Manual Scaled Data")
print(raw_train_data)

normed_train_labels = raw_train_data['Label'].values.reshape(-1, 1)
normed_train_data = raw_train_data.drop(columns=['Label'])

try:
    normed_train_data = raw_train_data.drop(columns=['Label', 'Label_chel'])
    normed_train_labels = raw_train_data[['Label', 'Label_chel']].values.reshape(-1, 2)
except:
    print("Couldn't find Chel.")

seed = np.random.randint(1000, size=1)[0]
W_train, W_val, _, _ = train_test_split(W_tmp, normed_train_labels, test_size=0.2, random_state=seed)
X_train, X_val, y_train, y_val = train_test_split(normed_train_data, normed_train_labels, test_size=0.2, random_state=seed)

print(raw_train_data)

#X_train = scaler_x.fit_transform(X_train)
#y_train = scaler_y.fit_transform(y_train)


#X_val = scaler_x.fit_transform(X_val)
#y_val = scaler_y.fit_transform(y_val)

print(X_train)
print(y_train)

###########################################################################
# norm_layer = Normalization()
# norm_layer.adapt(X_train)
# print(norm_layer(X_train))
# print(scaler_x.fit_transform(X_train))

# print(y_train.std())
# print(y_train.mean())

# rescaling_layer = Rescaling(scale=y_train.std(), offset=y_train.mean())
# y_train = scaler_y.fit_transform(y_train)
# print(rescaling_layer(y_train))
# print(scaler_y.inverse_transform(y_train))
###########################################################################

inputLayer = Input(shape=(normed_train_data.shape[1],))
#x = norm_layer(inputLayer)
#x = BatchNormalization()(inputLayer)
####
x = Dense(100, activation='relu')(inputLayer)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
# ####
outputLayer = Dense(normed_train_labels.shape[1], activation='linear')(x)
# x = Dense(1, activation='linear')(x)
# outputLayer = rescaling_layer(x)

####
model = Model(inputs=inputLayer, outputs=outputLayer)

model.compile(optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001),                     
             loss = 'mae',
             metrics = ['mse'])
model.summary()


start = time.time()
print("Start training")
history = model.fit(X_train, y_train, 
                    epochs = 150, validation_data = [X_val, y_val, W_val],                                            
                    batch_size = 50, verbose = 1,
                    sample_weight=W_train,
                    callbacks = [
                    EarlyStopping(monitor='val_loss', patience=5, verbose=1),
                    TensorBoard(log_dir=hf.ensure_trailing_slash(m_Dirs.ModelDir()) + title + "_" + name + "_TensorBoardLog",
                             histogram_freq=5)
                    #CustomCallback()
                    ])
                    #ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)])
                    # TerminateOnNaN()]) 
end = time.time()
print('Time spent on training: ' + str(int((end - start)/60)) + ' minutes' ) 

model.save(m_Dirs.ModelDir() + '/' + title + "_" + name + ".h5")
model.save(m_Dirs.ModelDir() + '/' + title + "_" + name )

hist_df = pd.DataFrame(history.history) 
hist_df.insert(loc=0, column='epoch', value=hist_df.index + 1)
#hist_df.to_csv(filelistname + '/history/' + title + '.csv', index=False, header=True)

fig = plt.figure(figsize=(8,4))

# loss
plt.subplot(121)
plt.plot(history.history['val_loss'])
plt.plot(history.history['loss'])
plt.title('Loss: Mean Absolute Error')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.34,0.42]) 

# metrics
plt.subplot(122)
plt.plot(history.history['val_mse'])
plt.plot(history.history['mse'])
plt.title('Metrics: Mean Squared Error')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.24,0.40])
plt.tight_layout()
fig.savefig(m_Dirs.ModelDir() + '/' + title + "_" + name + '.png')

print(title + "_" + name + ".h5")
