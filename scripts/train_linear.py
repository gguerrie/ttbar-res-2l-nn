#!/bin/python
# normalizzo dati e label

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input, Conv2D, Dropout, Flatten, Concatenate, Reshape, BatchNormalization, Normalization, Rescaling
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, Callback
import numpy as np
import pandas as pd
import os, sys, time
import glob
import argparse
import matplotlib.ticker as ticker
#from keras import keras.utils
#from keras.utils import plot_model
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


########################################################
parser = argparse.ArgumentParser(description='DNN trainer')
parser.add_argument( '-i', '--filelistname', default="v0.1/data_preparation/training/" )
parser.add_argument( '-n', '--name',  default="standard_train" )
 
args         = parser.parse_args()
filelistname = args.filelistname
name = args.name


paths = [filelistname + 'Model/', filelistname + 'Plots/', filelistname + 'Data/']

# Check whether the specified path exists or not
for var in paths:
    isExist = os.path.exists(var)

    if not isExist:
    
        # Create a new directory because it does not exist 
        os.makedirs(var)
        print("The " + var + " directory is created!")


class CustomCallback(Callback):
    def on_epoch_end(self, epoch, logs=None):
        y_pred = model.predict(X_val)
        df = pd.DataFrame(y_pred, columns=['Prediction'])
        df['Truth'] = y_val
        print("")
        print('values: {} at epoch: {}'.format(df.head(20), epoch))

class Linear(keras.layers.Layer):
    def __init__(self, input_dim, units, mn, std):
        super(Linear, self).__init__()
        w_init = tf.random_normal_initializer(mean=std, stddev=1.)
        self.w = tf.Variable(
            initial_value=w_init(shape=(input_dim, units), dtype="float32"),
            trainable=True)
        print("Printing W: " + str(self.w.value))
        b_init = tf.random_normal_initializer(mean=mn, stddev=1.)
        self.b = tf.Variable(
            initial_value=b_init(shape=(units,), dtype="float32"), trainable=True)
        print("Printing B: " + str(self.b.value))

    def get_config(self):
        config = super().get_config()
        config.update({
            "w": self.w,
            "b": self.b,
        })
        return config

    def call(self, inputs):
        print(tf.matmul(inputs, self.w) + self.b)
        return tf.matmul(inputs, self.w) + self.b


#Find the training dataset in the specified folder
# input_raw_data = glob.glob(os.path.join(filelistname, 'training*_uniform_flip_norm.csv'))
# input_raw_data = glob.glob(os.path.join(filelistname, 'train_uniform_normed_6000000.csv'))
input_raw_data = glob.glob(os.path.join(paths[2], 'merged_uniform.h5'))
#input_raw_data = glob.glob(os.path.join(paths[2], 'merged.h5'))

print(input_raw_data)

raw_train_data = pd.read_hdf(input_raw_data[0])

normed_train_labels = raw_train_data['Label'].values.reshape(-1, 1)

#normed_train_data = raw_train_data.drop(columns=['Label', 'eventNumber'])
normed_train_data = raw_train_data.drop(columns=['Label'])


title = 'size_' + str(len(normed_train_data))   


X_train, X_val, y_train, y_val = train_test_split(normed_train_data, normed_train_labels, test_size=0.2)

std = pd.DataFrame(raw_train_data.std()).T / 1e6
std.to_hdf(paths[0] + "std_" + title + "_" + name + ".h5", key="df", mode="w")
mean = pd.DataFrame(raw_train_data.mean()).T / 1e6
mean.to_hdf(paths[0] + "mean_" + title + "_" + name + ".h5", key="df", mode="w")

scaler_x = StandardScaler()
scaler_y = StandardScaler()

X_train = scaler_x.fit_transform(X_train)
#y_train = scaler_y.fit_transform(y_train)
y_train = y_train / 1e6


X_val = scaler_x.fit_transform(X_val)
#y_val = scaler_y.fit_transform(y_val)
y_val = y_val / 1e6

###########################################################################
# norm_layer = Normalization()
# norm_layer.adapt(X_train)
# print(norm_layer(X_train))
# print(scaler_x.fit_transform(X_train))

# print(y_train.std())
# print(y_train.mean())

# rescaling_layer = Rescaling(scale=y_train.std(), offset=y_train.mean())
# y_train = scaler_y.fit_transform(y_train)
# print(rescaling_layer(y_train))
# print(scaler_y.inverse_transform(y_train))
###########################################################################

linear_layer = Linear( input_dim=1, units=1, mn=mean['Label'], std=std['Label'])

inputLayer = Input(shape=(normed_train_data.shape[1],))
#x = norm_layer(inputLayer)
#x = BatchNormalization()(inputLayer)
####
x = Dense(100, activation='relu')(inputLayer)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
# ####
#x = linear_layer(x)
outputLayer = Dense(1, activation='linear')(x)
#x = Dense(1, activation='linear')(x)
#outputLayer = linear_layer(x)

####
model = Model(inputs=inputLayer, outputs=outputLayer)

model.compile(optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001),                     
             loss = 'mae',
             metrics = ['mse'])
model.summary()


start = time.time()
print("Start training")
history = model.fit(X_train, y_train, 
                    epochs = 150, validation_data = [X_val,y_val],                                            
                    batch_size = 50, verbose = 1, 
                    callbacks = [
                    EarlyStopping(monitor='val_loss', patience=10, verbose=1), 
                    CustomCallback()])
                    #ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)])
                    # TerminateOnNaN()]) 
end = time.time()
print('Time spent on training: ' + str(int((end - start)/60)) + ' minutes' ) 

model.save(paths[0] + title + "_" + name + ".h5")
model.save(paths[0] + title + "_" + name )

hist_df = pd.DataFrame(history.history) 
hist_df.insert(loc=0, column='epoch', value=hist_df.index + 1)
#hist_df.to_csv(filelistname + '/history/' + title + '.csv', index=False, header=True)

fig = plt.figure(figsize=(8,4))

# loss
plt.subplot(121)
plt.plot(history.history['val_loss'])
plt.plot(history.history['loss'])
plt.title('Loss: Mean Absolute Error')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.34,0.42]) 

# metrics
plt.subplot(122)
plt.plot(history.history['val_mse'])
plt.plot(history.history['mse'])
plt.title('Metrics: Mean Squared Error')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['val', 'train'], loc='upper right')
axes = plt.gca()
#axes.set_ylim([0.24,0.40])
plt.tight_layout()
fig.savefig(paths[1] + title + "_" + name + '.png')

print(title + "_" + name + ".h5")
