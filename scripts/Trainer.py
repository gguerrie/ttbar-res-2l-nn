#!/usr/bin/env python3
"""
Main steering script for training
"""
import argparse
import os
from pickle import load
from shutil import copyfile
import HelperModules.HelperFunctions as hf
from HelperModules import Settings
from HelperModules.Directories import Directories
from HelperModules.PlotsDataHandler import DataHandler
#from HelperModules import Processor

if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    PARSER.add_argument("-c", "--configfile", help="Config file", required=True)
    ARGS = PARSER.parse_args()


    # Here we create the necessary settings objects and read information from the config file
    CFG_SETTINGS = Settings.Settings(ARGS.configfile, option="all")
    NFOLDS       = CFG_SETTINGS.get_General().get("Folds")
    MODEL        = CFG_SETTINGS.get_Model()
    DIRS         = Directories(CFG_SETTINGS.get_General().get("Job"))

    
    
    # Copy the config file to the job directory
    cfg_path, cfg_extension = os.path.splitext(ARGS.configfile)
    copyfile(ARGS.configfile, hf.ensure_trailing_slash(DIRS.ConfigDir()) +  os.path.basename(cfg_path) + "_training_step" + cfg_extension)
    CFG_SETTINGS.Print()

    DH = DataHandler(CFG_SETTINGS)
    for Fold in range(NFOLDS):
        CFG_SETTINGS.get_Model().CompileAndTrain(DH.get_TrainInputs(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False), DH.get_TrainLabels(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False), DH.get_TrainWeights_sc(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False), DIRS.ModelDir(), Fold=str(Fold))

    
    
    # # Copy the config file to the job directory
    # copyfile(ARGS.configfile, hf.ensure_trailing_slash(DIRS.ConfigDir())+os.path.basename(ARGS.configfile).replace(".cfg", "_training_step.cfg"))
    # CFG_SETTINGS.Print()

    # m_ScaleInput = CFG_SETTINGS.get_General().get("InputScaling")
    # # m_ScaleOutput = CFG_SETTINGS.get_General().get("OutputScaling")
    # #print(m_ScaleInput)
    # #print(m_ScaleOutput)

    # print("Initializing DataHandler..")
    # DH = DataHandler(CFG_SETTINGS)
    # print("DataHandler initialized.")
    # CFG_SETTINGS.get_Model().CompileAndTrain(DH.get_TrainInputs( returnNonZeroLabels=False, returnNominalOnly=False).values, DH.get_TrainLabels( returnNonZeroLabels=False, returnNominalOnly=False).values, DIRS.ModelDir(), m_ScaleInput, m_ScaleOutput)