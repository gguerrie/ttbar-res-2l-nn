#!/usr/bin/env python3

import argparse, os, sys, subprocess
import HelperModules
from HelperModules import Settings
from HelperModules.MessageHandler import ErrorMessage, WelcomeMessage, OptimiserMessage, EnvironmentalCheck

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    parser.add_argument("-cp", "--configpath", help="Output path for config files for hyperparameter optimisation", required=True)
    parser.add_argument("--RunOption", help="Available options for the HTCondor to run scripts.", choices=["ConversionAndTraining","Converter","Trainer"], required=True)
    parser.add_argument("--NNetworks", help="Number of networks to be trained", type=int, required=True, default=10)
    parser.add_argument("--RandomSeed", help="Random seed for determining the hyperparameters", type=int)
    args = parser.parse_args()

    WelcomeMessage("Optimiser")
    EnvironmentalCheck()

    # Here we create the necessary settings objects and read information from the config file
    cfg_settings = Settings.Settings(args.configfile, option="all")
    OptimisedDNN = HelperModules.DNNModel.DNNOptimisation(args, cfg_settings,args.NNetworks)
    OptimisedDNN.Write_Configs()
    subfile = OptimisedDNN.Define_Submission()
    
    OptimiserMessage('Looking for condor..')
    try:
        # Check if condor responds
        if subprocess.Popen("condor_version", stdout=subprocess.PIPE).communicate():
            # Launch the jobs
            command = 'condor_submit -name sn.ts.infn.it -spool ' + subfile
            OptimiserMessage('Submitting the following command: ' + subfile )
            process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()
    except:
        OptimiserMessage('Sorry, didn\'t find any bird here :c')
        

                    
