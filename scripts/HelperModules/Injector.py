"""
Module handling the injection of predictions
back into root ntuples.
"""
import collections
import os
from pickle import load
from array import array
import numpy as np
import pandas as pd
import uproot
import ROOT
import HelperModules.HelperFunctions as hf
from HelperModules.Directories import Directories
from HelperModules.Settings import Settings
from HelperModules.MessageHandler import ErrorMessage
from HelperModules.ModelHandler import ModelHandler

def load_data(f, t, inputs):
    '''
    Method to load ntuples from a file using uproot.

    Keyword arguments:
    f (string): file name
    t (string): tree name

    Returns:
    e (Pandas Dataframe): Ntuples in pandas DataFrame format.
    '''
    Events = uproot.open(f"{f}:{t}")
    try:
        e = Events.arrays(inputs, library="pd")
    except uproot.exceptions.KeyInFileError as e:
        ErrorMessage(f"The variable {e.args[0]} is missing in tree {t} in file {f}. Consider using the 'IgnoreTreenames' option.")
    return e

def do_scaling(df_X, scaler, nFolds, varnames):
    df_X_Folds = [df_X[df_X["eventNumber"] % nFolds == i] for i in range(nFolds)]
    # Reorder the event numbers and concatenate them again. We need this for the keys of our prediction dictionary
    df_X_Folds = [X.drop(["eventNumber"], axis=1) for X in df_X_Folds]
    # Make sure we actually have values to pass to the scaler. Otherwise this will crash
    if scaler is None:
        return [pd.DataFrame(X.values, columns=varnames) if len(X.values)!=0 else [] for X in df_X_Folds]
    return [pd.DataFrame(scaler.transform(X.values), columns=varnames) if len(X.values)!=0 else [] for X in df_X_Folds]

class Injector(object):
    '''
    Class handling the injection of ntuples into root files.
    Takes args from argparse as parameter.
    '''
    def __init__(self, args):
        self.m_args = args
        self.m_CfgSettings = [Settings(ConfigPath, option="all") for ConfigPath in args.configfile]
        self.m_Dirs = [Directories(s.get_General().get("Job")) for s in self.m_CfgSettings]
        self.m_mhandlers = [ModelHandler(s.get_Model(), d) for s,d in zip(self.m_CfgSettings, self.m_Dirs)]
        self.m_models = [s.get_Model() for s in self.m_CfgSettings]

    def check_modelnames(self):
        '''
        Method to check whether the passed model names are unique.
        '''
        ModelNames = [Model.get("Name") for Model in [s.get_Model() for s in self.m_CfgSettings]]
        if len(ModelNames) != len(set(ModelNames)):
            Duplicates = [item for item, count in collections.Counter(ModelNames).items() if count > 1]
            ErrorMessage(f"You are using models that have identical `Model Names`. The following names occur multiple times {Duplicates}.")

    def predict_folds(self, mhandler, x_in, e_numbers, nFolds, settings):
        '''
        Method to predict individual folds using a 'ModelHandler' object.
        The ModelHandler object will take care of the predictions.
        The Predictions are then re-ordered such that the output array has
        the original ordering.

        Keyword arguments:
        mhandler (ModelHandler object): The ModelHandler object which takes care of the predictions
        x_in (List of Pandas Dataframe): List of Pandas DataFrame objects with length nFolds.
        e_numbers (numpy array): Array of numbers identifying which event goes into which fold.
        settings (Settings object): Settings object which is necessary for handling GNNs.

        Returns:
        P_array (numpy array of numpy array): 2D numpy array containing all predictions for one fold.
        '''
        if nFolds != 1:
            folds = ["_Fold" + str(i) for i in range(nFolds)]
        else:
            folds = [""]
        model_folds = [mhandler.load_model(fold=f) for f in folds]
        P = [mhandler.predict(model_fold, x_fold, settings) for model_fold, x_fold in zip(model_folds, x_in) if len(x_fold)>0]
        e_array = [[] for i in range(nFolds)]
        e_array_ind = [[] for i in range(nFolds)]
        for ind, e_number in enumerate(e_numbers):
            e_array[int(e_number % nFolds)].append(int(e_number))
            e_array_ind[int(e_number % nFolds)].append(ind)
        e_array_trunc = np.concatenate(e_array).astype(int)
        e_array_ind_trunc = np.concatenate(e_array_ind).astype(int)
        P_trunc = np.concatenate(P).astype(float)
        P_array = np.array([None] * len(e_array_trunc))
        for ind, val in zip(e_array_ind_trunc, P_trunc):
            P_array[ind] = val
        return P_array

    def do_PreCalculation(self, fName, treename):
        '''
        Function performing the pre-calculation, i.e. calculating
        the predictions given by the model.

        Keyword arguments:
        fName (string): file name
        treename (string): treename

        Returns:
        c_dicts (list of dictionaries): List of dicts representing the model(s) predictions.
        '''
        Predictions, column_names = [], []
        for model, modelpath, mhandler, settings, in zip(self.m_models,
                                                         self.m_args.model_path,
                                                         self.m_mhandlers,
                                                         self.m_CfgSettings):
            varnames = [v.get("Name") for v in settings.get_Variables()]
            model_inputs = settings.get_VarNames().copy()
            model_inputs += [settings.get_General().get("FoldSplitVariable")]
            X_raw_df = load_data(f=fName, t=treename, inputs=model_inputs)
            X_Scaled = do_scaling(df_X=X_raw_df,
                                  scaler=load(open(f"{hf.ensure_trailing_slash(modelpath)}scaler.pkl", 'rb')),
                                  nFolds=settings.get_General().get("Folds"),
                                  varnames=varnames)
            Predictions.append(self.predict_folds(mhandler=mhandler,
                                                  x_in=X_Scaled,
                                                  e_numbers=X_raw_df[settings.get_General().get("FoldSplitVariable")].values,
                                                  nFolds=settings.get_General().get("Folds"),
                                                  settings=settings))
            column_names.append([f"{model.get('Name')}_{cl}" for cl in model.get("OutputLabels")])
        c_dicts = [dict(zip(np.concatenate(column_names), p)) for p in np.concatenate(Predictions)]
        del column_names, Predictions
        return c_dicts

    def process_tree(self, in_File, out_File, tName, fName):
        Tree = in_File.Get(tName)
        if tName not in self.m_args.tree_wildcard and self.m_args.tree_wildcard != "":
            Tree.Delete()
            return
        # black listing a few trees which are simply copied
        if any([tName in c.get_General().get("IgnoreTreenames") for c in self.m_CfgSettings]):
            NewTree = Tree.CloneTree(-1, "fast")
            out_File.Write()
            Tree.Delete()
            return
        NewTree = Tree.CloneTree(0)
        NewBranches = {}
        for model in self.m_models:
            for l in model.get("OutputLabels"):
                NewBranches[f"{model.get('Name')}_{l}"] = array('f', [0])
        for BranchName, NewBranch in NewBranches.items():
            NewTree.Branch(BranchName, NewBranch, BranchName + "/F")
        if Tree.GetEntries() == 0:
            out_File.Write()
            Tree.Delete()
            return
        event_dicts = self.do_PreCalculation(fName=fName, treename=tName)
        for entry_number, event_dict in zip(range(0, Tree.GetEntries()), event_dicts):
            Tree.GetEntry(entry_number)
            for name in NewBranches:
                NewBranches[name][0] = event_dict[name]
            NewTree.Fill()
        event_dict.clear()
        NewBranches.clear()
        Tree.Delete()
        out_File.Write()

    def do_Injection(self, fName):
        '''
        Function to perform the injection of ntuples into the actual root file.

        Keyordword arguments:
        fName (string): file name
        '''
        # Create a new root file
        File = ROOT.TFile(fName)
        NewFile = ROOT.TFile(fName.replace(os.path.abspath(self.m_args.input_path), os.path.abspath(self.m_args.output_path)), "recreate")
        # Loop over trees in root file
        keys = File.GetListOfKeys()
        Treenames = []
        for key in keys:
            if key.GetClassName() != "TTree":  # ignore TObjects that are not TTrees
                continue
            if key.ReadObj().GetName() == "":  # ignore TTrees that do not have a name
                continue
            Treenames.append(key.ReadObj().GetName())
        for tName in Treenames:
            self.process_tree(in_File=File,
                              out_File=NewFile,
                              tName=tName,
                              fName=fName)
        del Treenames
        NewFile.Close()
        NewFile.Delete()
        File.Close()
        File.Delete()
