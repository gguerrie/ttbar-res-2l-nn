
from numpy import unique
from ModelModules.DNNModel import DNNModel
from ModelModules.BDTModel import BDTModel
from ModelModules.ACGANModel import ACGANModel
from ModelModules.WGANModel import WGANModel
from ModelModules.GNNModel import GNNModel
from HelperModules.MessageHandler import InfoDelimiter, ErrorMessage
from HelperModules.Sample import Sample
from HelperModules.Variable import Variable, Variable_WithMergedLeptons
from HelperModules.GraphNode import GraphNode
from HelperModules.HelperFunctions import clear_config, get_groups, check_groups
from HelperModules.OptionHandler import OptionHandler

class Settings(object):
    """
    This class serves as a storage class for all settings that are read from the config file.

    Instance variables:
    option   --- Option to determine which settings are read and evaluated. Possible are 'all', 'samples' or 'model'.
    FileName --- File name (string) of the config file.
    model    --- String describing the type of model that shall be used later in the code.
    samples  --- List of samples (list of strings) that are picked up by the code and passed to the converter.
    general  --- Additional general settings which are defined in the GeneralSettings class.
    """
    def __init__(self, FileName, option="all"):
        """
        Init method of Settings class.
        """
        self.m_option = option.lower()
        self.m_model = None
        self.m_samples = []
        self.m_variables = []
        self.m_graphnodes = []
        self.m_general = None
        self.read_blocks(FileName)
        if len(set([v.get("Name") for v in self.m_variables])) < len([v.get("Name") for v in self.m_variables]):
            ErrorMessage("Detected VARIABLE blocks with identical 'Name' parameters. You need to check this!")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def read_blocks(self, FileName):
        # Iterating through groups of settings (i.e. GENERAL, DNNMODEL/WGANMODEL and SAMPLE)
        self.m_groups = get_groups(clear_config(FileName))
        check_groups(self.m_groups)
        for group in self.m_groups:
            # Detection of the GENERAL block
            if group[0] == "GENERAL" and (self.m_option == "all" or self.m_option == "samples"):
                self.m_general = GeneralSettings(group)
            # Detection of each SAMPLE block
            if group[0] == "SAMPLE" and (self.m_option == "all" or self.m_option == "samples"):
                self.m_samples.append(Sample(group))
            # Detection of each VARIABLE block
            if group[0] == "VARIABLE" and (self.m_option == "all" or self.m_option == "samples"):
                self.m_variables.append(Variable(group))
            # Detection of each GRAPHNODE block
            if group[0] == "GRAPHNODE" and (self.m_option == "all" or self.m_option == "samples"):
                self.m_graphnodes.append(GraphNode(group))
            # Detection of the DNNModel block
            if group[0] == "DNNMODEL" and (self.m_option == "all" or self.m_option == "model"):
                self.m_model = DNNModel(group, len(self.get_Variables()))
            # Detection of the HomoGNNModel block
            if group[0]=="GNNMODEL" and (self.m_option == "all" or self.m_option == "model"):
                self.m_model = GNNModel(group,self.get_GraphNodes(), self.get_Variables())
            # Detection of the BDTModel block
            if group[0] == "BDTMODEL" and (self.m_option == "all" or self.m_option == "model"):
                self.m_model = BDTModel(group, len(self.get_Variables()))
            # Detection of the WGANModel block
            if group[0] == "ACGANMODEL" and (self.m_option == "all" or self.m_option == "model"):
                self.m_model = ACGANModel(group, len(self.get_Variables()))
            if group[0] == "WGANMODEL" and (self.m_option == "all" or self.m_option == "model"):
                self.m_model = WGANModel(group, len(self.get_Variables()))

        if self.get_General().get("DileptonMerge"):
            self.m_model = DNNModel(group, len(self.get_VarNames_WithMergedLeptons()))
        
    def get_Model(self):
        return self.m_model

    def get_Samples(self):
        return self.m_samples

    def get_Variables(self):
        return self.m_variables

    def get_Variables_WithMergedLeptons(self):
        self.m_variables = []
        for group in self.m_groups:
            # Detection of each VARIABLE block
            if group[0] == "VARIABLE" and (self.m_option == "all" or self.m_option == "samples"):
                if str(Variable_WithMergedLeptons(group)) == "None":
                    continue
                else:
                    #print(Variable_WithMergedLeptons(group))
                    self.m_variables.append(Variable_WithMergedLeptons(group))
        return self.m_variables

    def get_GraphNodes(self):
        return self.m_graphnodes

    def get_VarNames(self):
        return [var.get("Name") for var in self.get_Variables()]

    def get_VarNames_WithMergedLeptons(self):
        #return [var.get("Name") for var in self.get_Variables_WithMergedLeptons()]
        list = []
        for var in self.get_Variables():
            if var.get("Name").startswith('el'):
                if var.get("Name") == 'el_pt[:,0]':
                    for x in ['lep1_pt', 'lep1_eta', 'lep1_phi', 'lep1_e']:
                        list.append(x) 
                else:
                    continue
            elif var.get("Name").startswith('mu'):
                if var.get("Name") == 'mu_pt[:,1]':
                    for x in ['lep2_pt', 'lep2_eta', 'lep2_phi', 'lep2_e']:
                        list.append(x)
                else:
                    continue
            else:
                list.append(var.get("Name"))
        return list

    def get_VarLabels(self):
        return [var.get("Label") for var in self.get_Variables()]

    def get_VarLabels_WithMergedLeptons(self):
        return [var.get("Label") for var in self.get_Variables_WithMergedLeptons()]

    def get_NominalSamples(self):
        return [s for s in self.m_samples if s.get("Type") != "Systematic"] # only return non-systematic samples

    def get_SystSamples(self):
        return [s for s in self.m_samples if s.get("Type") == "Systematic"] # only return systematic samples

    def get_General(self):
        return self.m_general

    def get_Groups(self):
        return self.m_groups

    def Print(self):
        if(self.m_option == "all" or self.m_option == "samples"):
            self.m_general.Print()
            InfoDelimiter()
        if(self.m_option == "all" or self.m_option == "model"):
            self.m_model.Print()
            InfoDelimiter()
        if(self.m_option == "all" or self.m_option == "samples"):
            for sample in self.m_samples:
                sample.Print()
                InfoDelimiter()

class GeneralSettings(object):
    """
    This class serves as a storage class for general settings that are read from the config file.
    """
    def __init__(self, group):
        self.m_generaloptions = OptionHandler()
        self.m_generaloptions.set_description("GENERAL")
        self.m_generaloptions.register_option("Job", optiondefault=None)
        self.m_generaloptions.register_option("NtuplePath", optiondefault=None)
        self.m_generaloptions.register_option("DileptonMerge", optiondefault=True)
        self.m_generaloptions.register_option("FillNaN", optiondefault="zeros")
        self.m_generaloptions.register_option("DoNOnly", optiondefault=None)
        self.m_generaloptions.register_option("Selection", optiondefault="")
        self.m_generaloptions.register_option("MCWeight", optiondefault="")
        self.m_generaloptions.register_option("Treename", optiondefault=None)
        self.m_generaloptions.register_option("IgnoreTreenames", optiondefault=[], optionislist=True)
        self.m_generaloptions.register_option("InputScaling", optiondefault=None)
        self.m_generaloptions.register_option("RecalcScaler", optiondefault=True)
        self.m_generaloptions.register_option("OutputScaling", optiondefault=None)
        self.m_generaloptions.register_option("WeightScaling", optiondefault=True)
        self.m_generaloptions.register_option("WeightsToUnity", optiondefault=True)
        self.m_generaloptions.register_option("TreatNegWeights", optiondefault="Scale")
        self.m_generaloptions.register_option("Folds", optiondefault=2)
        self.m_generaloptions.register_option("FoldSplitVariable", optiondefault="eventNumber")
        self.m_generaloptions.register_option("ATLASLabel", optiondefault="")
        self.m_generaloptions.register_option("CMLabel", optiondefault="")
        self.m_generaloptions.register_option("CustomLabel", optiondefault="")
        self.m_generaloptions.register_option("DoCtrlPlots", optiondefault=True)
        self.m_generaloptions.register_option("DoYields", optiondefault=False)
        self.m_generaloptions.register_option("Blinding", optiondefault=None)
        self.m_generaloptions.register_option("SBBlinding", optiondefault=None)
        self.m_generaloptions.register_option("SignificanceBlinding", optiondefault=None)
        self.m_generaloptions.register_option("UnderFlowBin", optiondefault=True)
        self.m_generaloptions.register_option("OverFlowBin", optiondefault=True)
        self.m_generaloptions.register_option("PlotFormat", optiondefault=["pdf", "png", "eps"], optionislist=True)
        self.m_generaloptions.register_option("LossPlotXScale", optiondefault="Log")
        self.m_generaloptions.register_option("LossPlotYScale", optiondefault="Linear")
        self.m_generaloptions.register_option("StackPlotScale", optiondefault="Linear")
        self.m_generaloptions.register_option("StackPlot2DScale", optiondefault="Linear")
        self.m_generaloptions.register_option("SeparationPlotScale", optiondefault="Linear")
        self.m_generaloptions.register_option("ConfusionPlotScale", optiondefault="Linear")
        self.m_generaloptions.register_option("ConfusionMatrixNorm", optiondefault="row")
        self.m_generaloptions.register_option("KSPlotScale", optiondefault="Linear")
        self.m_generaloptions.register_option("PermImportancePlotScale", optiondefault="Linear")
        self.m_generaloptions.register_option("PermImportanceShuffles", optiondefault=5)
        self.m_generaloptions.register_option("LargeCorrelation", optiondefault=0.5)
        self.m_generaloptions.register_option("RatioMax", optiondefault=1.55)
        self.m_generaloptions.register_option("RatioMin", optiondefault=0.45)
        self.m_generaloptions.register_option("ROCSampling", optiondefault=100)
        self.m_generaloptions.register_option("MinSBSignalYield", optiondefault=0.3)
        self.m_generaloptions.register_option("MinSsqrtBSignalYield", optiondefault=0.3)
        self.m_generaloptions.register_option("NbinsSB2DPlots", optiondefault=100)
        self.m_generaloptions.register_option("SoverBSF", optiondefault=1)
        self.m_generaloptions.register_option("SoversqrtBSF", optiondefault=1)

        self.m_generaloptions.register_check(optionname="Job", check="set")
        self.m_generaloptions.register_check(optionname="NtuplePath", check="set")
        self.m_generaloptions.register_check(optionname="Treename", check="set")
        self.m_generaloptions.register_check(optionname="InputScaling", check="inlist", lst=["none", "minmax", "minmax_symmetric", "standard"])
        self.m_generaloptions.register_check(optionname="TreatNegWeights", check="inlist", lst=["None", "Scale", "SetZero"])
        self.m_generaloptions.register_check(optionname="Folds", check="larger", thr=1)
        self.m_generaloptions.register_check(optionname="PlotFormat", check="is_subset", lst=["pdf", "png", "eps", "jpeg", "gif", "svg"])
        self.m_generaloptions.register_check(optionname="LossPlotXScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="LossPlotYScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="StackPlotScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="StackPlot2DScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="SeparationPlotScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="ConfusionPlotScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="ConfusionMatrixNorm", check="is_inlist", lst=["row","column","None"])
        self.m_generaloptions.register_check(optionname="KSPlotScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="PermImportancePlotScale", check="is_inlist", lst=["Log", "Linear"])
        self.m_generaloptions.register_check(optionname="PermImportanceShuffles", check="larger", thr=1)
        self.m_generaloptions.register_check(optionname="LargeCorrelation", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="LargeCorrelation", check="smaller", thr=1)
        self.m_generaloptions.register_check(optionname="RatioMin", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="RatioMax", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="RatioMax", check="larger", thr="RatioMin")
        self.m_generaloptions.register_check(optionname="ROCSampling", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="MinSBSignalYield", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="MinSBSignalYield", check="smaller", thr=1)
        self.m_generaloptions.register_check(optionname="MinSsqrtBSignalYield", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="MinSsqrtBSignalYield", check="smaller", thr=1)
        self.m_generaloptions.register_check(optionname="NbinsSB2DPlots", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="SoverBSF", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="SoversqrtBSF", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="SBBlinding", check="larger", thr=0)
        self.m_generaloptions.register_check(optionname="SignificanceBlinding", check="larger", thr=0)

        for item in group:
            self.m_generaloptions.set_option(item)
        self.m_generaloptions.check_options()

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get(self, name):
        """
        Getter method to return options
        Keyword arguments:
        name --- name of the option to be returned
        """
        return self.m_generaloptions.get(name)

    def do_CtrlPlots(self):
        return self.m_generaloptions.get("DoCtrlPlots")

    def do_Yields(self):
        return self.m_generaloptions.get("DoCtrlPlots")

    def Print(self):
        self.m_generaloptions.Print(fn=print)
