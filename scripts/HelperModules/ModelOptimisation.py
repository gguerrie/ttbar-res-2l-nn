import os
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import OptimiserMessage, ErrorMessage
from collections import defaultdict

class ModelOptimisation(object):
    """
    Class handling the optimisation of models
    by defining config files (e.g. for HTCondor)
    """

    def __init__(self, args, cfg_settings, n_iter=10):
        self.m_args = args
        self.m_cfgsettings = cfg_settings
        self.m_niter = n_iter
        self.m_SeedModel = cfg_settings.get_Model()
        self.m_P = defaultdict(dict)
        self.m_SubNames = {}

    def get_Parameter(self, par):
        return self.m_Parameters[par]

    def Define_DNNParameterDict(self):
        # Limits for optimisation parameters
        self.m_Parameters = {
            "MinLayers": self.m_SeedModel.get("MinLayers"),
            "MaxLayers": self.m_SeedModel.get("MaxLayers"),
            "Nodes": range(self.m_SeedModel.get("MinNodes"), self.m_SeedModel.get("MaxNodes"), self.m_SeedModel.get("StepNodes")),
            "SetActivationFunctions": self.m_SeedModel.get("SetActivationFunctions")
        }
        for iteration in range (self.m_niter):
            # first define the node structure because other things depend on it
            Nodes = [str(item) for item in np.random.choice(self.get_Parameter("Nodes"), np.random.randint(self.get_Parameter("MinLayers"), self.get_Parameter("MaxLayers"), 1), replace=True).tolist()]
            N_Layers = len(Nodes)
            self.m_P["P%d" % iteration]["Nodes"] = ",".join(Nodes)

            # now define subsequent parameters
            N_Layers = len(self.m_P["P%d"%iteration]["Nodes"])
            Choice_Array = range(0, N_Layers)
            DropoutSize = np.random.randint(0, N_Layers, 1)
            BatchNormSize = np.random.randint(0, N_Layers, 1)
            self.m_P["P%d" % iteration]["DropoutIndece"] = ",".join([str(item) for item in sorted(np.random.choice(Choice_Array, DropoutSize, replace=False).tolist())])
            self.m_P["P%d" % iteration]["BatchNormIndece"] = ",".join([str(item) for item in sorted(np.random.choice(Choice_Array, BatchNormSize, replace=False).tolist())])
            self.m_P["P%d" % iteration]["ActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(self.get_Parameter("SetActivationFunctions")) for _ in range(N_Layers)]])
            if not self.m_P["P%d" % iteration]["DropoutIndece"]:
                self.m_P["P%d" % iteration]["DropoutIndece"] = "-1"
            if not self.m_P["P%d" % iteration]["BatchNormIndece"]:
                self.m_P["P%d" % iteration]["BatchNormIndece"] = "-1"

    def Define_BDTParameterDict(self):
        # Limits for optimisation parameters
        if self.m_SeedModel.get("MaxFeatures") is None:
            ErrorMessage("No specific value for 'MaxFeatures'  was given in the MODEL block. You need to check this!")
        try:
            int(self.m_SeedModel.get("MaxFeatures"))
        except ValueError:
            ErrorMessage("MaxFeatures has to be an integer for hyperparameter optimisation to work!")
        self.m_Parameters = {
                "MaxFeatures": self.m_SeedModel.get("MaxFeatures"),
                "MaxDepth": self.m_SeedModel.get("MaxDepth"),
                "LearningRate": np.arange(self.m_SeedModel.get("MinLearningRate"),self.m_SeedModel.get("MaxLearningRate")+self.m_SeedModel.get("StepLearningRate"),self.m_SeedModel.get("StepLearningRate"))}
        for iteration in range (self.m_niter):
            self.m_P["P%d" % iteration]["MaxDepth"] = str(np.random.randint(1, self.get_Parameter("MaxDepth")))
            self.m_P["P%d" % iteration]["MaxFeatures"] = str(np.random.randint(1, self.get_Parameter("MaxFeatures")))
            self.m_P["P%d" % iteration]["LearningRate"] = str(np.random.choice(self.get_Parameter("LearningRate")))

    def Write_Configs(self):
        """ Method to actually write new config files based
        on a given seed config file
        """
        SeedGroups = self.m_cfgsettings.get_Groups()
        SeedModelName = self.m_cfgsettings.get_Model().get("Name")
        for i, group in enumerate(SeedGroups):
            if group[0] == "DNNMODEL":
                ModelGroup, ModelType = i, "DNN"
            if group[0] == "BDTMODEL":
                ModelGroup, ModelType = i, "BDT"
        if self.m_args.RandomSeed is not None:
            np.random.seed(self.m_args.RandomSeed)
        if ModelType == "DNN":
            self.Define_DNNParameterDict()
        elif ModelType == "BDT":
            self.Define_BDTParameterDict()
        for key, ParameterDict in self.m_P.items():
            hf.create_output_dir(hf.ensure_trailing_slash(self.m_args.configpath))
            for ParameterKey, ParameterValue in ParameterDict.items():
                for i, item in enumerate(SeedGroups[ModelGroup]):
                    if ParameterKey == item.split("=",1)[0].replace(" ","").replace("\t",""):
                        SeedGroups[ModelGroup][i] = "%s = %s"%(SeedGroups[ModelGroup][i].split("=", 1)[0], ParameterValue)
                    if item.split("=",1)[0].replace(" ","").replace("\t","") == "Name":
                        SeedGroups[ModelGroup][i] = SeedGroups[ModelGroup][i].split("=", 1)[0]+"= "+ SeedModelName + "_" + key
                    if item.split("=",1)[0].replace(" ","").replace("\t","") == "Verbosity":
                        if ModelType == "DNN":
                            SeedGroups[ModelGroup][i] = SeedGroups[ModelGroup][i].split("=", 1)[0] + "= 2"  # making sure verbosity is always 2 for DNN for HTCondor to reduce output size
                        elif ModelType == "BDT":
                            SeedGroups[ModelGroup][i] = SeedGroups[ModelGroup][i].split("=", 1)[0] + "= 1"  # making sure verbosity is always 1 for BDT for HTCondor to reduce output size
            f = open(hf.ensure_trailing_slash(self.m_args.configpath) + SeedModelName + "_" + key + ".cfg", "w")
            OptimiserMessage("Generating config file for %s" % (SeedModelName + "_" + key))
            for group in SeedGroups:
                for j, item in enumerate(group):
                    if j == 0:
                        f.write("%s\n" % item)
                    else:
                        f.write("\t%s\n" % item)
            f.close()

    def Fill_SubFile(self, Option, QueueSize):
        """
        Method to fill the submission files with content

        Keyword arguments:
        Option --- String identifying the type of submssion file
        QueueSize --- Integer describing the number of jobs to be submitted
        """
        self.m_SubNames[Option] = hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_General().get("Job") + "_" + Option + ".sub"
        f = open(self.m_SubNames[Option], "w")
        f.write("###################\n")
        f.write("### Job Options ###\n")
        f.write("###################\n")
        f.write("executable\t\t= $ENV(MVA_TRAINER_BASE_DIR)/scripts/HTCondorScripts/HTCondorMain.sh\n")
        f.write("arguments\t\t= " + Option + " $ENV(MVA_TRAINER_BASE_DIR)/" + hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_Model().get("Name") + "_P$(ProcId).cfg\n")
        f.write("request_memory\t\t= 2000\n")
        f.write("+MaxRuntime\t\t= 28800\n")  # Setting the maximum run time to 8 hours (because this does not exist as an independent flavour)
        f.write('+JobBatchName\t\t= "' + self.m_cfgsettings.get_Model().get("Name") + '"\n')
        f.write("\n")
        f.write("###################\n")
        f.write("### OutputFiles ###\n")
        f.write("###################\n")
        f.write("log\t\t= $ENV(MVA_TRAINER_BASE_DIR)/" + hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_Model().get("Name") + "_$(ClusterId)_P$(ProcId)" + ".log\n")
        f.write("error\t\t= $ENV(MVA_TRAINER_BASE_DIR)/" + hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_Model().get("Name") + "_$(ClusterId)_P$(ProcId)" + "_error" + ".txt\n")
        f.write("output\t\t= $ENV(MVA_TRAINER_BASE_DIR)/" + hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_Model().get("Name") + "_$(ClusterId)_P$(ProcId)" + "_output" + ".txt\n")
        f.write("getenv\t\t= True\n")
        f.write("stream_error\t\t= True\n")
        f.write("stream_output\t\t= True\n")
        f.write('requirements\t\t= OpSysAndVer == "CentOS7"\n')
        f.write("should_transfer_files\t= YES\n")
        f.write("when_to_transfer_output\t= ON_EXIT\n")
        f.write("\n")
        f.write("#####################\n")
        f.write("### Notifications ###\n")
        f.write("#####################\n")
        f.write("#notify_user\t\t= %s@cern.ch\n" % (os.environ.get("USER")))
        f.write("#notification\t\t= always\n")
        f.write("queue " + str(QueueSize))
        f.close()
        OptimiserMessage("Submission file generated: %s" % (self.m_SubNames[Option]))

    def Define_Submission(self):
        """
        Method to define submissions files for HTCondor
        """
        hf.create_output_dir(hf.ensure_trailing_slash(self.m_args.configpath))
        OptimiserMessage("Generating HTCondor submission file(s) for %s ..." % (self.m_cfgsettings.get_General().get("Job")))
        if self.m_args.RunOption == "ConversionAndTraining":
            self.Fill_SubFile("Converter", 1)  # No need to run the converter niter times, once is enough
            self.Fill_SubFile("Trainer", self.m_niter)
            OptimiserMessage("Generating HTCondor DAG file for %s ..." % (self.m_cfgsettings.get_General().get("Job")))
            f_DAG = open(hf.ensure_trailing_slash(self.m_args.configpath) + self.m_cfgsettings.get_General().get("Job") + ".dag", "w")
            f_DAG.write("JOB Conversion %s\n" % (self.m_SubNames["Converter"]))
            f_DAG.write("JOB Training %s\n" % (self.m_SubNames["Trainer"]))
            f_DAG.write("PARENT Conversion CHILD Training\n")
            f_DAG.close()
        if self.m_args.RunOption == "Converter":
            self.Fill_SubFile("Converter", 1)  # No need to run the converter niter times, once is enough
        if self.m_args.RunOption == "Trainer":
            self.Fill_SubFile("Trainer", self.m_niter)

                    
        
        
