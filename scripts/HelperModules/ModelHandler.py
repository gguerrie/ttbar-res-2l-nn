'''
Module to handle model loading and predicting.
'''
import json
from pickle import load
import torch
import numpy as np
import HelperModules.HelperFunctions as hf
from torch_geometric.loader import DataLoader
from tensorflow.keras.models import load_model
from HelperModules.GraphBuilder import GraphBuilder
from ModelModules.GNNModel import HomoGNNModel, HeteroGNNModel

class ModelHandler():
    '''
    Class to handle model loading and predicting.
    '''
    def __init__(self, modelsettings, dirs):
        self.m_modelsettings = modelsettings
        self.m_dirs = dirs

    def load_metadata(self, fold):
        '''
        Method to load metadata for a GNN from file.

        Keyword arguments:
        fold (string): String identifying the relevant fold.

        Returns:
        json.load(json_file) (dictionary): Dictionary containing model metadata.
        '''
        with open("%s%s_%s_metadata%s.json"%(hf.ensure_trailing_slash(self.m_dirs.ModelDir()),
                                             self.m_modelsettings.get("Name"),
                                             self.m_modelsettings.get("Type"),
                                             fold)) as json_file:
            return json.load(json_file)

    def load_history(self, fold):
        return "%s%s_%s_history%s.json"%(hf.ensure_trailing_slash(self.m_dirs.ModelDir()),
                                         self.m_modelsettings.get("Name"),
                                         self.m_modelsettings.get("Type"),
                                         fold)

    def get_model_path(self, fold):
        '''
        Method to get the model path.

        Keyword arguments:
        fold (string): String identifying the relevant fold.

        Returns:
        model path (string).
        '''
        if (self.m_modelsettings.isClassificationDNN()
            or self.m_modelsettings.isReconstruction()
            or self.m_modelsettings.isClassificationWGAN()
            or self.m_modelsettings.isClassificationACGAN()
            or self.m_modelsettings.isRegressionDNN()):
            ending = ".h5"
        if (self.m_modelsettings.isClassificationBDT()
            or self.m_modelsettings.isRegressionBDT()
            or self.m_modelsettings.isClassificationGNN()):
            ending = ".pkl"
        return "%s%s_%s%s%s"%(hf.ensure_trailing_slash(self.m_dirs.ModelDir()),
                              self.m_modelsettings.get("Name"),
                              self.m_modelsettings.get("Type"),
                              fold,
                              ending)

    def load_model(self, fold):
        '''
        Method to actually load the model

        Keyword arguments:
        fold (string): String identifying the relevant fold.

        Returns:
        A model object.
        '''
        # Keras
        if (self.m_modelsettings.isClassificationDNN()
            or self.m_modelsettings.isReconstruction()
            or self.m_modelsettings.isClassificationWGAN()
            or self.m_modelsettings.isClassificationACGAN()
            or self.m_modelsettings.isRegressionDNN()):
            return load_model(self.get_model_path(fold), compile=False)
        # Scikit learn
        if (self.m_modelsettings.isClassificationBDT()
            or self.m_modelsettings.isRegressionBDT()):
            with open(self.get_model_path(fold), 'rb') as f:
                return load(f)
        # pytorch
        if self.m_modelsettings.isClassificationGNN():
            # for pytorch we have to "reconstruct" the model
            metadata = self.load_metadata(fold)
            metadata["EdgeTypes"] = [tuple(l) for l in metadata["EdgeTypes"]] # json turns these into lists, we need hashable tuples
            if self.m_modelsettings._isHomogeneous():
                model = HomoGNNModel(metadata)
            else:
                model = HeteroGNNModel(metadata)
            model.load_state_dict(torch.load(self.get_model_path(fold)))
            return model

    def predict(self, model, x, settings=None):
        '''
        Method to use a model for inference (predictions)

        Keyword arguments:
        model (a model object)
        x (Pandas Dataframe): Input data.
        settings (Settings object): needed for GNN inference.

        Returns:
        Predictions (np.array)
        '''
        isBinary = (len(self.m_modelsettings.get("OutputLabels")) == 1 or len(self.m_modelsettings.get("OutputLabels")) == 0) and self.m_modelsettings.isClassification()
        if (self.m_modelsettings.isClassificationDNN()
            or self.m_modelsettings.isReconstruction()
            or self.m_modelsettings.isClassificationWGAN()
            or self.m_modelsettings.isClassificationACGAN()
            or self.m_modelsettings.isRegressionDNN()):
            return model.predict(x)
        if self.m_modelsettings.isClassificationBDT():
            if isBinary:
                return model.predict_proba(x)[:,1]
            return model.predict_proba(x)
        if self.m_modelsettings.isRegressionBDT():
            return model.predict(x)
        if self.m_modelsettings.isClassificationGNN():
            gb = GraphBuilder(settings.get_GraphNodes(), settings.get_Variables())
            if self.m_modelsettings._isHomogeneous():
                x_g = [gb.build_HomGraph(row) for row in x.to_dict('records')]
            else:
                x_g = [gb.build_HetGraph(row) for row in x.to_dict('records')]
            loader = DataLoader(x_g, batch_size=self.m_modelsettings.get("BatchSize"))
            out = []
            for data in loader:
                out.extend(model(data).detach().numpy())
            return np.array(out)
