"""
Module for a DNN model
"""

import os
from collections import defaultdict
import pandas as pd
import numpy as np
import tensorflow as tf
import HelperModules.HelperFunctions as hf
import HelperModules.Settings as st
from HelperModules.BasicModel import BasicModel
from HelperModules.MessageHandler import TrainerMessage, TrainerDoneMessage, OptimiserMessage
from tensorflow.keras.layers import Add, Dense, Dropout, concatenate, BatchNormalization, Input, multiply
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard, Callback
from tensorflow.keras import regularizers
from HelperModules.Preprocessing import Preprocess, ScaleInputs
from sklearn.model_selection import train_test_split


class DNNModel(BasicModel):
    """
    Class for implementing feed-forward-style neural networks.
    This class serves as a base class for any such type of neural network.
    """

    def __init__(self, group, input_dim):
        super().__init__(group)
        self.m_InputDim = input_dim  # Input dimensionality of the data
        self.m_kerasmodel = None  # Keras model object
    
        self.m_options.set_description("DNNMODEL")
        self.m_options.register_option("Nodes", optiondefault=None, optionislist=True)
        self.m_options.register_option("Epochs", optiondefault=100)
        self.m_options.register_option("BatchSize", optiondefault=32)
        self.m_options.register_option("MaxNodes", optiondefault=100)
        self.m_options.register_option("MinNodes", optiondefault=10)
        self.m_options.register_option("StepNodes", optiondefault=10)
        self.m_options.register_option("MaxLayers", optiondefault=7)
        self.m_options.register_option("MinLayers", optiondefault=1)
        self.m_options.register_option("PredesignedModel", optiondefault=None)
        self.m_options.register_option("Patience", optiondefault=None)
        self.m_options.register_option("MinDelta", optiondefault=None)
        self.m_options.register_option("DropoutIndece", optiondefault=[], optionislist=True)
        self.m_options.register_option("DropoutProb", optiondefault=0.1)
        self.m_options.register_option("BatchNormIndece", optiondefault=[], optionislist=True)
        self.m_options.register_option("SetActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("ActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("KernelInitialiser", optiondefault="glorot_uniform")
        self.m_options.register_option("KernelRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("BiasRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("ActivityRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("OutputSize", optiondefault=None)
        self.m_options.register_option("OutputActivation", optiondefault=None)
        self.m_options.register_option("Loss", optiondefault=None)
        self.m_options.register_option("Optimiser", optiondefault="Nadam")
        self.m_options.register_option("SmoothingAlpha", optiondefault=None)
        self.m_options.register_option("UseTensorBoard", optiondefault=False)
        self.m_options.register_option("RegressionTarget", optiondefault=None)
        self.m_options.register_option("RegressionTargetLabel", optiondefault=None)
        self.m_options.register_option("RegressionScale", optiondefault=1.0)
        self.m_options.register_option("ReweightTargetDistribution", optiondefault=True)
        self.m_options.register_option("HuberDelta", optiondefault=None)

        self.m_options.register_check(optionname="Nodes", check="set")
        self.m_options.register_check(optionname="Nodes", check="equal_len", l="ActivationFunctions")
        self.m_options.register_check(optionname="Epochs", check="larger", thr=0)
        self.m_options.register_check(optionname="BatchSize", check="larger", thr=0)
        self.m_options.register_check(optionname="MinNodes", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxNodes", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxNodes", check="larger", thr="MinNodes")
        self.m_options.register_check(optionname="MinLayers", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxLayers", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxLayers", check="larger", thr="MinLayers")
        self.m_options.register_check(optionname="Patience", check="larger", thr=0)
        self.m_options.register_check(optionname="MinDelta", check="larger", thr=0)
        self.m_options.register_check(optionname="DropoutProb", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="ActivationFunctions", check="set")
        self.m_options.register_check(optionname="KernelInitialiser", check="inlist", lst=["glorot_uniform", "glorot_normal", "lecun_normal", "lecun_uniform"])
        self.m_options.register_check(optionname="KernelRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="BiasRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="ActivityRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="OutputActivation", check="set")
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="OutputSize", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputLabels", check="equal_len", l="OutputSize")
        self.m_options.register_check(
            optionname="Loss",
            check="inlist",
            lst=[
                "binary_crossentropy",
                "categorical_crossentropy",
                "mean_squared_error",
                "mean_absolute_error",
                "mean_absolute_percentage_error",
                "mean_squared_logarithmic_error",
                "cosine_similarity",
                "huber_loss",
                "log_cosh"])

        for item in self.m_Group:
            self.m_options.set_option(item)
        self.m_options.check_options()

    ###########################################################################
    ############################### Misc methods ##############################
    ###########################################################################
    def get_Optimiser(self):
        """
        Getter method to return the actual optimiser object
        """
        optimisers = {"Adadelta": tf.keras.optimizers.Adadelta(learning_rate=self.get("LearningRate"), name='Adadelta'),
                      "Adagrad": tf.keras.optimizers.Adagrad(learning_rate=self.get("LearningRate"), name='Adagrad'),
                      "Adam": tf.keras.optimizers.Adam(learning_rate=self.get("LearningRate"), name='Adam'),
                      "Adamax": tf.keras.optimizers.Adamax(learning_rate=self.get("LearningRate"), name='Adamax'),
                      "Ftrl": tf.keras.optimizers.Ftrl(learning_rate=self.get("LearningRate"), name='Ftrl'),
                      "Nadam": tf.keras.optimizers.Nadam(learning_rate=self.get("LearningRate"), name='Nadam'),
                      "RMSprop": tf.keras.optimizers.RMSprop(learning_rate=self.get("LearningRate"), name='RMSprop'),
                      "SGD": tf.keras.optimizers.SGD(learning_rate=self.get("LearningRate"), name='SGD')}
        return optimisers[self.get("Optimiser")]

    def DefineRegulariser(self, Regulariser, l1, l2):
        """
        Simple function to create a list of keras.regularizers objects
        """
        Regularisers = []
        Regulariser_strings = ["None"] * len(self.get("Nodes")) if Regulariser is None else Regulariser
        for R in Regulariser_strings:
            if R == "l1":
                Regularisers.append(regularizers.l1(l1=l1))
            elif R == "l2":
                Regularisers.append(regularizers.l2(l2=l2))
            elif R == "l1_l2":
                Regularisers.append(regularizers.l1_l2(l1=l1, l2=l2))
            elif R == "None":
                Regularisers.append(None)
        return Regularisers

    def DNNCompile(self):
        """
        Compile a simple densely connected NN. This is our heavy lifting method.
        It is used whenever the user uses the "Nodes" option in the config file to build
        a densely connected NN.
        """
        DropoutIndece = self.get("DropoutIndece")
        BatchNormIndece = self.get("BatchNormIndece")
        # Set up the regularisers
        # KernelRegularisers = self.DefineRegulariser(self.get("KernelRegulariser"), 0.01, 0.01)
        # BiasRegularisers = self.DefineRegulariser(self.get("BiasRegulariser"), 0.01, 0.01)
        # ActivityRegularisers = self.DefineRegulariser(self.get("ActivityRegulariser"), 0.01, 0.01)
        # Lets build a feed forward NN using keras from scratch
        Layers = []
        Layers.append(Input(shape=(self.m_InputDim,), name="Input_Layer"))
        for NodeLayerID, nodes in enumerate(self.get("Nodes")):
            Layers.append(Dense(nodes,
                                name="Hidden_Layer_" + str(NodeLayerID),
                                activation=self.get("ActivationFunctions")[NodeLayerID],
                                #kernel_initializer=self.get("KernelInitialiser"),
                                #kernel_regularizer=KernelRegularisers[NodeLayerID],
                                #bias_regularizer=BiasRegularisers[NodeLayerID],
                                #activity_regularizer=ActivityRegularisers[NodeLayerID]
                                )(Layers[-1]))
            if DropoutIndece:
                if NodeLayerID in DropoutIndece:
                    Layers.append(Dropout(self.get("DropoutProb"))(Layers[-1])) # Adding a Dropout layers
            if BatchNormIndece:
                if NodeLayerID in BatchNormIndece:
                    Layers.append(BatchNormalization()(Layers[-1]))  # Adding BatchNormalisation layers
        Layers.append(Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output_Layer")(Layers[-1]))
        self.m_kerasmodel = tf.keras.Model(inputs=Layers[0], outputs=Layers[-1])
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()


    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def DNNTrain(self, X, Y, output_path, m_ScaleInput, m_ScaleOutput):
        """
        Training a simple densely connected NN. This method serves as the "heavy lifter" for training.
        It should work well for not to complicated inputs/outputs.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """

        class CustomCallback(Callback):
            def __init__(self, DNNModel, x_val, y_val):
                self.DNNModel = DNNModel
                self.x_val = x_val
                self.y_val = y_val
            def on_epoch_end(self, epoch, logs=None):
#                print(self.DNNModel)
                y_pred = self.DNNModel.m_kerasmodel.predict(self.x_val)
                df = pd.DataFrame(y_pred, columns=['Prediction'])
                df['Truth'] = self.y_val
                TrainerMessage('values: {} at epoch: {}'.format(df.head(20), epoch))


        TrainerMessage("Training with a dataset of " + str(X.shape[0]) + " events.")

        # Definition of Callbacks
        callbacks = []
        if self.get("UseTensorBoard"):
            tb = TensorBoard(log_dir=hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_TensorBoardLog",
                             histogram_freq=5)
            callbacks.append(tb)
        if self.get("Patience") is not None and self.get("MinDelta") is not None:
            es = EarlyStopping(monitor='val_loss', min_delta=self.get("MinDelta"), patience=self.get("Patience"), restore_best_weights=True)
            callbacks.append(es)
        seed = np.random.randint(1000, size=1)[0]
        print("X:")
        print(X)
        print("Y:")
        print(Y)
        X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=self.get("ValidationSize"), random_state=seed)
        #callbacks.append(CustomCallback(self, X_val, y_val))
        print("Scaling input")
        X_train, self.m_Scaler = ScaleInputs(X_train,
                                            m_ScaleInput, 
                                            None)
        X_val, self.m_Scaler = ScaleInputs(X_val,
                                            m_ScaleInput, 
                                            None)
        if m_ScaleOutput is not None:
            if self.get("OutputSize") == 2:
                print("Scaling for output size = 2")
                y_train, _ = ScaleInputs(y_train, m_ScaleOutput, None)
                y_val, _ = ScaleInputs(y_val, m_ScaleOutput, None)
            else:
                y_train, _ = ScaleInputs(y_train.reshape(-1, 1), m_ScaleOutput, None)
                y_train = y_train.T[0]
                y_val, _ = ScaleInputs(y_val.reshape(-1, 1), m_ScaleOutput, None)
                y_val = y_val.T[0]
        
        print("X TRAIN:")
        print(X_train)
        print("Y TRAIN:")
        print(y_train)

        TrainerMessage("Starting Training...")
        if callbacks:
            history = self.m_kerasmodel.fit(X_train, y_train, epochs=self.get("Epochs"), batch_size=self.get("BatchSize"), verbose=self.get("Verbosity"), validation_data=[X_val,y_val], callbacks=callbacks)
        else:
            history = self.m_kerasmodel.fit(X_train, y_train, epochs=self.get("Epochs"), batch_size=self.get("BatchSize"), verbose=self.get("Verbosity"), validation_data=[X_val,y_val])

        TrainerDoneMessage("Training done! Writing model to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + ".h5")
        self.m_kerasmodel.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + ".h5")
        arch = self.m_kerasmodel.to_json()
        # save the architecture string to a file somehow, the below will work
        TrainerDoneMessage("Training done! Writing model architecture to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Architecture.json")
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Architecture.json", 'w') as arch_file:
            arch_file.write(arch)
        # now save the weights as an HDF5 file
        TrainerDoneMessage("Training done! Writing model weights to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Weights.h5")
        self.m_kerasmodel.save_weights(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Weights.h5")
        self.m_kerasmodel.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type"))
        TrainerDoneMessage("Writing history to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + ".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + ".json", mode='w') as f:
            hist_df.to_json(f)

    def CompileAndTrain(self, X, Y, output_path, m_ScaleInput, m_ScaleOutput):
        """
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        output_path --- Output path where the model.h5 files and the history is to be saved
        """
        if len(self.get("OutputLabels")) > 2 and self.get("Loss") == "categorical_crossentropy":
            Y = np.array(hf.OneHot(Y, len(self.get("OutputLabels")))).astype(np.float32)
        else:
            Y = np.array(Y).astype(np.float32)
        if self.get("SmoothingAlpha") is not None:
            Y = self.smooth_labels(Y, K=self.__OutputSize, alpha=self.get("SmoothingAlpha"))

        self.DNNCompile()
        self.DNNTrain(X, Y, output_path, m_ScaleInput, m_ScaleOutput)

    def Print(self, fn=print):
        """
        Simple print method.
        """
        self.m_options.Print(fn)


class DNNOptimisation(object):
    """
    Class handling the optimisation of a DNN
    by defining config files (e.g. for HTCondor)
    """

    def __init__(self, args, cfg_settings, n_iter=10):
        self.m_niter = n_iter # number of models to select for randomized search
        self.m_cfgsettings = cfg_settings
        self.m_SeedModel = cfg_settings.get_Model()
        self.m_args = args
        self.m_P = defaultdict(dict)
        self.m_SubNames = {}

        # Limits for optimisation parameters
        self.m_Parameters = {
            "MinLayers": self.m_SeedModel.get("MinLayers"),
            "MaxLayers": self.m_SeedModel.get("MaxLayers"),
            "Nodes": range(self.m_SeedModel.get("MinNodes"), self.m_SeedModel.get("MaxNodes"), self.m_SeedModel.get("StepNodes")),
            "SetActivationFunctions": self.m_SeedModel.get("SetActivationFunctions")
        }

    def get_Parameter(self, par):
        """
        Getter method to return parameter par

        Keyword arguments:
        par --- string identifying the parameter to be returned.
        """
        return self.m_Parameters[par]

    def Define_ParameterDict(self):
        """
        Function to fill a dictionary with different parameter configurations
        """
        if self.m_args.RandomSeed is not None:
            np.random.seed(self.m_args.RandomSeed)
        for i in range(self.m_niter):
            self.m_P["P" + str(i)]["Nodes"] = np.random.choice(self.get_Parameter("Nodes"), np.random.randint(self.get_Parameter("MinLayers"), self.get_Parameter("MaxLayers"), 1), replace=True).tolist()
            N_Layers = len(self.m_P["P" + str(i)]["Nodes"])
            Choice_Array = range(0, N_Layers)
            DropoutSize = np.random.randint(0, N_Layers, 1)
            BatchNormSize = np.random.randint(0, N_Layers, 1)
            self.m_P["P" + str(i)]["DropoutIndece"] = sorted(np.random.choice(Choice_Array, DropoutSize, replace=False).tolist())
            self.m_P["P" + str(i)]["BatchNormIndece"] = sorted(np.random.choice(Choice_Array, BatchNormSize, replace=False).tolist())
            self.m_P["P" + str(i)]["ActivationFunctions"] = [np.random.choice(self.get_Parameter("SetActivationFunctions")) for _ in range(N_Layers)]
            if not self.m_P["P" + str(i)]["DropoutIndece"]:
                self.m_P["P" + str(i)]["DropoutIndece"] = [-1]
            if not self.m_P["P" + str(i)]["BatchNormIndece"]:
                self.m_P["P" + str(i)]["BatchNormIndece"] = [-1]

    def get_ParameterDict(self):
        """
        Getter method to return the entire parameter dict
        """
        return self.m_P

    def Print_ParameterDict(self):
        """
        Method to print the entire parameter dict
        """
        Parameter_df = pd.DataFrame.from_dict(self.get_ParameterDict())
        print(Parameter_df)
        OptimiserMessage("Resulting parameter configurations:")

