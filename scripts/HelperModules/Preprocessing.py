from pickle import dump
import numpy as np
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.Directories import Directories
from sklearn.preprocessing import MinMaxScaler, StandardScaler

def negWeightTreatment(W, Y, N, Option="Scale"):
    """Function to treat negative event weights.

    Keyword arguments:
    W --- Array (float) of weights.
    Y --- Array (float) of Labels.
    N --- Array (string) sample names.
    Option --- string to determine which treatment option is used.

    returns list with weights.
    """
    if len(W) != len(Y):
        ErrorMessage("Weights and labels should have same length but have length "+str(len(W))+" and "+str(len(Y))+".")
    # If we decide we want to handle negative weights we simply set them to zero. This is not optimal.
    if Option == "SetZero":
        W[W < 0] = 0
    if Option == "Scale":
        unique_identifiers = np.unique(N)
        factors = {identifier: np.sum(W[N == identifier])/np.sum(W[(W > 0)&(N == identifier)]) for identifier in unique_identifiers}
        W_corr = [factors[identifier] for identifier in N]
        #W *= W_corr
        W = (W * W_corr)
        W[W < 0] = 0
    if Option == "None":
        WarningMessage("No treatment of negative weights is chosen. The training is potentially unstable.")
    return W

def ScaleWeights(W, Y, Balance=True, Scale=True):
    """Function to scale the input weights to prevent the training to be imbalanced.

    Keyword arguments:
    W --- Array (float) of weights.
    Y --- Array (float) of Labels.

    returns list with scaled weights.
    """
    if Balance:
        unique_labels = np.unique(Y[~np.isnan(Y)])
        for i in unique_labels:
            W[Y == i] /= np.sum(W[Y == i])
    if Scale:
        W[~np.isnan(Y)] /= np.mean(W[~np.isnan(Y)])
    return W

def ScaleInputs(X, Option="minmax", scaler=None):
    """Function to scale the input features. Possible options are 'minmax', 'standard' and 'none'
    If 'minmax' is chosen the variables are scaled into [0,1].
    If 'standard' is chosen the varibales are standardised by removing the mean and scaling to unit variance.
    If 'none' is chosen no input scaling is applied.

    Keyword arguments:
    X      --- Array (float) of features.
    Option ---String to determine the scaling option.
    scaler --- Scaler object in case it is already available

    returns array of scaled features and corresponding scaler.
    """
    if scaler is None:
        if Option.lower() == "none":
            return X, None
        elif Option.lower() == "minmax":
            scaler = MinMaxScaler()
        elif Option.lower() == "minmax_symmetric":
            scaler = MinMaxScaler(feature_range=(-1, 1))
        elif Option.lower() == "standard":
            scaler = StandardScaler()
        else:
            ErrorMessage("Unknown scaling method: "+Option)
        X = scaler.fit_transform(X)
        return X, scaler
    else:
        try:
            X = scaler.transform(X)
        except ValueError as ve:
            ErrorMessage("Encounted an issue when applying the scaler to converted events: %s If you changed your variables you need to set 'RecalcScaler' to True."%ve)
        return X, scaler

def Preprocess(X, Y, W, N, settings, scaler=None):
    """Function to perform the preprocessing steps.

    Keyword arguments:
    X        --- Array (float) of features.
    Y        --- Array (float) of Labels.
    W        --- Array (float) of weights.
    settings --- Settings from the config file.

    returns array of scaled features, weights and corresponding scaler.
    """
    if scaler == None: 
        dumpit = True
    else:
        dumpit = False
    print(dumpit)
    X, scaler = ScaleInputs(X, settings.get_General().get("InputScaling"), scaler)
    Dirs = Directories("%s"%settings.get_General().get("Job"))
    if dumpit == True:
        dump(scaler, open(hf.ensure_trailing_slash(Dirs.ModelDir())+"scaler.pkl", 'wb'))

    ### Negative weight treatment ###
    W = negWeightTreatment(W, Y, N, settings.get_General().get("TreatNegWeights"))

    ### Weight scaling ###
    if settings.get_Model().isRegression():
        return (X, ScaleWeights(W,
                                Y,
                                Balance=False, # don't perform weight scaling for regression models
                                Scale=settings.get_General().get("WeightsToUnity")),
            scaler)
    return (X, ScaleWeights(W,
                            Y,
                            Balance=settings.get_General().get("WeightScaling"),
                            Scale=settings.get_General().get("WeightsToUnity")),
            scaler)
