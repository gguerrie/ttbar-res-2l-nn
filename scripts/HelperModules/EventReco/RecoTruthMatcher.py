import numpy as np

class RecoTruthMatcher:
    def __init__(self, CutVal, DRBased=True, invMBased=False, mPole=None, MatchSF=True, MatchSC=True):
        self.__CutVal = CutVal
        self.__DRBased = DRBased # Whether the matching is based on DeltaR
        self.__invMBased = invMBased # Whether the matching is based on mass
        self.__MatchSF = MatchSF # Only match when objects have same flavour
        self.__MatchSC = MatchSC # Only match when objects have same charge
        self.__mPole = mPole # Pole mass to be used for mass based matching
        
    def DeltaR(self, vec_a_info, vec_b_info):
        """
        Calculate the DeltaR between two LorentzVector objects
        Keyword Arguments:
        vec_a --- First LorentzVector to use
        vec_b --- Second LorentzVector to use
        """
        if self.__MatchSF and vec_a_info[2] != vec_b_info[2]: return 9999 # flavour check
        elif self.__MatchSC and vec_a_info[1] != vec_b_info[1]: return 9999 # charge check
        else: return vec_a_info[0].DeltaR(vec_b_info[0])

    def DeltaRMatrix(self, vec_a_info, vec_b_info):
        return np.frompyfunc(self.DeltaR, 2, 1).outer(vec_a_info, vec_b_info)

    def InvariantMass(self, vec_a_info, vec_b_info):
        """
        Calculate the invariant between two LorentzVector objects
        Keyword Arguments:
        vec_a --- First LorentzVector to use
        vec_b --- Second LorentzVector to use
        """
        if self.__MatchSF and vec_a_info[2] != vec_b_info[2]: return 9999 # flavour check
        elif self.__MatchSC and vec_a_info[1] != vec_b_info[1]: return 9999 # charge check
        else:
            invmass_vec = reco_vec_info_1[0] + reco_vec_info_2[0]
            return abs(invmass_vec.M()-self.__mPole)
        

    def DeltaMassMatrix(self, vec_a_info, vec_b_info, pole_mass):
        return np.frompyfunc(self.InvariantMass, 2, 1).outer(vec_a_info, vec_b_info)

    def DetermineIndece(self, arg, dim = 4):
        """
        calculate 1D indece of matrix with Dimension = dim into 2D indece
        
        ---- arguments: ----
        arg : 1D indece
        dim : dimension of matrix

        """
        x_arg = arg % dim
        y_arg = arg // dim
        return x_arg, y_arg

    def CheckDoublematches(self, DeltaR_matrix, dim):
        """
        Function to check, if there is an object which can me matched with multiple other objects.
        If there is a doubke match, choose the match with the smaller Delta R

        Keyword Arguments:
        DeltaR_matrix --- matrix containing all DeltaR values of all objects with Delta R < CutVal and fulfilling flavour/charge requirements
        dim           --- n of an nxm matrix
        """
        inverse_mask = ~DeltaR_matrix.mask
        check_matches = np.nonzero(inverse_mask)
        if(len(check_matches[0]) != len(np.unique(check_matches[0])) or len(check_matches[1]) != len(np.unique(check_matches[1]))):
            minimal_deltaR = int(np.argmin(DeltaR_matrix))
            min_argx, min_argy = self.DetermineIndece(minimal_deltaR, dim)
            DeltaR_matrix[min_argy,:] = np.ma.masked
            DeltaR_matrix[:, min_argx] = np.ma.masked
            DeltaR_matrix[min_argy, min_argx] = self.__CutVal
            inverse_mask= ~DeltaR_matrix.mask
            return self.CheckDoublematches(DeltaR_matrix, dim)
        else:
            return inverse_mask

    def MatchRecoTruth(self, reco_indece, truth_vec_info, reco_vec_info):
        """
        This is an algorithm that performs reco-truth matching  based on a deltaR or mass cut
        
        Keyword Arguments:
        truth_vec_info --- list of tuples of TLorentzVectors charge and flavour representing truth objects
        reco_indece    --- list of integers representing truth indece
        reco_vec_info  --- list of tuples of TLorentzVectors charge and flavour representing reco objects
        """
        if (len(truth_vec_info) == 0 or len(reco_vec_info) == 0): return [-1,-1,-1,-1]
        if self.__DRBased: M = np.ma.array(self.DeltaRMatrix(reco_vec_info, truth_vec_info))
        elif self.__invMBased: M = np.ma.array(self.MassMatrix(reco_vec_info, truth_vec_info))
        M = np.ma.masked_greater(M, self.__CutVal)
        inverse_mask = self.CheckDoublematches(M, len(truth_vec_info))
        reco_indece = inverse_mask.dot(reco_indece) - 1
        return reco_indece
