from ROOT import TLorentzVector
import numpy as np

class TruthObject(object):
    """
    Class representing truth objects such as leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt      --- Transverse momentum in MeV
    eta     --- Pseudorapidity
    phi     --- Azimuthal angle
    m       --- Mass in MeV
    charge  --- Charge
    flavour --- absolute value of pdgID
    """
    def __init__(self, pt, eta, phi, m, pdgID):
        self.__pt       = np.float32(pt/1000) # Need to convert to GeV
        self.__phi      = np.float32(phi)
        self.__eta      = np.float32(eta)
        self.__m        = np.float32(m/1000) # Need to convert to GeV
        self.__v        = TLorentzVector()
        self.__v.SetPtEtaPhiM(self.__pt, self.__eta, self.__phi, self.__m)
        # if the matching should not depend on charge/flavour, set the pdgID to 999
        if pdgID == 999:
            self.charge = pdgID
        else:
            self.__charge   = -np.sign(pdgID)
        self.__flavour  = abs(pdgID)

    def Vector(self):
        return self.__v

    def Charge(self):
        return self.__charge
    
    def Type(self):
        return self.__flavour
