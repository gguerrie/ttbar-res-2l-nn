from ROOT import TLorentzVector
import numpy as np

class RecoObject(object):
    """
    Class representing reconstructed objects such as charged leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt      --- Transverse momentum
    eta     --- Pseudorapidity
    phi     --- Azimuthal angle
    e       --- Energy
    charge  --- Charge
    flavour --- absolute value of pdgID
    """
    def __init__(self, pt, eta, phi, e, charge, flavour):
        self.__pt       = pt
        self.__phi      = phi
        self.__eta      = eta
        self.__e        = e
        self.__v        = TLorentzVector()
        self.__v.SetPtEtaPhiE(self.__pt, self.__eta, self.__phi, self.__e)
        self.__charge   = charge
        self.__flavour  = flavour

    def Vector(self):
        return self.__v
    
    def Charge(self):
        return self.__charge

    def Type(self):
        return self.__flavour
