from ROOT import TLorentzVector
from HelperModules.EventReco.TruthObject import TruthObject
from HelperModules.EventReco.RecoObject import RecoObject
from HelperModules.EventReco.RecoTruthMatcher import RecoTruthMatcher
from HelperModules.MessageHandler import ErrorMessage
import numpy as np

class ttXEventVariables(object):
    def __init__(self):
        self.__EventVariables = {"RecoVariables3L": ["pT_1jet","pT_2jet","pT_3jet","pT_4jet",
                                                     "e_1jet","e_2jet","e_3jet","e_4jet",
                                                     "eta_1jet","eta_2jet","eta_3jet","eta_4jet",
                                                     "phi_1jet","phi_2jet","phi_3jet","phi_4jet",
                                                     "pT_1lep","pT_2lep","pT_3lep",
                                                     "e_1lep","e_2lep","e_3lep",
                                                     "eta_1lep","eta_2lep","eta_3lep",
                                                     "phi_1lep","phi_2lep","phi_3lep",
                                                     "charge_1lep", "charge_2lep", "charge_3lep",
                                                     "type_1lep", "type_2lep", "type_3lep",
                                                     "eventNumber"],
                                 "RecoVariables4L": ["pT_1jet","pT_2jet","pT_3jet","pT_4jet",
                                                     "e_1jet","e_2jet","e_3jet","e_4jet",
                                                     "eta_1jet","eta_2jet","eta_3jet","eta_4jet",
                                                     "phi_1jet","phi_2jet","phi_3jet","phi_4jet",
                                                     "pT_1lep","pT_2lep","pT_3lep","pT_4lep",
                                                     "e_1lep","e_2lep","e_3lep","e_4lep",
                                                     "eta_1lep","eta_2lep","eta_3lep","eta_4lep",
                                                     "phi_1lep","phi_2lep","phi_3lep","phi_4lep",
                                                     "charge_1lep", "charge_2lep", "charge_3lep", "charge_4lep",
                                                     "type_1lep", "type_2lep", "type_3lep", "type_4lep",
                                                     "eventNumber"],
                                 "TruthVariables": ["MC_b_from_t_pt","MC_b_from_tbar_pt",
                                                    "MC_b_from_t_m","MC_b_from_tbar_m",
                                                    "MC_b_from_t_eta","MC_b_from_tbar_eta",
                                                    "MC_b_from_t_phi","MC_b_from_tbar_phi",
                                                    "MC_Wdecay1_from_t_pdgId","MC_Wdecay1_from_tbar_pdgId",
                                                    "MC_Wdecay2_from_t_pdgId","MC_Wdecay2_from_tbar_pdgId",
                                                    "MC_Wdecay1_from_t_pt","MC_Wdecay1_from_tbar_pt",
                                                    "MC_Wdecay2_from_t_pt","MC_Wdecay2_from_tbar_pt",
                                                    "MC_Wdecay1_from_t_eta","MC_Wdecay1_from_tbar_eta",
                                                    "MC_Wdecay2_from_t_eta","MC_Wdecay2_from_tbar_eta",
                                                    "MC_Wdecay1_from_t_phi","MC_Wdecay1_from_tbar_phi",
                                                    "MC_Wdecay2_from_t_phi","MC_Wdecay2_from_tbar_phi",
                                                    "MC_Wdecay1_from_t_m","MC_Wdecay1_from_tbar_m",
                                                    "MC_Wdecay2_from_t_m","MC_Wdecay2_from_tbar_m",
                                                    "MC_Zdecay1_pt","MC_Zdecay2_pt",
                                                    "MC_Zdecay1_eta","MC_Zdecay2_eta",
                                                    "MC_Zdecay1_phi","MC_Zdecay2_phi",
                                                    "MC_Zdecay1_m","MC_Zdecay2_m",
                                                    "MC_Zdecay1_pdgId","MC_Zdecay2_pdgId",
                                                    "eventNumber"]}
    def Reco3L(self):
        return self.__EventVariables["RecoVariables3L"]

    def Reco4L(self):
        return self.__EventVariables["RecoVariables4L"]

    def Truth(self):
        return self.__EventVariables["TruthVariables"]

class ttXEvent(object):
    """
    Class for ttZ event reconstruction.

    Keyword arguments:
    VariablesDict --- Dictionary holding all variables necessary for event reconstruction and definition.
    """
    def __init__(self, VariablesDict, nLeps):
        self.__VariablesDict = VariablesDict
        self.__JetIndeceDict = {"b_from_t"     : 0,
                                "b_from_tbar"  : 1,
                                "q1_from_t"    : 2,
                                "q2_from_t"    : 3,
                                "q1_from_tbar" : 4,
                                "q2_from_tbar" : 5}
        self.__LeptonIndeceDict = {"l1_from_Z"   : 0,
                                   "l2_from_Z"   : 1,
                                   "l_from_t"    : 2,
                                   "l_from_tbar" : 3}
        self.__CONSIDERNJETS = 4
        self.__CONSIDERNLEPS = nLeps
        self.__Z_matched            = False
        self.__Z_half_matched       = False
        self.__l_from_t_matched     = False
        self.__l_from_tbar_matched  = False
        self.__b_from_t_matched     = False
        self.__b_from_tbar_matched  = False
        self.__q1_from_t_matched    = False
        self.__q1_from_tbar_matched = False
        self.__q2_from_t_matched    = False
        self.__q2_from_tbar_matched = False
        # For sanity checks
        self.__isLeptonic = False
        self.__isDilepton = False
        self.__isFullHadr = False
        self.__isUndef    = False

        self.__RecoJets = {str(i)+"jet": RecoObject(self.__VariablesDict["pT_"+str(i)+"jet"],
                                                    self.__VariablesDict["eta_"+str(i)+"jet"],
                                                    self.__VariablesDict["phi_"+str(i)+"jet"],
                                                    self.__VariablesDict["e_"+str(i)+"jet"], 999, 999) if self.__VariablesDict["pT_"+str(i)+"jet"]!=0 else None for i in range(1,self.__CONSIDERNJETS+1)}
        self.__RecoLeptons = {str(i)+"lep": RecoObject(self.__VariablesDict["pT_"+str(i)+"lep"],
                                                       self.__VariablesDict["eta_"+str(i)+"lep"],
                                                       self.__VariablesDict["phi_"+str(i)+"lep"],
                                                       self.__VariablesDict["e_"+str(i)+"lep"],
                                                       self.__VariablesDict["charge_"+str(i)+"lep"],
                                                       self.__VariablesDict["type_"+str(i)+"lep"]) if self.__VariablesDict["pT_"+str(i)+"lep"]!=0 else None for i in range(1,self.__CONSIDERNLEPS+1)}
        self.__TruthJets = {}
        self.__TruthLeptons = {}

    def get_EventVariables(self,Type):
        return self.__EventVariables[Type]
        
    def Define_TruthZLeps(self):
        self.__TruthLeptons["l1_from_Z"] = TruthObject(self.__VariablesDict["MC_Zdecay1_pt"],
                                                       self.__VariablesDict["MC_Zdecay1_eta"],
                                                       self.__VariablesDict["MC_Zdecay1_phi"],
                                                       self.__VariablesDict["MC_Zdecay1_m"],
                                                       self.__VariablesDict["MC_Zdecay1_pdgId"])
        self.__TruthLeptons["l2_from_Z"] = TruthObject(self.__VariablesDict["MC_Zdecay2_pt"],
                                                       self.__VariablesDict["MC_Zdecay2_eta"],
                                                       self.__VariablesDict["MC_Zdecay2_phi"],
                                                       self.__VariablesDict["MC_Zdecay2_m"],
                                                       self.__VariablesDict["MC_Zdecay2_pdgId"])
    def Define_TruthbJets(self):
        self.__TruthJets["b_from_t"] = TruthObject(self.__VariablesDict["MC_b_from_t_pt"],
                                                   self.__VariablesDict["MC_b_from_t_eta"],
                                                   self.__VariablesDict["MC_b_from_t_phi"],
                                                   self.__VariablesDict["MC_b_from_t_m"], 999)
        self.__TruthJets["b_from_tbar"] = TruthObject(self.__VariablesDict["MC_b_from_tbar_pt"],
                                                      self.__VariablesDict["MC_b_from_tbar_eta"],
                                                      self.__VariablesDict["MC_b_from_tbar_phi"],
                                                      self.__VariablesDict["MC_b_from_tbar_m"], 999)

    def Define_TruthTopDecays(self):
        ##### t side ####
        if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [1,2,3,4,5,6]): # top decays hadronically
            self.__TruthJets["q1_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],
                                                        self.__VariablesDict["MC_Wdecay1_from_t_eta"],
                                                        self.__VariablesDict["MC_Wdecay1_from_t_phi"],
                                                        self.__VariablesDict["MC_Wdecay1_from_t_m"], 999)
            self.__TruthJets["q2_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],
                                                        self.__VariablesDict["MC_Wdecay2_from_t_eta"],
                                                        self.__VariablesDict["MC_Wdecay2_from_t_phi"],
                                                        self.__VariablesDict["MC_Wdecay2_from_t_m"], 999)
            self.__TruthLeptons["l_from_t"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,12,13,14,15,16]): # top decays leptonically
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],
                                                              self.__VariablesDict["MC_Wdecay1_from_t_eta"],
                                                              self.__VariablesDict["MC_Wdecay1_from_t_phi"],
                                                              self.__VariablesDict["MC_Wdecay1_from_t_m"],
                                                              self.__VariablesDict["MC_Wdecay1_from_t_pdgId"])
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],
                                                              self.__VariablesDict["MC_Wdecay2_from_t_eta"],
                                                              self.__VariablesDict["MC_Wdecay2_from_t_phi"],
                                                              self.__VariablesDict["MC_Wdecay2_from_t_m"],
                                                              self.__VariablesDict["MC_Wdecay2_from_t_pdgId"])
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            self.__TruthLeptons["l_from_t"] = None
        ##### tbar side ####
        if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [1,2,3,4,5,6]): # tbar decays hadronically
            self.__TruthJets["q1_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"],
                                                           self.__VariablesDict["MC_Wdecay1_from_tbar_eta"],
                                                           self.__VariablesDict["MC_Wdecay1_from_tbar_phi"],
                                                           self.__VariablesDict["MC_Wdecay1_from_tbar_m"], 999)
            self.__TruthJets["q2_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"],
                                                           self.__VariablesDict["MC_Wdecay2_from_tbar_eta"],
                                                           self.__VariablesDict["MC_Wdecay2_from_tbar_phi"],
                                                           self.__VariablesDict["MC_Wdecay2_from_tbar_m"], 999)
            self.__TruthLeptons["l_from_tbar"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,12,13,14,15,16]): # tbar decays leptonically
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"],
                                                                 self.__VariablesDict["MC_Wdecay1_from_tbar_eta"],
                                                                 self.__VariablesDict["MC_Wdecay1_from_tbar_phi"],
                                                                 self.__VariablesDict["MC_Wdecay1_from_tbar_m"],
                                                                 self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"])
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"],
                                                                 self.__VariablesDict["MC_Wdecay2_from_tbar_eta"],
                                                                 self.__VariablesDict["MC_Wdecay2_from_tbar_phi"],
                                                                 self.__VariablesDict["MC_Wdecay2_from_tbar_m"],
                                                                 self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"])
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            self.__TruthLeptons["l_from_tbar"] = None

    def DefineTruthttZFinalState(self):
        self.Define_TruthZLeps()
        self.Define_TruthbJets()
        self.Define_TruthTopDecays()

    def DefineDecayChannel(self):
        nLeptons = len([1 for k,v in self.__TruthLeptons.items() if "l_from" in k and v != None])
        if nLeptons == 0:
            self.__isFullHadr = True
        elif nLeptons == 1:
            self.__isLeptonic = True
        elif nLeptons == 2:
            self.__isDilepton = True
        # Sanity check to make sure event is fullfilling exactly one category
        EventType = iter([self.__isFullHadr, self.__isLeptonic, self.__isDilepton])
        if not(any(EventType) and not any(EventType)) and not self.__isUndef:
            ErrorMessage("Event fullfills more than one event category (fully hadronic, leptonic, dileptonic)!")

    def MatchWithTruth(self, nItems, option="Jets"):
        '''
        Method to match reco objects with truth objects.
        Keyword Arguments:
        nItems --- Number of items to be matched (e.g. 3 only consider first three objects)
        option --- Can be either "Jets" or "Leptons"
        '''
        self.DefineTruthttZFinalState()
        if(option=="Jets"):
            Matcher = RecoTruthMatcher(CutVal = 0.4, DRBased=True, MatchSF=True, MatchSC=False)
            # Truth jets
            b_from_t     = self.__TruthJets["b_from_t"]
            b_from_tbar  = self.__TruthJets["b_from_tbar"]
            q1_from_t    = self.__TruthJets["q1_from_t"]
            q2_from_t    = self.__TruthJets["q2_from_t"]
            q1_from_tbar = self.__TruthJets["q1_from_tbar"]
            q2_from_tbar = self.__TruthJets["q2_from_tbar"]

            RecoJetsVectors = [item.Vector() for key,item in self.__RecoJets.items() if item != None]
            RecoVectorsInfos = np.empty(len(RecoVectors), dtype = [("p4", TLorentzVector),
                                                                   ("charge", np.int32),
                                                                   ("flavour", np.int32)])
            RecoJetsVectorsInfos["p4"]      = RecoJetsVectors
            RecoJetsVectorsInfos["charge"]  = [item.Charge() for key, item in self.__RecoJets.items() if item != None]
            RecoJetsVectorsInfos["flavour"] = [item.Type() for key,item in self.__RecoJets.items() if item != None]

            TruthJetsVectors = [self.__TruthJets[key].Vector() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key] != None]
            TruthJetsVectorsInfos = np.empty(len(TruthVectors), dtype = [("p4", TLorentzVector),
                                                                         ("charge", np.int32),
                                                                         ("flavour", np.int32)])
            TruthJetVectorsInfos["p4"]      = TruthJetVectors
            TruthJetVectorsInfos["charge"]  = [self.__TruthJets[key].Charge() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key] != None]
            TruthJetVectorsInfos["flavour"] = [self.__TruthJets[key].Type() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key] != None]

            RecoJetIndece = Matcher.MatchRecoTruth(reco_indece=[value+1 for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key] != None],
                                                   truth_vec_info=TruthJetVectorsInfos,
                                                   reco_vec_info=RecoJetVectorsInfos) # Here the matching for jets happens
            if(0 in RecoJetIndece):
                self.__b_from_t_matched = True
            if(1 in RecoJetIndece):
                self.__b_from_tbar_matched = True
            if(2 in RecoJetIndece):
                self.__q1_from_t_matched = True
            if(3 in RecoJetIndece):
                self.__q2_from_t_matched = True
            if(4 in RecoJetIndece):
                self.__q1_from_tbar_matched = True
            if(5 in RecoJetIndece):
                self.__q2_from_tbar_matched = True
            if(nItems > len(RecoJetIndece)):
                RecoJetIndece.tolist().extend([-2] * (nItems - len(RecoJetIndece)))
            return list(RecoJetIndece)

        elif(option=="Leptons"):
            Matcher = RecoTruthMatcher(CutVal = 0.1, DRBased=True, MatchSF=True, MatchSC=True)
            l1_from_Z   = self.__TruthLeptons["l1_from_Z"]
            l2_from_Z   = self.__TruthLeptons["l2_from_Z"]
            l_from_t    = self.__TruthLeptons["l_from_t"]
            l_from_tbar = self.__TruthLeptons["l_from_tbar"]

            RecoLeptonVectors = list([item.Vector() for key,item in self.__RecoLeptons.items() if item !=None])
            RecoLeptonVectorsInfos = np.empty(len(RecoLeptonVectors), dtype = [("p4", TLorentzVector),
                                                                               ("charge", np.int32),
                                                                               ("flavour", np.int32)])
            RecoLeptonVectorsInfos["p4"]      = RecoLeptonVectors
            RecoLeptonVectorsInfos["charge"]  = [item.Charge() for key, item in self.__RecoLeptons.items() if item != None]
            RecoLeptonVectorsInfos["flavour"] = [item.Type() for key,item in self.__RecoLeptons.items() if item !=None]
 
            TruthLeptonVectors = [self.__TruthLeptons[key].Vector() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos = np.empty(len(TruthLeptonVectors), dtype = [("p4", TLorentzVector),
                                                                                 ("charge", np.int32),
                                                                                 ("flavour", np.int32)])
            TruthLeptonVectorsInfos["p4"]      = [self.__TruthLeptons[key].Vector() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos["charge"]  = [self.__TruthLeptons[key].Charge() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos["flavour"] = [self.__TruthLeptons[key].Type() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            RecoLeptonIndece = Matcher.MatchRecoTruth(reco_indece=[value+1 for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key] != None],
                                                      truth_vec_info=TruthLeptonVectorsInfos,
                                                      reco_vec_info=RecoLeptonVectorsInfos) # Here the matching for leptons happens
            if(0 in RecoLeptonIndece and 1 in RecoLeptonIndece):
                self.__Z_matched = True
            elif((0 in RecoLeptonIndece) != (1 in RecoLeptonIndece)):
                self.__Z_half_matched = True

            if(2 in RecoLeptonIndece):
                self.__l_from_t_matched = True
            if(3 in RecoLeptonIndece):
                self.__l_from_tbar_matched = True
            if(nItems > len(RecoLeptonIndece)):
               RecoLeptonIndece.tolist().extend([-2] * (nItems - len(RecoLeptonIndece)))
            return list(RecoLeptonIndece)
        else:
            ErrorMessage("Unrecognized reconstruction option for truth-reco matching!")

    def NumberLepCombis(self):
        if self.__CONSIDERNLEPS==3:
            return 3 # 3 lepton combinations possible, if 3 leptons on reco level
        elif self.__CONSIDERNLEPS==4:
            return 6 # 6 lepton combinations possible, if 4 leptons on reco level
    
    def is3L(self):
        return self.__isLeptonic

    def is4L(self):
        return self.__isDilepton
 
    def get_ZIndece(self):
        indece = self.MatchWithTruth(nItems=self.__CONSIDERNLEPS, option="Leptons")
        return [indece.index(i) if i in indece else -1 for i in [0,1]]
        
    def get_ZIndece_1D(self, NoRecoIsValid=True):
        Zindece = self.get_ZIndece()
        # if there are 3 leptons, the following combinations are sufficient:
        if -1 in Zindece:
            if NoRecoIsValid: 
                if self.__CONSIDERNLEPS==3:
                    return 3
                elif self.__CONSIDERNLEPS==4:
                    return 6
            else:
                return -2
        elif 0 in Zindece and 1 in Zindece:
            return 0
        elif 0 in Zindece and 2 in Zindece:
            return 1
        elif 1 in Zindece and 2 in Zindece:
            return 2
        #if there are 4 leptons, one needs additional combinations:
        elif 0 in Zindece and 3 in Zindece:
            return 3
        elif 1 in Zindece and 3 in Zindece:
            return 4
        elif 2 in Zindece and 3 in Zindece:
            return 5
