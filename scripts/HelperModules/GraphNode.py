'''
Module to build individual nodes for graphs.
'''

from HelperModules.OptionHandler import OptionHandler
from HelperModules.MessageHandler import ErrorMessage

class GraphNode():
    '''
    Class to describe an node for a graph (and subsequently GNN).
    '''
    def __init__(self, group):
        self.m_active = True
        self.m_graphnodeoptions = OptionHandler()
        self.m_graphnodeoptions.set_description("GRAPHNODE")
        self.m_graphnodeoptions.register_option("Name", optiondefault=None)
        self.m_graphnodeoptions.register_option("Type", optiondefault=None)
        self.m_graphnodeoptions.register_option("Features", optiondefault=None, optionislist=True)
        self.m_graphnodeoptions.register_option("Targets", optiondefault=None, optionislist=True)
        self.m_graphnodeoptions.register_option("EdgeFeatures", optiondefault=None, optionisnestedlist=True)
        self.m_graphnodeoptions.register_option("PruneIfValue", optiondefault=None, optionislist=True)

        self.m_graphnodeoptions.register_check(optionname="Name", check="set")
        self.m_graphnodeoptions.register_check(optionname="Type", check="set")
        for item in group:
            self.m_graphnodeoptions.set_option(item)
        self.m_graphnodeoptions.check_options()

    def get(self, name):
        '''
        Getter method to return options.
        Keyword arguments:
        name (string): name of the option to be returned

        Returns:
        self.m_graphnodeoptions.get(name) (OptionHandler option object)
        '''
        return self.m_graphnodeoptions.get(name)

    def isglobal(self):
        '''
        Method to define whether a node is "global" or not.

        Returns:
        self.get("Type").lower() == "global" (boolean): Whether node is global or not
        '''
        return self.get("Type").lower() == "global"

    def isactive(self):
        '''
        Method to define whether a node is active or not.

        Returns:
        self.m_active (boolean): Whether node is active or not
        '''
        return self.m_active

    def deactivate(self):
        '''
        Method to deactivate a node.
        Subsequently is active attribute is set to False.
        '''
        self.m_active = False

    def check_pruning_settings(self):
        '''
        Method check whether the number of pruning values defined equals the number of features.
        '''
        if self.get("PruneIfValue") is not None:
            if len(self.get("PruneIfValue")) != len(self.get("Features")):
                ErrorMessage(f"Length of 'PruneIfValue' list does not match length of 'Features' parameter for GRAPHNODE {self.get('Name')}")
