"""
This module takes care of the data manipulation
"""
from pickle import dump
import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.Directories import Directories
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from HelperModules.Settings import Settings


class Processor(object):

    def __init__(self, args):
        cfg_Settings = Settings(args.configfile, option="all")
        self.m_args = args
        self.m_cfg_Settings = cfg_Settings
        self.m_Dirs = Directories(cfg_Settings.get_General().get("Job"))
        cfg_Settings = Settings(args.configfile, option="all")
        self.m_args = args
        self.m_cfg_Settings = cfg_Settings
        self.m_Variables = cfg_Settings.get_Variables()
        self.m_VarNames = [var.get("Name") for var in self.m_Variables] + [self.m_cfg_Settings.get_General().get("FoldSplitVariable")]
        self.m_SanitizedVariables = hf.sanitize_root(self.m_VarNames)
        self.DileptonMerge = self.m_cfg_Settings.get_General().get("DileptonMerge")
        #self.PhiMirroring = self.m_cfg_Settings.get_General().get("PhiMirroring")


    def LeptonMerger(self, X):

        # Lepton 1 pt, eta, phi and E (taking el0 and mu1)
        X['lep1_pt'] = X['el_pt[:,0]'].fillna(X['mu_pt[:,1]'])
        X['lep1_eta'] = X['el_eta[:,0]'].fillna(X['mu_eta[:,1]'])
        X['lep1_phi'] = X['el_phi[:,0]'].fillna(X['mu_phi[:,1]'])
        X['lep1_e'] = X['el_e[:,0]'].fillna(X['mu_e[:,1]'])

        # Lepton 1 pt, eta, phi and E (taking el1 and mu0)
        X['lep2_pt'] = X['el_pt[:,1]'].fillna(X['mu_pt[:,0]'])
        X['lep2_eta'] = X['el_eta[:,1]'].fillna(X['mu_eta[:,0]'])
        X['lep2_phi'] = X['el_phi[:,1]'].fillna(X['mu_phi[:,0]'])
        X['lep2_e'] = X['el_e[:,1]'].fillna(X['mu_e[:,0]'])

        X = X.drop(columns=['el_pt[:,0]', 'el_eta[:,0]', 'el_phi[:,0]', 'el_e[:,0]', 'el_pt[:,1]', 'el_eta[:,1]', 'el_phi[:,1]', 'el_e[:,1]', 'mu_pt[:,1]', 'mu_eta[:,1]', 'mu_phi[:,1]', 'mu_e[:,1]', 'mu_pt[:,0]', 'mu_eta[:,0]', 'mu_phi[:,0]', 'mu_e[:,0]'])
        
        new_columns = ["Label","lep1_pt","lep1_eta","lep1_phi","lep1_e","lep2_pt","lep2_eta","lep2_phi","lep2_e"]
        temp = X.drop(columns=["Label","lep1_pt","lep1_eta","lep1_phi","lep1_e","lep2_pt","lep2_eta","lep2_phi","lep2_e"]).columns.values.tolist()
        print(temp)
        new_columns += temp
        X = X.reindex(columns = new_columns)

        print(X)

        return X


    def Uniform(self, X, lower_edge, upper_edge, binning):

        # Check wheter we have dilepton channel
        if self.DileptonMerge == True:
            try:
                X = self.LeptonMerger(X)
            except Exception as e:
                WarningMessage("Could not combine the leptons. %s!" % (e))

        select = []
        for N in range(lower_edge, upper_edge, binning):
            try:
                select.append(X[(X['Label'] < N+binning) & (X['Label'] > N)].sample(n=40000))
            except:
                select.append(X[(X['Label'] < N+binning) & (X['Label'] > N)])

        select = pd.concat(select)

        # Fill empty values for jets with mean
        if self.m_cfg_Settings.get_General().get("FillNaN") == 'mean':
            select = select.fillna(select.mean())
        elif self.m_cfg_Settings.get_General().get("FillNaN") == 'zeros':
            select = select.fillna(0)
        elif self.m_cfg_Settings.get_General().get("FillNaN") == 'median':
            select = select.fillna(select.median())
        #print(select['jet_pt[:,2]'].head(10))

        if self.PhiMirroring is True:
            ConverterMessage("Mirroring phi and shuffling..")
            select = self.Mirror(select)

        if self.m_args.namefile is not None:
            select.to_hdf(self.m_args.namefile + "_uniform.h5", key="df", mode="w")
            ConverterDoneMessage("Uniform hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + "_uniform.h5")
        else:
            select.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged_uniform.h5", key="df", mode="w")
            ConverterDoneMessage("Uniform hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged_uniform.h5")

        return select



    def Mirror(self, X):
        df = copy.deepcopy(X)
        # Select the columns with 'phi' in th3 name
        phi_cols = [col for col in df.columns if 'phi' in col]
        
        # Create opposite and concatenate
        df[phi_cols] = df[phi_cols]*-1
        X = pd.concat([X,df])

        # Shuffle the rows
        X = X.sample(frac = 1)

        return X




    def Scaler(self, X):
        
        scaler = MinMaxScaler()
        print(X)
        X = scaler.fit_transform(X)
        print(X)
        print(X.shape)
        X.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged_uniform_norm.h5", key="df", mode="w")
        ConverterDoneMessage("Uniform hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged_uniform_norm.h5")

        return X, scaler

