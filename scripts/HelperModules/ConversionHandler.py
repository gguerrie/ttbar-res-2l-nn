
"""
This module introduces a class handling the conversion of root files.
"""
from shutil import copyfile
from itertools import groupby
from operator import itemgetter
from PlotterModules.CtrlPlotHandler import CtrlPlotHandler
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage, WelcomeMessage, EnvironmentalCheck
from HelperModules.Directories import Directories
from HelperModules.Settings import Settings
from HelperModules.Processor import Processor
from HelperModules.EventReco.ttXReco import ttXEventVariables, ttXEvent
import HelperModules.HelperFunctions as hf
from os.path import exists
import uproot
import awkward as ak
import vector
import numpy as np
import pandas as pd
import glob
import os
import ROOT
import pickle
pickle.HIGHEST_PROTOCOL = 4

class ConversionHandler(object):
    """
    This class handles the conversion for all models.
    """

    def __init__(self, args):
        EnvironmentalCheck()
        WelcomeMessage("Converter")
        ROOT.gErrorIgnoreLevel = ROOT.kWarning
        cfg_Settings = Settings(args.configfile, option="all")

        self.m_args = args
        self.m_cfg_Settings = cfg_Settings
        self.m_Variables = cfg_Settings.get_Variables()
        self.m_VarNames = [var.get("Name") for var in self.m_Variables] + [self.m_cfg_Settings.get_General().get("FoldSplitVariable")]
        self.m_Model = cfg_Settings.get_Model()
        self.m_Samples = cfg_Settings.get_Samples()
        self.m_df_X_all = None
        self.m_df_Y_all = None
        self.m_df_W_all = None
        self.m_df_N_all = None
        self.m_df_T_all = None
        self.m_df_isNominal_all = None

        self.m_SampleTreeName = None
        self.m_SampleSelection = None
        self.m_SampleMCWeight = None
        self.m_SanitizedVariables = None
        self.m_SampleFilePaths = []
        self.m_nEmptyFrames = 0
        self.m_nEmptySamples = 0
        self.DileptonMerge = cfg_Settings.get_General().get("DileptonMerge")

        # if self.DileptonMerge == True:
        #     print("Printing DileptonMerge varlist:")
        #     print(cfg_Settings.get_VarNames_WithMergedLeptons())
        #     print(len(cfg_Settings.get_VarNames_WithMergedLeptons()))

        self.m_Dirs = Directories("%s"%cfg_Settings.get_General().get("Job"))
        
        self.m_DoCtrlPlots = cfg_Settings.get_General().do_CtrlPlots()
        if self.m_args.namefile is not None:
            self.m_DoCtrlPlots = False
        # Copy the config file to the job directory
        cfg_path, cfg_extension = os.path.splitext(self.m_args.configfile)
        copyfile(self.m_args.configfile, hf.ensure_trailing_slash(self.m_Dirs.ConfigDir()) + os.path.basename(cfg_path) + "_conversion_step" + cfg_extension)

    def is_Empty(self, Sample):
        """
        Method to check whether no events from a Sample passed selection.

        Keyword arguments:
        Sample --- Sample object
        """
        if self.m_SampleFilePaths == self.m_nEmptyFrames:
            WarningMessage("None of the ntuple files for %s contribute events passing the selection %s" % (Sample.get("Name"), self.m_SampleSelection))
            self.m_nEmptySamples += 1
            return True
        self.m_nEmptyFrames = 0
        return False

    def Check_LogicStrings(self):
        """
        Method to check whether the logic operators in string are valid,
        whether parenthesis are matched and whether string is python conform.
        """
        # Checking for matched brackets
        if not hf.matched(self.m_SampleSelection):
            ErrorMessage("Sample selection has unmatched brackets! %s" % (self.m_SampleSelection))
        if not hf.matched(self.m_SampleMCWeight):
            ErrorMessage("Sample selection has unmatched brackets! %s" % (self.m_SampleMCWeight))
        if any([item in self.m_SampleSelection for item in ["@", "->", "^"]]):
            ErrorMessage("Found c++ style character in selection! %s" % (self.m_SampleSelection))
        if any([item in self.m_cfg_Settings.get_General().get("MCWeight") for item in ["@", "->", "^"]]):
            ErrorMessage("Found c++ style character in weight!" % (self.m_cfg_Settings.get_General().get("MCWeight")))
        if "&&" in self.m_SampleSelection:
            ErrorMessage("Found c++ style '&&' in combined selection string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_SampleSelection:
            ErrorMessage("Found c++ style '||' in combined selection string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_SampleSelection.find("!")
        if self.m_SampleSelection!="" and idx!=-1 and self.m_SampleSelection[idx+1]!="=":
            ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_SampleSelection.find("!", idx + 1)
            if self.m_SampleSelection[idx+1]!="=" and idx!=-1:
                ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        if "&&" in self.m_SampleMCWeight:
            ErrorMessage("Found c++ style '&&' in combined MCWeight string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_SampleMCWeight:
            ErrorMessage("Found c++ style '||' in combined MCWeight string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_SampleMCWeight.find("!")
        if idx!=-1 and self.m_SampleMCWeight[idx+1]!="":
            ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_SampleMCWeight.find("!", idx + 1)
            if self.m_SampleMCWeight[idx+1]!="=" and idx!=-1:
                ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")

    def set_SampleMCWeight(self, Sample):
        '''
        Function combine Sample and general MCWeight in case a sample-specific MCWeight is given

        Keyword arguments:
        Sample --- Sample object to be checked for MCWeight
        '''
        if Sample.get("MCWeight") != "":
            self.m_SampleMCWeight = self.m_cfg_Settings.get_General().get("MCWeight") + "*" + Sample.get("MCWeight")
        else:
            self.m_SampleMCWeight = self.m_cfg_Settings.get_General().get("MCWeight")

    def set_SampleSelection(self, Sample):
        '''
        Function combine Sample and general selection in case a sample-specific selection is given

        Keyword arguments:
        Sample --- Sample object to be checked for selection string
        '''
        if Sample.get("Selection") != "":
            if self.m_cfg_Settings.get_General().get("Selection") != "":
                self.m_SampleSelection = "(%s)&(%s)" % (self.m_cfg_Settings.get_General().get("Selection"), Sample.get("Selection"))
            else:
                self.m_SampleSelection = Sample.get("Selection")
        else:
            self.m_SampleSelection = self.m_cfg_Settings.get_General().get("Selection")

    def InitialiseConversion(self, Sample):
        '''
        Function to initialise several different variables and objects that are needed
        for the conversion of ntuples from individual samples.

        Keyword arguments:
        Sample -- Sample object
        '''
        ConverterMessage("Processing %s (%s)" % (Sample.get("Name"), Sample.get("Type")))
        ### Resetting DataFrames ###
        self.m_df_X_all = None
        self.m_df_Y_all = None
        self.m_df_W_all = None
        self.m_df_N_all = None
        self.m_df_T_all = None
        self.m_df_isNominal_all = None
        ### Resetting some Variables ###
        if Sample.get("Treename") is not None:
            self.m_SampleTreename = Sample.get("Treename")
        else:
            self.m_SampleTreename = self.m_cfg_Settings.get_General().get("Treename")
        ### setting sample MC weight and selection ###
        self.set_SampleMCWeight(Sample)
        self.set_SampleSelection(Sample)
        self.Check_LogicStrings()
        ### sanitize the variablle inputs ###
        self.m_SanitizedVariables = hf.sanitize_root(self.m_VarNames)
        ### get list of filepaths ###
        self.m_SampleFilePaths = []
        for FileString in Sample.get("NtupleFiles"):
            if self.m_args.namefile is not None:
                self.m_SampleFilePaths.append(hf.ensure_trailing_slash(self.m_cfg_Settings.get_General().get("NtuplePath")) + self.m_args.namefile)
                print(self.m_SampleFilePaths)
            else:
                lst = glob.glob(hf.ensure_trailing_slash(self.m_cfg_Settings.get_General().get("NtuplePath")) + FileString)
                if len(lst) == 0:
                    ErrorMessage("Could not find any file with pattern %s in %s." % (FileString, hf.ensure_trailing_slash(self.m_cfg_Settings.get_General().get("NtuplePath"))))
                self.m_SampleFilePaths.extend(lst)

        for Filepath in self.m_SampleFilePaths:
            if not os.path.isfile(Filepath):
                ErrorMessage("Could not open %s. File does not exist" % Filepath)

    def PerformConversion(self):
        '''
        Main function to perform the conversion. It checks the type of the model and subsequently
        calls the corresponding conversion method to perform the conversion.
        Afterwards it merges the created .h5 datasets and (optionally) creates control plots.
        '''
        if not self.m_args.plots_only:
            if self.m_Model.isClassification():
                self.do_Classification_Conversion()
            elif self.m_Model.isReconstruction():
                self.do_Reconstruction_Conversion()
            elif self.m_Model.isRegression():
                self.do_Regression_Conversion()
            self.MergeDatasets()
        
        if self.DileptonMerge == True:
            self.m_VarNames = self.m_cfg_Settings.get_VarNames_WithMergedLeptons() + [self.m_cfg_Settings.get_General().get("FoldSplitVariable")]

        if self.m_DoCtrlPlots or self.m_args.plots_only:
            CtrlPlotHandler(self.m_cfg_Settings).do_CtrlPlots(plots=self.m_args.plots)

    def FinaliseConversion(self, Sample):
        '''
        Function to finalise the conversion by concatenating the pandas dataframes corresponding to
        individual folds. The user is notified at the end.

        Keyword arguments:
        Sample -- Sample object
        '''
        ConverterMessage("Finalising conversion for %s (%s)" % (Sample.get("Name"), Sample.get("Type")))
        # Adding all temporary lists together
        self.m_df_X_all = self.m_df_X_all.reset_index(drop=True)
        self.m_df_Y_all = self.m_df_Y_all.reset_index(drop=True)
        self.m_df_W_all = self.m_df_W_all.reset_index(drop=True)
        self.m_df_N_all = self.m_df_N_all.reset_index(drop=True)
        self.m_df_T_all = self.m_df_T_all.reset_index(drop=True)
        self.m_df_isNominal_all = self.m_df_isNominal_all.reset_index(drop=True)
        df_all = pd.concat([self.m_df_X_all,
                            self.m_df_Y_all,
                            self.m_df_W_all,
                            self.m_df_N_all,
                            self.m_df_T_all,
                            self.m_df_isNominal_all],
                           axis=1,
                           sort=False)

        if self.m_args.namefile is not None:
            df_all.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + ".h5", key="df", mode="w")
            ConverterDoneMessage("Processing " + Sample.get("Name") + " (" + self.m_args.namefile + ") DONE!")
            ConverterMessage("Sample saved at " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + ".h5")
        else:                   
            df_all.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + Sample.get("Name") + ".h5", key="df", mode="w")
            ConverterDoneMessage("Processing " + Sample.get("Name") + " (" + Sample.get("Type") + ") DONE!")

    def MergeDatasets(self):
        '''
        Simple function to merge the .h5 datasets which were created together into one big dataset containing everything.
        '''
        if self.m_nEmptySamples == len(self.m_Samples):
            ErrorMessage("None of the given Samples has events passing the respective selection! You need to check this!")
        ConverterMessage("Merging datasets...")

        DataProcessing = Processor(self.m_args)
        
        if self.m_args.namefile is not None:
            merged = pd.read_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + ".h5")
        else:
            dfs = [pd.read_hdf(fname) for fname in [hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + Sample.get("Name") + ".h5" for Sample in self.m_Samples]]
            merged = pd.concat(dfs)
            # Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
            seed = 1
            merged = merged.sample(frac=1, random_state=seed).reset_index(drop=True)

        if self.DileptonMerge == True:
            merged = DataProcessing.LeptonMerger(merged)

        if self.m_cfg_Settings.get_General().get("FillNaN") == 'mean':
            merged = merged.fillna(merged.mean())
        elif self.m_cfg_Settings.get_General().get("FillNaN") == 'zeros':
            merged = merged.fillna(0)
        elif self.m_cfg_Settings.get_General().get("FillNaN") == 'median':
            merged = merged.fillna(merged.median())

        if self.m_args.namefile is not None:
            merged.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + ".h5", key="df", mode="w")
            ConverterDoneMessage("Nominal hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + self.m_args.namefile + ".h5")
        else:
            merged.to_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged.h5", key="df", mode="w")
            ConverterDoneMessage("Merged hdf5 file (data) written to " + hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged.h5")
            
    def get_X(self, Filepath, Treename, Variables, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        Selection --- String representing the selection
        '''
        if not os.path.exists(Filepath):
            ErrorMessage("%s does not seem to exist. You need to check this!"%Filepath)
        try:
            X_Uncut = uproot.open(Filepath + ":" + Treename)
        except BaseException:
            ErrorMessage("Could not open %s tree in %s" % (Treename, Filepath))

        try:
            foldsplitvar = X_Uncut.arrays(self.m_cfg_Settings.get_General().get("FoldSplitVariable"), library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
        except KeyError as e:
            ErrorMessage("Could not find 'FoldSplitVariable' (%s) in %s : %s" % (self.m_cfg_Settings.get_General().get("FoldSplitVariable"), Filepath + ":" + Treename, e.args[0]))

        try:
            if Selection == "":
                X = X_Uncut.arrays(Variables, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
            else:
                X = X_Uncut.arrays(Variables, Selection, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                X.reset_index(drop=True)
        except uproot.exceptions.KeyInFileError as e:
            ErrorMessage("The variable '%s' is missing in tree '%s' in file '%s'. Consider using the 'IgnoreTreenames' option." % (e.args[0], Treename, Filepath))
        except Exception as e:
            ErrorMessage("Could not convert %s. %s!" % (Filepath, e))
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        if len(X) == 0:
            WarningMessage("No events added from %s" % Filepath)
            self.m_nEmptyFrames += 1
            return None
        elif len(X) < 50:
            WarningMessage("Less than 50 events added from %s" % Filepath)
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        # X = X.fillna(0)
        # for variable in Variables:
        #     if X[variable].isnull().values.any():
        #         ErrorMessage("Variable %s has nan entries!" % variable)
        return X

    
    def get_mttbar(self, Filepath, Treename, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        '''
        #print("Getting " + str(Filepath) + ":" + str(Treename) + " (get_Y)")
        vector.register_awkward()   # any record named "Momentum4D" will be Lorentz
        try:
            with uproot.open(Filepath + ":" + Treename) as Y:
                try:
                    ttMass = Y.arrays(self.m_cfg_Settings.get_General().get("FoldSplitVariable"), library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                except KeyError as e:
                    ErrorMessage("Could not find 'FoldSplitVariable' (%s) in %s : %s" % (self.m_cfg_Settings.get_General().get("FoldSplitVariable"), Filepath + ":" + Treename, e.args[0]))
                try:
                    vars = ["MC_t_pt", "MC_t_phi", "MC_t_eta", "MC_t_m", "MC_tbar_pt", "MC_tbar_phi", "MC_tbar_eta", "MC_tbar_m"]
                    #mttbar_arrays = Y.arrays(filter_name="MC_t*", Selection)
                    mttbar_arrays = Y.arrays(vars, entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                    t_vec = ak.zip({
                        "pt": mttbar_arrays.MC_t_pt,
                        "phi": mttbar_arrays.MC_t_phi,
                        "eta": mttbar_arrays.MC_t_eta,
                        "mass": mttbar_arrays.MC_t_m,
                    }, with_name="Momentum4D")
                    tbar_vec = ak.zip({
                        "pt": mttbar_arrays.MC_tbar_pt,
                        "phi": mttbar_arrays.MC_tbar_phi,
                        "eta": mttbar_arrays.MC_tbar_eta,
                        "mass": mttbar_arrays.MC_tbar_m,
                    }, with_name="Momentum4D")
                    reco_ttMass = (t_vec + tbar_vec).mass

                    ttMass['mttbar'] = reco_ttMass

                    # Temporarily remove eventNumber
                    #ttMass = ttMass.drop([self.m_cfg_Settings.get_General().get("FoldSplitVariable")], axis=1)

                except uproot.exceptions.KeyInFileError as e:
                    ErrorMessage("The variable '%s' is missing in tree '%s' in file '%s'. Consider using the 'IgnoreTreenames' option." % (e.args[0], Treename, Filepath))
                except Exception as e:
                    ErrorMessage("Could not convert %s. %s!" % (Filepath, e))
        except BaseException:
            ErrorMessage("Could not open %s tree in %s" % (Treename, Filepath))

        # Remove inf values
        ttMass = ttMass[~ttMass.isin([np.inf, -np.inf]).any(1)]
        return ttMass

################################################################################################################################################################################
################################################################################################################################################################################
################################################################################################################################################################################

    def get_neutrinos(self, Filepath, Treename, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        '''
        #print("Getting " + str(Filepath) + ":" + str(Treename) + " (get_Y)")
        vector.register_awkward()   # any record named "Momentum4D" will be Lorentz

        try:
            with uproot.open(Filepath + ":" + Treename) as Y:
                try:
                    ttMass = Y.arrays(self.m_cfg_Settings.get_General().get("FoldSplitVariable"), library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                except KeyError as e:
                    ErrorMessage("Could not find 'FoldSplitVariable' (%s) in %s : %s" % (self.m_cfg_Settings.get_General().get("FoldSplitVariable"), Filepath + ":" + Treename, e.args[0]))

                try:
                    vars = ["MC_Wdecay1_from_t_pt", "MC_Wdecay1_from_t_eta", "MC_Wdecay1_from_t_phi", "MC_Wdecay1_from_t_m", "MC_Wdecay2_from_tbar_pt", "MC_Wdecay2_from_tbar_phi", "MC_Wdecay2_from_tbar_eta", "MC_Wdecay2_from_tbar_m"]
                    #mttbar_arrays = Y.arrays(filter_name="MC_t*", Selection)
                    mttbar_arrays = Y.arrays(vars, entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                    t_vec = ak.zip({
                        "pt": mttbar_arrays.MC_Wdecay1_from_t_pt,
                        "phi": mttbar_arrays.MC_Wdecay1_from_t_phi,
                        "eta": mttbar_arrays.MC_Wdecay1_from_t_eta,
                        "mass": mttbar_arrays.MC_Wdecay1_from_t_m,
                    }, with_name="Momentum4D")

                    tbar_vec = ak.zip({
                        "pt": mttbar_arrays.MC_Wdecay2_from_tbar_pt,
                        "phi": mttbar_arrays.MC_Wdecay2_from_tbar_phi,
                        "eta": mttbar_arrays.MC_Wdecay2_from_tbar_eta,
                        "mass": mttbar_arrays.MC_Wdecay2_from_tbar_m,
                    }, with_name="Momentum4D")

                    neutrinos = (t_vec + tbar_vec).eta

                    ttMass['neutrinos'] = neutrinos

                except uproot.exceptions.KeyInFileError as e:
                    ErrorMessage("The variable '%s' is missing in tree '%s' in file '%s'. Consider using the 'IgnoreTreenames' option." % (e.args[0], Treename, Filepath))
                except Exception as e:
                    ErrorMessage("Could not convert %s. %s!" % (Filepath, e))
        except BaseException:
            ErrorMessage("Could not open %s tree in %s" % (Treename, Filepath))

        # Remove inf values
        ttMass = ttMass[~ttMass.isin([np.inf, -np.inf]).any(1)]
        ttMass = ttMass.drop(ttMass[(ttMass['Label'] >= 100.0) | (ttMass['Label'] <= -100.0)].index)
        return ttMass



    def get_chel(self, Filepath, Treename, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        '''
        try:
            with uproot.open(Filepath + ":" + Treename) as Y:
                try:
                    ttMass = Y.arrays(self.m_cfg_Settings.get_General().get("FoldSplitVariable"), library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                except KeyError as e:
                    ErrorMessage("Could not find 'FoldSplitVariable' (%s) in %s : %s" % (self.m_cfg_Settings.get_General().get("FoldSplitVariable"), Filepath + ":" + Treename, e.args[0]))
        except BaseException:
            ErrorMessage("Could not open %s tree in %s" % (Treename, Filepath))

        # Make the header known to the interpreter
        ROOT.gInterpreter.ProcessLine('#include "scripts/ROOT_Macros/Event.h"')
        ROOT.gSystem.Load('scripts/ROOT_Macros/Event_C.so')

        # We find all the C++ entities in Python, right away!
        a = ROOT.Event()
        x = a.Init(Filepath)   # prints Hello PyROOT!
        ttMass['chel'] = pd.Series(x)#.fillna(0) #= pd.DataFrame(x, columns =['Chel'])
        return ttMass


    def get_W(self, Filepath, Sample, DefaultLength, returnEventNumbers=False):
        '''
        Function to extract the weights for a specific sample using
        arrays from the uproot package. DefaultLength is used to
        define the weight vector in case we are handling data.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the weight vector
        '''

        W_Uncut = uproot.open(Filepath + ":" + self.m_SampleTreename)
        try:
            if self.m_cfg_Settings.get_General().get("MCWeight") == "" or self.m_cfg_Settings.get_General().get("MCWeight") == "1" or Sample.get("Type") == "Data":
                W_tmp = [1] * DefaultLength
                W = pd.DataFrame(data=np.array(W_tmp), columns=["Weight"])
                if returnEventNumbers:
                    if self.m_SampleSelection == "":
                        eventNumbers = W_Uncut.arrays([self.m_cfg_Settings.get_General().get("FoldSplitVariable")], library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly")).values
                    else:
                        eventNumbers = W_Uncut.arrays([self.m_cfg_Settings.get_General().get("FoldSplitVariable")], self.m_SampleSelection, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly")).values
                    W[self.m_cfg_Settings.get_General().get("FoldSplitVariable")] = eventNumbers
            elif self.m_SampleSelection == "":
                W = W_Uncut.arrays(self.m_SampleMCWeight, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                if returnEventNumbers:
                    eventNumbers = W_Uncut.arrays([self.m_cfg_Settings.get_General().get("FoldSplitVariable")], library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly")).values
                    W[self.m_cfg_Settings.get_General().get("FoldSplitVariable")] = eventNumbers
                W = W.rename(columns={self.m_SampleMCWeight: "Weight"})
            else:
                W = W_Uncut.arrays(self.m_SampleMCWeight, self.m_SampleSelection, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly"))
                if returnEventNumbers:
                    eventNumbers = W_Uncut.arrays([self.m_cfg_Settings.get_General().get("FoldSplitVariable")], self.m_SampleSelection, library="pd", entry_stop=self.m_cfg_Settings.get_General().get("DoNOnly")).values
                    W[self.m_cfg_Settings.get_General().get("FoldSplitVariable")] = eventNumbers
                W = W.rename(columns={self.m_SampleMCWeight: "Weight"})
        except Exception as e:
            ErrorMessage("Could not convert %s! %s" % (Filepath, e))
        return W

    def get_N(self, Sample, DefaultLength):
        '''
        Function to extract the names for a specific sample using
        arrays from the uproot package. DefaultLength is used to
        define the length of the name list.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the name list
        '''
        if Sample.get("Group") is not None:
            N = [Sample.get("Group")] * DefaultLength
        else:
            N = [Sample.get("Name")] * DefaultLength
        return N

    def get_isNominal(self, Sample, DefaultLength):
        '''
        Function to extract whether a sample is a nominal one or not
        for a specific sample using arrays from the uproot package.
        DefaultLength is used to define the length of the list in case
        we are handling data.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the isNominal list
        '''
        if Sample.get("Type") == "Data":
            isNominal = [True] * DefaultLength
        if Sample.get("Type") == "Systematic":
            isNominal = [False] * DefaultLength
        else:
            isNominal = [True] * DefaultLength
        return isNominal

    def do_Classification_Conversion(self):
        '''
        Function to perform the conversion for a standard classification case.
        '''
        for Sample in self.m_Samples:
            self.InitialiseConversion(Sample)
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.m_SampleFilePaths:
                X = self.get_X(Filepath=Filepath,
                               Treename=self.m_SampleTreename,
                               Variables=self.m_SanitizedVariables,
                               Selection=self.m_SampleSelection)
                if X is None:
                    continue
                DefaultLength = len(X)
                if isinstance(Sample.get("TrainLabel"),str):
                    Y = self.get_X(Filepath=Filepath,
                                   Treename=self.m_SampleTreename,
                                   Variables=[Sample.get("TrainLabel")],
                                   Selection=self.m_SampleSelection)
                else:
                    Y = np.array([int(Sample.get("TrainLabel"))] * len(X))
                Y = np.where(Y<0,np.nan,Y)
                W = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength)
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get("Type")] * len(X)
                df_X_list.append(X)
                df_Y_list.append(pd.DataFrame(data=np.array(Y), columns=["Label"]))
                df_W_list.append(W)
                df_N_list.append(pd.DataFrame(data=np.array(N), columns=["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data=np.array(T), columns=["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data=np.array(isNominal), columns=["isNominal"]))
            if not self.is_Empty(Sample):
                self.m_df_X_all = pd.concat(df_X_list)
                self.m_df_Y_all = pd.concat(df_Y_list)
                self.m_df_W_all = pd.concat(df_W_list)
                self.m_df_N_all = pd.concat(df_N_list)
                self.m_df_T_all = pd.concat(df_T_list)
                self.m_df_isNominal_all = pd.concat(df_isNominal_list)
                self.FinaliseConversion(Sample)
            else:
                continue

    def do_Reconstruction_Conversion(self):
        '''
        Function to perform the conversion for a standard reconstruction case.
        '''
        EventVariables = ttXEventVariables()
        if self.m_Model.is3LZReconstruction():
            nLeps = 3
            RecoVariables = EventVariables.Reco3L()
        if self.m_Model.is4LZReconstruction():
            nLeps = 4
            RecoVariables = EventVariables.Reco4L()
        TruthVariables = EventVariables.Truth()
        for Sample in self.m_Samples:
            self.InitialiseConversion(Sample)
            # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.m_SampleFilePaths:
                X = self.get_X(Filepath=Filepath,
                               Treename=self.m_SampleTreename,
                               Variables=self.m_SanitizedVariables,
                               Selection=self.m_SampleSelection)
                if X is None:
                    continue
                df_X = pd.DataFrame(X)
                if Sample.get("Type") == "Data":
                    Y = [np.nan] * len(X)
                else:
                    Y = []
                X_df_RecoVariables = self.get_X(Filepath=Filepath,
                                                Treename=self.m_SampleTreename,
                                                Variables=RecoVariables,
                                                Selection=self.m_SampleSelection)
                X_df_TruthVariables = self.get_X(Filepath=Filepath,
                                                 Treename="truth",
                                                 Variables=TruthVariables,
                                                 Selection="")
                X_merged = pd.merge(X_df_TruthVariables, X_df_RecoVariables, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                df_X = pd.merge(X_df_TruthVariables, df_X, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                Y = []
                for _, EventKinematics in X_merged.iterrows():
                    EventKinematicsDict = EventKinematics.to_dict()
                    e = ttXEvent(EventKinematicsDict, nLeps)
                    # Z-Reconstruction
                    Y.append(e.get_ZIndece_1D())
                    # OR using Labels: Approach Using the invariant mass works/works partly/does nor work
                    # Y.append(e.get_InvMassClasses(Nlep))
                    # Add more reconstruction methods here
                if Sample.get("Type") == "Data":
                    W = [1] * len(X_merged)
                else:
                    # We have to load eventNumber and MCWeight separate from one another or MCWeight's type is evaluated wrongly
                    DefaultLength = len(X_merged)
                    W_df = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength, returnEventNumbers=True)
                    W = pd.merge(X_df_TruthVariables, W_df, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])["Weight"].values
                DefaultLength = len(X_merged)
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get("Type")] * DefaultLength
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                removeTruth = TruthVariables[:-1]
                df_X = df_X.drop(removeTruth, axis=1)  # Getting rid of truth variables again (except evenNumber which is the last item)
                df_X_list.append(df_X)
                df_Y_list.append(pd.DataFrame(data=np.array(Y), columns=["Label"]))
                df_W_list.append(pd.DataFrame(data=np.array(W), columns=["Weight"]))
                df_N_list.append(pd.DataFrame(data=np.array(N), columns=["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data=np.array(T), columns=["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data=np.array(isNominal), columns=["isNominal"]))
            self.m_df_X_all = pd.concat(df_X_list)
            self.m_df_Y_all = pd.concat(df_Y_list)
            self.m_df_W_all = pd.concat(df_W_list)
            self.m_df_N_all = pd.concat(df_N_list)
            self.m_df_T_all = pd.concat(df_T_list)
            self.m_df_isNominal_all = pd.concat(df_isNominal_list)
            self.FinaliseConversion(Sample)

    def do_Regression_Conversion(self):
        """
        Performing the conversion for a regression model.
        """
        for Sample in self.m_Samples:
            self.InitialiseConversion(Sample)
            # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.m_SampleFilePaths:
                df_X = self.get_X(Filepath=Filepath,
                                  Treename=self.m_SampleTreename,
                                  Variables=self.m_SanitizedVariables,
                                  Selection=self.m_SampleSelection)
                if df_X is None:
                    continue
                # for Y we use get_X but just ask for other variables in the truth tree
                if Sample.get("Type") != "Data":
                    
                    label_list = self.m_cfg_Settings.get_Model().get("RegressionTarget")+[self.m_cfg_Settings.get_General().get("FoldSplitVariable")]
                    mttbar = False
                    chel = False
                    neutrinos = False
                    try:
                        label_list.remove('mttbar')
                        mttbar = True
                    except:
                        pass
                    try:
                        label_list.remove('chel')
                        chel = True
                    except:
                        pass
                    try:
                        label_list.remove('neutrinos')
                        neutrinos = True
                    except:
                        pass
                    
                    df_Y = self.get_X(Filepath=Filepath,
                                      Treename="truth",
                                      Variables=label_list,
                                      Selection="")
                    if mttbar == True:
                        df_Y_mttbar = self.get_mttbar(Filepath=Filepath,
                                            Treename=self.m_SampleTreename,
                                            Selection="")
                        df_Y = pd.merge(df_Y, df_Y_mttbar, how="outer", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                    if chel == True:
                        df_Y_chel = self.get_chel(Filepath=Filepath,
                                            Treename=self.m_SampleTreename,
                                            Selection="")                   
                        df_Y = pd.merge(df_Y, df_Y_chel, how="outer", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                    
                    if neutrinos == True:
                        df_Y_neutrinos = self.get_neutrinos(Filepath=Filepath,
                                            Treename=self.m_SampleTreename,
                                            Selection="")
                        df_Y = pd.merge(df_Y, df_Y_neutrinos, how="outer", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])

                    # REMOVE NAN VALUES SO IT WORKS!!
                    #drop_index = df_Y[df_Y.isnull().any(axis=1)].index
                    df_Y = df_Y.dropna().reset_index(drop=True)

                    for target, scale in zip(self.m_cfg_Settings.get_Model().get("RegressionTarget"),self.m_cfg_Settings.get_Model().get("RegressionScale")):
                        df_Y[target] *= scale
                    if len(self.m_cfg_Settings.get_Model().get("RegressionTarget")) == 1:
                        df_Y = df_Y.rename(columns={self.m_cfg_Settings.get_Model().get("RegressionTarget")[0]: "Label"})
                    else:
                        df_Y["Label"] = df_Y[self.m_cfg_Settings.get_Model().get("RegressionTarget")].values.tolist()
                    
                    df_X_merged = pd.merge(df_X, df_Y, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                    DefaultLength = len(df_X_merged)
                    df_W = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength, returnEventNumbers=True)
                    df_W_merged = pd.merge(df_Y, df_W, how="inner", on=[self.m_cfg_Settings.get_General().get("FoldSplitVariable")])
                else:
                    df_X_merged = df_X.copy()
                    df_X_merged["Label"] = np.array([np.nan] * len(df_X))
                    df_W_merged = pd.DataFrame(data=np.array([1] * len(df_X)), columns=["Weight"])
                    DefaultLength = len(df_X_merged)
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get("Type")] * DefaultLength
                df_X_list.append(df_X_merged[self.m_VarNames])
                df_Y_list.append(df_X_merged["Label"])
                df_W_list.append(df_W_merged["Weight"])
                df_N_list.append(pd.DataFrame(data=np.array(N), columns=["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data=np.array(T), columns=["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data=np.array(isNominal), columns=["isNominal"]))
            self.m_df_X_all = pd.concat(df_X_list)
            self.m_df_Y_all = pd.concat(df_Y_list)
            self.m_df_W_all = pd.concat(df_W_list)
            self.m_df_N_all = pd.concat(df_N_list)
            self.m_df_T_all = pd.concat(df_T_list)
            self.m_df_isNominal_all = pd.concat(df_isNominal_list)
            self.FinaliseConversion(Sample)

    def get_FullSampleMCWeight(self, Sample):
        '''
        Function combine Sample and general MCWeight in case a sample-specific MCWeight is given

        Keyword arguments:
        Sample --- Sample object to be checked for MCWeight
        '''
        if Sample.get("MCWeight") != "":
            return "%s*%s" % (self.m_cfg_Settings.get_General().get("MCWeight"), Sample.get("MCWeight"))
        return self.m_cfg_Settings.get_General().get("MCWeight")

    def get_FullSampleSelection(self, Sample):
        '''
        Function combine Sample and general selection in case a sample-specific selection is given

        Keyword arguments:
        Selection --- Selection string to be checked
        '''
        if Sample.get("Selection") != "":
            return "(%s)&(%s)" % (self.m_cfg_Settings.get_General().get("Selection"), Sample.get("Selection"))
        return self.m_cfg_Settings.get_General().get("Selection")
