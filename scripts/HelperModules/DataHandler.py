import copy
import json
from pickle import load
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
from HelperModules.Directories import Directories
from HelperModules.Preprocessing import Preprocess, ScaleInputs
from HelperModules.MessageHandler import ErrorMessage

class DataHandler(object):
    def __init__(self, Settings):
        ###################################################################
        ############################ Constants ############################
        ###################################################################
        self.m_WEIGHTNAME = "Weight" # Name of weight column in df
        self.m_PENALTYNAME = "PenaltyFactor" # Name of penalty factor column in df
        self.m_LABELNAME = "Label" # Name of label column in df
        self.m_SAMPLENAME = "Sample_Name" # Name of sample name column in df
        self.m_SAMPLETYPE = "Sample_Type" # Name of sample type column in df
        self.m_PREDICTIONNAME = "Prediction" # Name of prediction column in df
        self.m_FOLDIDENTIFIER = "eventNumber" # Name of fold identifier column in df
        self.m_ISNOMINAL = "isNominal" # Name of the identifier defining the nominal tree

        ###################################################################
        ####################### Settings variables ########################
        ###################################################################
        self.m_Samples = Settings.get_Samples() # List of all samples
        self.m_NominalSamples = Settings.get_NominalSamples() # List of nominal samples
        self.m_OutputPath = Settings.get_General().get("Job") # Path to the 'JOB' directory
        self.m_nFolds = Settings.get_General().get("Folds") # Number of folds
        self.m_ModelSettings = Settings.get_Model() # Object holding general Model information
        self.m_GeneralSettings = Settings.get_General() # Object holding general information
        self.m_OutputLabels = self.m_ModelSettings.get("OutputLabels") # List of output labels
        self.m_Dirs = Directories("%s"%Settings.get_General().get("Job")) # Directory object
        self.m_SampleFrac = self.m_ModelSettings.get("SampleFrac")
        self.m_ScaleOutput = self.m_GeneralSettings.get("OutputScaling")
        self.DileptonMerge = Settings.get_General().get("DileptonMerge")
        self.m_VarNames = Settings.get_VarNames() # List of variable names
        self.m_VarObjects = Settings.get_Variables() # List of variable objects
        self.m_VarLabels = Settings.get_VarLabels() # List of variable labels

        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if self.m_GeneralSettings.get("RecalcScaler") or self.m_GeneralSettings.get("InputScaling") == "none":
            self.m_Scaler = None
        else:
            try:
                self.m_Scaler = load(open(hf.ensure_trailing_slash(self.m_Dirs.ModelDir()) + "scaler.pkl", 'rb'))
            except BaseException:
                ErrorMessage("Could not load scalar from %s make sure the object exist and consider turning 'UseExistingScalar' off to regenerate a scaler" % (hf.ensure_trailing_slash(self.m_Dirs.ModelDir()) + "scaler.pkl"))
        self.m_DataFrame = pd.read_hdf(hf.ensure_trailing_slash(self.m_Dirs.DataDir()) + "merged.h5")
        if self.m_ModelSettings.isClassification():
            if any(self.m_DataFrame[self.m_LABELNAME].values)<0:
                ErrorMessage("Detected outdated converted files. You are running a newer version of the code with old .h5 files. Please run the conversion again to fix this!")
        self.m_DataFrame = self.m_DataFrame.sample(frac=self.m_SampleFrac, random_state=1)
        self.m_DataFrame["Index_tmp"] = np.arange(len(self.m_DataFrame))
        F_tmp = self.m_DataFrame[self.m_FOLDIDENTIFIER].values  # temporarily save fold identifier to reattach them later
        X_tmp, W_tmp, self.m_Scaler = Preprocess(X=copy.deepcopy(self.m_DataFrame[self.m_VarNames].values),
                                                 Y=copy.deepcopy(self.m_DataFrame[self.m_LABELNAME].values),
                                                 W=copy.deepcopy(self.m_DataFrame[self.m_WEIGHTNAME].values),
                                                 N=copy.deepcopy(self.m_DataFrame[self.m_SAMPLENAME].values),
                                                 settings=Settings,
                                                 scaler=self.m_Scaler)
        Y_tmp = copy.deepcopy(self.m_DataFrame[self.m_LABELNAME].values)
        if self.m_ModelSettings.isRegressionDNN():
            if self.m_ScaleOutput is not None:
                
                # dim = len(self.m_OutputLabels)
                
                # for i in range(dim):
                #     output = np.array([j[i] for j in Y_tmp]).reshape(-1, 1)

                Y_tmp, _ = ScaleInputs(np.vstack((Y_tmp)), self.m_ScaleOutput, None)
                Y_tmp = Y_tmp.T[0]
            if self.m_ModelSettings.get("ReweightTargetDistribution"):
                nbins = int(self.m_ModelSettings.get("ModelBinning")[0][0])
                xlow = self.m_ModelSettings.get("ModelBinning")[0][1]
                xhigh = self.m_ModelSettings.get("ModelBinning")[0][2]
                hist, bin_edges = np.histogram(Y_tmp[~np.isnan(self.m_DataFrame[self.m_LABELNAME].values)],
                                               bins=nbins,
                                               range=(xlow, xhigh),
                                               weights=W_tmp[~np.isnan(self.m_DataFrame[self.m_LABELNAME].values)])
                SF = np.array([np.max(hist)/h if h>0 else 1 for h in hist])
                indece = [ind - 1 for ind in np.digitize(Y_tmp, bin_edges)]
                indece = [ind if ind != nbins else nbins - 1 for ind in indece]
                W_corr = SF[indece]
                W_tmp *= W_corr
                W_tmp = W_tmp / np.mean(W_tmp)
        if self.m_ISNOMINAL in self.m_DataFrame:  # for older versions this variable might not exist yet
            isNominal_tmp = copy.deepcopy(self.m_DataFrame[self.m_ISNOMINAL].values)
        else:
            isNominal_tmp = [True] * len(Y_tmp)
            self.m_DataFrame[self.m_ISNOMINAL] = isNominal_tmp
        N_tmp = copy.deepcopy(self.m_DataFrame[self.m_SAMPLENAME].values)
        self.m_DataFrame_sc = pd.DataFrame(data=X_tmp, columns=self.m_VarNames)
        self.m_DataFrame_sc[self.m_WEIGHTNAME] = W_tmp
        self.m_DataFrame_sc[self.m_LABELNAME] = Y_tmp
        self.m_DataFrame_sc[self.m_ISNOMINAL] = isNominal_tmp
        self.m_DataFrame_sc[self.m_SAMPLENAME] = N_tmp
        self.m_DataFrame_sc[self.m_FOLDIDENTIFIER] = F_tmp  # reattaching fold identifer
        self.m_DataFrame_sc["Index_tmp"] = np.arange(len(self.m_DataFrame_sc))
        self.apply_Penalty()
        self.prep_VariablesFile()

    ###################################################################
    ########################## Class Methods ##########################
    ###################################################################
    def prep_VariablesFile(self):
        '''
        Function to prepare a variables.json file for the usage with lwtnn.
        '''
        if self.m_GeneralSettings.get("InputScaling") == "minmax" or self.m_GeneralSettings.get("InputScaling") == "minmax_symmetric":
            offsets = -self.m_Scaler.data_min_
            scales = self.m_Scaler.scale_
        elif self.m_GeneralSettings.get("InputScaling") == "standard":
            offsets = -self.m_Scaler.mean_
            scales = self.m_Scaler.scale_
        else:
            offsets = [0] * len(self.m_VarNames)  # Default offset is 0
            scales = [1] * len(self.m_VarNames)  # Default scale is 1
        Variables_Dict = {"input_sequences": [],
                          "inputs": [{"name": "Input_Variables",
                                      "variables": [{"name": var_name, "offset": float(offset), "scale": float(scale)} for var_name, offset, scale in zip(self.m_VarNames, offsets, scales)]}],
                          "outputs": [{"labels": [Label for Label in self.m_OutputLabels],
                                       "name": "Predictions"}]}
        try:
            with open(hf.ensure_trailing_slash(self.m_Dirs.LwtnnDir()) + self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type") + "_Variables.json", "w") as outfile:
                json.dump(Variables_Dict, outfile)
        except Exception as e:
            ErrorMessage("Could not convert variables into json file! %e" % e)

    def apply_Penalty(self):
        Penalty_dict = {Sample.get("Name"): Sample.get("PenaltyFactor") for Sample in self.m_Samples}
        # If a sample is in a group the penalty term is 1 by default
        if any([Sample.get("Group") is not None and Sample.get("PenaltyFactor") != 1 for Sample in self.m_Samples]):
            ErrorMessage("Can not use 'Group' for sample that has 'PenaltyFactor' defined")
        Groups = [Sample.get("Group") for Sample in self.m_Samples]
        for group in Groups:
            Penalty_dict[group] = 1
        N_tmp = self.m_DataFrame_sc[self.m_SAMPLENAME].values.tolist()
        Penalty_Factor = np.array([Penalty_dict[SampleName] for SampleName in N_tmp])
        self.m_DataFrame_sc[self.m_PENALTYNAME] = Penalty_Factor
        self.m_DataFrame_sc[self.m_WEIGHTNAME] = self.m_DataFrame_sc[self.m_PENALTYNAME] * self.m_DataFrame_sc[self.m_WEIGHTNAME]

    def get_Labels(self, returnNominalOnly=True):
        return self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME]

    def get_TrainInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return training inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[self.m_VarNames]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.m_VarNames]

    def get_TestInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return testing inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[self.m_VarNames]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.m_VarNames]

    def get_TrainInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[self.m_VarNames]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_VarNames]

    def get_TestInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[self.m_VarNames]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_VarNames]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_VarNames]

    def get_TrainLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_LABELNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[self.m_LABELNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_LABELNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME]

    def get_TestLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_LABELNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[self.m_LABELNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_LABELNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME]

    def get_TrainWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[self.m_WEIGHTNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.m_WEIGHTNAME]

    def get_TestWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[self.m_WEIGHTNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame(returnNominalOnly)[self.get_DataFrame(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame(returnNominalOnly)[self.m_LABELNAME]

    def get_TrainWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds != Fold]
            return tmp[self.m_WEIGHTNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_WEIGHTNAME]

    def get_TestWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if not returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[~tmp[self.m_LABELNAME].isnull()][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds != 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_FOLDIDENTIFIER] % self.m_nFolds == Fold]
            return tmp[self.m_WEIGHTNAME]
        if not returnNonZeroLabels and self.m_nFolds == 1:
            return self.get_DataFrame_sc(returnNominalOnly)[self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME] >= 0][self.m_WEIGHTNAME]
        if returnNonZeroLabels and self.m_nFolds == 1:
            tmp = self.get_DataFrame_sc(returnNominalOnly)[self.m_LABELNAME]

    def get_Weights(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.m_WEIGHTNAME]

    def get_Weights_sc(self, returnNominalOnly=True):
        return self.get_DataFrame_sc(returnNominalOnly)[self.m_WEIGHTNAME]

    def get_FoldIdentifier(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.m_FOLDIDENTIFIER]

    def get_Sample_Names(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.m_SAMPLENAME]

    def get_Sample_Types(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.m_SAMPLETYPE]

    def get_DataFrame(self, returnNominalOnly=True):
        if returnNominalOnly:
            return self.m_DataFrame[self.m_DataFrame[self.m_ISNOMINAL] == returnNominalOnly]
        return self.m_DataFrame

    def get_DataFrame_sc(self, returnNominalOnly=True):
        if returnNominalOnly:
            return self.m_DataFrame_sc[self.m_DataFrame_sc[self.m_ISNOMINAL] == returnNominalOnly]
        return self.m_DataFrame_sc

    def get(self, variable, returnNominalOnly=True):
        return self.m_DataFrame[self.m_DataFrame[self.m_ISNOMINAL] == returnNominalOnly][variable]

    def get_sc(self, variable, returnNominalOnly=True):
        return self.m_DataFrame_sc[self.m_DataFrame_sc[self.m_ISNOMINAL] == returnNominalOnly][variable]
