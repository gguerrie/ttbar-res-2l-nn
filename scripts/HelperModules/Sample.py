"""
Module for Sample objects
"""
from HelperModules.OptionHandler import OptionHandler

class Sample:
    """
    Class to describe a MC,data or systematic sample.
    """
    def __init__(self, group):
        """
        Init method of sample class.
        """
        self.m_sampleoptions = OptionHandler()
        self.m_sampleoptions.set_description("SAMPLE")
        self.m_sampleoptions.register_option("Name", optiondefault = None)
        self.m_sampleoptions.register_option("Type", optiondefault = None)
        self.m_sampleoptions.register_option("NtupleFiles", optiondefault = None, optionislist = True)
        self.m_sampleoptions.register_option("Treename", optiondefault = None)
        self.m_sampleoptions.register_option("Selection", optiondefault = "")
        self.m_sampleoptions.register_option("MCWeight", optiondefault = "")
        self.m_sampleoptions.register_option("TrainLabel", optiondefault = -1)
        self.m_sampleoptions.register_option("PenaltyFactor", optiondefault = 1)
        self.m_sampleoptions.register_option("FillColor", optiondefault = None)
        self.m_sampleoptions.register_option("Group", optiondefault = None)
        self.m_sampleoptions.register_option("ScaleToBkg", optiondefault = False)

        self.m_sampleoptions.register_check(optionname = "Name", check = "set")
        self.m_sampleoptions.register_check(optionname = "Type", check = "set")
        self.m_sampleoptions.register_check(optionname = "Type", check = "inlist", lst = ["Background","Signal","Signal_Scaled","Fake","Systematic", "Data"])
        self.m_sampleoptions.register_check(optionname = "TrainLabel", check = "set", except_case = "Data")
        self.m_sampleoptions.register_check(optionname = "FillColor", check = "set", except_case = "Data")
        self.m_sampleoptions.register_check(optionname = "NtupleFiles", check = "set")
        self.m_sampleoptions.register_check(optionname = "PenaltyFactor", check = "larger", thr = 0)

        for item in group:
            self.m_sampleoptions.set_option(item)
        self.m_sampleoptions.check_options()

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################

    def get(self,name):
        """
        Getter method to return sample option.

        Keyword arguments:
        name --- name of the option to be returned

        Returns value of given option
        """
        return self.m_sampleoptions.get(name)

    def Print(self,fn=print):
        self.m_sampleoptions.Print(fn)
