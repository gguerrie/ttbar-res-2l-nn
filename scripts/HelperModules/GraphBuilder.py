import numpy as np
import torch
from torch_geometric.data import Data, HeteroData
from HelperModules.MessageHandler import ErrorMessage

class GraphBuilder(object):
    '''
    Class to build graphs.

    Keyword arguments:
    gnodes (GraphNodes object): GraphNodes object which is read from the config file and defines the structure of the graph.
    '''
    def __init__(self, gnodes, variables):
        self.m_gnodes = gnodes
        self.m_edgetypes = [] # for heterogeneous models
        self.m_map = {}

        # checking whether all variables defined as edges or features are present as VARIABLE blocks
        vnames = [v.get("Name") for v in variables]
        for n in self.m_gnodes:
            if not set(n.get("Features")).issubset(set(vnames)):
                missmatch = list(set(n.get("Features"))-set(vnames))
                ErrorMessage(f"Some VARIABLE blocks seem to be missing for features of GRAPHNODE {n.get('Name')}: {missmatch}.")
            for edgefeatures in n.get("EdgeFeatures"):
                if not set(edgefeatures).issubset(set(vnames+["None"])):
                    missmatch = list(set(edgefeatures)-set(vnames+["None"]))
                    ErrorMessage(f"Some VARIABLE blocks seem to be missing for edge features of GRAPHNODE {n.get('Name')}: {missmatch}.")
        # checking whether all targets are defined as GRAPHNODE blocks
        for n in self.m_gnodes:
            if not set(n.get("Targets")).issubset(set([n.get("Name") for n in self.m_gnodes])):
                missmatch = list(set(n.get("Targets"))-set([n.get("Name") for n in self.m_gnodes]))
                ErrorMessage(f"Some VARIABLE blocks seem to be missing for GRAPHNODE {n.get('Name')} targets.")

    def get_nglobal(self):
        for node in self.m_gnodes:
            if node.isglobal():
                return len(node.get("Features"))
        return None

    def get_ntypes(self):
        return len(np.unique([node.get("Type") for node in self.m_gnodes if not node.isglobal()]))

    def get_edgetypes(self):
        return self.m_edgetypes

    def _prune_nodes(self, dfrow):
        '''
        Method to prune nodes in a graph.
        This is done by checking if any value in the node equals the 'PruneIfValue' for that feature.
        The node is then deactivated which means that it is not taken into account in the graph building.

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.
        '''
        for node in self.m_gnodes:
            if node.get("PruneIfValue") is not None:
                if any([dfrow[f]==prune_val for f, prune_val in zip(node.get("Features"),node.get("PruneIfValue"))]):
                    node.deactivate()
        return

    def _build_global(self, dfrow, data):
        nglobal = 0
        for node in self.m_gnodes:
            if node.isglobal() and node.isactive():
                nglobal += 1
                data.u = torch.from_numpy(np.array([np.array([dfrow[f] for f in node.get("Features")])]))
        if nglobal > 1:
            ErrorMessage("Found more than one GRAPHNODE block with type = Global!")
        return

    def _hasglobal(self):
        return any([node.isglobal() for node in self.m_gnodes])

    ###########################################################################
    ########################### homogeneous graphs ############################
    ###########################################################################
    def _build_HomGNNnodes(self, dfrow):
        '''
        Method to build the node feature matrix with shape [num_nodes, num_node_features].

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.

        Returns:
        nodedata (PyTorch tensor): The feature matrices for all node types.
        '''
        nodedata = []
        for node in self.m_gnodes:
            if not node.isglobal(): # node not global
                nodedata.append(np.array([dfrow[f] for f in node.get("Features")]))
                self.m_map[node.get("Name")] = {"Idx": len(nodedata)-1,
                                                "Node": node}
        return torch.from_numpy(np.array(nodedata))

    def _build_HomGNNedges(self, dfrow):
        '''
        Method to build:
        - the graph connectivity matrix (edge_index) for a homogeneous graph in COO format with shape [2, num_edges]
        - the edge feature matrix (edge_attr) for a homogeneous graph with shape [num_edges, num_edge_features]

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.

        Returns:
        edge_index (PyTorch tensor): The graph connectivity matrix.
        edge_attr (PyTorch tensor): The edge feature matrix.
        '''
        edge_index = [np.array([]),np.array([])]
        edge_attr = []
        for node in self.m_gnodes:
            if not node.isglobal() and node.isactive(): # source node not global and active
                source = node.get("Name")
                for target_ID, target in enumerate(node.get("Targets")):
                    if not self.m_map[target]["Node"].isglobal() and self.m_map[target]["Node"].isactive(): # same for target
                        edge_index[0] = np.append(edge_index[0],self.m_map[source]["Idx"])
                        edge_index[1] = np.append(edge_index[1],self.m_map[target]["Idx"])
                        edge_attr.append(np.array([dfrow[feature] for feature in node.get("EdgeFeatures")[target_ID] if feature!="None"]))
        return torch.from_numpy(np.array(edge_index)).contiguous().long(), torch.from_numpy(np.array(edge_attr))

    def build_HomGraph(self, dfrow, y=np.NaN, w=1.0):
        '''
        Method to build a homogeneous graph. The method used the private build methods for the nodes, edges and edge features.

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.
        y (float): Target label of the graph. Default is None.
        w (float): Sample weight of the graph used in training to weight the loss. Default is 1.0.

        Returns:
        data (PyTorch Data object): A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        '''
        self._prune_nodes(dfrow)
        data = Data()
        data.x = self._build_HomGNNnodes(dfrow)
        data.num_nodes = len(data.x)
        data.edge_index, data.edge_attr = self._build_HomGNNedges(dfrow)
        data.y, data.w = y, w
        if self._hasglobal():
            self._build_global(dfrow, data)
        return data

    ###########################################################################
    ########################### heterogenous graphs ###########################
    ###########################################################################
    def _build_HetGNNnodes(self, dfrow, data):
        '''
        Method to build the node feature matrix dictionary for a heterogeneous graph with shape {num_nodeTypes: [num_nodes, num_node_features]}.

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.
        data (PyTorch Data object): A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        '''
        for node in self.m_gnodes:
            if not node.isglobal(): # node not global
                nodeType = node.get("Type")
                if not nodeType in data.metadata()[0]:
                    data[nodeType].x = []
                data[nodeType].x.append([dfrow[f] for f in node.get("Features")])
                self.m_map[node.get("Name")] = {"Type": nodeType,
                                                "Idx": len(data[nodeType].x)-1,
                                                "Node": node}
        nodetypes, _ =  data.metadata()
        for nt in nodetypes:
            data[nt].x = torch.FloatTensor(data[nt].x)
        return

    def _build_HetGNNedges(self, dfrow, data):
        '''
        Method to build:
        - the graph connectivity matrix (edge_index) dictionary for a heterogeneous graph in COO format with shape {num_connection_types: [2, num_edges]}
        - the edge feature matrix (edge_attr) dictionary for a heterogeneous graph with shape {num_connection_types: [num_edges, num_edge_features]}

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.
        data (PyTorch Data object): A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        '''
        for node in self.m_gnodes:
            if not node.isglobal() and node.isactive(): # source not is not global and active
                source = node.get("Name")
                sourceType = node.get("Type")
                for target_ID, target in enumerate(node.get("Targets")):
                    if not self.m_map[target]["Node"].isglobal() and self.m_map[target]["Node"].isactive(): # same for target
                        targetType = self.m_map[target]["Type"]
                        key = (sourceType, sourceType+"_to_"+targetType, targetType)
                        if key not in self.m_edgetypes:
                            self.m_edgetypes.append(key)
                        if key not in data.metadata()[1]:
                            data[key].edge_index = [[self.m_map[source]["Idx"]], [self.m_map[target]["Idx"]]]
                            data[key].edge_attr = [[dfrow[feature] for feature in node.get("EdgeFeatures")[target_ID] if feature!="None"]]
                        else:
                            data[key].edge_index[0].append(self.m_map[source]["Idx"])
                            data[key].edge_index[1].append(self.m_map[target]["Idx"])
                            data[key].edge_attr.append([dfrow[feature] for feature in node.get("EdgeFeatures")[target_ID] if feature!="None"])
        for edgeType in data.metadata()[1]:
            data[edgeType].edge_index = torch.tensor(data[edgeType].edge_index, dtype=torch.long)
            data[edgeType].edge_attr = torch.tensor(data[edgeType].edge_attr,  dtype=torch.float)
        return

    def _build_Homglobal(self, dfrow):
        for node in self.m_gnodes:
            if node.isglobal() and node.isactive(): # node is global and active
                return torch.from_numpy(np.array([np.array([dfrow[f] for f in node.get("Features")])]))

    def build_HetGraph(self, dfrow, y=np.NaN, w=1.0):
        '''
        Method to build a heterogeneous graph. The method used the private build methods for the nodes, edges and edge features.

        Keyword arguments:
        dfrow (Pandas DataFrame row): Pandas DataFrame row containing all node variables.
        y (float): Target label of the graph. Default is None.
        w (float): Sample weight of the graph used in training to weight the loss. Default is 1.0.

        Returns:
        data (PyTorch Data object): A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        '''
        self._prune_nodes(dfrow)
        data = HeteroData()
        self._build_HetGNNnodes(dfrow, data)
        self._build_HetGNNedges(dfrow, data)
        data.y, data.w = y, w
        if self._hasglobal():
            self._build_global(dfrow, data)
        return data
