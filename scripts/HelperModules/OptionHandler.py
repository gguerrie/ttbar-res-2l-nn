"""
Module to handle the definition and reading of given options in the config file.
"""
import re
import difflib
from HelperModules.MessageHandler import ErrorMessage

def boolify(s):
    '''
    Function to convert string to boolean.
    Keyword arguments:
    s --- sring to be converted to boolean

    Returns False or True
    '''
    if s == 'True':
        return True
    if s == 'true':
        return True
    if s == 'False':
        return False
    if s == 'false':
        return False
    raise ValueError

def autoconvert(s):
    '''
    Function to automatically convert strings into
    the necessary types.
    Keyword arguments:
    s --- sring to be converted

    Returns int(string), float(string), etc.
    '''
    for fn in (boolify, int, float):
        try:
            return fn(s)
        except ValueError:
            pass
    return s

def prepstring(string):
    '''
    Function to prepare a string. I.e. searching for
    string indicators within a string from a config line.
    Keyword arguments:
    string --- passed string

    Returns string within indecators or string itself if no
    indicator is found.
    '''
    if not "\"" in string and not "'" in string:
        string = string.replace(" ", "")
        return string.replace('\\', '#')
    s_1 = [_.start() for _ in re.finditer("\"", string)] # strings with "
    s_2 = [_.start() for _ in re.finditer("'", string)] # strings with '
    if len(s_1) == 2:
        return string[s_1[0]+1:s_1[1]].replace('\\', '#')
    if len(s_2) == 2:
        return string[s_2[0]+1:s_2[1]].replace('\\', '#')

class option(object):
    '''
    Class defining an option.
    '''
    def __init__(self, optionname, optiondefault, optionislist=False, optionisnestedlist=False, optionisdeprecated=False, deprecationstring=""):
        '''
        Init function for option class
        '''
        self.m_name = optionname
        self.m_islist = optionislist
        self.m_isnestedlist = optionisnestedlist
        self.m_value = optiondefault
        self.m_isset = False
        self.m_check_fns = []
        self.m_check_kwargs = []
        self.m_optionisdeprecated = optionisdeprecated
        self.m_deprecationstring = deprecationstring

    def add_check_fn(self, fn):
        '''
        Simple helper function to append fn to
        list of functions to be checked.
        Keyword arguments:
        fn --- function object
        '''
        self.m_check_fns.append(fn)

    def add_check_kwargs(self, **kwargs):
        '''
        Simple helper function to append kwargs to
        list of kwargs to be checked.
        Keyword arguments:
        kwargs --- kwargs
        '''
        self.m_check_kwargs.append(kwargs)

    def perform_checks(self):
        '''
        Combine fn with corresponding kwargs for checks
        '''
        for check_fn, check_kwargs in zip(self.m_check_fns, self.m_check_kwargs):
            check_fn(**check_kwargs)

    def _clean_string(self, string):
        '''
        Method to clean the string of tabs and other things
        that are not being used. If necessary splits string
        into list or nested list.
        Keyword arguments:
        string --- passed string

        Returns cleaned string or list/nested list of string.
        '''
        string = " ".join(string.split()) # Removing whitespaces
        if self.m_islist:
            if string.count('"')>=2:
                return [prepstring(s) for s in string.split('"')[1::2]]
            elif string.count("'")>=2:
                return [prepstring(s) for s in string.split("'")[1::2]]
            else:
                return [prepstring(s) for s in string.split(",")]
        if self.m_isnestedlist:
            if string.count('"')>=2:
                return [[prepstring(a) for a in s.split('"')[1::2]] for s in string.split("|")]
            elif string.count("'")>=2:
                return [[prepstring(a) for a in s.split("'")[1::2]] for s in string.split("|")]
            else:
                return [[prepstring(a) for a in s.split(",")] for s in string.split("|")]
        return prepstring(string)

    def _convert_string(self, string):
        '''
        Method to convert each element into correct type.
        Keyword arguments:
        string --- passed string

        Returns list/nested list of object or object itself
        of correct type.
        '''
        string = self._clean_string(string)
        if self.m_islist:
            return [autoconvert(s) for s in string]
        if self.m_isnestedlist:
            return [[autoconvert(sub_sub_s) for sub_sub_s in sub_s] for sub_s in string]
        return autoconvert(string)

    def set_value(self, string, msg=""):
        '''
        Setter method for value attribute.
        Keyword arguments:
        string --- passed string
        msg --- error message (for exceptions)
        '''
        if msg == "":
            msg = "Something went wrong with option %s!"%self.m_name
        try:
            self.m_value = self._convert_string(string)
            self.m_isset = True
        except TypeError as e:
            ErrorMessage(msg + e)

    def get_value(self):
        '''
        Getter method for value attribute.

        Returns value attribute.
        '''
        return self.m_value

    def get_check_fns(self):
        return self.m_check_fns

    def is_set(self):
        '''
        Getter method for isset attribute.

        Returns isset attribute.
        '''
        return self.m_isset

    def __str__(self):
        '''
        Method to represent the class object as a string.
        '''
        if isinstance(self.m_value, list):
            return "Option: {:<25s}\t Value: {:<25s}".format(self.m_name, ",".join([str(val) for val in self.m_value]))
        return "Option: {:<25s}\t Value: {:<25s}".format(self.m_name, str(self.m_value))

class OptionHandler(object):
    '''
    Class defining the definition and interaction
    of several options.
    '''
    def __init__(self, *args):
        '''
        Init method of OptionHandler class
        '''
        if len(args) == 0:
            self.m_options = {}
            self.m_description = "NONEGIVEN"
        if len(args) == 1:
            self.m_options = args[0]._get_options()
            self.m_description = "NONEGIVEN"

    def get(self, name):
        return self.m_options[name].get_value()

    def set_description(self, description):
        self.m_description = description

    def _get_options(self):
        '''
        Getter method for options atribut.
        Returns options dictionary.
        '''
        return self.m_options

    def register_option(self, optionname, optiondefault, optionislist=False, optionisnestedlist=False, optionisdeprecated=False, deprecationstring=""):
        '''
        Method to define an option.
        Keyword arguments:
        optionname --- name of the option
        optionislist --- boolean, defining whether the option is a list
        optionisnestedlist --- boolean, defining whether the option is a nested list
        '''
        self.m_options[optionname] = option(optionname=optionname,
                                            optiondefault=optiondefault,
                                            optionislist=optionislist,
                                            optionisnestedlist=optionisnestedlist,
                                            optionisdeprecated=optionisdeprecated,
                                            deprecationstring=deprecationstring)

    def register_check(self, **kwargs):
        check = kwargs["check"]
        optionname = kwargs["optionname"]
        optionname = optionname[0] if isinstance(optionname, list) else optionname
        del kwargs["check"]
        if check == "inlist":
            self.m_options[optionname].add_check_fn(self._is_inlist)
        if check == "is_subset":
            self.m_options[optionname].add_check_fn(self._is_subset)
        elif check == "equal_len":
            self.m_options[optionname].add_check_fn(self._is_equal_len)
        elif check == "larger":
            self.m_options[optionname].add_check_fn(self._is_larger)
        elif check == "smaller":
            self.m_options[optionname].add_check_fn(self._is_smaller)
        elif check == "set":
            self.m_options[optionname].add_check_fn(self._is_set)
        self.m_options[optionname].add_check_kwargs(**kwargs)

    def check_options(self):
        for o in self.m_options.values():
            o.perform_checks()

    def _check_validity(self, option_string):
        '''
        Method to check whether a given option is valid.
        Keyword arguments:
        option_string --- passed option string

        Returns name of the option and the rest of the option
        string if the option string contains a valid key, i.e.
        if it was previously registered as an option.
        '''
        name, string = option_string.split("=", 1)
        name = name.replace(" ", "")
        if not name in self.m_options.keys():
            ErrorMessage("'%s' is not an allowed %s option! Did you mean: %s?"%(name, self.m_description, ", ".join(difflib.get_close_matches(name, self.m_options.keys()))
))
        return name, string

    def set_option(self, option_string):
        '''
        Method to set the value of a given option
        Keyword arguments:
        option_string --- option string from which the value is deduced
        '''
        if "=" in option_string:
            name, string = self._check_validity(option_string)
            if self.m_options[name].m_optionisdeprecated:
                ErrorMessage("Option %s is deprecated. %s"%(name, self.m_options[name].m_deprecationstring))
            self.m_options[name].set_value(string)

    def get_option(self, optionname):
        '''
        Getter method for an option.
        Keyword argumetns:
        optionname --- name of the option

        Returns the value corresponding to a given name.
        '''
        return self.m_options[optionname].get_value()

    def _is_inlist(self, optionname, lst, case=None, except_case=None):
        '''
        Checks whether a given options value is one
        from a given list.
        Keyword argumetns:
        optionname --- name of the option
        lst --- list of possible values
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value()!=case:
            return
        if not self.m_options[optionname].get_value() in lst:
            ErrorMessage("%s has invalid option set for %s! Allowed options are: %s"%(self.m_description, optionname, ", ".join(lst)))

    def _is_subset(self, optionname, lst, case=None, except_case=None):
        '''
        Checks whether a given options value is a
        subset from a given set.
        Keyword argumetns:
        optionname --- name of the option
        lst --- list of possible values -> converted to set
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value()!=case:
            return
        if not set(self.m_options[optionname].get_value()).issubset(set(lst)):
            ErrorMessage("%s has invalid option set for %s! Must be subset of %s."%(self.m_description, optionname, lst))

    def _is_equal_len(self, optionname, l, case=None, except_case=None):
        '''
        Checks whether a given options value is larger
        than a given length (l).
        Keyword argumetns:
        optionname --- name of the option
        l --- length value (can also be another options name)
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value()!=case:
            return
        if isinstance(l, str):
            self._is_set(l)
            if isinstance(self.m_options[l].get_value(), list):
                if len(self.m_options[optionname].get_value()) != len(self.m_options[l].get_value()):
                    ErrorMessage("Missmatching length between %s(%d) and %s(%d)!"%(optionname,
                                                                                   len(self.m_options[optionname].get_value()),
                                                                                   l,
                                                                                   len(self.m_options[l].get_value())))
            else:
                if len(self.m_options[optionname].get_value()) != self.m_options[l].get_value():
                    ErrorMessage("Missmatching length of %s(%d) and %s(%d)!"%(optionname,
                                                                              len(self.m_options[optionname].get_value()),
                                                                              l,
                                                                              self.m_options[l].get_value()))
        elif len(self.m_options[optionname].get_value()) != l:
            ErrorMessage("Missmatching length of %s(%d)! Should be %d!"%(optionname,
                                                                         len(self.m_options[optionname].get_value()),
                                                                         l))

    def _is_larger(self, optionname, thr, case=None, except_case=None):
        '''
        Checks whether a given options value is larger
        than a given threshold (thr).
        Keyword argumetns:
        optionname --- name of the option
        thr --- threshold value (can also be another options name)
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value()!=case:
            return
        if self.m_options[optionname].is_set():
            if isinstance(thr, str):
                if not self.m_options[optionname].get_value() > self.m_options[thr].get_value():
                    ErrorMessage("Expected %s to be larger than %f!"%(optionname, self.m_options[optionname].get_value()))
            elif not self.m_options[optionname].get_value() > thr:
                ErrorMessage("Expected %s to be larger than %f!"%(optionname, thr))

    def _is_smaller(self, optionname, thr, case=None, except_case=None):
        '''
        Checks whether a given options value is smaller
        than a given threshold (thr).
        Keyword argumetns:
        optionname --- name of the option
        thr --- threshold value (can also be another options optionname)
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value()!=case:
            return
        if self.m_options[optionname].is_set():
            if isinstance(thr, str):
                if not self.m_options[optionname].get_value() < self.m_options[thr].get_value():
                    ErrorMessage("Expected %s to be larger than %f!"%(optionname, self.m_options[optionname].get_value()))
            elif not self.m_options[optionname].get_value() < thr and self.m_options[optionname].is_set():
                ErrorMessage("Expected %s to be larger than %f!"%(optionname, thr))

    def _is_set(self, optionname, case=None, except_case=None):
        '''
        Checks whether a given option is set.
        Keyword argumetns:
        optionname --- name of the option
        case --- string identifier to veto specific cases
        exceptcase --- string identifier to not run this on specific cases
        '''
        if except_case is not None and self.m_options["Type"].get_value() == except_case:
            return
        if case is not None and self.m_options["Type"].get_value() != case:
            return
        if isinstance(optionname, list):
            if not any([self.m_options[name].is_set() for name in optionname]):
                ErrorMessage("%s has needs at least one of %s set!"%(self.m_description, optionname))
        else:
            if not self.m_options[optionname].is_set():
                ErrorMessage("%s has no %s set!"%(self.m_description, optionname))

    def Print(self, fn):
        '''
        Method to print the class object.
        '''
        fn("%s - Options:"%self.m_description)
        for o in self.m_options.values():
            fn("%s - %s"%(self.m_description, o))
