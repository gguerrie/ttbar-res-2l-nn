"""
Module to define a variable class to handle variables
"""

from array import array
import numpy as np
import re
from HelperModules.OptionHandler import OptionHandler

class Variable(object):
    """
    Class to describe an input variable.
    """
    def __init__(self, group=None, name=None, label=None, binning=None, custombinning=None):
        """
        Init method of variable class.
        """
        self.m_VarBinning = None

        self.m_variableoptions = OptionHandler()
        self.m_variableoptions.set_description("VARIABLE")
        self.m_variableoptions.register_option("Name", optiondefault=name)
        self.m_variableoptions.register_option("Label", optiondefault=label)
        self.m_variableoptions.register_option("Binning", optiondefault=binning, optionislist=True)
        self.m_variableoptions.register_option("CustomBinning", optiondefault=custombinning, optionislist=True)
        self.m_variableoptions.register_option("Ymin", optiondefault=0.0)
        self.m_variableoptions.register_option("Ymax", optiondefault=None)
        self.m_variableoptions.register_option("Scale", optiondefault="Linear")

        self.m_variableoptions.register_check(optionname="Name", check="set")
        self.m_variableoptions.register_check(optionname="Label", check="set")
        self.m_variableoptions.register_check(optionname=["Binning", "CustomBinning"], check="set")
        self.m_variableoptions.register_check(optionname="Scale", check="is_inlist", lst=["Linear", "Log"])

        if group is not None: # this is the case where we read from config file
            for item in group:
                self.m_variableoptions.set_option(item)
        else: # otherwise we set the options manually
            self.m_variableoptions.set_option("Name = %s"%name)
            self.m_variableoptions.set_option("Label = %s"%label)
            if binning is not None:
                self.m_variableoptions.set_option("Binning = %s"%(",".join([str(b) for b in binning])))
            if custombinning is not None:
                self.m_variableoptions.set_option("CustomBinning = %s"%(",".join([str(b) for b in custombinning])))
        self.m_variableoptions.check_options()

        # Get actual binning in array like format
        if self.get("CustomBinning") is not None:
            self.m_VarBinning = array("d", self.get("CustomBinning"))
        else:
            self.m_VarBinning = array("d", np.linspace(start=self.get("Binning")[1],
                                                         stop=self.get("Binning")[2],
                                                         num=int(self.get("Binning")[0]+1)))

    def get(self, name):
        """
        Getter method to return options.
        Keyword arguments:
        name --- name of the option to be returned
        """
        return self.m_variableoptions.get(name)

    def get_VarBinning(self):
        return self.m_VarBinning

    def __str__(self):
        """
        Method to represent a variable object as a string
        """
        return "Variable(%s)"%(self.get("Name"))


class Variable_WithMergedLeptons(object):
    """
    Class to describe an input variable.
    """
    def __init__(self, group=None, name=None, label=None, binning=None, custombinning=None):
        """
        Init method of variable class.
        """

        self.m_VarBinning = None

        self.m_variableoptions = OptionHandler()
        self.m_variableoptions.set_description("VARIABLE")
        self.m_variableoptions.register_option("Name", optiondefault=name)
        self.m_variableoptions.register_option("Label", optiondefault=label)
        self.m_variableoptions.register_option("Binning", optiondefault=binning, optionislist=True)
        self.m_variableoptions.register_option("CustomBinning", optiondefault=custombinning, optionislist=True)
        self.m_variableoptions.register_option("Ymin", optiondefault=0.0)
        self.m_variableoptions.register_option("Ymax", optiondefault=None)
        self.m_variableoptions.register_option("Scale", optiondefault="Linear")

        self.m_variableoptions.register_check(optionname="Name", check="set")
        self.m_variableoptions.register_check(optionname="Label", check="set")
        self.m_variableoptions.register_check(optionname=["Binning", "CustomBinning"], check="set")
        self.m_variableoptions.register_check(optionname="Scale", check="is_inlist", lst=["Linear", "Log"])
        
        if group is not None: # this is the case where we read from config file
            for item in group:
                self.m_variableoptions.set_option(item)
        else: # otherwise we set the options manually
            self.m_variableoptions.set_option("Name = %s"%name)
            self.m_variableoptions.set_option("Label = %s"%label)
            if binning is not None:
                self.m_variableoptions.set_option("Binning = %s"%(",".join([str(b) for b in binning])))
            if custombinning is not None:
                self.m_variableoptions.set_option("CustomBinning = %s"%(",".join([str(b) for b in custombinning])))
        self.m_variableoptions.check_options()

        nome = self.get("Name")
        etichetta = self.get("Label")

        # Get actual binning in array like format
        if self.get("CustomBinning") is not None:
            self.m_VarBinning = array("d", self.get("CustomBinning"))
        else:
            self.m_VarBinning = array("d", np.linspace(start=self.get("Binning")[1],
                                                         stop=self.get("Binning")[2],
                                                         num=int(self.get("Binning")[0]+1)))
        
        if re.search("el(.*)0\]", nome):
            temp1 = re.sub("el", "lep1", nome)
            temp2 = re.sub("\[\:\,0\]", "", temp1)
            self.m_variableoptions.set_option("Name = %s"%temp2)
            temp3 = re.sub("Electron", "Lepton 1", etichetta)
            self.m_variableoptions.set_option("Label = %s"%temp3)
        elif re.search("mu(.*)1\]", nome):
            temp1 = re.sub("mu", "lep2", nome)
            temp2 = re.sub("\[\:\,1\]", "", temp1)
            self.m_variableoptions.set_option("Name = %s"%temp2)
            temp3 = re.sub("Muon", "Lepton 2", etichetta)
            self.m_variableoptions.set_option("Label = %s"%temp3)

            
        

    def get(self, name):
        """
        Getter method to return options.
        Keyword arguments:
        name --- name of the option to be returned
        """
        return self.m_variableoptions.get(name)

    def get_VarBinning(self):
        return self.m_VarBinning

    def __str__(self):
        """
        Method to represent a variable object as a string
        """
        if re.search("el(.*)1\]", self.get("Name")) or re.search("mu(.*)0\]", self.get("Name")):
            return "None"
        else:
            return "Variable(%s)"%(self.get("Name"))
