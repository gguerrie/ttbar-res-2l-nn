from HelperModules.MessageHandler import WarningMessage

class Metrics(object):
    """This class defines labels for Metrics.
    """
    def __init__(self):
        self.m_Label = {
            "loss"                : "Loss",
            "accuracy"            : "Accuracy",
            "critic_real_loss"    : "Critic Real Loss",
            "critic_fake_loss"    : "Critic Fake Loss",
            "generator_loss"      : "Generator Loss",
            "mean_absolute_error" : "MAE",
            "mean_squared_error"  : "MSE"
            }

    def get_Label(self, metric):
        if metric in self.m_Label:
            return self.m_Label[metric]
        WarningMessage("Metric '{:s}' does not have a ".format(str(metric))
                       + "dedicated label! Please consider adding one to "
                       + "'scripts.HelperModules.Metrics'!")
        return str(metric)
