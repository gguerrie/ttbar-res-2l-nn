#!/usr/bin/env python3
"""
Main steering script for conversion
"""
import argparse
import re
import pandas as pd
import HelperModules as hf
from HelperModules.ConversionHandler import ConversionHandler
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.Processor import Processor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=False)
    parser.add_argument("-n", "--namefile", help="Single filename for parallel conversion", required=False)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    parser.add_argument("--plots", nargs="+", help="the dedicated plots to be produced, default is 'all'", default=["all"], choices=["all", "TrainStats", "DataMC", "nonNegativeWeights", "NegativeWeightSummary","TrainTest", "CorrelationMatrix", "CorrelationComparison", "Separation2D"])
    parser.add_argument("--plots-only", help="flag to set in case you only want to redo the plots",  action='store_true', dest="plots_only", default=False)
    args = parser.parse_args()

    DataProcessing = Processor(args)

    X = pd.read_hdf(args.namefile)
    # Make the distribution uniform
    MirroredData = DataProcessing.Mirror(X)

    filepath = args.namefile.removesuffix('.h5')
    MirroredData.to_hdf(filepath + "_mirrored.h5", key="df", mode="w")
    ConverterDoneMessage("Uniform hdf5 file (data) written to " + filepath + "_mirrored.h5")

    # Sklearn normalizing the distribution
    #DataProcessing.Scaler(UniformData)


