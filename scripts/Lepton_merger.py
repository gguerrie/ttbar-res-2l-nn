#!/usr/bin/env python3
"""
Main steering script for conversion
"""
import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.ConversionHandler import ConversionHandler
from HelperModules import Settings

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output directory", required=True)
    parser.add_argument("-n", "--namefile", help="Filename", required=True)

    args = parser.parse_args()

if os.path.exists(args.outdir):
    ConverterMessage("Writing files at: " + args.outdir)
else:
    ConverterMessage("Output folder not found, creating it at: " + args.outdir)
    os.makedirs(args.outdir)
    os.makedirs(args.outdir + "/Data")
    os.makedirs(args.outdir + "/Model")
    os.makedirs(args.outdir + "/Plots")

X = pd.read_hdf(args.namefile)

# Lepton 1 pt, eta, phi and E (taking el0 and mu1)
X['lep1_pt'] = X['el_pt[:,0]'].fillna(X['mu_pt[:,1]'])
X['lep1_eta'] = X['el_eta[:,0]'].fillna(X['mu_eta[:,1]'])
X['lep1_phi'] = X['el_phi[:,0]'].fillna(X['mu_phi[:,1]'])
X['lep1_e'] = X['el_e[:,0]'].fillna(X['mu_e[:,1]'])

# Lepton 1 pt, eta, phi and E (taking el1 and mu0)
X['lep2_pt'] = X['el_pt[:,1]'].fillna(X['mu_pt[:,0]'])
X['lep2_eta'] = X['el_eta[:,1]'].fillna(X['mu_eta[:,0]'])
X['lep2_phi'] = X['el_phi[:,1]'].fillna(X['mu_phi[:,0]'])
X['lep2_e'] = X['el_e[:,1]'].fillna(X['mu_e[:,0]'])

X = X.drop(columns=['el_pt[:,0]', 'el_eta[:,0]', 'el_phi[:,0]', 'el_e[:,0]', 'el_pt[:,1]', 'el_eta[:,1]', 'el_phi[:,1]', 'el_e[:,1]', 'mu_pt[:,1]', 'mu_eta[:,1]', 'mu_phi[:,1]', 'mu_e[:,1]', 'mu_pt[:,0]', 'mu_eta[:,0]', 'mu_phi[:,0]', 'mu_e[:,0]'])

new_columns = ["Label","lep1_pt","lep1_eta","lep1_phi","lep1_e","lep2_pt","lep2_eta","lep2_phi","lep2_e"]
temp = X.drop(columns=["Label","lep1_pt","lep1_eta","lep1_phi","lep1_e","lep2_pt","lep2_eta","lep2_phi","lep2_e"]).columns.values.tolist()
print(temp)
new_columns += temp
X = X.reindex(columns = new_columns)

# if Settings.get_General().get("FillNaN") == 'mean':
#     X = X.fillna(X.mean())
# elif Settings.get_General().get("FillNaN") == 'zeros':
X = X.fillna(0)
# elif Settings.get_General().get("FillNaN") == 'median':
#     X = X.fillna(X.median())

X.to_hdf( args.outdir + "/merged.h5", key="df", mode="w")
ConverterDoneMessage("Uniform hdf5 file written to " + args.outdir + "/merged.h5")