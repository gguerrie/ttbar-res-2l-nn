#!/usr/bin/env python3
"""
Main steering script for evaluation
"""
from shutil import copyfile
import argparse
import os
import ROOT
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import WelcomeMessage
from HelperModules import Settings
from HelperModules.Directories import Directories
from PlotterModules.RegressionPlotHandler import RegressionPlotHandler

if __name__ == "__main__":
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    args = parser.parse_args()
    WelcomeMessage("Evaluate")

    cfg_settings   = Settings.Settings(args.configfile, option="all") # The settings object
    output_path    = cfg_settings.get_General().get("Job")            # The path to the JOB directory
    Dirs           = Directories(output_path)                         # Directory object for easy storage management

    # Copy the config file to the job directory
    copyfile(args.configfile, hf.ensure_trailing_slash(Dirs.ConfigDir())+os.path.basename(args.configfile).replace(".cfg","_evaluation_step.cfg"))
    Model = cfg_settings.get_Model()
    if Model.isRegressionDNN() or Model.isRegressionBDT():
        DH = RegressionPlotHandler(cfg_settings)
        DH.do_Plots()

        