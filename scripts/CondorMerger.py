#!/usr/bin/env python3
"""
This module introduces a class handling the conversion of root files.
"""

from shutil import copyfile
#from PlotterModules.CtrlPlotHandler import CtrlPlotHandler
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.Directories import Directories
from HelperModules.Settings import Settings
from HelperModules.Processor import Processor
import HelperModules.HelperFunctions as hf
import argparse
import awkward as ak
import numpy as np
import pandas as pd
import glob
import os
import ROOT
import vector
import matplotlib.pyplot as plt
import pickle
pickle.HIGHEST_PROTOCOL = 4


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    parser.add_argument("-n", "--namefile", help="Single filename for parallel conversion", required=False)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    parser.add_argument("--plots", nargs="+", help="the dedicated plots to be produced, default is 'all'", default=["all"], choices=["all", "TrainStats", "DataMC", "nonNegativeWeights", "NegativeWeightSummary","TrainTest", "CorrelationMatrix", "CorrelationComparison", "Separation2D"])
    parser.add_argument("--plots-only", help="flag to set in case you only want to redo the plots",  action='store_true', dest="plots_only", default=False)
    args = parser.parse_args()



ROOT.gErrorIgnoreLevel = ROOT.kWarning
cfg_Settings = Settings(args.configfile, option="all")
m_args = args
m_cfg_Settings = cfg_Settings
m_Variables = cfg_Settings.get_Variables()
m_VarNames = [var.get("Name") for var in m_Variables] + [m_cfg_Settings.get_General().get("FoldSplitVariable")]
m_Model = cfg_Settings.get_Model()
DileptonMerge = cfg_Settings.get_General().get("DileptonMerge")

if DileptonMerge == True:
    print("Printing DileptonMerge varlist:")
    print(cfg_Settings.get_VarNames_WithMergedLeptons())
    print(len(cfg_Settings.get_VarNames_WithMergedLeptons()))

m_NTUPLELOCATION = cfg_Settings.get_General().get("NtuplePath")
m_Dirs = Directories(cfg_Settings.get_General().get("Job"))

print("Init Done")

lst = glob.glob(hf.ensure_trailing_slash(m_Dirs.DataDir()) + "*.h5")
dfs = [pd.read_hdf(fname) for fname in lst]
merged = pd.concat(dfs)
# Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
seed = np.random.randint(1000, size=1)[0]
merged = merged.sample(frac=1, random_state=seed).reset_index(drop=True)
print(merged)
merged.to_hdf(hf.ensure_trailing_slash(m_Dirs.DataDir()) + "merged.h5", key="df", mode="w")

