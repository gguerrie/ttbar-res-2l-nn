
"""
Module handling basic plotting functionalities
"""
import PlotterModules.AtlasStyle
import HelperModules.HelperFunctions as hf
from ROOT import TCanvas, THStack, TLegend, TGraph, TMultiGraph, gPad, gROOT, TPad, kBlue
import json
import itertools
import numpy as np
import pandas as pd

from pickle import load
from PlotterModules.AtlasLabel import DrawLabels
from HelperModules.PlotsDataHandler import DataHandler
from HelperModules.ModelHandler import ModelHandler
from HelperModules.Metrics import Metrics
from HelperModules.MessageHandler import PostProcessorMessage, WarningMessage
from HelperModules.Variable import Variable
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import plot_model

class BasicPlotHandler(DataHandler):
    """
    Class handling basic plotting functionalities
    """
    def __init__(self, Settings):
        self.m_Settings = Settings
        super().__init__(Settings)
        PostProcessorMessage("Initialising post-processing...")
        if self.m_ModelSettings.get("ClassColors") is not None:
            self.m_ClassColorDict = dict(zip(np.unique(self.get_DataFrame()[self.m_LABELNAME]), self.m_ModelSettings.get("ClassColors")))
        else:
            self.m_ClassColorDict = None
        self.m_isBinary = (len(self.m_ModelSettings.get("OutputLabels")) == 1 or len(self.m_ModelSettings.get("OutputLabels")) == 0) and self.m_ModelSettings.isClassification()
        self.m_ModelSettingsVariables = {class_label: Variable(name=class_label,
                                                               label=class_label,
                                                               binning=model_binning) for class_label, model_binning in zip(self.m_ModelSettings.get("OutputLabels"), self.m_ModelSettings.get("ModelBinning"))}
        self.m_SCALEFACTOR = 1.8
        self.m_SCALEFACTOR_DATAHIST = 1.7
        gROOT.SetBatch()
        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if self.m_nFolds != 1:
            Folds = ["_Fold" + str(i) for i in range(self.m_nFolds)]
        else:
            Folds = [""]
        self.m_mhandler = ModelHandler(self.m_ModelSettings, self.m_Dirs)
        self.m_ModelObjects = [self.m_mhandler.load_model(fold) for fold in Folds]
        self.m_HistoryNames = [self.m_mhandler.load_history(fold) for fold in Folds]
        self.m_Scaler = load(open("%sscaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))

        ###################################################################
        ########################### Predictions ###########################
        ###################################################################
        PostProcessorMessage("Determining model predictions...")
        F_tmp = [self.get_DataFrame_sc()[self.get_DataFrame_sc()[self.m_FOLDIDENTIFIER] % self.m_nFolds == i]["Index_tmp"].values for i in range(self.m_nFolds)]
        X_tmp = [(self.get_DataFrame_sc()[self.get_DataFrame_sc()[self.m_FOLDIDENTIFIER] % self.m_nFolds == i][self.m_VarNames]) for i in range(self.m_nFolds)]
        P_tmp = [self.m_mhandler.predict(model, x, settings=self.m_Settings) for model, x in zip(self.m_ModelObjects, X_tmp)]
        if self.m_nFolds != 1:
            P_tmp = np.array(list(itertools.chain.from_iterable(P_tmp)))
            F_tmp = np.array(list(itertools.chain.from_iterable(F_tmp)))
        # SCALING PREDICTIONS
        if self.m_GeneralSettings.get("OutputScaling") is not None:
            self.m_OutputScaler = load(open("%sout_scaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))
            P_tmp = self.m_OutputScaler.inverse_transform(P_tmp)
            #P_tmp = P_tmp*np.sqrt(self.m_OutputScaler.var_) + self.m_OutputScaler.mean_
        
        if self.m_isBinary:
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            P_df[self.m_PREDICTIONNAME] = P_tmp
        else:
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            for i, OutputLabel in enumerate(self.m_ModelSettings.get("OutputLabels")):
                P_df[OutputLabel] = P_tmp[:, i]
        try:
            print("Values smaller than 100")
            print(P_df[P_df['mttbar']<100])
            print("Values greater than 100")
            print(P_df[P_df['mttbar']>100])
        except:
            pass
        self.m_DataFrame = pd.merge(self.get_DataFrame(), P_df, how="inner", on=["Index_tmp"])
        self.m_DataFrame_sc = pd.merge(self.get_DataFrame_sc(), P_df, how="inner", on=["Index_tmp"])
        self.m_DataFrame = self.m_DataFrame.drop(["Index_tmp"], axis=1)
        self.m_DataFrame_sc = self.m_DataFrame_sc.drop(["Index_tmp"], axis=1)
        PostProcessorMessage("Finished post-processing.")
    ###########################################################################
    ############################# misc functions ##############################
    ###########################################################################

    def get_Predictions(self, PredictionColumn):
        """
        Getter method to return the 'PredictionColumn' from a pandas DataFrame object

        Keyword arguments:
        PredictionColumn --- String defining the column of the pandas DataFrame object to be returned.
        """
        return self.get_DataFrame()[PredictionColumn]

    def get_ModelObjects(self):
        """
        Getter method to return all model objects.
        """
        return self.m_ModelObjects

    def get_TrainPredictions(self, ColumnID=0, Fold=0, returnNonZeroLabels=False):
        """
        Getter method to return the predictions using the training set

        Keyword arguments:
        ColumnID --- Integer defining the column of the array. I.e. 0 for first column in a binary case
        Fold --- Integer defining the fold the training data belonged to
        returnNonZeroLabels --- Boolean, can be True or False. If set to True also events that where not trained on are returned (i.e. labels <0)
        """
        if self.m_ModelSettings.isClassificationBDT():
            if self.m_isBinary:
                return self.m_mhandler.predict(self.m_ModelObjects[Fold],
                                               self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels),
                                               settings=self.m_Settings)
        return self.m_mhandler.predict(self.m_ModelObjects[Fold],
                                       self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels),
                                       settings=self.m_Settings)[:, ColumnID]

    def get_TestPredictions(self, ColumnID=0, Fold=0, returnNonZeroLabels=False):
        """
        Getter method to return the predictions using the testing set

        Keyword arguments:
        ColumnID --- Integer defining the column of the array. I.e. 0 for first column in a binary case
        Fold --- Integer defining the fold the testing data belonged to
        returnNonZeroLabels --- Boolean, can be True or False. If set to True also events that where not trained on are returned (i.e. labels <0)
        """
        if self.m_ModelSettings.isClassificationBDT():
            if self.m_isBinary:
                return self.m_mhandler.predict(self.m_ModelObjects[Fold],
                                               self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels),
                                               settings=self.m_Settings)
        return self.m_mhandler.predict(self.m_ModelObjects[Fold],
                                       self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels),
                                       settings=self.m_Settings)[:, ColumnID]

    def get_HistoryNames(self):
        """
        Getter method to return the names of the histories
        """
        return self.m_HistoryNames

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################

    def do_StackPlot(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to plot a Stacked data/MC plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the output itself
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        Canvas = TCanvas("StackPlot", "StackPlot", 800, 600)
        PostProcessorMessage("Plotting Stacked Data/MC plot for %s."%OutputLabel)
        if self.m_ModelSettings.isClassification() or self.m_ModelSettings.isRegression():
            hists = hf.DefineAndFill(Binning=self.m_ModelSettingsVariables[OutputLabel].get_VarBinning(),
                                     Samples=self.m_Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning=self.m_ModelSettingsVariables[OutputLabel].get_VarBinning(),
                                     Samples=self.m_OutputLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        Stack = THStack()
        if len(hists) >= 6:
            Legend = TLegend(.50, .75 - len(hists) / 2. * 0.025, .90, .90)
            Legend.SetNColumns(2)
        else:
            Legend = TLegend(.50, .75 - len(hists) * 0.025, .90, .90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        data_hist, s_stack, b_stack = None, THStack(), THStack()
        for value in hists.values():
            if value[1]["Type"] != "Data":
                value[0].SetFillColor(value[1]["FillColor"])
                if not value[1]["FillColor"] == 0:
                    value[0].SetLineWidth(0)
                if value[1]["Type"] == "Signal":
                    s_stack.Add(value[0])
                if value[1]["Type"] == "Background":
                    b_stack.Add(value[0])
                Stack.Add(value[0])
                if value[1]["Type"] == "Signal" and value[1]["ScaleToBkg"]:
                    scaled_hist = value[0].Clone(value[1]["Name"] + "_scaled")
                    scaled_hist.Scale(sum([v[0].Integral() for v in hists.values() if v[1]["Type"] == "Background"]) / scaled_hist.Integral())
                    scaled_hist.SetFillColor(0)
                    scaled_hist.SetLineColor(value[1]["FillColor"])
                    scaled_hist.SetLineStyle(2)
                    scaled_hist.SetLineWidth(1)
                    Stack.Add(scaled_hist)
                    if self.m_GeneralSettings.do_Yields():
                        Legend.AddEntry(scaled_hist, "{}  {:.1f}".format(value[1]["Name"] + " scaled", scaled_hist.Integral()), "f")
                    else:
                        Legend.AddEntry(scaled_hist, value[1]["Name"] + " scaled", "f")
                if self.m_ModelSettings.isClassification():
                    if self.m_GeneralSettings.do_Yields():
                        Legend.AddEntry(value[0], "{}  {:.1f}".format(value[1]["Name"], value[0].Integral()), "f")
                    else:
                        Legend.AddEntry(value[0], value[1]["Name"], "f")
                elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
                    Legend.AddEntry(value[0], value[1]["Name"], "f")
            if value[1]["Type"] == "Data":
                data_hist = value[0]
                if self.m_ModelSettings.isClassification():
                    if self.m_GeneralSettings.do_Yields():
                        Legend.AddEntry(value[0], "{}  {:.1f}".format(value[1]["Name"], value[0].Integral()), "p")
                    else:
                        Legend.AddEntry(value[0], value[1]["Name"], "p")
                elif self.m_ModelSettings.isReconstruction():
                    Legend.AddEntry(value[0], value[1][1], "p")
        if data_hist is not None:  # In case we do have a data hist we create a ratio plot
            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            d_hist = data_hist.Clone("d_hist")
            MC_hist = Stack.GetStack().Last().Clone("MC_hist")
            ratio = hf.createRatio(d_hist, MC_hist)
            ratio.SetMaximum(self.m_GeneralSettings.get("RatioMax"))
            ratio.SetMinimum(self.m_GeneralSettings.get("RatioMin"))

            # Define blue unc. band and draw it on top of Stack and create legend entry for it
            UpperPad_errband = Stack.GetStack().Last().Clone("UpperPad_errband")
            UpperPad_errband.SetFillColor(kBlue)
            UpperPad_errband.SetFillStyle(3018)
            UpperPad_errband.SetMarkerSize(0)
            Legend.AddEntry(UpperPad_errband, "Uncertainty", "f")
            # Define blue unc. band for the lower pad
            LowerPad_errband = Stack.GetStack().Last().Clone("LowerPad_errband")
            for i in range(LowerPad_errband.GetNbinsX() + 1):
                if ratio.GetBinContent(i) == 0:
                    ratio.SetBinError(i, 0)
                else:
                    ratio.SetBinError(i, d_hist.GetBinError(i)/d_hist.GetBinContent(i)) # we want the scaled error and not the error of the ratio!
                if LowerPad_errband.GetBinContent(i) == 0:
                    LowerPad_errband.SetBinError(i, 0)
                else:
                    LowerPad_errband.SetBinError(i, LowerPad_errband.GetBinError(i)/LowerPad_errband.GetBinContent(i))
                    LowerPad_errband.SetBinContent(i, 1)
            LowerPad_errband.SetFillColor(kBlue)
            LowerPad_errband.SetFillStyle(3018)
            LowerPad_errband.SetMarkerSize(0)

            # Apply blinding
            s_hist = s_stack.GetStack().Last().Clone("s_hist")
            b_hist = b_stack.GetStack().Last().Clone("b_hist")
            for i in range(LowerPad_errband.GetNbinsX() + 1):
                if s_hist.GetBinContent(i) == 0 and b_hist.GetBinContent(i) == 0:
                    continue
                elif s_hist.GetBinContent(i) > 0 and b_hist.GetBinContent(i) <= 0:
                    ratio.SetBinContent(i, -9999)
                    data_hist.SetBinContent(i, -9999)
                if (self.m_GeneralSettings.get("SBBlinding") is not None and s_hist.GetBinContent(i)/b_hist.GetBinContent(i) > self.m_GeneralSettings.get("SBBlinding")
                    or (self.m_GeneralSettings.get("SignificanceBlinding") is not None and s_hist.GetBinContent(i)/np.sqrt(b_hist.GetBinContent(i)) > self.m_GeneralSettings.get("SignificanceBlinding"))
                    or (self.m_GeneralSettings.get("Blinding") is not None and s_hist.GetBinCenter(i) > self.m_GeneralSettings.get("Blinding"))):
                    ratio.SetBinContent(i, -9999)
                    data_hist.SetBinContent(i, -9999)

            # draw everything in first pad
            pad1.cd()
            Stack.Draw("hist")
            Stack.SetMaximum(Stack.GetMaximum() * 1.3)
            UpperPad_errband.Draw("e2 SAME")
            data_hist.Draw("ep SAME")
            # to avoid clipping the bottom zero, redraw a small axis
            axis = Stack.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("ep")
            ratio.GetXaxis().SetTitle(OutputLabel + " Classifier")
            LowerPad_errband.Draw("e2 SAME")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
        else:
            Stack.Draw("hist")
            Stack.GetXaxis().SetTitle(OutputLabel + " Classifier")
        Stack.GetYaxis().SetTitle("Events")
        # Let's make sure we have enough space for the labels
        Stack.SetMaximum(Stack.GetMaximum() * self.m_SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="data")
        # logscale or linear scale
        if self.m_GeneralSettings.get("StackPlotScale") == "Log":
            gPad.SetLogy()
        # Now we export our plots
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_" + OutputLabel + "." + file_extension)
        # Clean up
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        if data_hist is not None:
            gROOT.FindObject("s_hist").Delete()
            gROOT.FindObject("b_hist").Delete()
        Canvas.Close()
        del Canvas

    def do_PerformancePlot(self):
        """
        Plot a typical performance plot using a keras history object.
        The method will check whether the requested metric is indeed available.
        """
        metrics = self.m_ModelSettings.get("Metrics")
        if metrics is None:
            metrics = []
        metrics.append("loss")
        Metric_Labels = Metrics()
        for i, historyname in enumerate(self.get_HistoryNames()):
            with open(historyname) as f:
                History = json.load(f)
                for metric in metrics:
                    Canvas = TCanvas("PerformancePlot", "PerformancePlot", 800, 600)
                    mg = TMultiGraph()
                    if metric in History.keys():
                        PostProcessorMessage("Plotting training history for metric " + metric + " and fold " + str(i) + ".")
                        y = np.array(list(History[metric].values()))
                        x = np.arange(1, len(y) + 1, 1)
                        gr = TGraph(x.size, x.astype(np.double), y.astype(np.double))
                        gr.SetLineColor(2)
                        gr.SetLineWidth(2)
                        mg.Add(gr)
                        Legend = TLegend(.73, .75, .85, .85)
                        Legend.SetBorderSize(0)
                        Legend.SetTextFont(42)
                        Legend.SetTextSize(0.035)
                        Legend.AddEntry(gr, "Train", "L")
                        if "val_" + metric in History.keys():
                            y = np.array(list(History["val_" + metric].values()))
                            x = np.arange(1, len(y) + 1, 1)
                            gr_val = TGraph(x.size, x.astype(np.double), y.astype(np.double))
                            gr_val.SetLineColor(4)
                            gr_val.SetLineWidth(2)
                            mg.Add(gr_val)
                            Legend.AddEntry(gr_val, "Validation", "L")
                        mg.Draw("A")
                        Legend.Draw("SAME")
                        if self.m_GeneralSettings.get("LossPlotXScale") == "Log":
                            gPad.SetLogx()
                        if self.m_GeneralSettings.get("LossPlotYScale") == "Log":
                            gPad.SetLogy()
                        mg.GetYaxis().SetTitle(Metric_Labels.get_Label(metric))
                        if self.m_ModelSettings.isClassificationBDT() or self.m_ModelSettings.isRegressionBDT():
                            mg.GetXaxis().SetTitle("N Estimators")
                        else:
                            mg.GetXaxis().SetTitle("Epoch")
                        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
                        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + self.m_ModelSettings.get("Name") + "_" + str(i) + "_" + metric + "." + file_extension)
                    else:
                        WarningMessage("Metric " + metric + " does not exist. No history plot can be created!")
                    Canvas.Close()
                    del Canvas

    def do_ArchitecturePlot(self, left=0.02, right=0.98, bottom=0.02, top=0.98):
        """
        Function to create simple architecture plots of the model.

        Keyword arguments:
        left        --- Left border.
        right       --- Right border.
        bottom      --- Bottom border.
        top         --- Top border.
        """
        return # todo find a solution to marry plt and root
        PostProcessorMessage("Plotting model architecture.")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            plot_model(self.get_ModelObjects()[0], to_file=hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + "Model.png") # only store model pictures as png
        basename = self.m_ModelSettings.get("Name") + "_"
        if self.m_ModelSettings.get("PredesignedModel") is None:
            layer_sizes = [len(self.m_VarNames)]
            layer_sizes.extend(self.m_ModelSettings.get("Nodes"))
            layer_sizes.append(self.m_ModelSettings.get("OutputSize"))
            weight_array = []
            for i in range(len(layer_sizes) - 1):
                weight_array.append(np.ones((layer_sizes[i], layer_sizes[i + 1])))
            fig = plt.figure()
            ax = fig.gca()
            ax.axis('off')
            v_spacing = (top - bottom) / float(max(layer_sizes))
            h_spacing = (right - left) / float(len(layer_sizes) - 1)
            # Nodes
            for n, layer_size in enumerate(layer_sizes):
                layer_top = v_spacing * (layer_size - 1) / 2. + (top + bottom) / 2.
                for m in range(layer_size):
                    circle = plt.Circle((n * h_spacing + left, layer_top - m * v_spacing), v_spacing / 2., color='w', ec='k', zorder=4)
                    ax.add_artist(circle)
            # Edges
            for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
                layer_top_a = v_spacing * (layer_size_a - 1) / 2. + (top + bottom) / 2.
                layer_top_b = v_spacing * (layer_size_b - 1) / 2. + (top + bottom) / 2.
                for m in range(layer_size_a):
                    for o in range(layer_size_b):
                        line = plt.Line2D([n * h_spacing + left, (n + 1) * h_spacing + left], [layer_top_a - m * v_spacing, layer_top_b - o * v_spacing], c="black", linewidth=0.05, zorder=1)
                        ax.add_artist(line)
            fig.savefig(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "Model_Graph.png") # only store model pictures as png
            plt.close()
