"""
Module handling control plots
"""
import numpy as np
import pandas as pd
from ROOT import TH1D, TH2D, TCanvas, THStack, TLegend, TLine, gStyle, gPad, gROOT, TPad, kBlue, kRed
import HelperModules.HelperFunctions as hf
import PlotterModules.AtlasStyle
from PlotterModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from HelperModules.PlotsDataHandler import DataHandler
from HelperModules.MessageHandler import ConverterMessage
from sklearn.preprocessing import MinMaxScaler, StandardScaler


class CtrlPlotHandler(DataHandler):
    """
    Class handling control plots
    """

    def __init__(self, Settings):
        super().__init__(Settings)
        self.m_SCALEFACTOR = 1.4
        self.m_SCALEFACTOR_DATAHIST = 1.7
        self.m_RATIOPLOTMAX = 1.55
        self.m_RATIOPLOTMIN = 0.45
        self.DileptonMerge = Settings.get_General().get("DileptonMerge")
        # print("------------------------------------------")
        # print(Settings.get_VarNames_WithMergedLeptons())
        # if self.DileptonMerge == True:
        #     self.m_VarNames = Settings.get_VarNames_WithMergedLeptons()
        #     self.m_VarObjects = Settings.get_Variables_WithMergedLeptons() # List of variable objects
        #     self.m_VarLabels = Settings.get_VarLabels_WithMergedLeptons() # List of variable labels
        # else:
        #     self.m_VarNames = Settings.get_VarNames() # List of variable names
        #     self.m_VarObjects = Settings.get_Variables() # List of variable objects
        #     self.m_VarLabels = Settings.get_VarLabels() # List of variable labels
        gROOT.SetBatch()

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_CtrlPlots(self, plots=["all"]):
        """
        Wrapper function to produce all available plots in one go.
        """
        if "all" in plots or "TrainStats" in plots:
            self.do_TrainingStats()
        if "all" in plots or "DataMC" in plots:
            self.do_DataMCPlots()
        if "all" in plots or "DataMCscaled" in plots:
            self.do_DataMCPlots(scaled=True)
        if ("all" in plots or "nonNegativeWeights" in plots) and self.m_ModelSettings.isClassification():
            self.do_nonnegativeWeightPlots()
        if ("all" in plots or "NegativeWeightSummary" in plots) and self.m_GeneralSettings.get("TreatNegWeights") != "None":
            self.do_NegWeightSummary()
        if "all" in plots or "TrainTest" in plots:
            self.do_TrainTestPlots()
        if "all" in plots or "CorrelationMatrix" in plots:
            self.do_CorrelationMatrix_plot()
        if "all" in plots or "CorrelationComparison" in plots:
            self.do_CorrelationComparison()
        if ("all" in plots or "Separation2D" in plots) and self.m_ModelSettings.isClassification():
            if len(self.m_Samples) != 1:
                self.do_Separation2D()

    def do_DataMCPlots(self, scaled=False):
        """
        Wrapper function to produce all Data/MC plots in one go.
        """
        for variable in self.m_VarObjects:
            self.do_DataMCPlot(variable, scaled)

    def do_nonnegativeWeightPlots(self):
        """
        Wrapper function to produce all nonnegative weight plots in one go.
        """
        for variable in self.m_VarObjects:
            self.do_nonnegativeWeightsComparison(variable)

    def do_TrainTestPlots(self):
        """
        Wrapper function to produce all Train/Test for all variables plots in one go.
        """
        for variable in self.m_VarObjects:
            self.do_TrainTestPlot(variable)

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################

    def do_nonnegativeWeightsComparison(self, variable_object):
        """
        Function to create plots for variables to investigate the impact of negative weights
        """
        variable_name = variable_object.get("Name")
        variable_binning = variable_object.get_VarBinning()
        ConverterMessage("Plotting negative weight comparison plot for %s" % variable_name)
        stack_dict = {"sig": {},
                      "bkg": {},
                      "sigfinalised": {},
                      "bkgfinalised": {},
                      "sigratios": {},
                      "bkgratios": {},
                      "sigratioserr": {},
                      "bkgratioserr": {}}
        hists_dict = {}
        w_nominal = self.get_Weights().values
        hists_dict["nominal"] = hf.DefineAndFill(variable_binning,
                                                 Samples=self.m_NominalSamples,
                                                 xvals=self.get(variable_name).values,
                                                 xval_identifiers=self.get_Sample_Names().values,
                                                 weights=self.get_Weights().values,
                                                 binningoptimisation="None",
                                                 underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                                 overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        stack_dict["sig"]["nominal"] = THStack()
        stack_dict["bkg"]["nominal"] = THStack()
        if self.m_GeneralSettings.get("TreatNegWeights") == "Scale":
            identifiers = self.get_Sample_Names().values
            unique_identifiers = np.unique(identifiers)
            factors = {identifier: np.sum(w_nominal[identifiers == identifier]) / np.sum(w_nominal[(w_nominal > 0) & (identifiers == identifier)]) for identifier in unique_identifiers}
            w_samplewise = [factors[identifier] for identifier in identifiers]
            w_samplewise *= w_nominal
            w_samplewise[w_samplewise < 0] = 0
            hists_dict["samplewise"] = hf.DefineAndFill(variable_binning,
                                                        Samples=self.m_NominalSamples,
                                                        xvals=self.get(variable_name).values,
                                                        xval_identifiers=self.get_Sample_Names().values,
                                                        weights=w_samplewise,
                                                        binningoptimisation="None",
                                                        prefix="samplewise_hist",
                                                        underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                                        overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
            stack_dict["sig"]["samplewise"] = THStack()
            stack_dict["bkg"]["samplewise"] = THStack()
            
        if self.m_GeneralSettings.get("TreatNegWeights") == "SetZero" or self.m_GeneralSettings.get("TreatNegWeights") == "Scale":
            w_nonnegative = np.copy(w_nominal)
            w_nonnegative[w_nonnegative < 0] = 0
            hists_dict["nonnegative"] = hf.DefineAndFill(variable_binning,
                                                         Samples=self.m_NominalSamples,
                                                         xvals=self.get(variable_name).values,
                                                         xval_identifiers=self.get_Sample_Names().values,
                                                         weights=w_nonnegative,
                                                         binningoptimisation="None",
                                                         prefix="nonnegative_hist",
                                                         underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                                         overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
            stack_dict["sig"]["nonnegative"] = THStack()
            stack_dict["bkg"]["nonnegative"] = THStack()
        for key, hists in hists_dict.items():
            for value in hists.values():
                if value[1]["Type"] == "Data":
                    continue
                if value[1]["Type"] == "Signal":
                    stack_dict["sig"][key].Add(value[0])
                if value[1]["Type"] == "Background":
                    stack_dict["bkg"][key].Add(value[0])
        linestyle_dict = {"nominal": 1,
                          "nonnegative": 2,
                          "samplewise": 9}
        final_stack = THStack()
        maxerr = 0.55
        for key, stack in stack_dict["sig"].items():
            stack_dict["sigfinalised"][key] = stack.GetStack().Last().Clone("sig%s"%key)
            stack_dict["sigfinalised"][key].SetLineColor(2) # red
            stack_dict["sigfinalised"][key].SetMarkerSize(0)
            stack_dict["sigfinalised"][key].SetLineStyle(linestyle_dict[key])
            final_stack.Add(stack_dict["sigfinalised"][key])
            if key != "nominal":
                stack_dict["sigratios"][key] = hf.createRatio(stack_dict["sigfinalised"]["nominal"], stack_dict["sigfinalised"][key])
                stack_dict["sigratios"][key].SetMaximum(1 + maxerr)
                stack_dict["sigratios"][key].SetMinimum(1 - maxerr)
                stack_dict["sigratios"][key].SetLineStyle(linestyle_dict[key])
                stack_dict["sigratioserr"][key] = stack_dict["sigratios"][key].Clone("sigratioserr%s"%key)
                stack_dict["sigratioserr"][key].SetFillColor(kRed)
                stack_dict["sigratioserr"][key].SetLineStyle(linestyle_dict[key])
                stack_dict["sigratioserr"][key].SetFillStyle(3004)
                stack_dict["sigratioserr"][key].SetMarkerSize(0)
                for binID in range(stack_dict["sigratioserr"][key].GetNbinsX() + 1):
                    if stack_dict["sigratioserr"][key].GetBinContent(binID) == 0:
                        stack_dict["sigratioserr"][key].SetBinContent(binID, 1)
                        stack_dict["sigratioserr"][key].SetBinError(binID, maxerr)  # completely fill the ratio plot
        for key, stack in stack_dict["bkg"].items():
            stack_dict["bkgfinalised"][key] = stack.GetStack().Last().Clone("bkg%s"%key)
            stack_dict["bkgfinalised"][key].SetLineColor(4) # blue
            stack_dict["bkgfinalised"][key].SetMarkerSize(0)
            stack_dict["bkgfinalised"][key].SetLineStyle(linestyle_dict[key])
            final_stack.Add(stack_dict["bkgfinalised"][key])
            if key != "nominal":
                stack_dict["bkgratios"][key] = hf.createRatio(stack_dict["bkgfinalised"]["nominal"], stack_dict["bkgfinalised"][key])
                stack_dict["bkgratios"][key].SetMaximum(1 + maxerr)
                stack_dict["bkgratios"][key].SetMinimum(1 - maxerr)
                stack_dict["bkgratios"][key].SetLineStyle(linestyle_dict[key])
                stack_dict["bkgratioserr"][key] = stack_dict["bkgratios"][key].Clone("bkgratioserr%s"%key)
                stack_dict["bkgratioserr"][key].SetFillColor(kBlue)
                stack_dict["bkgratioserr"][key].SetLineStyle(linestyle_dict[key])
                stack_dict["bkgratioserr"][key].SetFillStyle(3005)
                stack_dict["bkgratioserr"][key].SetMarkerSize(0)
                for binID in range(stack_dict["bkgratioserr"][key].GetNbinsX() + 1):
                    if stack_dict["bkgratioserr"][key].GetBinContent(binID) == 0:
                        stack_dict["bkgratioserr"][key].SetBinContent(binID, 1)
                        stack_dict["bkgratioserr"][key].SetBinError(binID, maxerr)  # completely fill the ratio plot
        legend_dict = {"nominal": "(Nominal)",
                       "samplewise": "(Corrected)",
                       "nonnegative": "(W>0)"}
        Canvas = TCanvas("c1", "c1", 800, 600)
        Legend = TLegend(.58, .725, .95, .90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        for key, stack in stack_dict["sigfinalised"].items():
            Legend.AddEntry(stack, "Signal %s"%legend_dict[key], "L")
        for key, stack in stack_dict["bkgfinalised"].items():
            Legend.AddEntry(stack, "Background %s"%legend_dict[key], "L")
            
        pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
        pad1.SetBottomMargin(0)  # joins upper and lower plot
        pad1.Draw()
        # Lower ratio plot is pad2
        Canvas.cd()  # returns to main canvas before defining pad2
        pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
        pad2.SetTopMargin(0)  # joins upper and lower plot
        pad2.SetBottomMargin(0.3)
        pad2.Draw()
        pad1.cd()

        final_stack.Draw("hist e nostack")
        final_stack.GetYaxis().SetTitle("Fraction of Events")
        final_stack.GetXaxis().SetTitle(variable_object.get("Label"))
        # Let's make sure we have enough space for the labels and the legend
        final_stack.SetMaximum(np.max([[hist[0].GetMaximum() * self.m_SCALEFACTOR for hist in hists.values()] for hists in hists_dict.values()]))
        if self.m_GeneralSettings.get("ATLASLabel").lower() != "none":
            ATLASLabel(0.2, 0.85, self.m_GeneralSettings.get("ATLASLabel"))
        CustomLabel(0.2, 0.8, self.m_GeneralSettings.get("CMLabel"))
        if self.m_GeneralSettings.get("CustomLabel") != "":
            CustomLabel(0.2, 0.70, self.m_GeneralSettings.get("CustomLabel"))
        Legend.Draw("SAME")

        # to avoid clipping the bottom zero, redraw a small axis
        axis = final_stack.GetYaxis()
        axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

        # draw everything in second pad
        pad2.cd()
        first = True
        for key, hist in stack_dict["sigratios"].items():
            if first:
                hist.Draw("hist")
                hist.GetXaxis().SetTitle(variable_object.get("Label"))
                hist.GetYaxis().SetTitle("Ratio")
                first = False
            else:
                hist.Draw("hist same")
        for key, hist in stack_dict["sigratioserr"].items():
            hist.Draw("e2 same")
        for key, hist in stack_dict["bkgratios"].items():
            hist.Draw("hist same")
        for key, hist in stack_dict["bkgratioserr"].items():
            hist.Draw("e2 same")
        line = TLine(stack_dict["sigfinalised"]["nominal"].GetXaxis().GetXmin(), 1, stack_dict["sigfinalised"]["nominal"].GetXaxis().GetXmax(), 1)
        line.SetLineStyle(2)  # dashed
        line.Draw("Same")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "Separation_weightComparison_" + variable_name + "." + file_extension)
        # Clean up
        for key, stack in stack_dict["sig"].items():
            gROOT.FindObject("sig%s"%key).Delete()
        for key, stack in stack_dict["bkg"].items():
            gROOT.FindObject("bkg%s"%key).Delete()
        for key, stack in stack_dict["sigratioserr"].items():
            gROOT.FindObject("sigratioserr%s"%key).Delete()
        for key, stack in stack_dict["bkgratioserr"].items():
            gROOT.FindObject("bkgratioserr%s"%key).Delete()
        for _, hists in hists_dict.items():
            for key in hists.keys():
                gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_CorrelationComparison(self):
        identifiers = self.get_Sample_Names().values
        unique_identifiers = np.unique(identifiers)
        w_nominal = self.get_Weights()
        factors = {identifier: np.sum(w_nominal[identifiers == identifier]) / np.sum(w_nominal[(w_nominal > 0) & (identifiers == identifier)]) for identifier in unique_identifiers}
        w_corrected = [factors[identifier] for identifier in identifiers]
        w_corrected *= w_nominal
        w_corrected[w_corrected < 0] = 0
        ratio_array, correlation_array = [], []
        for i, var_i in enumerate(self.m_VarObjects):
            tmp_array, tmp_correlation_array = [], []
            for j, var_j in enumerate(self.m_VarObjects):
                if j > i:
                    tmp_array.append(0)
                    tmp_correlation_array.append(0)
                else:
                    h_name = "hist_%s_%s" % (var_i.get("Name"), var_j.get("Name"))
                    var_i_binning = var_i.get_VarBinning()
                    var_j_binning = var_j.get_VarBinning()
                    hist_nominal = TH2D(h_name, h_name, int(len(var_i_binning) - 1), var_i_binning, int(len(var_j_binning) - 1), var_j_binning)
                    hist_corrected = TH2D("%s_corrected" % h_name, "%s_corrected" % h_name, int(len(var_i_binning) - 1), var_i_binning, int(len(var_j_binning) - 1), var_j_binning)
                    for v_i, v_j, w in zip(self.get(var_i.get("Name")).values, self.get(var_j.get("Name")).values, w_nominal):
                        hist_nominal.Fill(v_i, v_j, w)
                    for v_i, v_j, w in zip(self.get(var_i.get("Name")).values, self.get(var_j.get("Name")).values, w_corrected):
                        hist_corrected.Fill(v_i, v_j, w)
                    tmp_array.append((hist_nominal.GetCorrelationFactor() - hist_corrected.GetCorrelationFactor()))
                    tmp_correlation_array.append(hist_nominal.GetCorrelationFactor())
                    gROOT.FindObject(h_name).Delete()
                    gROOT.FindObject("%s_corrected" % h_name).Delete()
            ratio_array.append(tmp_array)
            correlation_array.append(tmp_correlation_array)
        correlations = pd.DataFrame(np.array(correlation_array), columns=self.m_VarNames, index=self.m_VarNames)
        ratios = pd.DataFrame(np.array(ratio_array), columns=self.m_VarNames, index=self.m_VarNames)
        Canvas = TCanvas("c1", "c1", 1000, 1000)
        if len(ratios.keys()) > 0:
            hist_ratios = TH2D("LargeCorrelationsratios2Dhist", "", len(list(ratios.keys())), 0, len(list(ratios.keys())), len(list(ratios.keys())), 0, len(list(ratios.keys())))
            for xbin, var1 in enumerate(ratios.keys()):
                for ybin, var2 in enumerate(ratios.keys()):
                    hist_ratios.SetBinContent(xbin + 1, ybin + 1, ratios[var1][var2])
            hist_ratios.SetMaximum(0.01)
            hist_ratios.SetMinimum(-0.01)
            hist_ratios.SetMarkerSize(0.3 * 30 / len(self.m_VarNames))
            hist_ratios.GetXaxis().SetLabelSize(0.01 * 40 / len(ratios.keys()))
            hist_ratios.GetYaxis().SetLabelSize(0.01 * 40 / len(ratios.keys()))
            hist_ratios.GetZaxis().SetLabelSize(0.02)
            hist_ratios.GetZaxis().SetTitle("#DeltaCorrelation")
            Yaxis = hist_ratios.GetYaxis()
            Xaxis = hist_ratios.GetXaxis()
            label_dict = {var.get("Name"): var.get("Label") for var in self.m_VarObjects}
            for i, variable_label in enumerate(ratios.keys()):
                Xaxis.SetBinLabel(i + 1, label_dict[variable_label].replace("[GeV]", ""))
                Yaxis.SetBinLabel(i + 1, label_dict[variable_label].replace("[GeV]", ""))
            Xaxis.LabelsOption("v")
            Canvas.SetRightMargin(0.20)
            Canvas.SetBottomMargin(0.20)
            Canvas.SetLeftMargin(0.20)
        else:
            return
        gStyle.SetPaintTextFormat(".1e")
        hist_ratios.Draw("colz text45")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "CorrelationComparison." + file_extension)
        del Canvas

    def do_CorrelationMatrix_plot(self):
        """
        Function to produce a 2D correlation matrix plot between the input variables.
        """
        ConverterMessage("Plotting correlation matrices and exporting correlations to tables for the " + self.m_ModelSettings.get("Type") + " input variables.")
        correlations = [[] for i in range(len(self.m_VarNames))]
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1", "c1", 1000, 1000)
        correlations = self.get(self.m_VarNames).corr()
        correlations.to_csv(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "CorrelationMatrix.csv")
        hist_correlations = TH2D("Correlations2Dhist", "", len(correlations), 0, len(correlations), len(correlations), 0, len(correlations))
        for xbin, var1 in enumerate(self.m_VarNames):
            for ybin, var2 in enumerate(self.m_VarNames):
                hist_correlations.SetBinContent(xbin + 1, ybin + 1, correlations[var1][var2])
        ### Drawing all correlations ###
        hist_correlations.Draw("colz text45")
        hist_correlations.SetMarkerSize(0.4 * 30 / len(self.m_VarNames))
        hist_correlations.GetXaxis().SetLabelSize(0.01 * 40 / len(self.m_VarNames))
        hist_correlations.GetYaxis().SetLabelSize(0.01 * 40 / len(self.m_VarNames))
        hist_correlations.GetZaxis().SetLabelSize(0.04)
        Yaxis = hist_correlations.GetYaxis()
        Xaxis = hist_correlations.GetXaxis()
        for i, var in enumerate(self.m_VarObjects):
            Xaxis.SetBinLabel(i + 1, var.get("Label").replace("[GeV]", ""))
            Yaxis.SetBinLabel(i + 1, var.get("Label").replace("[GeV]", ""))
        Xaxis.LabelsOption("v")
        hist_correlations.SetMaximum(1.0)
        hist_correlations.SetMinimum(-1.0)
        Canvas.SetRightMargin(0.15)
        Canvas.SetBottomMargin(0.30)
        Canvas.SetLeftMargin(0.30)
        hist_correlations.GetZaxis().SetTitle("Correlation")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "CorrelationMatrix." + file_extension)
        del Canvas
        ### Drawing only large correlations ###
        Canvas = TCanvas("c1", "c1", 1000, 1000)
        Large_correlations_vars = [var for var in self.m_VarNames if any([True for x in correlations[var].values if (x > self.m_GeneralSettings.get("LargeCorrelation")) & (x != 1)])]
        Large_correlations = correlations[Large_correlations_vars].drop(list(set(self.m_VarNames) - set(Large_correlations_vars)))
        if len(Large_correlations.keys()) > 0:
            hist_large_correlations = TH2D("LargeCorrelations2Dhist", "", len(Large_correlations), 0, len(Large_correlations), len(Large_correlations), 0, len(Large_correlations))
            for xbin, var1 in enumerate(Large_correlations.keys()):
                for ybin, var2 in enumerate(Large_correlations.keys()):
                    hist_large_correlations.SetBinContent(xbin + 1, ybin + 1, Large_correlations[var1][var2])
            Large_correlations.to_csv(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "LargeCorrelationsMatrix.csv")
            hist_large_correlations.Draw("colz text45")
            label_dict = {var.get("Name"): var.get("Label") for var in self.m_VarObjects}
            hist_large_correlations.SetMarkerSize(0.4 * 40 / len(Large_correlations.keys()))
            hist_large_correlations.GetXaxis().SetLabelSize(0.01 * 40 / len(Large_correlations.keys()))
            hist_large_correlations.GetYaxis().SetLabelSize(0.01 * 40 / len(Large_correlations.keys()))
            hist_large_correlations.GetZaxis().SetLabelSize(0.04)
            Yaxis = hist_large_correlations.GetYaxis()
            Xaxis = hist_large_correlations.GetXaxis()
            for i, variable_label in enumerate(Large_correlations.keys()):
                Xaxis.SetBinLabel(i + 1, label_dict[variable_label].replace("[GeV]", ""))
                Yaxis.SetBinLabel(i + 1, label_dict[variable_label].replace("[GeV]", ""))
            Xaxis.LabelsOption("v")
            hist_large_correlations.SetMaximum(1.0)
            hist_large_correlations.SetMinimum(-1.0)
            Canvas.SetRightMargin(0.15)
            Canvas.SetBottomMargin(0.30)
            Canvas.SetLeftMargin(0.30)
            hist_large_correlations.GetZaxis().SetTitle("Correlation")
            DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "LargeCorrelationsMatrix." + file_extension)
            del Canvas

    def do_TrainingStats(self):
        """
        Function to produce a plot with training statistics
        """
        unique, counts = np.unique(self.get_Sample_Names(returnNominalOnly=False).values[self.get_Sample_Types(returnNominalOnly=False).values != "Data"], return_counts=True)
        # Sort the list and keep track of indece
        indece = sorted(range(len(counts)), key=lambda k: counts[k], reverse=True)
        counts_sorted = np.array(counts)[indece]
        unique_sorted = np.array(unique)[indece]
        Canvas = TCanvas("TrainingStats", "TrainingStats", 1150, 600)
        Histogram = TH1D("Histogram", "", int(len(counts_sorted)), 0, int(len(counts_sorted)))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25)  # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID, count in enumerate(counts_sorted):
            Histogram.SetBinContent(bin_ID + 1, count)
        Histogram.Draw("hbar")
        for bin_ID, name in enumerate(unique_sorted):
            Histogram.GetXaxis().SetBinLabel(bin_ID + 1, "#splitline{" + name + "}{" + "Nr. of events:" + str(counts_sorted[bin_ID]) + "}")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="right")
        gPad.SetLogx()
        gPad.SetGridx(1)
        Histogram.GetYaxis().SetTitle("Number of MC events")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "MC_Stats." + file_extension)
        del Canvas

    def do_DataMCPlot(self, variable_object, scaled=False):
        """
        Function to plot a Stacked data/MC plot.
        """
        variable_name = variable_object.get("Name")
        variable_binning = variable_object.get_VarBinning()
        Canvas = TCanvas("DataMCPlot", "DataMCPlot", 800, 600)
        if scaled:
            ConverterMessage("Plotting scaled stacked Data/MC plot for %s." % variable_name)
        else:
            ConverterMessage("Plotting stacked Data/MC plot for %s." % variable_name)
        if scaled and self.m_GeneralSettings.get("InputScaling") != "none":
            if self.m_GeneralSettings.get("InputScaling") == "minmax":
                scaler = MinMaxScaler()
            elif self.m_GeneralSettings.get("InputScaling") == "minmax_symmetric":
                scaler = MinMaxScaler(feature_range=(-1, 1))
            elif self.m_GeneralSettings.get("InputScaling") == "standard":
                scaler = StandardScaler()
            scaled_binning = scaler.fit_transform(np.array(variable_binning).reshape(-1,1)).T[0]
            hists = hf.DefineAndFill(scaled_binning,
                                 Samples=self.m_NominalSamples,
                                 xvals=self.get_sc(variable_name).values,
                                 xval_identifiers=self.get_Sample_Names().values,
                                 weights=self.get_Weights().values,
                                 binningoptimisation="None",
                                 underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                 overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        else:
            hists = hf.DefineAndFill(variable_binning,
                                     Samples=self.m_NominalSamples,
                                     xvals=self.get(variable_name).values,
                                     xval_identifiers=self.get_Sample_Names().values,
                                     weights=self.get_Weights().values,
                                     binningoptimisation="None",
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        Stack = THStack()
        if len(hists) >= 6:
            Legend = TLegend(.50, .75 - len(hists) / 2. * 0.025, .90, .90)
            Legend.SetNColumns(2)
        else:
            Legend = TLegend(.50, .75 - len(hists) * 0.025, .90, .90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        data_hist = None
        s_stack = THStack()
        b_stack = THStack()
        for key, value in hists.items():
            if value[1]["Type"] != "Data":
                Stack.Add(value[0])
                if value[1]["Type"] == "Signal":
                    s_stack.Add(value[0])
                if value[1]["Type"] == "Background":
                    b_stack.Add(value[0])
                value[0].SetFillColor(value[1]["FillColor"])
                if not value[1]["FillColor"] == 0:
                    value[0].SetLineWidth(0)
                if self.m_GeneralSettings.do_Yields():
                    Legend.AddEntry(value[0], "{}  {:.1f}".format(value[1]["Name"], value[0].Integral()), "f")
                else:
                    Legend.AddEntry(value[0], key, "f")
            if value[1]["Type"] == "Data":
                data_hist = value[0]
                if self.m_GeneralSettings.do_Yields():
                    Legend.AddEntry(value[0], "{}  {:.1f}".format(value[1]["Name"], value[0].Integral()), "p")
                else:
                    Legend.AddEntry(value[0], key, "p")
        if data_hist is not None:  # In case we do have a data hist we create a ratio plot
            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            d_hist = data_hist.Clone("d_hist")
            MC_hist = Stack.GetStack().Last().Clone("MC_hist")
            ratio = hf.createRatio(d_hist, MC_hist)
            ratio.SetMaximum(self.m_RATIOPLOTMAX)
            ratio.SetMinimum(self.m_RATIOPLOTMIN)

            # Define blue unc. band and draw it on top of Stack and create legend entry for it
            UpperPad_errband = Stack.GetStack().Last().Clone("UpperPad_errband")
            UpperPad_errband.SetFillColor(kBlue)
            UpperPad_errband.SetFillStyle(3018)
            UpperPad_errband.SetMarkerSize(0)
            Legend.AddEntry(UpperPad_errband, "Uncertainty", "f")
            # Define blue unc. band for the lower pad
            LowerPad_errband = Stack.GetStack().Last().Clone("LowerPad_errband")
            for i in range(LowerPad_errband.GetNbinsX() + 1):
                if LowerPad_errband.GetBinContent(i) == 0:
                    LowerPad_errband.SetBinError(i, 0)
                else:
                    LowerPad_errband.SetBinError(i, LowerPad_errband.GetBinError(i)/LowerPad_errband.GetBinContent(i))
                    LowerPad_errband.SetBinContent(i, 1)
            LowerPad_errband.SetFillColor(kBlue)
            LowerPad_errband.SetFillStyle(3018)
            LowerPad_errband.SetMarkerSize(0)

            # Apply blinding
            s_hist = s_stack.GetStack().Last().Clone("s_hist")
            b_hist = b_stack.GetStack().Last().Clone("b_hist")
            if self.m_GeneralSettings.get("SBBlinding") is not None:
                for i in range(LowerPad_errband.GetNbinsX() + 1):
                    if s_hist.GetBinContent(i) == 0 and b_hist.GetBinContent(i) == 0:
                        continue
                    elif b_hist.GetBinContent(i) <= 0:
                        ratio.SetBinContent(i, -9999)
                        data_hist.SetBinContent(i, -9999)
                    elif s_hist.GetBinContent(i)/b_hist.GetBinContent(i) > self.m_GeneralSettings.get("SBBlinding"):
                        ratio.SetBinContent(i, -9999)
                        data_hist.SetBinContent(i, -9999)
            if self.m_GeneralSettings.get("SignificanceBlinding") is not None:
                for i in range(LowerPad_errband.GetNbinsX() + 1):
                    if s_hist.GetBinContent(i) == 0 and b_hist.GetBinContent(i) == 0:
                        continue
                    elif b_hist.GetBinContent(i) <= 0:
                        ratio.SetBinContent(i, -9999)
                        data_hist.SetBinContent(i, -9999)
                    elif s_hist.GetBinContent(i)/np.sqrt(b_hist.GetBinContent(i)) > self.m_GeneralSettings.get("SignificanceBlinding"):
                        ratio.SetBinContent(i, -9999)
                        data_hist.SetBinContent(i, -9999)
            if self.m_GeneralSettings.get("Blinding") is not None:
                x_low = data_hist.GetXaxis().FindBin(self.m_GeneralSettings.get("Blinding"))
                for i in range(x_low, LowerPad_errband.GetNbinsX() + 1):
                    ratio.SetBinContent(i, -9999)
                    data_hist.SetBinContent(i, -9999)
            # draw everything in first pad
            pad1.cd()
            Stack.Draw("hist")
            Stack.GetYaxis().SetTitle("Events")
            UpperPad_errband.Draw("e2 SAME")
            data_hist.Draw("ep SAME")
            # to avoid clipping the bottom zero, redraw a small axis
            axis = Stack.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("ep")
            ratio.GetXaxis().SetTitle(variable_object.get("Label"))
            LowerPad_errband.Draw("e2 SAME")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
        else:
            Stack.Draw("hist")

        # Let's make sure we have enough space for the labels
        if variable_object.get("Ymax") is not None:
            Stack.SetMaximum(variable_object.get("Ymax"))
        else:
            Stack.SetMaximum(Stack.GetMaximum() * self.m_SCALEFACTOR)
        Stack.SetMinimum(variable_object.get("Ymin"))
        if variable_object.get("Scale") == "Log":
            if variable_object.get("Ymin") == 0:
                Stack.SetMinimum(1.0)
            gPad.SetLogy()
        Legend.Draw()
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="data")

        # Now we export our plots
        varPathName = hf.filter_VarPathName(variable_name)
        if scaled:
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + varPathName + "_scaled" + "." + file_extension)
        else:
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + varPathName + "." + file_extension)
                

        # Clean up
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_Separation2D(self, basename="2DSeparation"):
        """
        Function to plot the separation for each indivdual sample and calculate a summary

        Keywords arguments:
        basename    --- the base name of the plot e.g. '2DSeparation'. The code will save as .pdf, .png and .eps.
        """
        # First we need to get rid of data
        df_X = self.get_DataFrame()
        df_X = df_X[df_X[self.m_SAMPLETYPE] != "Data"]
        N = np.unique(df_X[self.m_SAMPLENAME].values)
        norm = {Sample_Name: np.sum(df_X[df_X[self.m_SAMPLENAME] == Sample_Name][self.m_WEIGHTNAME].values) for Sample_Name in N}
        Separation_dict = {}
        for variable_object in self.m_VarObjects:
            variable_name = variable_object.get("Name")
            varPathName = hf.filter_VarPathName(variable_name)
            Separation_dict[variable_name] = {}
            Canvas = TCanvas("c1" + variable_name, "c1", 800, 600)
            ConverterMessage("Plotting Separation for %s" % variable_name)
            variable_binning = variable_object.get_VarBinning()
            Hist2D = TH2D(variable_name + "_hist", "", int(len(variable_binning) - 1), variable_binning, len(N), 0, len(N))
            for i, Sample_Name in enumerate(N):
                for value, weight in zip(df_X[df_X["Sample_Name"] == Sample_Name][variable_name].values, df_X[df_X["Sample_Name"] == Sample_Name]["Weight"].values):
                    Hist2D.Fill(value, i, weight / norm[Sample_Name] * 100)
            Yaxis = Hist2D.GetYaxis()
            for i, Sample_Name in enumerate(N):
                Yaxis.SetBinLabel(i + 1, Sample_Name)
            Hist2D.Draw("colz")
            Hist2D.GetXaxis().SetTitle(variable_object.get("Label"))
            Canvas.SetRightMargin(0.15)
            Hist2D.GetZaxis().SetTitle("Fraction of Events [%]")
            DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + basename + "_" + varPathName + "." + file_extension)
            Canvas.Clear()

            ConverterMessage("Plotting separation summary for %s" % variable_name)
            gStyle.SetPaintTextFormat(".2f")
            Separation_Hist = TH2D("Separation_Hist", "", len(N), 0, len(N), len(N), 0, len(N))
            for i, Sample_Name_i in enumerate(N):
                for j, Sample_Name_j in enumerate(N):
                    if i <= j:
                        Separation_Hist.SetBinContent(i + 1, j + 1, 0)
                    else:
                        hist_a = Hist2D.ProjectionX("Hist_" + Sample_Name_i, i + 1, i + 1)
                        hist_b = Hist2D.ProjectionX("Hist_" + Sample_Name_j, j + 1, j + 1)
                        s = hf.Separation(hist_a, hist_b)
                        Separation_Hist.SetBinContent(i + 1, j + 1, s)
                        Separation_dict[variable_name][Sample_Name_i + "_vs_" + Sample_Name_j] = s
                        gROOT.FindObject("Hist_" + Sample_Name_i).Delete()
                        gROOT.FindObject("Hist_" + Sample_Name_j).Delete()
            Yaxis = Separation_Hist.GetYaxis()
            Xaxis = Separation_Hist.GetXaxis()
            for i, Sample_Name in enumerate(N):
                Yaxis.SetBinLabel(i + 1, Sample_Name)
                Xaxis.SetBinLabel(i + 1, Sample_Name)
            Separation_Hist.Draw("colz text")
            Separation_Hist.SetMaximum(50)
            Separation_Hist.SetMinimum(0)
            # Here we are not using DrawLabels() because we need one more label
            if self.m_GeneralSettings.get("ATLASLabel").lower() != "none":
                ATLASLabel(0.2, 0.85, self.m_GeneralSettings.get("ATLASLabel"))
            CustomLabel(0.2, 0.80, self.m_GeneralSettings.get("CMLabel"))
            if self.m_GeneralSettings.get("CustomLabel") != "":
                CustomLabel(0.2, 0.75, self.m_GeneralSettings.get("CustomLabel"))
            CustomLabel(0.2, 0.70, variable_object.get("Label").replace("[GeV]", ""))
            Separation_Hist.GetZaxis().SetTitle("S [%]")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + basename + "_Summary_" + varPathName + "." + file_extension)
            gROOT.FindObject("Separation_Hist").Delete()
            Canvas.Close()
            del Canvas

            df = pd.DataFrame(data=Separation_dict)
            df.to_latex(buf=hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + basename + "_Summary.tex", index=True, float_format="%.2f")
            df.to_csv(path_or_buf=hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + basename + "_Summary.csv", index=True, float_format="%.2f")

    def do_TrainTestPlot(self, variable_object):
        """
        Generate a plot with superimposed test and train variables for signal and background.
        This function is very similar to the do_TrainTestPlot from the ClassificationPlotHandler.

        Keyword arguments:
        variable_object --- An object of type variable
        """
        variable_binning = variable_object.get_VarBinning()
        variable_name = variable_object.get("Name")
        basename = "TrainTest_" + variable_name + "_"
        for i in range(self.m_nFolds):
            ConverterMessage("Plotting K-Fold comparison for %s classifier, Fold=%d" % (variable_name, i))
            Canvas = TCanvas("c1", "c1", 800, 600)
            Train_Hist = TH1D("Train_Hist" + str(i), "", int(len(variable_binning) - 1), variable_binning)
            Test_Hist = TH1D("Test_Hist" + str(i), "", int(len(variable_binning) - 1), variable_binning)

            Legend = TLegend(.63, .80 - 4 * 0.025, .85, .90)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.SetFillStyle(0)

            x_train = self.get_TrainInputs_prescaled(Fold=i, returnNonZeroLabels=False)[variable_name].values
            w_train = self.get_TrainWeights(Fold=i, returnNonZeroLabels=False).values
            x_test = self.get_TestInputs_prescaled(Fold=i, returnNonZeroLabels=False)[variable_name].values
            w_test = self.get_TestWeights(Fold=i, returnNonZeroLabels=False).values

            hf.FillHist(Train_Hist, x_train, w_train)
            hf.FillHist(Test_Hist, x_test, w_test)
            Legend.AddEntry(Train_Hist, "Train", "p")
            Legend.AddEntry(Test_Hist, "Test", "f")

            Train_Hist.Scale(1. / Train_Hist.Integral())
            Test_Hist.Scale(1. / Test_Hist.Integral())
            KS = Train_Hist.KolmogorovTest(Test_Hist)
            maximum = max(Train_Hist.GetMaximum(), Test_Hist.GetMaximum())

            Train_Hist.SetLineColor(2)  # red
            Test_Hist.SetLineColor(2)  # red

            Test_Hist.SetMarkerSize(0)
            Train_Hist.SetMarkerColor(2)
            Train_Hist.SetMarkerColor(4)

            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            ratio = hf.createRatio(Train_Hist, Test_Hist)
            ratio_errband = ratio.Clone("ratio_errband")
            maxerr = 0.55
            for binID in range(ratio_errband.GetNbinsX() + 1):
                if ratio_errband.GetBinContent(binID) == 0:
                    ratio_errband.SetBinContent(binID, 1)
                    ratio_errband.SetBinError(binID, maxerr)  # completely fill the ratio plot
            ratio_errband.SetFillColor(kRed)
            ratio_errband.SetFillStyle(3005)
            ratio_errband.SetMarkerSize(0)
            ratio.SetMaximum(1 + maxerr)
            ratio.SetMinimum(1 - maxerr)
            ratio.SetMarkerSize(0)
            # draw everything in first pad
            pad1.cd()
            Test_Hist.Draw("hist e")
            Test_Hist.GetYaxis().SetTitle("Fraction of Events")
            Test_Hist.GetXaxis().SetTitle(variable_object.get("Label"))
            Test_Hist.SetMaximum(maximum * 1.5)
            Train_Hist.Draw("e1X0 SAME")
            if self.m_GeneralSettings.get("ATLASLabel").lower() != "none":
                ATLASLabel(0.175, 0.85, self.m_GeneralSettings.get("ATLASLabel"))
            CustomLabel(0.175, 0.8, self.m_GeneralSettings.get("CMLabel"))
            CustomLabel(0.175, 0.75, "KS Test: P=%.3f" % (KS))
            if self.m_GeneralSettings.get("CustomLabel") != "":
                CustomLabel(0.2, 0.70, self.m_GeneralSettings.get("CustomLabel"))
            Legend.Draw("SAME")

            # to avoid clipping the bottom zero, redraw a small axis
            axis = Test_Hist.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("hist ")
            ratio_errband.Draw("e2 Same")
            line = TLine(ratio.GetXaxis().GetXmin(), 1, ratio.GetXaxis().GetXmax(), 1)
            line.SetLineStyle(2)  # dashed
            line.Draw("Same")
            ratio.GetXaxis().SetTitle(variable_object.get("Label"))
            ratio.GetYaxis().SetTitle("Train/Test")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + basename + str(i) + "." + file_extension)
            del Train_Hist, Test_Hist
            Canvas.Close()
            del Canvas

    def do_NegWeightSummary(self):
        ConverterMessage("Plotting negative weight summary plot")
        df = self.get(["Weight", "Sample_Name", "Sample_Type"])
        df = df[df["Sample_Type"] != "Data"]
        unique_names = np.unique(df["Sample_Name"].values)
        p_neg_weights = [len(df[(df["Weight"] < 0) & (df["Sample_Name"] == name)]["Weight"]) / len(df[df["Sample_Name"] == name]["Weight"]) for name in unique_names]
        yields_tot = np.sum(df["Weight"].values)
        p_yields = [np.sum(df[df["Sample_Name"] == name]["Weight"].values) / yields_tot for name in unique_names]
        p_yields_dict = dict(zip(unique_names, p_yields))
        indece = sorted(range(len(p_neg_weights)), key=lambda k: p_neg_weights[k], reverse=True)
        p_neg_weights_sorted = np.array(p_neg_weights)[indece]
        names_sorted = np.array(unique_names)[indece]
        p_yields_sorted = [p_yields_dict[name] for name in names_sorted]
        Canvas = TCanvas("NegativeWeightsStats", "NegativeWeightsStats", 1150, 600)
        hist_p_neg = TH1D("hist_p_neg", "", int(len(p_neg_weights_sorted)), 0, int(len(p_neg_weights_sorted)))
        hist_p_yields = TH1D("hist_p_yields", "", int(len(p_yields_sorted)), 0, int(len(p_yields_sorted)))
        hist_p_neg.SetFillColor(4)
        hist_p_neg.SetBarWidth(0.4)
        hist_p_neg.SetBarOffset(0.1)
        hist_p_yields.SetFillColor(2)
        hist_p_yields.SetBarWidth(0.4)
        hist_p_yields.SetBarOffset(0.5)
        for bin_ID, p_neg in enumerate(p_neg_weights_sorted):
            hist_p_neg.SetBinContent(bin_ID + 1, p_neg * 100)  # to get percent
        for bin_ID, p_yield in enumerate(p_yields_sorted):
            hist_p_yields.SetBinContent(bin_ID + 1, p_yield * 100)  # to get percent
        xmax = np.max([hist_p_neg.GetMaximum(), hist_p_yields.GetMaximum()])
        hist_p_neg.Draw("hbar")
        hist_p_yields.Draw("hbar same")
        hist_p_neg.GetYaxis().SetRangeUser(0, xmax * 1.2)
        for bin_ID, name in enumerate(names_sorted):
            hist_p_neg.GetXaxis().SetBinLabel(bin_ID + 1, name)
        Legend = TLegend(.68, .725, .95, .90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        Legend.AddEntry(hist_p_neg, "Number of W<0", "f")
        Legend.AddEntry(hist_p_yields, "Yields", "f")
        Legend.Draw("same")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        hist_p_neg.GetYaxis().SetTitle("Percentage of Total")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.CtrlPlotDir()) + "NegWeight_Stats." + file_extension)
        del Canvas
