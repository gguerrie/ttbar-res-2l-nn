import HelperModules.HelperFunctions as hf
from tensorflow.keras.models import load_model
from HelperModules.DataHandler import DataHandler
from HelperModules.Metrics import Metrics
from HelperModules.MessageHandler import PostProcessorMessage
from PlotterModules.AtlasLabel import DrawLabels
from PlotterModules.BasicPlotHandler import BasicPlotHandler
from ROOT import TH1D, TH2D, TCanvas, THStack, TLegend, TGraph, TMultiGraph, gPad, gROOT
from copy import copy
import json, glob
import numpy as np
import pandas as pd
from pickle import load

class GANPlotHandler(BasicPlotHandler):
    """
    Class providing plotting functionalities for GAN models.
    """
    def __init__(self,Settings):
        super().__init__(Settings)
        gROOT.SetBatch()

        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if(self.m_nFolds!=1):
            Folds = ["_Fold"+str(i) for i in range(self.m_nFolds)]
        else:
            Folds = ["0"]
        self.__Checkpoints = [glob.glob(hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.m_Dirs.ModelDir()))+Settings.get_Model().get("Name")+"_"+Settings.get_Model().get("Type")+Fold+"_checkpoint_weights*.h5") for Fold in Folds]
        self.__ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.m_Dirs.ModelDir()))+Settings.get_Model().get("Name")+"_"+Settings.get_Model().get("Type")+Fold+".h5" for Fold in Folds]
        self.__GeneratorNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.m_Dirs.ModelDir()))+Settings.get_Model().get("Name")+"_"+Settings.get_Model().get("Type")+"_Generator"+Fold+".h5" for Fold in Folds]
        self.__HistoryNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.m_Dirs.ModelDir()))+Settings.get_Model().get("Name")+"_"+Settings.get_Model().get("Type")+"_history"+Fold+".json" for Fold in Folds]
        self.__KerasModels = [load_model(modelname, compile=False) for modelname in self.__ModelNames]
        self.__GeneratorModels = [load_model(modelname, compile=False) for modelname in self.__GeneratorNames]
        self.__Scaler = load(open(hf.ensure_trailing_slash(self.m_Dirs.ModelDir())+"scaler.pkl",'rb'))

    ###########################################################################
    ############################# misc functions ##############################
    ###########################################################################
    def get_HistoryNames(self):
        return self.__HistoryNames

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self):
        """
        Wrapper function to produce all GAN plots.
        """
        self.do_PlotFakeReal()
        self.do_PlotFakeReal2D()
        self.do_HistoryPlot()

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def do_PlotFakeReal(self,nFakes=10000):
        """
        Plotting function to produce comparison plots of fake/real data.

        Keyword arguments:
        nFakes --- number of fake events to be generated
        """
        varnames = self._DataHandler__VarNames
        X_Real = self.get_sc(varnames).sample(n=nFakes)
        noise  = np.random.uniform(0,1,size=[nFakes, self.m_ModelSettings.get("LatentDim")])
        X_Fake = pd.DataFrame(data=self.__GeneratorModels[1].predict(noise), columns=varnames)
        for var in varnames:
            H_Real = TH1D("H_Real","H_Real",100,-1,1)
            H_Fake = TH1D("H_Fake","H_Fake",100,-1,1)
            H_Real.SetLineColor(2) # red
            H_Fake.SetLineColor(4) # red
            for x_r, x_f in zip(X_Real[var].values,X_Fake[var].values):
                H_Real.Fill(x_r)
                H_Fake.Fill(x_f)
            Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
            Stack = THStack()
            Stack.Add(H_Real)
            Stack.Add(H_Fake)
            Stack.Draw("hist nostack")
            Legend = TLegend(.73,.75,.85,.85)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.AddEntry(H_Real,"Real Events","F")
            Legend.AddEntry(H_Fake,"Generated Events","F")
            Legend.Draw("SAME")
            # gPad.SetLogy()
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+"RealFake_"+var+".pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+"RealFake_"+var+".png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+"RealFake_"+var+".eps")
            gROOT.FindObject("H_Real").Delete()
            gROOT.FindObject("H_Fake").Delete()
            Canvas.Close()
            del Canvas        

    def do_PlotFakeReal2D(self,nFakes=100000):
        """
        Plotting function to produce comparison plots of fake/real data.

        Keyword arguments:
        nFakes --- number of fake events to be generated
        """
        varnames = ["Variable_5", "Variable_6"]
        noise  = np.random.uniform(0,1,size=[nFakes, self.m_ModelSettings.get("LatentDim")])
        X_Fake = pd.DataFrame(data=self.__GeneratorModels[1].predict(noise), columns=varnames)
        real_hists = hf.DefineAndFill2D(
            Binning_1=[50.0, -1.0, 1.0],
            Binning_2=[50.0, -1.0, 1.0],
            Samples=self._DataHandler__Samples,
            Predictions_1=self._DataHandler__DataFrame_sc.get("Variable_5").values,
            Predictions_2=self._DataHandler__DataFrame_sc.get("Variable_6").values,
            N=self.get_Sample_Names().values,
            W=self.get_Weights().values)
        fake_hists = hf.DefineAndFill2D(
            Binning_1=[50.0, -1.0, 1.0],
            Binning_2=[50.0, -1.0, 1.0],
            Samples=self._DataHandler__Samples,
            Predictions_1=X_Fake["Variable_5"].values,
            Predictions_2=X_Fake["Variable_6"].values,
            N=np.array(["Toy_1"]*nFakes),
            W=self.get_Weights().values)
        for key,value in real_hists.items():
            if value[1][0]!="Data":
                if self.m_ModelSettings.isClassification():
                    PostProcessorMessage("Plotting 2D MC plot for "+key+".")
                    basename = self.m_ModelSettings.get("Name")+"_"+key+"_Real_2DMVA"
                elif self.m_ModelSettings.isReconstruction():
                    PostProcessorMessage("Plotting 2D MC plot for "+value[1][1]+".")
                    basename = value[1][1]+"_Real_2DMVA"
                Canvas = TCanvas("c1","c1",800,600)
                value[0].Draw("colz")
                Canvas.SetRightMargin(0.15)
                value[0].GetZaxis().SetTitle("Events")
                value[0].SetMinimum(0)
                DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="left")
                if self.m_GeneralSettings.get("StackPlot2DScale")=="Log":
                    gPad.SetLogz()
                for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"."+file_extension)
                del Canvas
            if value[1][0]=="Data":
                continue
        for key,value in fake_hists.items():
            if value[1][0]!="Data":
                if self.m_ModelSettings.isClassification():
                    PostProcessorMessage("Plotting 2D MC plot for "+key+".")
                    basename = self.m_ModelSettings.get("Name")+"_"+key+"_Fake_2DMVA"
                elif self.m_ModelSettings.isReconstruction():
                    PostProcessorMessage("Plotting 2D MC plot for "+value[1][1]+".")
                    basename = value[1][1]+"_Fake_2DMVA"
                Canvas = TCanvas("c1","c1",800,600)
                value[0].Draw("colz")
                Canvas.SetRightMargin(0.15)
                value[0].GetZaxis().SetTitle("Events")
                value[0].SetMinimum(0)
                DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="left")
                if self.m_GeneralSettings.get("StackPlot2DScale")=="Log":
                    gPad.SetLogz()
                for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"."+file_extension)
                del Canvas
            if value[1][0]=="Data":
                continue
    
    def do_HistoryPlot(self):
        """
        Function to produce a history plot containing generator and critic loss.
        """
        for i,historyname in enumerate(self.get_HistoryNames()):
            basename = self.m_ModelSettings.get("Name")+"_"+str(i)
            with open(historyname) as f:
                History = json.load(f)
                Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
                keys = History.keys()
                mg = TMultiGraph()
                if self.m_ModelSettings.isClassificationACGAN():
                    y_s = {"Discriminator Loss": np.array(list(History["d_loss"].values())),
                           "Generator Loss": np.array(list(History["g_loss"].values())),
                           "#chi^{2}/ndof": np.array(list(History["chi2/ndof"].values()))}
                if self.m_ModelSettings.isClassificationWGAN():
                    y_s = {"Critic Loss": np.array(list(History["Critic Loss"].values())),
                           "Generator Loss": np.array(list(History["Generator Loss"].values()))}
                x = np.arange(1,len(y_s["Generator Loss"])+1,1)
                graphs = {key: TGraph(x.size,x.astype(np.double),val.astype(np.double)) for key,val in y_s.items()}
                graphcolors = {"Loss (Real)": 2,
                               "Loss (Fake)": 8,
                               "Critic Loss": 2,
                               "Discriminator Loss": 2,
                               "Generator Loss": 4,
                               "Wasserstein_Distance": 1,
                               "#chi^{2}/ndof": 1}
                Legend = TLegend(.73,.75,.85,.85)
                Legend.SetBorderSize(0)
                Legend.SetTextFont(42)
                Legend.SetTextSize(0.035)
                for graph_name, graph in graphs.items():
                    graph.SetLineColor(graphcolors[graph_name])
                    graph.SetLineWidth(1)
                    mg.Add(graph)
                    Legend.AddEntry(graph, graph_name,"L")
                mg.Draw("A")
                Legend.Draw("SAME")
                # gPad.SetLogx()
                gPad.SetLogy()
                mg.SetMaximum(mg.GetHistogram().GetMaximum()*1.3)
                mg.GetYaxis().SetTitle("Loss")
                mg.GetXaxis().SetTitle("Epoch")
                DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_History.pdf")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_History.png")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_History.eps")
                Canvas.Close()
                del Canvas
