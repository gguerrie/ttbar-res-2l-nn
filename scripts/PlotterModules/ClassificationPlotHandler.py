"""
Module to handle plots for classification models
"""
import copy
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import PostProcessorMessage, WarningMessage, ErrorMessage
from HelperModules.Transfo import TransfoBinning
from HelperModules.ModelHandler import ModelHandler
from PlotterModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from PlotterModules.BasicPlotHandler import BasicPlotHandler
from sklearn.metrics import confusion_matrix, roc_curve, auc
from array import array
from ROOT import gPad, gStyle, gROOT, kRed, kBlue, TH1D, TH2D, TCanvas, THStack, TGraph, TLegend, TMultiGraph, TPad, TLine, vector


class ClassificationPlotHandler(BasicPlotHandler):
    """
    Class to handle the plots for classification models
    """

    def __init__(self, Settings):
        self.m_Settings = Settings
        super().__init__(Settings)

    ########################################################################
    ############################ misc functions ############################
    ########################################################################

    # def array_to_cpp_vector(vector_type: str, original_array) -> vector:
    #     """
    #     Convertes array to c++ style vector
    #     """
    #     result = vector(vector_type)()
    #     result += original_array
    #     return result

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self, plots=["all"]):
        """
        Wrapper function to produce all available plots in one go.
        """
        if "all" in plots or "ArchitecturePlot" in plots:
            if self.m_ModelSettings.isClassificationDNN() or self.m_ModelSettings.isReconstruction():
                self.do_ArchitecturePlot()
            if not (self.m_ModelSettings.isClassificationDNN() or self.m_ModelSettings.isReconstruction()) and "ArchitecturePlot" in plots:
                WarningMessage("You have requested architecture plots but your model configurations do not support this!")
        if "all" in plots or "PerformancePlots" in plots:
            if self.m_ModelSettings.isClassification() or self.m_ModelSettings.isReconstruction():
                self.do_PerformancePlot()
            elif not(self.m_ModelSettings.isClassificationDNN() or self.m_ModelSettings.isReconstruction()) and "PerformancePlots" in plots:
                WarningMessage("Youhave requested performance plots but your model configurations do not support this!")
        if "all" in plots or "WeightedAccuracies" in plots:
            if (self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction()):
                self.do_WeightedAccuracies()
            elif not(self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction()) and "WeightedAccuracies" in plots:
                WarningMessage("You have requested weighted Z-reco accuracy plots but your model configurations do not support this!")
        if "all" in plots or "EfficiencyPlot" in plots:
            if (self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction()) and "EfficiencyPlot" in plots:
                self.do_EfficiencyPlot()
            elif not (self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction()) and "EfficiencyPlot" in plots:
                WarningMessage("You have requested Z-reco efficiency plots but your model configurations do not support this!")
        if "all" in plots or "Stack2DPlots" in plots:
            if len(self.m_ModelSettings.get("OutputLabels")) >= 3:
                self.do_Stack2D()
            else: 
                WarningMessage("You have requested 2D stackplots but your model configurations do not support this!")
        if "all" in plots or "YieldsPlot" in plots:
            self.do_YieldsPlot()
        if "all" in plots or "StackPlots" in plots:
            self.do_StackPlots()
        if "all" in plots or "Separation1D" in plots:
            self.do_Separations1D()
        if "all" in plots or "TrainTestPlots" in plots:
            self.do_TrainTestPlots()
        if "all" in plots or "NonnegativeWeightsComparison" in plots:
            self.do_NonnegativeWeightsComparisons()
        if "all" in plots or "SoverBPlots" in plots:
            self.do_SoverBPlots()
        if "all" in plots or "SoverB2DPlots" in plots:
            if len(self.m_ModelSettings.get("OutputLabels")) >= 3:
                self.do_SoverB2DPlots()
            else:
                WarningMessage("You have requested 2D S/B and S/sqrt(B) plots but your model configurations do not support this!")
        if "all" in plots or "CorrelationPlots" in plots:
            self.do_CorrelationPlots()
        if "all" in plots or "BinaryConfusionPlots" in plots:
            self.do_BinaryConfusionPlots()
        if "all" in plots or "ROCCurvePlots" in plots:
            self.do_ROCCurvePlotsCombined()
        if "all" in plots or "AUCSummaries" in plots:
            self.do_AUCSummaries()
        if "all" in plots or "ConfusionMatrix" in plots:
            self.do_ConfusionMatrix()
        if "all" in plots or "PermutationImportances" in plots:
            self.do_PermutationImportances()

    def do_PermutationImportances(self):
        """
        Wrapper function to produce all permutation importance plots in one go.
        """
        if self.m_isBinary:
            self.do_PermutationImportance(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_PermutationImportance(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No Permutation Importance plot produced.".format(OutputLabel))

    def do_StackPlots(self):
        """
        Wrapper function to produce all stack plots in one go.
        """
        if self.m_isBinary:
            self.do_StackPlot(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                self.do_StackPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)

    def do_Separations1D(self):
        """
        Wrapper function to produce all 1D separation plots in one go.
        """
        if self.m_isBinary:
            self.do_Separation1D(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_Separation1D(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No 1D separation plot produced.".format(OutputLabel))

    def do_TrainTestPlots(self):
        """
        Wrapper function to produce all TrainTest plots in one go.
        """
        if self.m_isBinary:
            self.do_TrainTestPlot(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_TrainTestPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No K-Fold comparison produced.".format(OutputLabel))

    def do_NonnegativeWeightsComparisons(self):
        """
        Wrapper function to produce all non-negative weight comparison plots in one go.
        """
        if self.m_isBinary:
            self.do_NonnegativeWeightsComparison(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_NonnegativeWeightsComparison(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No non-negative comparison produced.".format(OutputLabel))

    def do_SoverBPlots(self):
        """
        Wrapper function to produce all S over B plots in one go.
        """
        if self.m_isBinary:
            self.do_SoverBPlot(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_SoverBPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No signal over background plots produced.".format(OutputLabel))

    def do_CorrelationPlots(self):
        """
        Wrapper function to produce all correlation plots in one go.
        """
        if self.m_isBinary:
            self.do_CorrelationPlot(OutputLabel=self.m_OutputLabels[0])
        else:
            for OutputLabel in self.m_OutputLabels:
                self.do_CorrelationPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel)

    def do_BinaryConfusionPlots(self):
        """
        Wrapper function to produce all binary confusion plots in one go.
        """
        if self.m_isBinary:
            self.do_BinaryConfusionPlot(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                self.do_BinaryConfusionPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)

    def do_ROCCurvePlotsCombined(self):
        """
        Wrapper function to produce all ROC curve plots in one go
        """
        if self.m_isBinary:
            self.do_ROCCurvePlotCombined(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                self.do_ROCCurvePlotCombined(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)

    def do_AUCSummaries(self):
        """
        Wrapper function to produce all AUCSummary plots in one go
        """
        if self.m_isBinary:
            self.do_AUCSummary(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                self.do_AUCSummary(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)

    def do_WeightedAccuracies(self):
        """
        Wrapper function to produce all Accuracy plots in one go
        """
        if self.m_isBinary:
            self.do_WeightedAccuracy(OutputLabel=self.m_OutputLabels[0], OutputID=1)
        else:
            for OutputID, OutputLabel in enumerate(self.m_OutputLabels):
                if OutputID in self.get_Labels().values:
                    self.do_WeightedAccuracy(PredictionColumn=OutputLabel, OutputLabel=OutputLabel, OutputID=OutputID)
                else:
                    WarningMessage("No event in class {}: No accuracy plots produced.".format(OutputLabel))

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################

    def do_Stack2D(self):
        """
        Function to produce a 2D stacked plot for a 3-class multi-class classifier.
        """
        OutputLabels = self.m_ModelSettings.get("OutputLabels")
        Axis_indeces = np.unique([s.get("TrainLabel") for s in self.m_Samples if s.get("TrainLabel")>=0 and s.get("Type")!="Signal"])
        for i in range (0, len(Axis_indeces)-1):
            for j in range (i+1, len(Axis_indeces)):
                # Defining histograms filling them and scaling them to unity
                if self.m_ModelSettings.isClassification():
                    hists = hf.DefineAndFill2D(
                        Binning_1=[50.0, 0.0, 1.0],
                        Binning_2=[50.0, 0.0, 1.0],
                        Samples=self.m_Samples,
                        Predictions_1=self.m_DataFrame.get(OutputLabels[Axis_indeces[i]]).values,
                        Predictions_2=self.m_DataFrame.get(OutputLabels[Axis_indeces[j]]).values,
                        N=self.get_Sample_Names().values,
                        W=self.get_Weights().values)
                elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
                    hists = hf.DefineAndFill2D(
                        Binning_1=[50.0, 0.0, 1.0],
                        Binning_2=[50.0, 0.0, 1.0],
                        Samples=self.m_OutputLabels,
                        Predictions_1=self.m_DataFrame.get(OutputLabels[Axis_indeces[i]]).values,
                        Predictions_2=self.m_DataFrame.get(OutputLabels[Axis_indeces[j]]).values,
                        N=self.get_Labels().values,
                        W=self.m_DataFrame[self.m_WEIGHTNAME].values,
                        SamplesAreClasses=True)

                for key, value in hists.items():
                    if value[1][0] == "Data":
                        continue
                    if value[1][0] != "Data":
                        if self.m_ModelSettings.isClassification():
                            PostProcessorMessage("Plotting 2D MC plot for " + key + ".")
                            basename = "%s_%s_2DMVA_%s_%s"%(self.m_ModelSettings.get("Name"),
                                                            key,
                                                            OutputLabels[Axis_indeces[i]],
                                                            OutputLabels[Axis_indeces[j]])
                        elif self.m_ModelSettings.isReconstruction():
                            PostProcessorMessage("Plotting 2D MC plot for " + value[1][1] + ".")
                            basename = "%s_%s_2DMVA_%s_%s"%(self.m_ModelSettings.get("Name"),
                                                            value[1][1],
                                                            OutputLabels[Axis_indeces[i]],
                                                            OutputLabels[Axis_indeces[j]])
                        Canvas = TCanvas("c1", "c1", 800, 600)
                        value[0].Draw("colz")
                        Canvas.SetRightMargin(0.15)
                        value[0].GetZaxis().SetTitle("Events")
                        value[0].GetXaxis().SetTitle(OutputLabels[Axis_indeces[0]] + " Score")
                        value[0].GetYaxis().SetTitle(OutputLabels[Axis_indeces[1]] + " Score")
                        value[0].SetMinimum(0)
                        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="right_3")
                        if self.m_GeneralSettings.get("StackPlot2DScale") == "Log":
                            gPad.SetLogz()
                        if self.m_ModelSettings.isClassification():
                            CustomLabel(0.4, 0.7, key + " Response")
                        if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
                            CustomLabel(0.4, 0.7, value[1][1] + " Response")
                        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "." + file_extension)
                        del Canvas
                # print(hists)
                # for key in hists.keys():
                #     print(key)
                #     gROOT.FindObject(key).Delete()

    def do_Separation1D(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to generate a one dimensional separation plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")

        PostProcessorMessage("Plotting 1D separation plot for " + OutputLabel + " classifier.")
        Binning = self.m_ModelSettingsVariables[OutputLabel].get_VarBinning()
        if self.m_ModelSettings.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_OutputLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key, value in hists.items():
            if value[1]["Type"] == "Data":
                continue
            if value[1]["TrainingLabel"] == OutputID and value[1]["Type"] != "Data":
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"] != OutputID and value[1]["Type"] != "Data":
                temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        Stack = THStack()
        s_hist.SetLineColor(2)  # red
        b_hist.SetLineColor(4)  # blue
        s_hist.Scale(1. / s_hist.Integral())
        b_hist.Scale(1. / b_hist.Integral())
        Stack.Add(s_hist)
        Stack.Add(b_hist)
        Canvas = TCanvas("c1", "c1", 800, 600)
        Legend = TLegend(.63, .75, .85, .85)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        if self.m_ModelSettings.isClassification():
            if self.m_isBinary:
                Legend.AddEntry(s_hist, "Signal", "L")
            else:
                Legend.AddEntry(s_hist, OutputLabel, "L")
            Legend.AddEntry(b_hist, "Background", "L")
        Stack.Draw("hist nostack")
        Stack.GetYaxis().SetTitle("Fraction of Events")
        Stack.GetXaxis().SetTitle(OutputLabel + " Classifier")
        # Let's make sure we have enough space for the labels and the legend
        Stack.SetMaximum(Stack.GetMaximum() * self.m_SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        CustomLabel(0.2, 0.7, "Separation=%.2f%%" % (hf.Separation(s_hist, b_hist) * 100))  # We want to get percent
        if self.m_GeneralSettings.get("SeparationPlotScale") == "Log":
            gPad.SetLogy()
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Separation_" + OutputLabel + "." + file_extension)
        # Clean up
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_SoverBPlot(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to generate S/B and a S/sqrt(B) plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        PostProcessorMessage("Plotting signal over background plot.")

        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        Binning = self.m_ModelSettingsVariables[OutputLabel].get_VarBinning()
        if self.m_ModelSettings.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_ModelSettings.get("OutputLabels"),
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        Canvas = TCanvas("c1", "c1", 800, 600)
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key, value in hists.items():
            if value[1]["TrainingLabel"] == OutputID and value[1]["Type"] != "Data":
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"] != OutputID and value[1]["Type"] != "Data":
                temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        edges = array("d", [s_hist.GetBinLowEdge(bin_id) for bin_id in range(1, s_hist.GetNbinsX() + 2)])
        s_over_b_hist = TH1D("", "", int(len(edges) - 1), edges)
        s_over_sqrtb_hist = TH1D("", "", int(len(edges) - 1), edges)
        s_over_sqrtb_hist.SetLineColor(4)  # blue
        s_over_b_hist.SetLineColor(2)  # red
        for b in range(1, s_hist.GetXaxis().GetNbins() + 1):
            sum_s_bin_cont = 0
            sum_b_bin_cont = 0
            sum_s_bin_cont = sum([s_hist.GetBinContent(j) for j in range(b, s_hist.GetXaxis().GetNbins() + 1)])
            sum_b_bin_cont = sum([b_hist.GetBinContent(j) for j in range(b, b_hist.GetXaxis().GetNbins() + 1)])
            if sum_s_bin_cont < 0 or sum_b_bin_cont <= 0:
                sum_s_bin_cont = 0
                WarningMessage("Detected negative bin yields for bin {}. Ratio was set to zero".format(b))
            else:
                s_over_b_hist.SetBinContent(b, sum_s_bin_cont / sum_b_bin_cont)
                s_over_sqrtb_hist.SetBinContent(b, sum_s_bin_cont / np.sqrt(sum_b_bin_cont))
        s_over_b_hist.Draw("hist")
        if not self.m_isBinary:
            s_over_b_hist.GetXaxis().SetTitle(OutputLabel + " Classifier")
            s_over_sqrtb_hist.GetXaxis().SetTitle(OutputLabel + " Classifier")
        else:
            s_over_b_hist.GetXaxis().SetTitle("DNN Output")
            s_over_sqrtb_hist.GetXaxis().SetTitle("DNN Output")
        s_over_b_hist.GetYaxis().SetTitle("S/B")
        s_over_sqrtb_hist.GetYaxis().SetTitle("S/#sqrt{B}")

        s_over_b_hist.SetMaximum(s_over_b_hist.GetMaximum() * self.m_SCALEFACTOR)
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + OutputLabel + "_SoB_Binary." + file_extension)
        s_over_sqrtb_hist.Draw()
        s_over_sqrtb_hist.SetMaximum(s_over_sqrtb_hist.GetMaximum() * self.m_SCALEFACTOR)
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + OutputLabel + "_SosB_Binary." + file_extension)

        # Doing some cleanup
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_PermutationImportance(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Generate a horizontal bar chart wich displays the permutation importance.


        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        if self.m_isBinary:
            ColumnID = 0
        else:
            ColumnID = OutputID
        PostProcessorMessage("Plotting permutation importance for " + OutputLabel + " classifier.")
        basename = self.m_ModelSettings.get("Name") + "_"
        Canvas = TCanvas("PermutationImportance", "PermutationImportance", 800, 800)
        auc_list_complete = []
        X = self.get_TestInputs(Fold=0).values
        Y_Pred = self.get_TestPredictions(ColumnID=ColumnID, Fold=0)
        Y = self.get_TestLabels(Fold=0).values
        W = self.get_TestWeights(Fold=0).values
        W[W < 0] = 0
        Y[Y != OutputID] = -99
        Y[Y == OutputID] = 1
        Y[Y == -99] = 0
        fpr, tpr, _ = roc_curve(np.array(Y), np.array(Y_Pred), sample_weight=np.array(W))
        if len(fpr) < self.m_GeneralSettings.get("ROCSampling"):
            ErrorMessage("Only %d fpr values are available but 'ROCSampling' was set to %d. You need to have a look at this!"%(len(fpr), self.m_GeneralSettings.get("ROCSampling")))
        sampling = int(len(fpr) / self.m_GeneralSettings.get("ROCSampling"))
        nominal_auc = auc(fpr[::sampling], tpr[::sampling])
        for i in range(len(self.m_VarNames)):
            auc_list = []
            shuffled = copy.deepcopy(X)  # deep copy because shuffle shuffles in place
            mhandler = ModelHandler(self.m_ModelSettings, self.m_Dirs)
            for _ in range(self.m_GeneralSettings.get("PermImportanceShuffles")):
                np.random.shuffle(shuffled[:, i])
                shuffled_df = pd.DataFrame(shuffled, columns=self.m_VarNames)
                if self.m_isBinary:  # binary case
                    Y_Pred = mhandler.predict(self.get_ModelObjects()[0], shuffled_df, self.m_Settings)
                else:  # multi-class case
                    Y_Pred = mhandler.predict(self.get_ModelObjects()[0], shuffled_df, self.m_Settings)[:, OutputID]
                fpr, tpr, _ = roc_curve(np.array(Y), np.array(Y_Pred), sample_weight=np.array(W))
                auc_list.append((nominal_auc - auc(fpr[::sampling], tpr[::sampling])) / nominal_auc)
            auc_list_complete.append(auc_list)

        # Calculate mean:
        auc_list = np.mean(auc_list_complete, axis=1)
        # Sort the AUC list and keep track of indece to apply them on Variables later
        indece = sorted(range(len(auc_list)), key=lambda k: auc_list[k], reverse=True)
        auc_sorted = np.array(auc_list)[indece]
        Variables_sorted = np.array(self.m_VarLabels)[indece]
        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram", "", int(len(auc_sorted)), 0, len(auc_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25)  # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID, mean in enumerate(auc_sorted):
            Histogram.SetBinContent(bin_ID + 1, mean)
        Histogram.Draw("hbar")
        Histogram.GetYaxis().SetLabelSize(0.04)
        Histogram.GetXaxis().SetLabelSize(0.015 * 40 / len(auc_list))
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        CustomLabel(0.6, 0.85, OutputLabel + " Classifier")
        for bin_ID, var in enumerate(Variables_sorted):
            Histogram.GetXaxis().SetBinLabel(bin_ID + 1, var.replace(" [GeV]", ""))
        if self.m_GeneralSettings.get("PermImportancePlotScale") == "Log":
            gPad.SetLogx()
        gPad.SetGridx(1)
        Canvas.SetRightMargin(0.1)
        Canvas.SetLeftMargin(0.25)
        Histogram.SetMaximum(1.0)
        Histogram.GetYaxis().SetTitle("(AUC_{nom.}-AUC)/AUC_{nom.}")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "Permutation_Importance" + OutputLabel + "." + file_extension)
        del Canvas

    def do_TrainTestPlot(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Generate a plot with superimposed test and train results for signal and background.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        if self.m_isBinary:
            ColumnID = 0
        else:
            ColumnID = OutputID
        basename = "TrainTest_" + self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        ModelBinning = self.m_ModelSettingsVariables[OutputLabel].get_VarBinning()

        for i in range(self.m_nFolds):
            PostProcessorMessage("Plotting K-Fold comparison for " + OutputLabel + " classifier, Fold=" + str(i))
            Canvas = TCanvas("c1", "c1", 800, 600)
            p_train = self.get_TrainPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_train = self.get_TrainLabels(Fold=i, returnNonZeroLabels=False).values
            w_train = self.get_TrainWeights(Fold=i, returnNonZeroLabels=False).values
            p_test = self.get_TestPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_test = self.get_TestLabels(Fold=i, returnNonZeroLabels=False).values
            w_test = self.get_TestWeights(Fold=i, returnNonZeroLabels=False).values
            if self.m_ModelSettings.get("BinningOptimisation") != "None":
                Train_Signal_Hist = TH1D("Train_Signal_Hist_temp" + str(i), "", 1000, ModelBinning[0], ModelBinning[-1])
                Test_Signal_Hist = TH1D("Test_Signal_Hist_temp" + str(i), "", 1000, ModelBinning[0], ModelBinning[-1])
                Train_Background_Hist = TH1D("Train_Background_Hist_temp" + str(i), "", 1000, ModelBinning[0], ModelBinning[-1])
                Test_Background_Hist = TH1D("Test_Background_Hist_temp" + str(i), "", 1000, ModelBinning[0], ModelBinning[-1])
                hf.FillHist(Train_Signal_Hist, p_train[y_train == OutputID], w_train[y_train == OutputID])
                hf.FillHist(Test_Signal_Hist, p_test[y_test == OutputID], w_test[y_test == OutputID])
                hf.FillHist(Train_Background_Hist, p_train[y_train != OutputID], w_train[y_train != OutputID])
                hf.FillHist(Test_Background_Hist, p_test[y_test != OutputID], w_test[y_test != OutputID])
                if self.m_ModelSettings.get("BinningOptimisation") == "TransfoD_symmetric":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(len(ModelBinning) / 2), zs=int(len(ModelBinning) / 2))
                elif self.m_ModelSettings.get("BinningOptimisation") == "TransfoD_flatS":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(0), zs=int(len(ModelBinning)))
                elif self.m_ModelSettings.get("BinningOptimisation") == "TransfoD_flatB":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(len(ModelBinning)), zs=0)
                optimised_binning = Transfo.TransfoD()
                Train_Signal_Hist = Train_Signal_Hist.Rebin(len(optimised_binning) - 1, "Train_Signal_Hist" + str(i), optimised_binning)
                Test_Signal_Hist = Test_Signal_Hist.Rebin(len(optimised_binning) - 1, "Test_Signal_Hist" + str(i), optimised_binning)
                Train_Background_Hist = Train_Background_Hist.Rebin(len(optimised_binning) - 1, "Train_Background_Hist" + str(i), optimised_binning)
                Test_Background_Hist = Test_Background_Hist.Rebin(len(optimised_binning) - 1, "Test_Background_Hist" + str(i), optimised_binning)
            else:
                Train_Signal_Hist = TH1D("Train_Signal_Hist" + str(i), "", int(len(ModelBinning) - 1), ModelBinning)
                Test_Signal_Hist = TH1D("Test_Signal_Hist" + str(i), "", int(len(ModelBinning) - 1), ModelBinning)
                Train_Background_Hist = TH1D("Train_Background_Hist" + str(i), "", int(len(ModelBinning) - 1), ModelBinning)
                Test_Background_Hist = TH1D("Test_Background_Hist" + str(i), "", int(len(ModelBinning) - 1), ModelBinning)
                hf.FillHist(Train_Signal_Hist, p_train[y_train == OutputID], w_train[y_train == OutputID])
                hf.FillHist(Test_Signal_Hist, p_test[y_test == OutputID], w_test[y_test == OutputID])
                hf.FillHist(Train_Background_Hist, p_train[y_train != OutputID], w_train[y_train != OutputID])
                hf.FillHist(Test_Background_Hist, p_test[y_test != OutputID], w_test[y_test != OutputID])

            Legend = TLegend(.63, .80 - 4 * 0.025, .85, .90)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.SetFillStyle(0)

            if self.m_isBinary:
                Legend.AddEntry(Train_Signal_Hist, "Signal (Train)", "p")
                Legend.AddEntry(Test_Signal_Hist, "Signal (Test)", "f")
            else:
                Legend.AddEntry(Train_Signal_Hist, OutputLabel + " (Train)", "p")
                Legend.AddEntry(Test_Signal_Hist, OutputLabel + " (Test)", "f")
            Legend.AddEntry(Train_Background_Hist, "Background (Train)", "p")
            Legend.AddEntry(Test_Background_Hist, "Background (Test)", "f")
            Train_Signal_Hist.Scale(1. / Train_Signal_Hist.Integral())
            Test_Signal_Hist.Scale(1. / Test_Signal_Hist.Integral())
            Train_Background_Hist.Scale(1. / Train_Background_Hist.Integral())
            Test_Background_Hist.Scale(1. / Test_Background_Hist.Integral())
            KS_Signal = Train_Signal_Hist.KolmogorovTest(Test_Signal_Hist)
            KS_Background = Train_Background_Hist.KolmogorovTest(Test_Background_Hist)

            maximum = max(Train_Signal_Hist.GetMaximum(), Test_Signal_Hist.GetMaximum(), Train_Background_Hist.GetMaximum(), Test_Background_Hist.GetMaximum())

            Train_Signal_Hist.SetLineColor(2)  # red
            Test_Signal_Hist.SetLineColor(2)  # red
            Train_Background_Hist.SetLineColor(4)  # blue
            Test_Background_Hist.SetLineColor(4)  # blue

            Test_Signal_Hist.SetMarkerSize(0)
            Test_Background_Hist.SetMarkerSize(0)
            Train_Signal_Hist.SetMarkerColor(2)
            Train_Background_Hist.SetMarkerColor(4)

            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            sig_ratio = hf.createRatio(Train_Signal_Hist, Test_Signal_Hist)
            bkg_ratio = hf.createRatio(Train_Background_Hist, Test_Background_Hist)
            sig_errband = sig_ratio.Clone("sig_errband")
            bkg_errband = bkg_ratio.Clone("bkg_errband")
            maxerr = 0.55
            for binID in range(sig_errband.GetNbinsX() + 1):
                if sig_errband.GetBinContent(binID) == 0:
                    sig_errband.SetBinContent(binID, 1)
                    sig_errband.SetBinError(binID, maxerr)  # completely fill the ratio plot
                if bkg_errband.GetBinContent(binID) == 0:
                    bkg_errband.SetBinContent(binID, 1)
                    bkg_errband.SetBinError(binID, maxerr)  # completely fill the ratio plot
            sig_errband.SetFillColor(kRed)
            sig_errband.SetFillStyle(3004)
            sig_errband.SetMarkerSize(0)
            bkg_errband.SetFillColor(kBlue)
            bkg_errband.SetFillStyle(3005)
            bkg_errband.SetMarkerSize(0)

            sig_ratio.SetMaximum(1 + maxerr)
            sig_ratio.SetMinimum(1 - maxerr)
            bkg_ratio.SetMaximum(1 + maxerr)
            bkg_ratio.SetMinimum(1 - maxerr)
            sig_ratio.SetMarkerSize(0)
            bkg_ratio.SetMarkerSize(0)
            # draw everything in first pad
            pad1.cd()
            Test_Signal_Hist.Draw("hist e")
            Test_Signal_Hist.GetYaxis().SetTitle("Fraction of Events")
            Test_Signal_Hist.GetXaxis().SetTitle("DNN Output")
            Test_Signal_Hist.SetMaximum(maximum * 1.5)
            Test_Background_Hist.Draw("hist e SAME")
            Train_Signal_Hist.Draw("e1X0 SAME")
            Train_Background_Hist.Draw("e1X0 SAME")
            if self.m_GeneralSettings.get("ATLASLabel").lower() != "None":
                ATLASLabel(0.175, 0.85, self.m_GeneralSettings.get("ATLASLabel"))
            CustomLabel(0.175, 0.8, self.m_GeneralSettings.get("CMLabel"))
            CustomLabel(0.175, 0.75, "KS Test: Sig.(Bkg.) P=%.3f (%.3f)" % (KS_Signal, KS_Background))
            if self.m_GeneralSettings.get("CustomLabel") != "":
                CustomLabel(0.2, 0.70, self.m_GeneralSettings.get("CustomLabel"))
            Legend.Draw("SAME")

            # to avoid clipping the bottom zero, redraw a small axis
            axis = Test_Signal_Hist.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            sig_ratio.Draw("hist")
            bkg_ratio.Draw("hist same")
            sig_errband.Draw("e2 same")
            bkg_errband.Draw("e2 same")
            line = TLine(0, 1, 1, 1)
            line.SetLineStyle(2)  # dashed
            line.Draw("Same")
            sig_ratio.GetXaxis().SetTitle(OutputLabel + " Classifier")
            sig_ratio.GetYaxis().SetTitle("Train/Test")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
            if self.m_GeneralSettings.get("KSPlotScale") == "Log":
                gPad.SetLogy()
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "" + OutputLabel + "_" + str(i) + "." + file_extension)
            del Train_Signal_Hist, Test_Signal_Hist, Train_Background_Hist, Test_Background_Hist, sig_ratio, bkg_ratio, sig_errband, bkg_errband
            Canvas.Close()
            del Canvas

    def do_CorrelationPlot(self, PredictionColumn=None, OutputLabel=None):
        """
        Function to produce a correlation plot between inputs and classifier outputs.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        PostProcessorMessage("Plotting correlation plot for " + OutputLabel + " classifier.")
        correlations = [[] for i in range(len(self.m_VarNames))]
        X = self.get(self.m_VarNames).values
        if self.m_ModelSettings.isClassification():
            N = self.m_DataFrame[self.m_SAMPLENAME].values
        if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            N = self.get_Labels().values
            L = {i: n for i, n in enumerate(self.m_ModelSettings.get("OutputLabels"))}
            L[-2] = "None"
        P = self.get(PredictionColumn).values
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1", "c1", 800, 600)
        for i, var in enumerate(self.m_VarNames):
            for n in np.unique(N):
                if n == "Data":
                    continue
                correlations[i].append(np.corrcoef(X[N == n, i], P[N == n])[0][1])
        hist = TH2D("correlationhist", "", len(correlations), 0, len(correlations), len(correlations[0]), 0, len(correlations[0]))
        for xbin in range(np.array(correlations).shape[0]):
            for ybin in range(np.array(correlations).shape[1]):
                hist.SetBinContent(xbin + 1, ybin + 1, correlations[xbin][ybin])
        hist.Draw("colz text45")
        Yaxis = hist.GetYaxis()
        Xaxis = hist.GetXaxis()
        if self.m_ModelSettings.isClassification():
            for i, Sample_Name in enumerate([n for n in np.unique(N) if n != "Data"]):
                Yaxis.SetBinLabel(i + 1, Sample_Name)
        if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            for i, Class_Name in enumerate([n for n in np.unique(N)]):
                Yaxis.SetBinLabel(i + 1, L[Class_Name])

        for i, var in enumerate(self.m_VarLabels):
            Xaxis.SetBinLabel(i + 1, var.replace("[GeV]", ""))
        Xaxis.LabelsOption("v")
        hist.SetMaximum(1.0)
        hist.SetMinimum(-1.0)
        Canvas.SetRightMargin(0.15)
        Canvas.SetBottomMargin(0.25)
        hist.GetZaxis().SetTitle("Correlation")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + "Correlation_" + OutputLabel + "." + file_extension)
        Canvas.Close()
        del Canvas

    def do_BinaryConfusionPlot(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Generate a 2D colour plot for all samples to study samples that lead to confusion

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- LaAccuracyto the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        PostProcessorMessage("Plotting binary confusion plot for " + OutputLabel + " classifier.")
        basename = self.m_ModelSettings.get("Name") + "_"
        Binning = self.m_ModelSettingsVariables[OutputLabel].get_VarBinning()
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1", "c1", 800, 600)

        if self.m_ModelSettings.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.m_OutputLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                     OutputID=OutputID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.m_ModelSettings.get("BinningOptimisation"),
                                     underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                     overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        edges = array("d", [list(hists.values())[0][0].GetBinLowEdge(bin_id) for bin_id in range(1, list(hists.values())[0][0].GetNbinsX() + 2)])
        if self.m_ModelSettings.isClassification():
            N_dict = {n: i for i, n in enumerate(np.unique(self.get_Sample_Names().values))}
            W_dict = {n: 1 / np.sum(self.get_Weights().values[self.get_Sample_Names().values == n]) for n in np.unique(self.get_Sample_Names().values)}
            hist = TH2D("hist", "hist", int(len(edges) - 1), edges, len(N_dict), 0, len(N_dict))
            for y_pred, n, w in zip(self.get(PredictionColumn).values, self.get_Sample_Names().values, self.get_Weights().values):
                hist.Fill(y_pred, N_dict[n], w * W_dict[n] * 100)
        elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            N_dict = {n: i + 1 for i, n in enumerate(self.m_ModelSettings.get("OutputLabels"))}  # keep Bin 0 for "no class"
            N_dict["None"] = 0
            L_dict = {n: i for i, n in enumerate(np.unique(self.get_Labels()))}
            W_dict = {n: 1 / np.sum(self.get_Weights().values[self.get_Labels().values == n]) for n in np.unique(self.get_Labels())}
            hist = TH2D("hist", "hist", int(len(edges) - 1), edges, len(N_dict), 0, len(N_dict))
            for y_pred, n, w in zip(self.get(PredictionColumn).values, self.get_Labels().values, self.get_Weights().values):
                hist.Fill(y_pred, L_dict[n], w * W_dict[n] * 100)
        Yaxis = hist.GetYaxis()
        for k, v in N_dict.items():
            Yaxis.SetBinLabel(v + 1, k)
        hist.Draw("colz")
        Canvas.SetRightMargin(0.15)
        hist.GetZaxis().SetTitle("Fraction of Events [%]")
        hist.GetXaxis().SetTitle(OutputLabel + " Classifier Output")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        if self.m_GeneralSettings.get("ConfusionPlotScale") == "Log":
            gPad.SetLogz()
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "Confusion_" + OutputLabel + "." + file_extension)
        Canvas.Close()
        del Canvas

    def do_ROCCurvePlotCombined(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        if self.m_isBinary:
            ColumnID = 0
        else:
            ColumnID = OutputID

        Canvas = TCanvas("ROCCurvePlotCombined", "ROCCurvePlotCombined", 800, 600)
        Legend = TLegend(.63, .75 - 0.04 * self.m_nFolds, .85, .85)  # Legend
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        mg = TMultiGraph()
        TrainGraphs, TestGraphs, TrainAUCs, TestAUCs = list(), list(), list(), list()
        PostProcessorMessage("Plotting ROC curve for " + OutputLabel + " classifier")
        basename = self.m_ModelSettings.get("Name") + "_"
        for i in range(self.m_nFolds):
            test_mvaTargets = np.array((self.get_TestLabels(Fold=i, returnNonZeroLabels=True).values == OutputID).tolist(), dtype=bool)
            train_mvaTargets = np.array((self.get_TrainLabels(Fold=i, returnNonZeroLabels=True).values == OutputID).tolist(), dtype=bool)
            test_mvaValues = array('f', self.get_TestPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=True).tolist())
            train_mvaValues = array('f', self.get_TrainPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=True).tolist())
            W_test_tmp = self.get_TestWeights(Fold=i, returnNonZeroLabels=True).values
            W_train_tmp = self.get_TrainWeights(Fold=i, returnNonZeroLabels=True).values
            test_mvaWeights = array('f', W_test_tmp)
            train_mvaWeights = array('f', W_train_tmp)
            train_fpr, train_tpr, _ = roc_curve(train_mvaTargets, train_mvaValues, sample_weight=train_mvaWeights)
            test_fpr, test_tpr, _ = roc_curve(test_mvaTargets, test_mvaValues, sample_weight=test_mvaWeights)
            if len(train_fpr) < self.m_GeneralSettings.get("ROCSampling"):
                ErrorMessage("Only %d train_fpr values are available but 'ROCSampling' was set to %d. You need to have a look at this!"%(len(train_fpr), self.m_GeneralSettings.get("ROCSampling")))
            train_sampling = int(len(train_fpr) / self.m_GeneralSettings.get("ROCSampling"))
            test_sampling = int(len(test_fpr) / self.m_GeneralSettings.get("ROCSampling"))
            train_auc = auc(train_fpr[::train_sampling], train_tpr[::train_sampling])
            test_auc = auc(test_fpr[::test_sampling], test_tpr[::test_sampling])
            TrainCurve = TGraph(len(train_fpr[::train_sampling]), array('f', train_fpr[::train_sampling]), array('f', train_tpr[::train_sampling]))
            TestCurve = TGraph(len(test_fpr[::test_sampling]), array('f', test_fpr[::test_sampling]), array('f', test_tpr[::test_sampling]))
            TrainGraphs.append(copy.deepcopy(TrainCurve))
            TestGraphs.append(copy.deepcopy(TestCurve))
            TrainAUCs.append(train_auc)
            TestAUCs.append(test_auc)
            Legend.AddEntry(TrainGraphs[i], "Training %d (AUC=%.3f)" % (i + 1, TrainAUCs[i]), "L")
            Legend.AddEntry(TestGraphs[i], "Testing %d (AUC=%.3f)" % (i + 1, TestAUCs[i]), "L")
            TrainGraphs[i].SetLineColor(kBlue - 10 + i * 2)  # blue(ish), we subtract 10 to use the "full" colour wheel
            TestGraphs[i].SetLineColor(kRed - 10 + i * 2)  # red(ish), we subtract 10 to use the "full" colour wheel

            mg.Add(TrainGraphs[i])
            mg.Add(TestGraphs[i])

        # We need some space for the labels
        mg.GetYaxis().SetRangeUser(0., 1.7)
        mg.GetXaxis().SetRangeUser(0., 1.)
        # Setting axis titles
        mg.GetYaxis().SetTitle("True Positive Rate")
        mg.GetXaxis().SetTitle("False Positive Rate")
        mg.Draw("AL")
        Legend.Draw("Same")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        CustomLabel(0.2, 0.70, OutputLabel)
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "ROCCurve_Combined_" + OutputLabel + "." + file_extension)
        Canvas.Close()
        del Canvas

    def do_AUCSummary(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        OutputLabel       --- Name of the class itself
        OutputID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")

        Canvas = TCanvas("AUCSummary", "AUCSummary", 800, 600)
        gStyle.SetPaintTextFormat(".4f")

        # get rid of data
        P = self.get(PredictionColumn).values[self.get_Sample_Types().values != "Data"]
        W = self.get_Weights().values[self.get_Sample_Types().values != "Data"]
        W[W < 0] = 0
        if self.m_ModelSettings.isClassification():
            N = self.get_Sample_Names().values[self.get_Sample_Types().values != "Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values != "Data"]
            T = self.get_Sample_Types().values[self.get_Sample_Types().values != "Data"]
        if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            N = self.get_Labels().values[self.get_Sample_Types().values != "Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values != "Data"]
        if OutputLabel is None:
            NT_dict = dict(zip(N, T))
            Background = [n for n, t in NT_dict.items() if t == "Background"]
            Y[Y != 1] = 0
            AUC = []
            for bkg in Background:
                mvaValues = np.array(P[(N == bkg) | (T == "Signal")])
                mvaTargets = np.array(Y[(N == bkg) | (T == "Signal")])
                mvaWeights = np.array(W[(N == bkg) | (T == "Signal")])
                fpr, tpr, _ = roc_curve(mvaTargets, mvaValues, sample_weight=mvaWeights)
                if len(fpr) < self.m_GeneralSettings.get("ROCSampling"):
                    ErrorMessage("Only %d fpr values are available but 'ROCSampling' was set to %d. You need to have a look at this!"%(len(fpr), self.m_GeneralSettings.get("ROCSampling")))
                sampling = int(len(fpr) / self.m_GeneralSettings.get("ROCSampling"))
                AUC.append(auc(fpr[::sampling], tpr[::sampling]))
            PostProcessorMessage("Plotting AUC summary.")
        else:
            if self.m_ModelSettings.isClassification():
                NY_dict = dict(zip(N, Y))
                NT_dict = dict(zip(N, T))
                Background = [n for n, y in NY_dict.items() if y != OutputID and NT_dict[n] != "Data"]
            elif self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
                Name_dict = {Class: OutputLabel for Class, OutputLabel in enumerate(self.m_ModelSettings.get("OutputLabels"))}
                Name_dict[-2] = "None"
                Background = np.unique([y for y in Y if y != OutputID])
            Y[Y != OutputID] = -99
            Y[Y == OutputID] = 1
            Y[Y == -99] = 0
            AUC = []
            for bkg in Background:
                mvaValues = np.array(P[(N == bkg) | (Y == 1)])
                mvaTargets = np.array(Y[(N == bkg) | (Y == 1)])
                mvaWeights = np.array(W[(N == bkg) | (Y == 1)])
                fpr, tpr, _ = roc_curve(mvaTargets, mvaValues, sample_weight=mvaWeights)
                if len(fpr) < self.m_GeneralSettings.get("ROCSampling"):
                    ErrorMessage("Only %d fpr values are available but 'ROCSampling' was set to %d. You need to have a look at this!"%(len(fpr), self.m_GeneralSettings.get("ROCSampling")))
                sampling = int(len(fpr) / self.m_GeneralSettings.get("ROCSampling"))
                AUC.append(auc(fpr[::sampling], tpr[::sampling]))
            PostProcessorMessage("Plotting AUC summary for " + OutputLabel + " classifier.")
        # Sort the AUC list and keep track of indece
        indece = sorted(range(len(AUC)), key=lambda k: AUC[k], reverse=True)
        AUC_sorted = np.array(AUC)[indece]
        Background_sorted = np.array(Background)[indece]

        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram", "", int(len(AUC_sorted)), 0, len(AUC_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25)  # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID, mean in enumerate(AUC_sorted):
            Histogram.SetBinContent(bin_ID + 1, mean)
        Histogram.Draw("hist Text")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="left")
        for bin_ID, _ in enumerate(Background_sorted):
            if self.m_ModelSettings.isClassification():
                Histogram.GetXaxis().SetBinLabel(bin_ID + 1, str(Background_sorted[bin_ID]))
            if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
                Histogram.GetXaxis().SetBinLabel(bin_ID + 1, Name_dict[Background_sorted[bin_ID]])
        Histogram.GetYaxis().SetTitle("AUC")
        Histogram.GetXaxis().SetTitle("Background")
        Histogram.SetMaximum(1.3)
        Histogram.SetMinimum(0.5)
        CustomLabel(0.2, 0.7, OutputLabel + " Classifier")  # To clarify the classifier in multi-class cases
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_AUCSummary_" + OutputLabel + "." + file_extension)
        del Canvas

    def do_WeightedAccuracy(self, PredictionColumn=None, OutputLabel=None, OutputID=None):
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        Categories = self.get_Sample_Names()
        Pred = self.get_Predictions(self.m_ModelSettings.get("OutputLabels")).values
        Labels = self.get_Labels().values
        Weights = self.get_Weights().values
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        PostProcessorMessage("Plotting Accuracy plot for " + OutputLabel + " classifier.")
        accuracy = {}
        precision = {}
        recall = {}
        for Category in np.unique(Categories):
            Y = Pred[Categories == Category]
            N = Labels[Categories == Category]
            W = Weights[Categories == Category]
            TP = FP = FN = TN = noclass = 0
            for y, n, w in zip(Y, N, W):
                class_pred = np.argmax(y)
                if class_pred == OutputID and n == OutputID:
                    TP += w
                elif class_pred == OutputID and n != OutputID:
                    FP += w
                elif class_pred != OutputID and n == OutputID:
                    FN += w
                elif class_pred != OutputID and n != OutputID and n != -2:
                    TN += w
                elif n == -2:
                    noclass += w
            accuracy[Category] = (TP + TN) / (noclass + TP + TN + FP + FN)
            if TP != 0 or FP != 0:
                precision[Category] = TP / (TP + FP)
            else:
                precision[Category] = 0
            if TP != 0 or FN != 0:
                recall[Category] = TP / (TP + FN)
            else:
                recall[Category] = 0
        Hist_Acc = TH1D("HistAcc" + OutputLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Prec = TH1D("HistPrec" + OutputLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Rec = TH1D("HistRec" + OutputLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Acc.SetFillColor(4)  # blue
        Hist_Prec.SetFillColor(2)  # red
        Hist_Rec.SetFillColor(3)  # green
        Canvas = TCanvas("Accuracy", "Accuracy", 800, 600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Category in zip(range(1, len(accuracy) + 1), accuracy.keys()):
            Hist_Acc.SetBinContent(binID, accuracy[Category])
            Hist_Prec.SetBinContent(binID, precision[Category])
            Hist_Rec.SetBinContent(binID, recall[Category])
        Stack = THStack()
        Stack.Add(Hist_Acc)
        Stack.Add(Hist_Prec)
        Stack.Add(Hist_Rec)
        Stack.Draw("NOSTACKB TEXT")
        for binID, Category in zip(range(1, Hist_Acc.GetNbinsX() + 1), accuracy.keys()):
            Stack.GetXaxis().SetBinLabel(binID, Category)
        Stack.SetMaximum(1.5)
        Stack.SetMinimum(0)
        Stack.GetXaxis().SetTitle("Sample")
        Stack.GetYaxis().SetTitle("Accuracy/Precision/Recall")
        CustomLabel(0.2, 0.8, OutputLabel + " Classifier")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Accuracy_" + OutputLabel + "." + file_extension)
        del Canvas
        del Hist_Acc
        del Hist_Prec
        del Hist_Rec

    def do_YieldsPlot(self):
        W = self.get_Weights().values
        N_evts = np.ones(len(W))
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        PostProcessorMessage("Plotting class distribution plot.")
        Canvas = TCanvas("ClassDistr", "ClassDistr", 800, 600)
        if self.m_ModelSettings.is3LZReconstruction() or self.m_ModelSettings.is4LZReconstruction():
            Y = self.get_Labels().values
            N = self.m_ModelSettings.get("OutputLabels")
            Yields_Classes = {}
            N_evts_Classes = {}
            for y, n in enumerate(N):
                W_Class = W[Y == y]
                N_evts_Class = N_evts[Y == y]
                Yields_Classes[n] = sum(W_Class)
                N_evts_Classes[n] = sum(N_evts_Class)
            Hist_ClassTotalYields = TH1D("Hist_Total_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassTotalYields_unw = TH1D("Hist_Total_class_unw", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassRelYields = TH1D("Hist_Rel_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            hf.FillYieldHist(Hist_ClassTotalYields, Yields_Classes)
            hf.FillYieldHist(Hist_ClassRelYields, Yields_Classes, total_number_events=sum(W[Y != -2]))
            hf.FillYieldHist(Hist_ClassTotalYields_unw, N_evts_Classes)
            Hist_ClassTotalYields.GetXaxis().SetTitle("Classes")
            Hist_ClassRelYields.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetYaxis().SetTitleOffset(1.6)
            Hist_ClassTotalYields.SetMarkerSize(2)
            Hist_ClassTotalYields_unw.SetMarkerSize(2)
            Hist_ClassRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_ClassTotalYields.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Classes_Yields." + file_extension)
            Hist_ClassRelYields.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Classes_Relative_Yields." + file_extension)
            gStyle.SetPaintTextFormat(".0f")
            Hist_ClassTotalYields_unw.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Classes_Yields_unw." + file_extension)
            del Hist_ClassTotalYields
            del Hist_ClassRelYields
            del Hist_ClassTotalYields_unw
        if len(np.unique(self.get_Sample_Names())) > 1:
            S = self.get_Sample_Names().values
            N = np.unique(self.get_Sample_Names())
            Yields_Samples = {}
            N_evts_Samples = {}
            for n in N:
                W_Sample = W[S == n]
                N_evts_Sample = N_evts[S == n]
                Yields_Samples[n] = sum(W_Sample)
                N_evts_Samples[n] = sum(N_evts_Sample)
            Hist_SampleTotalYields = TH1D("Hist_Total_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            Hist_SampleTotalYields_unw = TH1D("Hist_Total_sample_unw", "", len(N_evts_Samples), 0, len(N_evts_Samples))
            Hist_SampleRelYields = TH1D("Hist_Rel_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            hf.FillYieldHist(Hist_SampleTotalYields, Yields_Samples)
            hf.FillYieldHist(Hist_SampleTotalYields_unw, N_evts_Samples)
            hf.FillYieldHist(Hist_SampleRelYields, Yields_Samples, total_number_events=sum(W))
            Hist_SampleTotalYields.GetXaxis().SetTitle("Samples")
            Hist_SampleRelYields.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetYaxis().SetTitleOffset(1.6)
            Hist_SampleTotalYields.SetMarkerSize(2)
            Hist_SampleTotalYields_unw.SetMarkerSize(2)
            Hist_SampleRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_SampleTotalYields.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Samples_Yields." + file_extension)
            Hist_SampleRelYields.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Samples_Relative_Yields." + file_extension)
            gStyle.SetPaintTextFormat(".0f")
            Hist_SampleTotalYields_unw.Draw("HIST TEXT")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Samples_Yields_unw." + file_extension)
            del Hist_SampleTotalYields
            del Hist_SampleRelYields
            del Hist_SampleTotalYields_unw
        del Canvas

    def do_EfficiencyPlot(self):
        basename = self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type")
        PostProcessorMessage("Plotting efficiency plot")
        efficiency = {}
        if self.m_isBinary:
            PredictionColumn = self.m_PREDICTIONNAME
        else:
            PredictionColumn = self.m_ModelSettings.get("OutputLabels")
        for Sample in np.unique(self.get_Sample_Names()):
            Y = self.get_Predictions(PredictionColumn).values[self.get_Sample_Names() == Sample]
            N = self.get_Labels().values[self.get_Sample_Names() == Sample]
            W = self.get_Weights().values[self.get_Sample_Names() == Sample]
            correct = incorrect = noclass = 0
            for y, n, w in zip(Y, N, W):
                class_pred = np.argmax(y)
                if class_pred == n:
                    correct += w
                elif class_pred != n and n != -2:
                    incorrect += w
            efficiency[Sample] = (correct) / (correct + incorrect)
        Hist_Eff = TH1D("HistEff", "", len(efficiency), 0, len(efficiency))
        Hist_Eff.SetFillColor(4)  # blue
        Canvas = TCanvas("Efficiency", "Efficiency", 800, 600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Sample in zip(range(1, len(efficiency) + 1), efficiency.keys()):
            Hist_Eff.SetBinContent(binID, efficiency[Sample])
        Hist_Eff.Draw("HIST TEXT")
        for binID, Sample in zip(range(1, Hist_Eff.GetNbinsX() + 1), efficiency.keys()):
            Hist_Eff.GetXaxis().SetBinLabel(binID, Sample)
        Hist_Eff.SetMaximum(1.5)
        Hist_Eff.SetMinimum(0)
        Hist_Eff.SetMarkerSize(2)
        Hist_Eff.GetXaxis().SetTitle("Sample")
        Hist_Eff.GetYaxis().SetTitle("Efficiency")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Efficiencies." + file_extension)
        del Canvas
        del Hist_Eff

    def do_ConfusionMatrix(self):
        """
        Produces a confusion matrix plot
        """
        PostProcessorMessage("Plotting Confusion matrix")
        gStyle.SetPadRightMargin(0.17)
        if self.m_isBinary:
            Y_pred = [round(pred) for pred in self.get_Predictions(self.m_PREDICTIONNAME)[~np.isnan(self.get_Labels().values)]]
            Y_true = self.get_Labels().values[~np.isnan(self.get_Labels().values)]
            Classes = ["Background", "Signal"]
            W = self.get_Weights()[~np.isnan(self.get_Labels().values)]
        else:
            Probs = self.get_Predictions(self.m_ModelSettings.get("OutputLabels")).values
            Y_pred = [np.argmax(prob) for prob in Probs[~np.isnan(self.get_Labels().values)]]
            Y_true = self.get_Labels().values[~np.isnan(self.get_Labels().values)]
            Classes = self.m_ModelSettings.get("OutputLabels")
            W = self.get_Weights()[~np.isnan(self.get_Labels().values)]
        ConfMatrix = confusion_matrix(Y_true, Y_pred, sample_weight=W)
        if self.m_GeneralSettings.get("ConfusionMatrixNorm") == "column":
            ConfMatrix = ConfMatrix/np.sum(ConfMatrix, axis=0)
        elif self.m_GeneralSettings.get("ConfusionMatrixNorm") == "row":
            ConfMatrix = (ConfMatrix.T/np.sum(ConfMatrix, axis=1)).T
        ConfMatrix_Hist = TH2D("ConfMat", "", len(Classes), 0, len(Classes), len(Classes), 0, len(Classes))
        for binIDx, _ in enumerate(Classes):
            ConfMatrix_Hist.GetXaxis().SetBinLabel(binIDx + 1, Classes[binIDx])
            ConfMatrix_Hist.GetYaxis().SetBinLabel(binIDx + 1, Classes[binIDx])
            for binIDy in range(len(Classes)):
                ConfMatrix_Hist.SetBinContent(binIDx + 1, binIDy + 1, ConfMatrix[binIDy][binIDx])
        ConfMatrix_Hist.GetXaxis().SetTitle("Prediction")
        ConfMatrix_Hist.GetYaxis().SetTitle("True Label")
        ConfMatrix_Hist.GetZaxis().SetTitle("Fraction of Events")
        ConfMatrix_Hist.SetMarkerSize(2)
        Canvas = TCanvas("ConfMatrix", "ConfMatrix", 1000, 600)
        gStyle.SetPaintTextFormat(".4f")
        ConfMatrix_Hist.Draw("colz text")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="top_outside")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + self.m_ModelSettings.get("Name") + "_" + self.m_ModelSettings.get("Type") + "_Confusion_matrix." + file_extension)
        del Canvas
        del ConfMatrix_Hist

    def do_NonnegativeWeightsComparison(self, PredictionColumn=None, OutputLabel=None, OutputID=1):
        """
        Function to create plots for variables to investigate the impact of negative weights
        """
        if PredictionColumn is None:
            PredictionColumn = self.m_PREDICTIONNAME
        PostProcessorMessage("Plotting negative weight comparison plot for %s classifier." % OutputLabel)
        w_tmp = self.get_Weights().values
        identifiers = self.get_Sample_Names().values
        unique_identifiers = np.unique(identifiers)
        factors = {identifier: np.sum(w_tmp[identifiers == identifier]) / np.sum(w_tmp[(w_tmp > 0) & (identifiers == identifier)]) for identifier in unique_identifiers}
        w_corr = [factors[identifier] for identifier in identifiers]
        w_corr *= w_tmp
        w_corr[w_corr < 0] = 0
        w_tmp[w_tmp < 0] = 0
        Binning = array("d", np.linspace(start=0.0, stop=1.0, num=51))
        hists = hf.DefineAndFill(Binning,
                                 Samples=self.m_Samples,
                                 xvals=self.get_Predictions(PredictionColumn).values,
                                 xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                 weights=self.m_DataFrame[self.m_WEIGHTNAME].values,
                                 OutputID=OutputID,
                                 binningoptimisation="None",
                                 underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                 overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        hists_nonnegative = hf.DefineAndFill(Binning,
                                             Samples=self.m_NominalSamples,
                                             xvals=self.get_Predictions(PredictionColumn).values,
                                             xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                             weights=w_tmp,
                                             binningoptimisation="None",
                                             prefix="nonnegative_hist",
                                             underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                             overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        hists_corr = hf.DefineAndFill(Binning,
                                      Samples=self.m_NominalSamples,
                                      xvals=self.get_Predictions(PredictionColumn).values,
                                      xval_identifiers=self.m_DataFrame[self.m_SAMPLENAME].values,
                                      weights=w_corr,
                                      binningoptimisation="None",
                                      prefix="corr_hist",
                                      underflowbin=self.m_GeneralSettings.get("UnderFlowBin"),
                                      overflowbin=self.m_GeneralSettings.get("OverFlowBin"))
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        temp_s_stack_nonnegative = THStack()
        temp_b_stack_nonnegative = THStack()
        temp_s_stack_corr = THStack()
        temp_b_stack_corr = THStack()
        for key, value in hists.items():
            if value[1]["Type"] == "Data":
                continue
            if value[1]["TrainingLabel"] == OutputID and value[1]["Type"] != "Data":
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"] != OutputID and value[1]["Type"] != "Data":
                temp_b_stack.Add(value[0])
        for key, value in hists_nonnegative.items():
            if value[1]["Type"] == "Data":
                continue
            if value[1]["TrainingLabel"] == OutputID and value[1]["Type"] != "Data":
                temp_s_stack_nonnegative.Add(value[0])
            if value[1]["TrainingLabel"] != OutputID and value[1]["Type"] != "Data":
                temp_b_stack_nonnegative.Add(value[0])
        for key, value in hists_corr.items():
            if value[1]["Type"] == "Data":
                continue
            if value[1]["TrainingLabel"] == OutputID and value[1]["Type"] != "Data":
                temp_s_stack_corr.Add(value[0])
            if value[1]["TrainingLabel"] != OutputID and value[1]["Type"] != "Data":
                temp_b_stack_corr.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        s_hist_nonnegative = temp_s_stack_nonnegative.GetStack().Last().Clone("s_hist_nonnegative")
        b_hist_nonnegative = temp_b_stack_nonnegative.GetStack().Last().Clone("b_hist_nonnegative")
        s_hist_corr = temp_s_stack_corr.GetStack().Last().Clone("s_hist_corr")
        b_hist_corr = temp_b_stack_corr.GetStack().Last().Clone("b_hist_corr")
        s_hist.SetLineColor(2)  # red
        b_hist.SetLineColor(4)  # blue
        s_hist_nonnegative.SetLineColor(2)  # red
        b_hist_nonnegative.SetLineColor(4)  # blue
        s_hist_nonnegative.SetLineStyle(2)  # dashed
        b_hist_nonnegative.SetLineStyle(2)  # dashed
        s_hist_corr.SetLineColor(2)  # red
        b_hist_corr.SetLineColor(4)  # blue
        s_hist_corr.SetLineStyle(9)  # dashed
        b_hist_corr.SetLineStyle(9)  # dashed
        s_hist.SetMarkerSize(0)
        b_hist.SetMarkerSize(0)
        s_hist_nonnegative.SetMarkerSize(0)
        b_hist_nonnegative.SetMarkerSize(0)
        s_hist_corr.SetMarkerSize(0)
        b_hist_corr.SetMarkerSize(0)
        Stack = THStack()
        Stack.Add(s_hist)
        Stack.Add(b_hist)
        Stack.Add(s_hist_nonnegative)
        Stack.Add(b_hist_nonnegative)
        Stack.Add(s_hist_corr)
        Stack.Add(b_hist_corr)
        Canvas = TCanvas("c1", "c1", 800, 600)
        Legend = TLegend(.58, .725, .95, .90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        if self.m_isBinary:
            Legend.AddEntry(s_hist, "Signal", "L")
            Legend.AddEntry(s_hist_nonnegative, "Signal (W>0)", "L")
            Legend.AddEntry(s_hist_corr, "Signal (Corrected)", "L")
        else:
            Legend.AddEntry(s_hist, OutputLabel, "L")
            Legend.AddEntry(s_hist_nonnegative, OutputLabel + " (W>0)", "L")
            Legend.AddEntry(s_hist_corr, OutputLabel + " (Corrected)", "L")
        Legend.AddEntry(b_hist, "Background", "L")
        Legend.AddEntry(b_hist_nonnegative, "Background (W>0)", "L")
        Legend.AddEntry(b_hist_corr, "Background (Corrected)", "L")

        pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
        pad1.SetBottomMargin(0)  # joins upper and lower plot
        pad1.Draw()
        # Lower ratio plot is pad2
        Canvas.cd()  # returns to main canvas before defining pad2
        pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
        pad2.SetTopMargin(0)  # joins upper and lower plot
        pad2.SetBottomMargin(0.3)
        pad2.Draw()
        s_ratio = hf.createRatio(s_hist, s_hist_nonnegative)
        b_ratio = hf.createRatio(b_hist, b_hist_nonnegative)
        s_ratio_errband = s_ratio.Clone("s_ratio_errband")
        b_ratio_errband = b_ratio.Clone("b_ratio_errband")
        s_ratio_corrected = hf.createRatio(s_hist, s_hist_corr)
        b_ratio_corrected = hf.createRatio(b_hist, b_hist_corr)
        s_ratio_corrected_errband = s_ratio_corrected.Clone("s_ratio_corrected_errband")
        b_ratio_corrected_errband = b_ratio_corrected.Clone("b_ratio_corrected_errband")
        maxerr = 0.55
        for binID in range(s_ratio_errband.GetNbinsX() + 1):
            if s_ratio_errband.GetBinContent(binID) == 0:
                s_ratio_errband.SetBinContent(binID, 1)
                s_ratio_errband.SetBinError(binID, maxerr)  # completely fill the ratio plot
            if b_ratio_errband.GetBinContent(binID) == 0:
                b_ratio_errband.SetBinContent(binID, 1)
                b_ratio_errband.SetBinError(binID, maxerr)  # completely fill the ratio plot
            if s_ratio_corrected_errband.GetBinContent(binID) == 0:
                s_ratio_corrected_errband.SetBinContent(binID, 1)
                s_ratio_corrected_errband.SetBinError(binID, maxerr)  # completely fill the ratio_corrected plot
            if b_ratio_corrected_errband.GetBinContent(binID) == 0:
                b_ratio_corrected_errband.SetBinContent(binID, 1)
                b_ratio_corrected_errband.SetBinError(binID, maxerr)  # completely fill the ratio_corrected plot
        s_ratio_errband.SetFillColor(kRed)
        b_ratio_errband.SetFillColor(kBlue)
        s_ratio_errband.SetFillStyle(3004)
        b_ratio_errband.SetFillStyle(3005)
        s_ratio_errband.SetMarkerSize(0)
        b_ratio_errband.SetMarkerSize(0)
        s_ratio.SetLineStyle(2)
        b_ratio.SetLineStyle(2)
        s_ratio.SetMaximum(1 + maxerr)
        b_ratio.SetMaximum(1 + maxerr)
        s_ratio.SetMinimum(1 - maxerr)
        b_ratio.SetMinimum(1 - maxerr)
        s_ratio.SetMarkerSize(0)
        b_ratio.SetMarkerSize(0)
        s_ratio_corrected_errband.SetFillColor(kRed)
        b_ratio_corrected_errband.SetFillColor(kBlue)
        s_ratio_corrected_errband.SetFillStyle(3004)
        b_ratio_corrected_errband.SetFillStyle(3005)
        s_ratio_corrected_errband.SetMarkerSize(0)
        b_ratio_corrected_errband.SetMarkerSize(0)
        s_ratio_corrected.SetLineStyle(9)
        b_ratio_corrected.SetLineStyle(9)
        s_ratio_corrected.SetMaximum(1 + maxerr)
        b_ratio_corrected.SetMaximum(1 + maxerr)
        s_ratio_corrected.SetMinimum(1 - maxerr)
        b_ratio_corrected.SetMinimum(1 - maxerr)
        s_ratio_corrected.SetMarkerSize(0)
        b_ratio_corrected.SetMarkerSize(0)
        # draw everything in first pad
        pad1.cd()

        Stack.Draw("hist e nostack")
        Stack.GetYaxis().SetTitle("Fraction of Events")
        Stack.GetXaxis().SetTitle(OutputLabel + " Classifier")
        # Let's make sure we have enough space for the labels and the legend
        Stack.SetMaximum(np.max([s_hist.GetMaximum(), b_hist.GetMaximum(), s_hist_nonnegative.GetMaximum(), b_hist_nonnegative.GetMaximum()]) * self.m_SCALEFACTOR)
        if self.m_GeneralSettings.get("ATLASLabel").lower() != "none":
            ATLASLabel(0.2, 0.85, self.m_GeneralSettings.get("ATLASLabel"))
        CustomLabel(0.2, 0.8, self.m_GeneralSettings.get("CMLabel"))
        if self.m_GeneralSettings.get("CustomLabel") != "":
            CustomLabel(0.2, 0.70, self.m_GeneralSettings.get("CustomLabel"))
        Legend.Draw("SAME")

        # to avoid clipping the bottom zero, redraw a small axis
        axis = Stack.GetYaxis()
        axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

        # draw everything in second pad
        pad2.cd()
        s_ratio.Draw("hist ")
        b_ratio.Draw("hist same")
        s_ratio_corrected.Draw("hist same")
        b_ratio_corrected.Draw("hist same")
        s_ratio_errband.Draw("e2 Same")
        b_ratio_errband.Draw("e2 Same")
        s_ratio_corrected_errband.Draw("e2 Same")
        b_ratio_corrected_errband.Draw("e2 Same")
        line = TLine(s_ratio.GetXaxis().GetXmin(), 1, s_ratio.GetXaxis().GetXmax(), 1)
        line.SetLineStyle(2)  # dashed
        line.Draw("Same")
        s_ratio.GetXaxis().SetTitle(OutputLabel + " Classifier")
        s_ratio.GetYaxis().SetTitle("Ratio")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + "Separation_weightComparison_" + OutputLabel + "." + file_extension)
        # Clean up
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        gROOT.FindObject("s_hist_nonnegative").Delete()
        gROOT.FindObject("b_hist_nonnegative").Delete()
        gROOT.FindObject("s_hist_corr").Delete()
        gROOT.FindObject("b_hist_corr").Delete()
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        for key in hists_nonnegative.keys():
            gROOT.FindObject(key).Delete()
        for key in hists_corr.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_SoverB2DPlots(self):
        PostProcessorMessage("Plotting 2D signal over background plot.")
        OutputLabels = self.m_ModelSettings.get("OutputLabels")
        Axis_indeces = np.unique([s.get("TrainLabel") for s in self.m_Samples if s.get("TrainLabel")>=0 and s.get("Type")!="Signal"])
        for i in range (0, len(Axis_indeces)-1):
            for j in range (i+1, len(Axis_indeces)):
                basename =  "%s_%s_%s_%s"%(self.m_ModelSettings.get("Name"),
                                           self.m_ModelSettings.get("Type"),
                                           OutputLabels[Axis_indeces[i]],
                                           OutputLabels[Axis_indeces[j]])
                if self.m_ModelSettings.isClassification():
                    hists = hf.DefineAndFill2D(
                        Binning_1=[self.m_GeneralSettings.get("NbinsSB2DPlots"), 0.0, 1.0],
                        Binning_2=[self.m_GeneralSettings.get("NbinsSB2DPlots"), 0.0, 1.0],
                        Samples=self.m_Samples,
                        Predictions_1=self.m_DataFrame.get(OutputLabels[Axis_indeces[i]]).values,
                        Predictions_2=self.m_DataFrame.get(OutputLabels[Axis_indeces[j]]).values,
                        N=self.get_Sample_Names().values,
                        W=self.get_Weights().values)
                sig_stack = THStack()
                bkg_stack = THStack()
                for key, value in hists.items():
                    if value[1][0] == "Data":
                        continue
                    if value[1][0] == "Signal":
                        sig_stack.Add(value[0])
                    if value[1][0] == "Background":
                        bkg_stack.Add(value[0])
                sig_hist = sig_stack.GetStack().Last().Clone("sig_hist")
                bkg_hist = bkg_stack.GetStack().Last().Clone("bkg_hist")
                SoverB2D = TH2D("SoverB2D", "SoverB2D", self.m_GeneralSettings.get("NbinsSB2DPlots"),0.0,1.0,self.m_GeneralSettings.get("NbinsSB2DPlots"),0.0,1.0)
                SoversqrtB2D = TH2D("SoversqrtB2D", "SoversqrtB2D", self.m_GeneralSettings.get("NbinsSB2DPlots"),0.0,1.0,self.m_GeneralSettings.get("NbinsSB2DPlots"),0.0,1.0)
                max_val_SB, max_val_SsqrtB, max_x_SB, max_y_SB, max_x_SsqrtB, max_y_SsqrtB = 0, 0, 0, 0, 0, 0
                sig_tot = sig_hist.Integral()
                for bin_j in range(1, self.m_GeneralSettings.get("NbinsSB2DPlots")+1):
                    for bin_k in range(1, self.m_GeneralSettings.get("NbinsSB2DPlots")+1-bin_j):
                        if bkg_hist.Integral(0,bin_k,0,bin_j)!=0:
                            sig_count = sig_hist.Integral(0,bin_k,0,bin_j)
                            bkg_count = bkg_hist.Integral(0,bin_k,0,bin_j)
                            if sig_count>self.m_GeneralSettings.get("MinSBSignalYield")*sig_tot and bkg_count>0 and sig_count>0:
                                if sig_count/bkg_count > max_val_SB:
                                    max_val_SB = sig_count/bkg_count
                                    max_x_SB = bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1)
                                    max_y_SB = bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1)
                                SoverB2D.Fill(bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), sig_count/bkg_count)
                            if sig_count>self.m_GeneralSettings.get("MinSsqrtBSignalYield")*sig_tot and bkg_count>0 and sig_count>0:
                                if sig_count/np.sqrt(bkg_count) > max_val_SsqrtB:
                                    max_val_SsqrtB = sig_count/np.sqrt(bkg_count)
                                    max_x_SsqrtB = bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1)
                                    max_y_SsqrtB = bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1)
                                SoversqrtB2D.Fill(bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), sig_count/np.sqrt(bkg_count))
                        else:
                            SoverB2D.Fill(bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), 0)
                            SoversqrtB2D.Fill(bin_k/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), bin_j/(self.m_GeneralSettings.get("NbinsSB2DPlots")+1), 0)

                Canvas = TCanvas("SoverB2D_Canvas", "SoverB2D_Canvas", 800, 600)
                #SoverB2D.Draw("CONT1Z")
                SoverB2D.Draw("COLZ")
                Canvas.SetRightMargin(0.15)
                SoverB2D.GetXaxis().SetTitle(OutputLabels[Axis_indeces[i]] + " Score")
                SoverB2D.GetYaxis().SetTitle(OutputLabels[Axis_indeces[j]] + " Score")
                SoverB2D.GetZaxis().SetTitle("S/B")
                SoverB2D.SetMaximum(max_val_SB)
                SoverB2D.SetMinimum(0.0)
                DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="right_2")
                CustomLabel(0.5, 0.7, "Max. S/B at (%.2f,%.2f)" % (max_x_SB, max_y_SB))
                for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_SoverB2D." + file_extension)
                del Canvas

                Canvas = TCanvas("SoversqrtB2D_Canvas", "SoversqrtB2D_Canvas", 800, 600)
                SoversqrtB2D.Draw("COLZ")
                Canvas.SetRightMargin(0.15)
                SoversqrtB2D.GetXaxis().SetTitle(OutputLabels[Axis_indeces[i]] + " Score")
                SoversqrtB2D.GetYaxis().SetTitle(OutputLabels[Axis_indeces[j]] + " Score")
                SoversqrtB2D.GetZaxis().SetTitle("S/#sqrt{B}")
                SoversqrtB2D.SetMaximum(max_val_SsqrtB)
                SoversqrtB2D.SetMinimum(0.0)
                DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="right_2")
                CustomLabel(0.5, 0.7, "Max. S/#sqrt{B} at (%.2f,%.2f)" % (max_x_SsqrtB, max_y_SsqrtB))
                for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_SoversqrtB2D." + file_extension)
                del Canvas
        
                SoverB2D_norm = SoverB2D.Clone("SoverB2D_norm")
                SoversqrtB2D_norm = SoversqrtB2D.Clone("SoversqrtB2D_norm")
                SoverB2D_norm.Scale(1/max_val_SB * self.m_GeneralSettings.get("SoverBSF"))
                SoversqrtB2D_norm.Scale(1/max_val_SsqrtB * self.m_GeneralSettings.get("SoversqrtBSF"))
                Combined2D = SoverB2D_norm.Clone("Combined2D")
                Combined2D.Add(SoversqrtB2D_norm)
                Combined2D.Scale(1/( self.m_GeneralSettings.get("SoverBSF") +  self.m_GeneralSettings.get("SoversqrtBSF")))
                Canvas = TCanvas("Combined2D_Canvas", "Combined2D_Canvas", 800, 600)
                Combined2D.Draw("COLZ")
                Canvas.SetRightMargin(0.15)
                Combined2D.GetXaxis().SetTitle(OutputLabels[Axis_indeces[i]] + " Score")
                Combined2D.GetYaxis().SetTitle(OutputLabels[Axis_indeces[j]] + " Score")
                Combined2D.GetZaxis().SetTitle("Combination Score")
                Combined2D.SetMaximum(1)
                DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC", align="right_2")
                for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir()) + basename + "_Combined2D." + file_extension)
                del Canvas
                # for key in hists.keys():
                #     gROOT.FindObject(key).Delete()
