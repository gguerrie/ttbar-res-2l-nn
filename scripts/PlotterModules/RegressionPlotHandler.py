import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import PostProcessorMessage
from PlotterModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from PlotterModules.BasicPlotHandler import BasicPlotHandler
from ROOT import gROOT, TCanvas, THStack, TH1F, TH2F, TLegend, TLine, gPad
import numpy as np
import pandas as pd
from pickle import load

class RegressionPlotHandler(BasicPlotHandler):
    def __init__(self,Settings):
        super().__init__(Settings)

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    
    def Unscale(self, scaler, X):
        mean = scaler.mean_
        std = np.sqrt(scaler.var_)
        # print(mean)
        # print(std)
        validation = X*std + mean
        return validation
    
    def do_Plots(self):
        """
        Wrapper function to produce all available plots in one go.
        """
        self.do_PerformancePlot()
        ### self.do_ArchitecturePlot()
        for OutputLabel in self.m_OutputLabels:
            self.do_StackPlot(PredictionColumn=OutputLabel, OutputLabel=OutputLabel)
        for TruthColumn, RegressionTargetLabel, PredictionColumn in zip(self.m_ModelSettings.get("RegressionTarget"),self.m_ModelSettings.get("RegressionTargetLabel"),self.m_OutputLabels):
            self.do_RegressionVersusTruthPlot(PredictionColumn=PredictionColumn, TruthColumn=TruthColumn, RegressionTargetLabel=RegressionTargetLabel)
        for TruthColumn, RegressionTargetLabel, PredictionColumn in zip(self.m_ModelSettings.get("RegressionTarget"),self.m_ModelSettings.get("RegressionTargetLabel"),self.m_OutputLabels):
            self.do_RegressionVersusTruthPlot2D(PredictionColumn=PredictionColumn, TruthColumn=TruthColumn, RegressionTargetLabel=RegressionTargetLabel)
        for TruthColumn, RegressionTargetLabel, PredictionColumn in zip(self.m_ModelSettings.get("RegressionTarget"),self.m_ModelSettings.get("RegressionTargetLabel"),self.m_OutputLabels):
            self.do_ErrorPlot1d(PredictionColumn=PredictionColumn, TruthColumn=TruthColumn, RegressionTargetLabel=RegressionTargetLabel)
        self.do_ReweightingPlot()

    def do_RegressionVersusTruthPlot(self, PredictionColumn=None, TruthColumn=None, RegressionTargetLabel=None):
        PostProcessorMessage("Plotting regressed variable (%s) versus truth variable (%s)."%(PredictionColumn, TruthColumn))
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.m_ModelSettings.get("Name")+"_"+self.m_ModelSettings.get("Type")
        Binning = self.m_ModelSettingsVariables[PredictionColumn].get_VarBinning()

        # print(np.vstack((self.get_Labels().values)))
        # print(np.amax(np.vstack((self.get_Labels().values))[:,0]))
        maxValue = max([np.amax(np.vstack((self.get_Labels().values))[:,0]), np.amax(self.get_Predictions(PredictionColumn).values)])
        minValue = min([np.amin(np.vstack((self.get_Labels().values))[:,0]), np.amin(self.get_Predictions(PredictionColumn).values)])
        # print("===============")
        # print(maxValue)
        # print(minValue)
        # print("===============")
        
        # TruthDistribution = TH1F("TruthDistribution", "TruthDistribution",int(len(Binning)-1),Binning)
        # PredDistribution  = TH1F("PredDistribution", "PredDistribution",int(len(Binning)-1),Binning)
        TruthDistribution = TH1F("TruthDistribution", "TruthDistribution",int(len(Binning)-1), minValue, maxValue)
        PredDistribution  = TH1F("PredDistribution", "PredDistribution",int(len(Binning)-1), minValue, maxValue)
        
        # Process labels to inverse scale them
        if self.m_GeneralSettings.get("OutputScaling") is not None:
            self.m_OutputScaler = load(open("%sout_scaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))
            #labels_rescaled = self.m_OutputScaler.inverse_transform(np.vstack((self.get_Labels().values)))
            labels_rescaled = self.Unscale(self.m_OutputScaler, np.vstack((self.get_Labels().values)))
            df_Y = pd.DataFrame(data=labels_rescaled, columns=self.m_ModelSettings.get("RegressionTarget"))
            # REMOVE IMMEDIATELY
            #df_Y = pd.DataFrame(data=self.get_Labels().values.tolist(), columns=self.m_ModelSettings.get("RegressionTarget"))
        else:
            df_Y = pd.DataFrame(data=self.get_Labels().values.tolist(), columns=self.m_ModelSettings.get("RegressionTarget"))

        print("Truth shape: %s", df_Y[TruthColumn].shape)
        print("Prediction shape: %s", self.get_Predictions(PredictionColumn).shape)
        hf.FillHist(TruthDistribution, values=df_Y[TruthColumn], weights=self.get_Weights())
        hf.FillHist(PredDistribution, values=self.get_Predictions(PredictionColumn), weights=self.get_Weights())
        TruthDistribution.SetLineColor(2) # red
        PredDistribution.SetLineColor(4) # blue
        HistStack = THStack()
        HistStack.Add(TruthDistribution)
        HistStack.Add(PredDistribution)
        HistStack.Draw("hist nostack")
        gPad.SetLogy()
        HistStack.GetYaxis().SetTitle("Events")
        HistStack.GetXaxis().SetTitle(RegressionTargetLabel)
        HistStack.SetMaximum(HistStack.GetMaximum()*1.2)
        Legend = TLegend(.75,.75,.90,.90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        Legend.AddEntry(TruthDistribution,"Truth","L")
        Legend.AddEntry(PredDistribution,"Prediction","L")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        Legend.Draw()
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_Truth_versus_Prediction_"+PredictionColumn+"."+file_extension)
        gROOT.FindObject("TruthDistribution").Delete()
        gROOT.FindObject("PredDistribution").Delete()
        Canvas.Close()
        del Canvas

    def do_RegressionVersusTruthPlot2D(self, PredictionColumn=None, TruthColumn=None, RegressionTargetLabel=None):
        PostProcessorMessage("Plotting Regression/Truth Matrix for %s versus %s"%(PredictionColumn, TruthColumn))
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.m_ModelSettings.get("Name")+"_"+self.m_ModelSettings.get("Type")
        Binning = self.m_ModelSettingsVariables[PredictionColumn].get_VarBinning()
        Distribution2D = TH2F("Distribution2D", "Distribution2D",int(len(Binning)-1),Binning,int(len(Binning)-1),Binning)
        
        # Process labels to inverse scale them
        if self.m_GeneralSettings.get("OutputScaling") is not None:
            self.m_OutputScaler = load(open("%sout_scaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))
            labels_rescaled = self.m_OutputScaler.inverse_transform(np.vstack((self.get_Labels().values)))
            df_Y = pd.DataFrame(data=labels_rescaled, columns=self.m_ModelSettings.get("RegressionTarget"))
        else:
            df_Y = pd.DataFrame(data=self.get_Labels().values.tolist(), columns=self.m_ModelSettings.get("RegressionTarget"))

        hf.FillHist2D(Distribution2D, values_1=df_Y[TruthColumn], values_2=self.get(PredictionColumn), weights=self.get_Weights()/np.sum(self.get_Weights())*100)
        Distribution2D.Draw("colz")
        Distribution2D.SetMinimum(0)
        Diagonal_Line = TLine(Binning[1],Binning[1],Binning[-1],Binning[-1])
        Diagonal_Line.SetLineColor(1)
        Diagonal_Line.SetLineWidth(2)
        Canvas.SetRightMargin(0.15)
        Canvas.SetTopMargin(0.1)
        Distribution2D.GetZaxis().SetTitle("Percentage of Events [%]")
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC",align="top_outside")
        Distribution2D.GetXaxis().SetTitle("%s (Pred.)"%RegressionTargetLabel)
        Distribution2D.GetYaxis().SetTitle("%s (Truth.)"%RegressionTargetLabel)
        Diagonal_Line.Draw("Same")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_Truth_versus_Prediction2D_"+PredictionColumn+"."+file_extension)
        gROOT.FindObject("Distribution2D").Delete()
        Canvas.Close()
        del Canvas

    def do_ErrorPlot1d(self, PredictionColumn=None, TruthColumn=None, RegressionTargetLabel=None):
        PostProcessorMessage("Plotting error distribution for %s"%PredictionColumn)
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.m_ModelSettings.get("Name")+"_"+self.m_ModelSettings.get("Type")
        # Process labels to inverse scale them
        if self.m_GeneralSettings.get("OutputScaling") is not None:
            self.m_OutputScaler = load(open("%sout_scaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))
            labels_rescaled = self.m_OutputScaler.inverse_transform(np.vstack((self.get_Labels().values)))
            df_Y = pd.DataFrame(data=labels_rescaled, columns=self.m_ModelSettings.get("RegressionTarget"))
        else:
            df_Y = pd.DataFrame(data=self.get_Labels().values.tolist(), columns=self.m_ModelSettings.get("RegressionTarget"))

        ErrorDistribution = TH1F("ErrorDistribution", "ErrorDistribution",
                                 100,
                                 np.mean(self.get_Predictions(PredictionColumn)/df_Y[TruthColumn])-3*np.std(self.get_Predictions(PredictionColumn)/df_Y[TruthColumn]),
                                 np.mean(self.get_Predictions(PredictionColumn)/df_Y[TruthColumn])+3*np.std(self.get_Predictions(PredictionColumn)/df_Y[TruthColumn]))
        hf.FillHist(ErrorDistribution, values=(self.get_Predictions(PredictionColumn)/df_Y[TruthColumn]), weights=self.get_Weights())
        ErrorDistribution.Draw("hist")
        ErrorDistribution.GetYaxis().SetTitle("Events")
        ErrorDistribution.GetXaxis().SetTitle("%s (Prediction/Truth)"%RegressionTargetLabel)
        DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
        for file_extension in self.m_GeneralSettings.get("PlotFormat"):
            Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+basename+"_ErrorDistribution."+file_extension)
        gROOT.FindObject("ErrorDistribution").Delete()
        Canvas.Close()
        del Canvas

    def do_ReweightingPlot(self):
        # Process labels to inverse scale them
        if self.m_GeneralSettings.get("OutputScaling") is not None:
            self.m_OutputScaler = load(open("%sout_scaler.pkl"%hf.ensure_trailing_slash(self.m_Dirs.ModelDir()), 'rb'))
            labels_rescaled = self.m_OutputScaler.inverse_transform(np.vstack((self.get_Labels().values)))
            Y_tmp = labels_rescaled.T
        else:
            Y_tmp = np.vstack((self.get_Labels().values)).T

        rows, _ = Y_tmp.shape
        for row in range(rows):
            PostProcessorMessage("Plotting "+self.m_ModelSettings.get("RegressionTargetLabel")[row]+" Reweighted distribution.")       
            Canvas = TCanvas("c1","c1",800,600)
            nbins = int(self.m_ModelSettings.get("ModelBinning")[row][0])
            xlow = self.m_ModelSettings.get("ModelBinning")[row][1]
            xhigh = self.m_ModelSettings.get("ModelBinning")[row][2]
            ReweightedDistribution = TH1F("ReweightedDistribution", "ReweightedDistribution",nbins,xlow,xhigh)
            hf.FillHist(ReweightedDistribution, values=(Y_tmp[row]), weights=self.get_Weights_sc())
            ReweightedDistribution.Draw("hist")
            ReweightedDistribution.GetYaxis().SetTitle("Events")
            ReweightedDistribution.GetXaxis().SetTitle(self.m_ModelSettings.get("RegressionTargetLabel")[row]+" rescaled")
            DrawLabels(self.m_GeneralSettings.get("ATLASLabel"), self.m_GeneralSettings.get("CMLabel"), self.m_GeneralSettings.get("CustomLabel"), Option="MC")
            for file_extension in self.m_GeneralSettings.get("PlotFormat"):
                Canvas.SaveAs(hf.ensure_trailing_slash(self.m_Dirs.MVAPlotDir())+self.m_ModelSettings.get("Name")+"_"+self.m_ModelSettings.get("Type")+"_"+self.m_ModelSettings.get("RegressionTargetLabel")[row]+"_Reweighting."+file_extension)
            gROOT.FindObject("ReweightedDistribution").Delete()
            Canvas.Close()
            del Canvas
        
