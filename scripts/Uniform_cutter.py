#!/usr/bin/env python3
"""
Main steering script for conversion
"""
import argparse
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage
from HelperModules.ConversionHandler import ConversionHandler
from HelperModules.Processor import Processor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--outdir", help="Output directory", required=True)
    parser.add_argument("-n", "--namefile", nargs="+", help="Filenames for parallel conversion", required=True)

    args = parser.parse_args()

if os.path.exists(args.outdir):
    ConverterMessage("Writing files at: " + args.outdir)
else:
    ConverterMessage("Output folder not found, creating it at: " + args.outdir)
    os.makedirs(args.outdir)
    os.makedirs(args.outdir + "/Data")
    os.makedirs(args.outdir + "/Model")
    os.makedirs(args.outdir + "/Plots")
    

dfs = [pd.read_hdf(file) for file in args.namefile]
X = pd.concat(dfs)

# Make the distribution uniform
lower_edge = 350000
upper_edge = 5000000
binning = 100000
# For neutrinos
# lower_edge = -5
# upper_edge = 5
# binning = 0.2
select = []
for N in np.arange(lower_edge, upper_edge, binning):
    try:
        #select.append(X[(X['Label'] < N+binning) & (X['Label'] > N)].sample(n=600000))
        select.append(X[(X['Label'] < N+binning) & (X['Label'] > N)].sample(n=120000))
    except:
        select.append(X[(X['Label'] < N+binning) & (X['Label'] > N)])

select = pd.concat(select)
seed = np.random.randint(1000, size=1)[0]
select = select.sample(frac=1, random_state=seed).reset_index(drop=True)
print(select)

select.to_hdf( args.outdir + "/Data/merged_uniform.h5", key="df", mode="w")
ConverterDoneMessage("Uniform hdf5 file written to " + args.outdir + "/Data/merged_uniform.h5")

fig = plt.figure(figsize=(16, 16), constrained_layout=True)
a0 = plt.subplot(111)
bin = np.arange(lower_edge,upper_edge,binning)
a0.hist(select['Label'],  bins=500, range=(0, 3e6), alpha=1, label="TRUTH $t \overline{t}$ mass", stacked=True, fill=True, histtype='step', color='orangered')
fig.savefig(args.outdir + '/Plots/merged_uniform.png')
ConverterDoneMessage("Image saved at: " + args.outdir + "/Plots/merged_uniform.png")