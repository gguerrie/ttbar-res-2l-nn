

void Injector::initialize(std::shared_ptr<top::TopConfig> config, TFile *file,
                                const std::vector<std::string> &extraBranches) {

  ///-- Let the base class do all the hard work --///
  ///-- It will setup TTrees for each systematic with a standard set of variables --///
  top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

  /// setup reco-level tree///
  for (auto systematicTree : treeManagers()) {
    systematicTree->makeOutputVariable(m_NNscore, "NNscore");
  }

  // initialize the ONNXWrapper
  // we need to specify the path to the model and the list of variables
  m_NN = std::make_unique<ONNXWrapper>(
      PathResolver::find_file("MLExampleSaver/model.onnx", "DATAPATH"),
      PathResolver::find_file("MLExampleSaver/model_variables.txt", "DATAPATH"));
}

void Injector::calculateEvent(const top::Event &event) {

  int n_bjets = 0;
  float bjet_1_pt = -1.; // pT of the leading-pT jet among b-jets passing 85% eff WP

  // some initial selection
  // require at least one jet passing 85% btag WP
  for (const xAOD::Jet *jet : event.m_jets) {
    if (jet->auxdataConst<int>("tagWeightBin_DL1r_Continuous") > 1) {
      ++n_bjets;
      // store the leading b-jet pT
      if (bjet_1_pt < 0.)
        bjet_1_pt = jet->pt();
    }
  }
  if (n_bjets == 0)
    return;

  // compute input variables for the NN
  // H_T -- start with all selected jets
  float HT = 0.;
  for (const xAOD::Jet *jet : event.m_jets) {
    HT += jet->pt();
  }

  // we will treat electrons and muons together
  // create a single merged vector of lepton particles and sort it
  // all xAOD::Electrons and xAOD::Muons inherit from xAOD::IParticle
  std::vector<const xAOD::IParticle*> leptons;
  leptons.reserve(event.m_electrons.size() + event.m_muons.size());
  for (const xAOD::Electron *el : event.m_electrons)
    leptons.push_back(el);
  for (const xAOD::Muon *mu : event.m_muons)
    leptons.push_back(mu);

  auto compare_pt = [](const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
    return (p1->pt() > p2->pt());
  };
  std::sort(leptons.begin(), leptons.end(), compare_pt);

  // add leptons to HT
  for (const xAOD::IParticle* lep : leptons) {
    HT += lep->pt();
  }

  // add MET to H_T
  HT += event.m_met->met();

  // fill the inputs, we check just in case, that we have the required number of jets & leptons in event
  m_NN_inputs["H_T"] = HT*1e-3;
  m_NN_inputs["n_bjets"] = n_bjets;
  m_NN_inputs["bjet_1_pt"] = bjet_1_pt*1e-3;

  if (event.m_jets.size() >= 2) {
    m_NN_inputs["jet_1_pt"] = event.m_jets.at(0)->pt()*1e-3;
    m_NN_inputs["jet_2_pt"] = event.m_jets.at(1)->pt()*1e-3;
    m_NN_inputs["jet_1_twb"] = event.m_jets.at(0)->auxdataConst<int>("tagWeightBin_DL1r_Continuous");
    m_NN_inputs["jet_2_twb"] = event.m_jets.at(1)->auxdataConst<int>("tagWeightBin_DL1r_Continuous");
  } else {
    throw std::runtime_error("Event has less than two jets, can't compute NN value");
  }

  if (leptons.size() >= 2) {
    m_NN_inputs["lep_1_pt"] = leptons.at(0)->pt()*1e-3;
    m_NN_inputs["lep_2_pt"] = leptons.at(1)->pt()*1e-3;
  } else {
    throw std::runtime_error("Event has less than two leptons, can't compute NN value");
  }

  // get output from the NN (dictionary with output nodes)
  std::map<std::string, float> outputs;
  m_NN->evaluate(m_NN_inputs, outputs);
  // our network has one output node, store the value in NTuples
  m_NNscore = outputs.at("output");

  ///-- Let the base class do all the hard work --///
  top::EventSaverFlatNtuple::calculateEvent(event);

}

