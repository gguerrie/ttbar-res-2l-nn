#ifndef EVENT_H_
#define EVENT_H_
#include <TChain.h>
#include <TFile.h>
#include <Python.h>
// Header file for the classes stored in the TTree if any.
#include <vector>

class Event { public :
   const char *m_name; //!pointer to the analyzed TTree or TChain
   TTree *fChain;   //!pointer to the analyzed TTree or TChain
   // Declaration of leaf types
   
   Float_t MC_t_afterFSR_pt;
   Float_t MC_t_afterFSR_phi;
   Float_t MC_t_afterFSR_eta;
   Float_t MC_t_afterFSR_m;

   Float_t MC_tbar_afterFSR_pt;
   Float_t MC_tbar_afterFSR_phi;
   Float_t MC_tbar_afterFSR_eta;
   Float_t MC_tbar_afterFSR_m;

   Float_t MC_Wdecay1_from_t_pt;
   Float_t MC_Wdecay1_from_t_eta;
   Float_t MC_Wdecay1_from_t_phi;
   Float_t MC_Wdecay1_from_t_m;
   Float_t MC_Wdecay2_from_t_pt;
   Float_t MC_Wdecay2_from_t_eta;
   Float_t MC_Wdecay2_from_t_phi;
   Float_t MC_Wdecay2_from_t_m;

   Float_t MC_Wdecay1_from_tbar_pt;
   Float_t MC_Wdecay1_from_tbar_eta;
   Float_t MC_Wdecay1_from_tbar_phi;
   Float_t MC_Wdecay1_from_tbar_m;
   Float_t MC_Wdecay2_from_tbar_pt;
   Float_t MC_Wdecay2_from_tbar_eta;
   Float_t MC_Wdecay2_from_tbar_phi;
   Float_t MC_Wdecay2_from_tbar_m;

   Int_t MC_Wdecay1_from_t_pdgId;
   Int_t MC_Wdecay2_from_t_pdgId;
   Int_t MC_Wdecay1_from_tbar_pdgId;
   Int_t MC_Wdecay2_from_tbar_pdgId;

   // std::vector<float> *MC_t_afterFSR_pt;
   // std::vector<float> *MC_t_afterFSR_phi;
   // std::vector<float> *MC_t_afterFSR_eta;
   // std::vector<float> *MC_t_afterFSR_m;

   // std::vector<float> *MC_tbar_afterFSR_pt;
   // std::vector<float> *MC_tbar_afterFSR_phi;
   // std::vector<float> *MC_tbar_afterFSR_eta;
   // std::vector<float> *MC_tbar_afterFSR_m;

   // std::vector<float> *MC_Wdecay1_from_t_pt;
   // std::vector<float> *MC_Wdecay1_from_t_eta;
   // std::vector<float> *MC_Wdecay1_from_t_phi;
   // std::vector<float> *MC_Wdecay1_from_t_m;
   // std::vector<float> *MC_Wdecay2_from_t_pt;
   // std::vector<float> *MC_Wdecay2_from_t_eta;
   // std::vector<float> *MC_Wdecay2_from_t_phi;
   // std::vector<float> *MC_Wdecay2_from_t_m;

   // std::vector<float> *MC_Wdecay1_from_tbar_pt;
   // std::vector<float> *MC_Wdecay1_from_tbar_eta;
   // std::vector<float> *MC_Wdecay1_from_tbar_phi;
   // std::vector<float> *MC_Wdecay1_from_tbar_m;
   // std::vector<float> *MC_Wdecay2_from_tbar_pt;
   // std::vector<float> *MC_Wdecay2_from_tbar_eta;
   // std::vector<float> *MC_Wdecay2_from_tbar_phi;
   // std::vector<float> *MC_Wdecay2_from_tbar_m;

   // std::vector<int> *MC_Wdecay1_from_t_pdgId;
   // std::vector<int> *MC_Wdecay2_from_t_pdgId;
   // std::vector<int> *MC_Wdecay1_from_tbar_pdgId;
   // std::vector<int> *MC_Wdecay2_from_tbar_pdgId;

   ULong64_t eventNumber;
 
   // List of branches
   TBranch *b_MC_t_afterFSR_pt; //!
   TBranch *b_MC_t_afterFSR_phi; //!
   TBranch *b_MC_t_afterFSR_eta; //!
   TBranch *b_MC_t_afterFSR_m; //!

   TBranch *b_MC_tbar_afterFSR_pt; //!
   TBranch *b_MC_tbar_afterFSR_phi; //!
   TBranch *b_MC_tbar_afterFSR_eta; //!
   TBranch *b_MC_tbar_afterFSR_m; //!

   TBranch *b_MC_Wdecay1_from_t_pt; //!
   TBranch *b_MC_Wdecay1_from_t_eta; //!
   TBranch *b_MC_Wdecay1_from_t_phi; //!
   TBranch *b_MC_Wdecay1_from_t_m; //!
   TBranch *b_MC_Wdecay2_from_t_pt; //!
   TBranch *b_MC_Wdecay2_from_t_eta; //!
   TBranch *b_MC_Wdecay2_from_t_phi; //!
   TBranch *b_MC_Wdecay2_from_t_m; //!

   TBranch *b_MC_Wdecay1_from_tbar_pt; //!
   TBranch *b_MC_Wdecay1_from_tbar_eta; //!
   TBranch *b_MC_Wdecay1_from_tbar_phi; //!
   TBranch *b_MC_Wdecay1_from_tbar_m; //!
   TBranch *b_MC_Wdecay2_from_tbar_pt; //!
   TBranch *b_MC_Wdecay2_from_tbar_eta; //!
   TBranch *b_MC_Wdecay2_from_tbar_phi; //!
   TBranch *b_MC_Wdecay2_from_tbar_m; //!

   TBranch *b_MC_Wdecay1_from_t_pdgId; //!
   TBranch *b_MC_Wdecay2_from_t_pdgId; //!
   TBranch *b_MC_Wdecay1_from_tbar_pdgId; //!
   TBranch *b_MC_Wdecay2_from_tbar_pdgId; //!

   TBranch *b_eventNumber; //!

   //Event(const char *name);
   Event();
   virtual ~Event();
   virtual Int_t GetEntry(Long64_t entry);
   // virtual Long64_t LoadTree(Long64_t entry);
   virtual PyObject *Init(const char *name);
   //virtual void ComputeChel(TChain *fChain);
};
#endif
