
import json
import pandas as pd
import tensorflow as tf
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import TrainerDoneMessage
from HelperModules.OptionHandler import OptionHandler
from ModelModules.BasicModel import BasicModel
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import train_test_split
import numpy as np

class WGANModel(BasicModel):
    """
    The WGAN (Wasserstein GAN) is introduced/motivated in https://arxiv.org/abs/1701.07875 (Martin Arjovsky, et al.)
    This WGAN implementation is based on https://arxiv.org/abs/1704.00028.
    Instead of using the discriminator to predict whether a sample is real or fake the dicriminator in a WGAN
    the discriminator serves as a "critic" that scores the realness or fakeness of a given sample.
    The WGAN seeks a minimisation of the mathematical distance between the real distribution and the distribution
    of the generated samples. Typical distance measures are the Kullback-Leibler divergence, Jensen-Shannon divergence
    or the Earth-Mover distance (also known as Wasserstein distance).
    """

    def __init__(self, group, input_dim):
        super().__init__(group)
        self.InputDim = input_dim  # Input dimensionality of the data
        self.m_options.set_description("WGANMODEL")
        self.m_options.register_option("Epochs", optiondefault=100)
        self.m_options.register_option("OutputSize", optiondefault=None)
        self.m_options.register_option("LatentDim", optiondefault=128)
        self.m_options.register_option("SmoothingAlpha", optiondefault=None)
        self.m_options.register_option("nCriticUpdates", optiondefault=5)
        self.m_options.register_option("GPWeight", optiondefault=10)
        self.m_options.register_option("CriticLR", optiondefault=0.0001)
        self.m_options.register_option("GeneratorLR", optiondefault=0.0001)
        self.m_options.register_option("BatchSize", optiondefault=32)

        self.m_options.register_check(optionname="BatchSize", check="larger", thr=0)
        self.m_options.register_check(optionname="Epochs", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="LatentDim", check="larger", thr=0)
        self.m_options.register_check(optionname="SmoothingAlpha", check="smaller", thr=0.5)
        self.m_options.register_check(optionname="SmoothingAlpha", check="larger", thr=0.0)
        self.m_options.register_check(optionname="CriticLR", check="larger", thr=0.0)
        self.m_options.register_check(optionname="GeneratorLR", check="larger", thr=0.0)
        self.m_options.register_check(optionname="nCriticUpdates", check="larger", thr=0)
        self.m_options.register_check(optionname="GPWeight", check="larger", thr=0.0)

        for item in self.m_Group:
            self.m_options.set_option(item)
        self.m_options.check_options()

        self.kerasmodel = None      # Keras model object
        self.Critic = None      # Critic model for GAN Training
        self.Generator = None      # Generator model for GAN Taining
        self.Critic_Optimizer = Adam(learning_rate=self.get("CriticLR"), beta_1=0.5, beta_2=0.9)
        self.Generator_Optimizer = Adam(learning_rate=self.get("GeneratorLR"), beta_1=0.5, beta_2=0.9)
    ############################################################################
    ############################ GAN Helper Methods ############################
    ############################################################################

    def Gradient_Penalty(self, batch_size, Real_Data, Fake_Data):
        """
        Calculates the gradient penalty on interpolated data.
        This penalty is then added to the critic's loss to enforce a
        Lipschitz contraint

        Keyword arguments:
        batch_size --- Batch size
        Real_Data  --- Set of real events
        Fake_Data  --- Set of fake events
        """
        Alpha = tf.random.normal([batch_size, 1], 0.0, 1.0)
        Difference = Fake_Data - Real_Data
        Interpolated = Real_Data + Alpha * Difference

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(Interpolated)
            Prediction = self.Critic(Interpolated, training=True)

        Gradients = gp_tape.gradient(Prediction, [Interpolated])[0]
        Norm = tf.sqrt(tf.reduce_sum(tf.square(Gradients), axis=[1]))
        GP = tf.reduce_mean((Norm - 1.0)**2)
        return GP

    def train_step(self, Real_Data):
        """
        This function overrides the keras train_step function.

        Keyword arguments:
        Real_Data  --- Set of real events
        """
        if isinstance(Real_Data, tuple):
            Real_Data = Real_Data[0]

        batch_size = tf.shape(Real_Data)[0]

        for i in range(self.get("nCriticUpdates")):
            Random_Latent_Vectors = tf.random.normal(shape=(batch_size, self.get("LatentDim")))
            with tf.GradientTape() as tape:
                Fake_Data = self.Generator(Random_Latent_Vectors, training=True)
                fake_logits = self.Critic(Fake_Data, training=True)
                real_logits = self.Critic(Real_Data, training=True)

                Critic_Cost = self.Critic_Loss_Function(Real_Data=real_logits, Fake_Data=fake_logits)
                GP = self.Gradient_Penalty(batch_size, Real_Data, Fake_Data)
                Critic_Loss = Critic_Cost + GP * self.get("GPWeight")

            Critic_Gradient = tape.gradient(Critic_Loss, self.Critic.trainable_variables)
            self.Critic_Optimizer.apply_gradients(zip(Critic_Gradient, self.Critic.trainable_variables))

        Random_Latent_Vectors = tf.random.normal(shape=(batch_size, self.get("LatentDim")))
        with tf.GradientTape() as tape:
            generated_data = self.Generator(Random_Latent_Vectors, training=True)
            gen_data_logits = self.Critic(generated_data, training=True)
            Generator_Loss = self.Generator_Loss_Function(gen_data_logits)

        Generator_Gradient = tape.gradient(Generator_Loss, self.Generator.trainable_variables)
        self.Generator_Optimizer.apply_gradients(zip(Generator_Gradient, self.Generator.trainable_variables))
        return {"Critic Loss": Critic_Loss, "Generator Loss": Generator_Loss}

    def Critic_Loss_Function(self, Real_Data, Fake_Data):
        """
        Implementation of the loss function of the WGAN critic.

        Keyword arguments:
        Real_Data  --- Set of real events
        Fake_Data  --- Set of fake events
        """
        real_loss = tf.reduce_mean(Real_Data)
        fake_loss = tf.reduce_mean(Fake_Data)
        return fake_loss - real_loss

    def Generator_Loss_Function(self, Fake_Data):
        """
        Implementation of the loss function of the WGAN generator.

        Keyword arguments:
        Fake_Data  --- Set of fake events
        """
        return -tf.reduce_mean(Fake_Data)

    def Define_Generator(self):
        """
        Building a generator model which takes noise as input and produces fake events
        """
        Generator = Sequential()
        Generator.add(Dense(2048, input_dim=self.get("LatentDim"), activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(2048, activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(1024, activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(256, activation="relu"))
        Generator.add(Dense(self.InputDim, activation='tanh'))
        return Generator

    def Define_Critic(self):
        """
        Building a critic model which takes event information as input and
        'criticises' them.
        """
        Critic = Sequential()
        Critic.add(Dense(2048, input_dim=self.InputDim, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(2048, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(1024, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(256, activation="linear"))
        Critic.add(Dense(self.get("OutputSize")))
        return Critic

    ###########################################################################
    ############################ Model Compilation ############################
    ###########################################################################

    def compile(self):
        """
        Compilation of the WGAN model. Effectively stacking them on top of each other.
        The critic layers are turned of for the training.
        """
        super(WGANModel, self).compile()
        Generator = self.Define_Generator()
        Critic = self.Define_Critic()
        self.Generator = Generator
        self.Critic = Critic

    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def Train(self, X, Y, W, output_path, Fold):
        """
        Training the WGAN model.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if Fold != "":
            Fold = "_Fold" + Fold
        c1_hist, c2_hist, g_hist = list(), list(), list()
        for i in range(self.get("Epochs")):
            c1_tmp, c2_tmp = list(), list()
            for _ in range(self.get("nCriticUpdates")):
                choice = np.random.randint(0, len(X), size=self.get("BatchSize"))
                X_real = X[choice][:]
                W_real = W[choice][:]
                Y_real = -np.ones((self.get("BatchSize"), 1))
                c_loss1 = self.Critic.train_on_batch(X_real, Y_real, sample_weight=W_real)
                c1_tmp.append(c_loss1)

                noise = np.random.uniform(0, 1, size=[self.get("BatchSize"), self.get("LatentDim")])
                X_fake = self.Generator.predict(noise)
                Y_fake = np.ones((self.get("BatchSize"), 1))
                # update critic model weights
                c_loss2 = self.Critic.train_on_batch(X_fake, Y_fake)
                c2_tmp.append(c_loss2)
            c1_hist.append(np.mean(c1_tmp))
            c2_hist.append(np.mean(c2_tmp))

            X_gan = np.random.uniform(-1, 1, size=[self.get("BatchSize"), self.get("LatentDim")])
            Y_gan = np.ones((self.get("BatchSize"), 1))
            # update the generator via the critic's error
            g_loss = self.kerasmodel.train_on_batch(X_gan, Y_gan)
            g_hist.append(g_loss)
            print('Epoch %d/%d' % (i + 1, self.get("Epochs")))
            print('critic_real_loss: %.3f - critic_fake_loss: %.3f - generator_loss:%.3f' % (c1_hist[-1], c2_hist[-1], g_loss))

        WGAN_Loss_dict = {"Critic_Real_Loss": dict(zip(range(len(c1_hist)), c1_hist)),
                          "Critic_Fake_Loss": dict(zip(range(len(c2_hist)), c2_hist)),
                          "Generator_Loss": dict(zip(range(len(g_hist)), g_hist))}

        TrainerDoneMessage("Training done! Writing model to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        self.kerasmodel.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        self.Generator.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Generator" + Fold + ".h5")
        TrainerDoneMessage("Writing history to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json")
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json", mode='w') as f:
            json.dump(WGAN_Loss_dict, f)

    def CompileAndTrain(self, X, Y, W, output_path, Fold="", Variables=None):
        '''
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        cbk = WGANMonitor(num_events=10000, latent_dim=self.get("LatentDim"))
        if(len(np.unique(Y)) > 2):
            Y = np.array(hf.OneHot(Y)).astype(np.float32)
        else:
            Y = np.array(Y).astype(np.float32)
        if self.get("SmoothingAlpha") is not None:
            Y = self.smooth_labels(Y, self.get("SmoothingAlpha"))
        seed = np.random.randint(1000, size=1)[0]
        X_train, _, y_train, _ = train_test_split(X, Y, test_size=self.get("ValidationSize"), random_state=seed)
        self.compile()
        history = self.fit(X_train, y_train, batch_size=self.get("BatchSize"), epochs=self.get("Epochs"), callbacks=[cbk])
        TrainerDoneMessage("Training done! Writing model to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        self.Critic.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        self.Generator.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_Generator" + Fold + ".h5")
        TrainerDoneMessage("Writing history to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json", mode='w') as f:
            hist_df.to_json(f)

    def Print(self, fn=print):
        """
        Simple print method.
        """
        self.m_options.Print(fn)


class WGANMonitor(tf.keras.callbacks.Callback):
    def __init__(self, num_events=10000, latent_dim=128):
        self.num_events = num_events
        self.latent_dim = latent_dim

    def on_epoch_end(self, epoch, logs=None):
        random_latent_vectors = tf.random.normal(shape=(self.num_events, self.latent_dim))
        generated_events = self.model.Generator(random_latent_vectors)
        print(generated_events[:, 0])
        print(generated_events[:, 1])
