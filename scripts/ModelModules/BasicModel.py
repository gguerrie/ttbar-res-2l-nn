"""
Module to build a basic model from which more complicated model modules inherit.
"""
import tensorflow as tf
from HelperModules.OptionHandler import OptionHandler

class BasicModel(tf.keras.Model):
    """
    This class serves as the base class for any machine learning model in the framework.
    All major properties should be defined in here and then passed down via inheritance.
    All sub-classes should implicitly call the constructor of this class.
    """
    def __init__(self, group):
        self.m_Group = group # String containing all config options for the model
        self.m_options = OptionHandler() # description is set later
        self.m_options.register_option("Name", optiondefault=None)
        self.m_options.register_option("Type", optiondefault=None)
        self.m_options.register_option("LearningRate", optiondefault=0.001)
        self.m_options.register_option("StepLearningRate", optiondefault=0.0001)
        self.m_options.register_option("MaxLearningRate", optiondefault=0.1)
        self.m_options.register_option("MinLearningRate", optiondefault=0.0001)
        self.m_options.register_option("ValidationSize", optiondefault=0.2)
        self.m_options.register_option("Metrics", optiondefault=None, optionislist=True)
        self.m_options.register_option("ClassLabels", optiondefault=None, optionisdeprecated=True, deprecationstring="Replaced by 'OutputLabels' option.")
        self.m_options.register_option("OutputLabels", optiondefault=None, optionislist=True)
        self.m_options.register_option("ModelBinning", optiondefault=None, optionisnestedlist=True)
        self.m_options.register_option("ClassColors", optiondefault=None, optionislist=True)
        self.m_options.register_option("SampleFrac", optiondefault=1)
        self.m_options.register_option("BinningOptimisation", optiondefault="None")
        self.m_options.register_option("Verbosity", optiondefault = 1)

        self.m_options.register_check(optionname = "Name", check = "set")
        self.m_options.register_check(optionname = "Type", check = "set")
        self.m_options.register_check(optionname = "Type", check = "inlist", lst=['Classification-DNN',
                                                                                  'Classification-BDT',
                                                                                  'Classification-WGAN',
                                                                                  'Classification-ACGAN',
                                                                                  'Classification-GNN',
                                                                                  'Regression-DNN',
                                                                                  '3LZ-Reconstruction',
                                                                                  '4LZ-Reconstruction'])
        self.m_options.register_check(optionname = "LearningRate", check = "larger", thr=0)
        self.m_options.register_check(optionname = "StepLearningRate", check = "larger", thr=0.00001)
        self.m_options.register_check(optionname = "MaxLearningRate", check = "larger", thr=0)
        self.m_options.register_check(optionname = "MinLearningRate", check = "larger", thr=0)
        self.m_options.register_check(optionname = "MaxLearningRate", check = "larger", thr="MinLearningRate")
        self.m_options.register_check(optionname = "ValidationSize", check = "larger", thr=0)
        self.m_options.register_check(optionname = "ValidationSize", check = "smaller", thr=1)
        self.m_options.register_check(optionname = "OutputLabels", check = "set")
        self.m_options.register_check(optionname = "ModelBinning", check = "set")
        self.m_options.register_check(optionname = "ModelBinning", check = "equal_len", l="OutputLabels")
        self.m_options.register_check(optionname = "BinningOptimisation", check = "inlist", lst=["None","TransfoD_symmetric","TransfoD_flatS","TransfoD_flatB"])
        self.m_options.register_check(optionname = "Verbosity", check = "inlist", lst = [0,1,2])

        # Making sure tensorflow only grabs as much memory as it needs and not ALL OF IT!
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                for gpu in gpus:
                    tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        super().__init__()
        # del gpus

    ######################################################################
    ############################ Misc Methods ############################
    #####################################################################
    def smooth_labels(self, Y, K, alpha=0.1):
        '''
        Function to smooth labels as presented in arXiv:1906.02629 and arXiv:1512.00567

        Keyword Arguments:
        Y     --- Labels (list)
        alpha --- Smoothing parameter (should be between 0 and < 0.5)
        '''
        return Y*(1-alpha)+alpha/K

    def get(self,name):
        """
        Getter method to return options
        Keyword arguments:
        name --- name of the option to be returned
        """
        return self.m_options.get(name)

    def isClassification(self):
        """
        Defines whether a model is a classification model
        """
        return (self.isClassificationDNN()
                or self.isClassificationBDT()
                or self.isClassificationWGAN()
                or self.isClassificationGNN()
                or self.isClassificationACGAN())

    def isClassificationDNN(self):
        """
        Defines whether a model is a classification model (DNN)
        """
        return self.m_options.get("Type")=="Classification-DNN"

    def isClassificationBDT(self):
        """
        Defines whether a model is a classification model (BDT)
        """
        return self.m_options.get("Type")=="Classification-BDT"

    def isClassificationGNN(self):
        """
        Defines whether a model is a classification model (GNN)
        """
        return self.m_options.get("Type")=="Classification-GNN"

    def isClassificationWGAN(self):
        """
        Defines whether a model is a classification model (WGAN)
        """
        return self.m_options.get("Type")=="Classification-WGAN"

    def isClassificationACGAN(self):
        """
        Defines whether a model is a classification model (ACGAN)
        """
        return self.m_options.get("Type")=="Classification-ACGAN"

    def isRegressionDNN(self):
        """
        Defines whether a model is a regression model (DNN)
        """
        return self.m_options.get("Type")=="Regression-DNN"

    def isRegressionBDT(self):
        """
        Defines whether a model is a regression model (BDT)
        """
        return self.m_options.get("Type")=="Regression-BDT"

    def isRegression(self):
        """
        Defines whether a model is a regression model
        """
        return self.isRegressionBDT() or self.isRegressionDNN()

    def is3LZReconstruction(self):
        """
        Defines whether a model is a model for 3LZ reconstruction
        """
        return self.m_options.get("Type")=="3LZ-Reconstruction"

    def is4LZReconstruction(self):
        """
        Defines whether a model is a model for 4LZ reconstruction
        """
        return self.m_options.get("Type")=="4LZ-Reconstruction"

    def isReconstruction(self):
        """
        Defines whether a model is a reconstruction model
        """
        return self.is3LZReconstruction() or self.is4LZReconstruction()

    def get_nLeps(self):
        """
        Method to return the number of expected leptons
        """
        if self.is3LZReconstruction():
            return 3
        if self.is4LZReconstruction():
            return 4
        return 0

    ##############################################################################
    ############################ Printing Information ############################
    ##############################################################################

    def Print(self,fn=print):
        """
        Method to represent the class objects as a very basic string.
        """
        self.m_options.Print(fn)
