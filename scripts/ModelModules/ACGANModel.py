"""
Module for a ACGAN model
"""
import copy, json
import numpy as np
import tensorflow as tf
import pandas as pd
import HelperModules.HelperFunctions as hf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense, Input, LeakyReLU, Dropout, BatchNormalization, Reshape, Conv2DTranspose, Flatten, Conv2D
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.initializers import RandomNormal
from HelperModules.OptionHandler import OptionHandler
from HelperModules.MessageHandler import TrainerDoneMessage, TrainerMessage
from ModelModules.BasicModel import BasicModel
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from scipy.stats import wasserstein_distance, chisquare, chi2_contingency
"""
Module for a ACGAN model
"""
class ACGANModel(BasicModel):
    """
    Class for implementing an Auxiliary Classifier GAN (ACGAN).
    Paper: https://arxiv.org/abs/1610.09585
    """
    def __init__(self, group, input_dim):
        super().__init__(group)
        self.InputDim = input_dim # Input dimensionality of the data
        
        self._options = OptionHandler(self._options, "ACGANMODEL")
        self._options.register_option("Epochs", optiondefault=100)
        self._options.register_option("BatchSize", optiondefault=32)
        self._options.register_option("OutputSize", optiondefault = None)
        self._options.register_option("LatentDim", optiondefault = 128)
        self._options.register_option("DiscriminatorDropoutIndece", optiondefault = [], optionislist = True)
        self._options.register_option("DiscriminatorDropoutProb", optiondefault = 0.1)
        self._options.register_option("GeneratorDropoutIndece", optiondefault = [], optionislist = True)
        self._options.register_option("GeneratorDropoutProb", optiondefault = 0.1)
        self._options.register_option("GeneratorNodes", optiondefault = None, optionislist = True)
        self._options.register_option("DiscriminatorBatchNormIndece", optiondefault = [], optionislist = True)
        self._options.register_option("GeneratorBatchNormIndece", optiondefault = [], optionislist = True)
        self._options.register_option("GeneratorActivations", optiondefault = None, optionislist = True)
        self._options.register_option("GeneratorOutputActivation", optiondefault = "tanh")
        self._options.register_option("DiscriminatorNodes", optiondefault = None, optionislist = True)
        self._options.register_option("DiscriminatorActivations", optiondefault = None, optionislist = True)
        self._options.register_option("DiscriminatorOutputActivation", optiondefault = None)
        self._options.register_option("DiscriminatorLoss", optiondefault = "binary_crossentropy")
        self._options.register_option("SmoothingAlpha", optiondefault = None)
        self._options.register_option("Patience", optiondefault = None)
        self._options.register_option("MinDelta", optiondefault = None)
        self._options.register_option("DiscriminatorLR", optiondefault = 0.0001)
        self._options.register_option("GeneratorLR", optiondefault = 0.0001)
        self._options.register_option("BatchSize", optiondefault=32)

        self._options.register_check(optionname = "Epochs", check = "larger", thr=0)
        self._options.register_check(optionname = "BatchSize", check = "larger", thr=0)
        self._options.register_check(optionname = "OutputSize", check = "set")
        self._options.register_check(optionname = "LatentDim", check = "larger", thr = 0)
        self._options.register_check(optionname = "DiscriminatorDropoutProb", check = "larger", thr = 0)
        self._options.register_check(optionname = "GeneratorDropoutProb", check = "larger", thr = 0)
        self._options.register_check(optionname = "GeneratorNodes", check = "set")
        self._options.register_check(optionname = "GeneratorActivations", check = "set")
        self._options.register_check(optionname = "DiscriminatorNodes", check = "set")
        self._options.register_check(optionname = "DiscriminatorActivations", check = "set")
        self._options.register_check(optionname = "SmoothingAlpha", check = "smaller", thr = 0.5)
        self._options.register_check(optionname = "SmoothingAlpha", check = "larger", thr = 0.0)
        self._options.register_check(optionname = "DiscriminatorLR", check = "larger", thr = 0.0)
        self._options.register_check(optionname = "GeneratorLR", check = "larger", thr = 0.0)
        self._options.register_check(optionname = "Patience", check = "larger", thr = 0)
        self._options.register_check(optionname = "MinDelta", check = "larger", thr = 0)
        
        for item in self.m_Group:
            self._options.set_option(item)
        self._options.check_options()
        self.d_optimizer = None
        self.g_optimizer = None
        self.discriminator = None
        self.generator = None

    def _generate_real_events(self, X, n_samples):
        """
        Returns real events
        """
        ix = np.random.randint(0, X.shape[0], n_samples)
        X = X[ix]
        y = np.zeros((n_samples, 1))
        # y = y + (np.random.random(y.shape) * 0.5)
        return X, y
        
    def _generate_latent_points(self, n_samples, n_classes = 2):
        x_input = np.random.normal(loc = 0.0, scale = 1.0, size = self.get("LatentDim") * n_samples)
        z_input = x_input.reshape(n_samples, self.get("LatentDim"))
        labels = np.random.randint(0, n_classes-1, n_samples)
        return z_input

    def _generate_fake_events(self, g_model, n_samples):
        z_input = self._generate_latent_points(n_samples)
        fake_events = g_model.predict(z_input)
        y = np.ones((n_samples,1))
        # y = y - 0.3 + (np.random.random(y.shape) * 0.5)
        return fake_events, y

    def _define_NN(self, nodes, activations, out_activation, in_size, out_size, name):
        layer_id = 0
        in_layer = Input(shape=(in_size,), name=name+"_Input")
        for n, a in zip(nodes,activations):
            if layer_id == 0:
                l = in_layer
            else:
                l = model_tmp
            if a == "LeakyReLU":
                model_tmp = Dense(units = n,
                                  kernel_initializer = "he_uniform",
                                  name = name+"_"+str(layer_id))(l)
                model_tmp = LeakyReLU()(model_tmp)
            else:
                model_tmp = Dense(units = n,
                                  activation = a,
                                  kernel_initializer = "he_uniform",
                                  name = name+"_"+str(layer_id))(l)
            if layer_id in self.get(name+"DropoutIndece"):
                model_tmp = Dropout(self.get(name+"DropoutProb"))(model_tmp)
            if layer_id in self.get(name+"BatchNormIndece"):
                model_tmp = BatchNormalization()(model_tmp)
            layer_id += 1
        out_layer = Dense(units = out_size,
                          activation = out_activation)(model_tmp)
        return Model(in_layer, out_layer, name = name)
        
    
    def _define_discriminator(self):
        D_input = Input(shape=(self.InputDim,))

        D = Dense(128)(D_input)
        D = Reshape((8, 8, 2))(D)

        D = Conv2D(64, kernel_size=3, strides=1, padding="same")(D)
        D = LeakyReLU(alpha=0.2)(D)

        D = Conv2D(32, kernel_size=3, strides=1, padding="same")(D)
        #D = BatchNormalization()(D)
        D = LeakyReLU(alpha=0.2)(D)

        D = Conv2D(16, kernel_size=3, strides=1, padding="same")(D)
        #D = BatchNormalization()(D)
        D = LeakyReLU(alpha=0.2)(D)

        D = Flatten()(D)
        #D = BatchNormalization()(D)
        D = LeakyReLU(alpha=0.2)(D)

        D = Dropout(0.2)(D)
        
        D_output = Dense(1, activation="sigmoid")(D)
        #    D_output = Dense(1)(D)

        discriminator = Model(D_input, D_output)
        return discriminator

        
        # discriminator = self._define_NN(nodes = self.get("DiscriminatorNodes"),
        #                                 activations = self.get("DiscriminatorActivations"),
        #                                 out_activation = self.get("DiscriminatorOutputActivation"),
        #                                 in_size = self.InputDim,
        #                                 out_size = self.get("OutputSize"),
        #                                 name = "Discriminator")
        # return discriminator

    # define the standalone generator model
    def _define_generator(self):
        input_layer = Input(shape=(self.get("LatentDim"),), name="Generator_Input")
        model_tmp = Dense(self.get("LatentDim"))(input_layer)
        model_tmp = BatchNormalization()(model_tmp)
        model_tmp = LeakyReLU(alpha=0.2)(model_tmp)
        model_tmp = Reshape((8, 8, 2))(model_tmp)

        model_tmp = Conv2DTranspose(32, kernel_size=2, strides=1, padding="same")(model_tmp)
        model_tmp = BatchNormalization()(model_tmp)
        model_tmp = LeakyReLU(alpha=0.2)(model_tmp)

        model_tmp = Conv2DTranspose(16, kernel_size=3, strides=1, padding="same")(model_tmp)
        model_tmp = BatchNormalization()(model_tmp)
        model_tmp = LeakyReLU(alpha=0.2)(model_tmp)

        model_tmp = Flatten()(model_tmp)
        output_layer = Dense(self.InputDim, activation=self.get("GeneratorOutputActivation"))(model_tmp)
        return Model(input_layer,output_layer)
        
        # return self._define_NN(nodes = self.get("GeneratorNodes"),
        #                        activations = self.get("GeneratorActivations"),
        #                        out_activation = None,
        #                        in_size = self.get("LatentDim"),
        #                        out_size = self.InputDim,
        #                        name = "Generator")

    def compile(self, d_optimizer, g_optimizer, loss_fn):
        super(ACGANModel, self).compile()
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.loss_fn = loss_fn

    def train_step(self, X_real):
        if isinstance(X_real, tuple):
            X_real = X_real[0]
        # Sample random points in the latent space
        batch_size = tf.shape(X_real)[0]
        random_latent_vectors = tf.random.normal(shape=(batch_size, self.get("LatentDim")))
        X_fake = self.generator(random_latent_vectors)
        X_combined = tf.concat([X_fake,X_real], axis = 0)
        labels = tf.concat([tf.ones((batch_size,1)), tf.zeros((batch_size,1))], axis = 0)
        labels += 0.05 * tf.random.uniform(tf.shape(labels)) # Add random noise of the labels

        with tf.GradientTape() as tape:
            predictions = self.discriminator(X_combined)
            d_loss = self.loss_fn(labels, predictions)
        grads = tape.gradient(d_loss, self.discriminator.trainable_weights)
        self.d_optimizer.apply_gradients(zip(grads, self.discriminator.trainable_weights))

        random_latent_vectors = tf.random.normal(shape=(batch_size, self.get("LatentDim")))
        misleading_labels = tf.zeros((batch_size,1))

        #Train the generator
        with tf.GradientTape() as tape:
            predictions = self.discriminator(self.generator(random_latent_vectors))
            g_loss = self.loss_fn(misleading_labels, predictions)
        grads = tape.gradient(g_loss, self.generator.trainable_weights)
        self.g_optimizer.apply_gradients(zip(grads, self.generator.trainable_weights))
        return {"d_loss": d_loss, "g_loss": g_loss}

    def CompileAndTrain(self, X, Y, W, output_path, Fold=""):
        self.discriminator = self._define_discriminator()
        self.generator = self._define_generator()
        self.compile(d_optimizer=tf.keras.optimizers.Adam(learning_rate=self.get("DiscriminatorLR"), beta_1=0.5, beta_2=0.9),
                     g_optimizer=tf.keras.optimizers.Adam(learning_rate=self.get("GeneratorLR"), beta_1=0.5, beta_2=0.9),
                     loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=False),)
        self.ACGANTrain(X, Y, W, output_path, Fold=Fold)
        
    def ACGANTrain(self, X, Y, W, output_path, Fold=""):
        """
        Training ACGAN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        TrainerMessage("Starting Training...")
        if Fold!="":
            Fold = "_Fold"+Fold
        callbacks = []
        print()
        tb = TensorBoard(log_dir=hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+"_TensorBoardLog"+Fold,
                         histogram_freq=5)
        callbacks.append(tb)
        gm = GANMonitor(X, input_dim=self.InputDim)
        callbacks.append(gm)
        if self.get("Patience") is not None and self.get("MinDelta") is not None:
            es = EarlyStopping(monitor='chi2/ndof',
                               min_delta=self.get("MinDelta"),
                               patience = self.get("Patience"),
                               restore_best_weights=True,
                               verbose=1)
            callbacks.append(es)
        history = self.fit(X, epochs = self.get("Epochs"), callbacks = callbacks)
        TrainerDoneMessage("Training done! Writing Generator model to "+ hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+"_Generator"+Fold+".h5")
        self.generator.save(hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+"_Generator"+Fold+".h5")
        TrainerDoneMessage("Training done! Writing Discriminator model to "+ hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+Fold+".h5")
        self.discriminator.save(hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+Fold+".h5")
        TrainerDoneMessage("Writing history to "+ hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+"_history"+Fold+".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path)+self.get("Name")+"_"+self.get("Type")+"_history"+Fold+".json", mode='w') as f:
            hist_df.to_json(f)

class GANMonitor(tf.keras.callbacks.Callback):
    def __init__(self, X, input_dim, num_events=1000, latent_dim=128):
        self.num_events = num_events
        self.latent_dim = latent_dim
        self.input_dim = input_dim
        self.X = X

    def on_epoch_end(self, epoch, logs):
        random_latent_vectors = tf.random.normal(shape=(self.num_events, self.latent_dim))
        X_fake = self.model.generator(random_latent_vectors)
        X_real = self.X[np.random.choice(self.X.shape[0], size=self.num_events, replace=False),:]
        nbins = 10
        h_real, h_fake = [], []
        for i in range(self.input_dim):
            h_real_vals, h_real_edges = np.histogram(X_real[:,i], bins=nbins, range=(-1,1))
            h_fake_vals, h_fake_edges = np.histogram(X_fake[:,i], bins=nbins, range=(-1,1))
            h_real.append(h_real_vals)
            h_fake.append(h_fake_vals)
        h_real = np.array(h_real).T
        h_fake = np.array(h_fake).T
        logs['chi2/ndof']=np.sum(chisquare(h_fake,h_real)[0])/(nbins*self.X.shape[1])
