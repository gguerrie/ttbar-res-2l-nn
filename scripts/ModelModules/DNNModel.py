"""
Module for a DNN model
"""

import os
from collections import defaultdict
import pandas as pd
import numpy as np
import tensorflow as tf
import HelperModules.HelperFunctions as hf
from HelperModules.OptionHandler import OptionHandler
from ModelModules.BasicModel import BasicModel
from HelperModules.MessageHandler import TrainerMessage, TrainerDoneMessage, OptimiserMessage
from tensorflow.keras.layers import Add, Dense, Dropout, concatenate, BatchNormalization, Input, multiply
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras import regularizers
from sklearn.model_selection import train_test_split


class DNNModel(BasicModel):
    """
    Class for implementing feed-forward-style neural networks.
    This class serves as a base class for any such type of neural network.
    """

    def __init__(self, group, input_dim):
        super().__init__(group)
        self.m_InputDim = input_dim  # Input dimensionality of the data
        self.m_kerasmodel = None  # Keras model object

        self.m_options.set_description("DNNMODEL")
        self.m_options.register_option("Nodes", optiondefault=None, optionislist=True)
        self.m_options.register_option("Epochs", optiondefault=100)
        self.m_options.register_option("BatchSize", optiondefault=32)
        self.m_options.register_option("MaxNodes", optiondefault=100)
        self.m_options.register_option("MinNodes", optiondefault=10)
        self.m_options.register_option("StepNodes", optiondefault=10)
        self.m_options.register_option("MaxLayers", optiondefault=7)
        self.m_options.register_option("MinLayers", optiondefault=1)
        self.m_options.register_option("PredesignedModel", optiondefault=None)
        self.m_options.register_option("Patience", optiondefault=None)
        self.m_options.register_option("MinDelta", optiondefault=None)
        self.m_options.register_option("DropoutIndece", optiondefault=[], optionislist=True)
        self.m_options.register_option("DropoutProb", optiondefault=0.1)
        self.m_options.register_option("BatchNormIndece", optiondefault=[], optionislist=True)
        self.m_options.register_option("SetActivationFunctions", optiondefault=["relu","sigmoid","tanh","elu"], optionislist=True)
        self.m_options.register_option("ActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("KernelInitialiser", optiondefault="glorot_uniform")
        self.m_options.register_option("KernelRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("BiasRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("ActivityRegulariser", optiondefault=None, optionislist=True)
        self.m_options.register_option("OutputSize", optiondefault=None)
        self.m_options.register_option("OutputActivation", optiondefault=None)
        self.m_options.register_option("Loss", optiondefault=None)
        self.m_options.register_option("Optimiser", optiondefault="Nadam")
        self.m_options.register_option("SmoothingAlpha", optiondefault=None)
        self.m_options.register_option("UseTensorBoard", optiondefault=False)
        self.m_options.register_option("RegressionTarget", optiondefault=[None], optionislist=True)
        self.m_options.register_option("RegressionTargetLabel", optiondefault=[None], optionislist=True)
        self.m_options.register_option("RegressionScale", optiondefault=[1.0], optionislist=True)
        self.m_options.register_option("ReweightTargetDistribution", optiondefault=True)
        self.m_options.register_option("HuberDelta", optiondefault=None)

        self.m_options.register_check(optionname="Nodes", check="set")
        self.m_options.register_check(optionname="Nodes", check="equal_len", l="ActivationFunctions")
        self.m_options.register_check(optionname="Epochs", check="larger", thr=0)
        self.m_options.register_check(optionname="BatchSize", check="larger", thr=0)
        self.m_options.register_check(optionname="MinNodes", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxNodes", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxNodes", check="larger", thr="MinNodes")
        self.m_options.register_check(optionname="MinLayers", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxLayers", check="larger", thr=0)
        self.m_options.register_check(optionname="MaxLayers", check="larger", thr="MinLayers")
        self.m_options.register_check(optionname="Patience", check="larger", thr=0)
        self.m_options.register_check(optionname="MinDelta", check="larger", thr=0)
        self.m_options.register_check(optionname="DropoutProb", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="ActivationFunctions", check="set")
        self.m_options.register_check(optionname="KernelInitialiser", check="inlist", lst=["glorot_uniform", "glorot_normal", "lecun_normal", "lecun_uniform"])
        self.m_options.register_check(optionname="KernelRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="BiasRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="ActivityRegulariser", check="inlist", lst=[None, "None", "l1", "l2", "l1_l2"])
        self.m_options.register_check(optionname="OutputActivation", check="set")
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="OutputSize", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputLabels", check="equal_len", l="OutputSize")
        self.m_options.register_check(optionname="RegressionTargetLabel", check="equal_len", l="RegressionTarget", case="Regression-DNN")
        self.m_options.register_check(optionname="RegressionTargetLabel", check="equal_len", l="RegressionScale", case="Regression-DNN")
        self.m_options.register_check(optionname="RegressionTarget", check="equal_len", l="RegressionScale", case="Regression-DNN")
        self.m_options.register_check(optionname="RegressionTarget", check="equal_len", l="OutputLabels", case="Regression-DNN")
        self.m_options.register_check(
            optionname="Loss",
            check="inlist",
            lst=[
                "binary_crossentropy",
                "categorical_crossentropy",
                "mean_squared_error",
                "mean_absolute_error",
                "mean_absolute_percentage_error",
                "mean_squared_logarithmic_error",
                "cosine_similarity",
                "huber_loss",
                "log_cosh"])

        for item in self.m_Group:
            self.m_options.set_option(item)
        self.m_options.check_options()

    ###########################################################################
    ############################### Misc methods ##############################
    ###########################################################################
    def get_Optimiser(self):
        """
        Getter method to return the actual optimiser object
        """
        optimisers = {"Adadelta": tf.keras.optimizers.Adadelta(learning_rate=self.get("LearningRate"), name='Adadelta'),
                      "Adagrad": tf.keras.optimizers.Adagrad(learning_rate=self.get("LearningRate"), name='Adagrad'),
                      "Adam": tf.keras.optimizers.Adam(learning_rate=self.get("LearningRate"), name='Adam'),
                      "Adamax": tf.keras.optimizers.Adamax(learning_rate=self.get("LearningRate"), name='Adamax'),
                      "Ftrl": tf.keras.optimizers.Ftrl(learning_rate=self.get("LearningRate"), name='Ftrl'),
                      "Nadam": tf.keras.optimizers.Nadam(learning_rate=self.get("LearningRate"), name='Nadam'),
                      "RMSprop": tf.keras.optimizers.RMSprop(learning_rate=self.get("LearningRate"), name='RMSprop'),
                      "SGD": tf.keras.optimizers.SGD(learning_rate=self.get("LearningRate"), name='SGD')}
        return optimisers[self.get("Optimiser")]

    def DefineRegulariser(self, Regulariser, l1, l2):
        """
        Simple function to create a list of keras.regularizers objects
        """
        Regularisers = []
        Regulariser_strings = ["None"] * len(self.get("Nodes")) if Regulariser is None else Regulariser
        for R in Regulariser_strings:
            if R == "l1":
                Regularisers.append(regularizers.l1(l1=l1))
            elif R == "l2":
                Regularisers.append(regularizers.l2(l2=l2))
            elif R == "l1_l2":
                Regularisers.append(regularizers.l1_l2(l1=l1, l2=l2))
            elif R == "None":
                Regularisers.append(None)
        return Regularisers

    def DNNCompile(self):
        """
        Compile a simple densely connected NN. This is our heavy lifting method.
        It is used whenever the user uses the "Nodes" option in the config file to build
        a densely connected NN.
        """
        DropoutIndece = self.get("DropoutIndece")
        BatchNormIndece = self.get("BatchNormIndece")
        # Set up the regularisers
        KernelRegularisers = self.DefineRegulariser(self.get("KernelRegulariser"), 0.01, 0.01)
        BiasRegularisers = self.DefineRegulariser(self.get("BiasRegulariser"), 0.01, 0.01)
        ActivityRegularisers = self.DefineRegulariser(self.get("ActivityRegulariser"), 0.01, 0.01)
        # Lets build a feed forward NN using keras from scratch
        Layers = []
        Layers.append(Input(shape=(self.m_InputDim,), name="Input_Layer"))
        for NodeLayerID, nodes in enumerate(self.get("Nodes")):
            Layers.append(Dense(nodes,
                                name="Hidden_Layer_" + str(NodeLayerID),
                                activation=self.get("ActivationFunctions")[NodeLayerID],
                                kernel_initializer=self.get("KernelInitialiser"),
                                kernel_regularizer=KernelRegularisers[NodeLayerID],
                                bias_regularizer=BiasRegularisers[NodeLayerID],
                                activity_regularizer=ActivityRegularisers[NodeLayerID])(Layers[-1]))
            if DropoutIndece:
                if NodeLayerID in DropoutIndece:
                    Layers.append(Dropout(self.get("DropoutProb"))(Layers[-1])) # Adding a Dropout layers
            if BatchNormIndece:
                if NodeLayerID in BatchNormIndece:
                    Layers.append(BatchNormalization()(Layers[-1]))  # Adding BatchNormalisation layers
        Layers.append(Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output_Layer")(Layers[-1]))
        self.m_kerasmodel = tf.keras.Model(inputs=Layers[0], outputs=Layers[-1])
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    def PDNNCompile(self):
        """
        Compile a custom connected NN with two parallel layers.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.m_InputDim,))
        Dense_Layer = Dense(50, activation="relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1 = Dense(50, activation="relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2 = Dense(50, activation="relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer = Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output")(Dense_Layer)
        self.m_kerasmodel = tf.keras.Model(inputs=Input_Layer, outputs=Output_Layer)
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    def DPDNNCompile(self):
        """
        Compile a custom connected NN with four parallel layers.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.m_InputDim,))
        Dense_Layer = Dense(50, activation="relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1 = Dense(50, activation="relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2 = Dense(50, activation="relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_3 = Dropout(0.2)(Dense_Layer_3)
        Dense_Layer_3 = Dense(50, activation="relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4 = Dense(50, activation="relu")(Dense_Layer)
        Dropout_Layer_4 = Dropout(0.2)(Dense_Layer_4)
        Dense_Layer_4 = Dense(50, activation="relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer = Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output")(Dense_Layer)
        self.m_kerasmodel = tf.keras.Model(inputs=Input_Layer, outputs=Output_Layer)
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    def PADNNCompile(self):
        """
        Compile a custom connected NN with two parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.m_InputDim,))
        Dense_Layer = Dense(50, activation="relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation="sigmoid")(Dense_Layer_1)
        Attention_Layer_1 = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1 = Dropout(0.2)(Attention_Layer_1)
        Dense_Layer_1 = Dense(50, activation="relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation="sigmoid")(Dense_Layer_2)
        Attention_Layer_2 = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2 = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2 = Dense(50, activation="relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer = Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output")(Dense_Layer)
        self.m_kerasmodel = tf.keras.Model(inputs=Input_Layer, outputs=Output_Layer)
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    def DPADNNCompile(self):
        """
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.m_InputDim,))
        Dense_Layer = Dense(50, activation="relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation="sigmoid")(Dense_Layer_1)
        Attention_Layer_1 = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1 = Dropout(0.2)(Attention_Layer_1)
        Dense_Layer_1 = Dense(50, activation="relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation="sigmoid")(Dense_Layer_2)
        Attention_Layer_2 = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2 = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2 = Dense(50, activation="relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_3 = Dense(50, activation="sigmoid")(Dense_Layer_3)
        Attention_Layer_3 = multiply([Dense_Layer_3, Attention_Prob_Layer_3])
        Dropout_Layer_3 = Dropout(0.2)(Attention_Layer_3)
        Dense_Layer_3 = Dense(50, activation="relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4 = Dense(50, activation="relu")(Dense_Layer)
        Attention_Prob_Layer_4 = Dense(50, activation="sigmoid")(Dense_Layer_4)
        Attention_Layer_4 = multiply([Dense_Layer_4, Attention_Prob_Layer_4])
        Dropout_Layer_4 = Dropout(0.2)(Attention_Layer_4)
        Dense_Layer_4 = Dense(50, activation="relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer = Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output")(Dense_Layer)
        self.m_kerasmodel = tf.keras.Model(inputs=Input_Layer, outputs=Output_Layer)
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=self.get_Optimiser(),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    def ResDNNCompile(self):
        """
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.m_InputDim,))
        Dense_Layer = Dense(50, activation="relu")(Input_Layer)

        Shortcut = Dense_Layer
        Dense_Layer = Dense(50, activation="relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation="relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation="relu")(Add_Layer)

        Shortcut = Dense_Layer
        Dense_Layer = Dense(50, activation="relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation="relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation="relu")(Add_Layer)

        Output_Layer = Dense(self.get("OutputSize"), activation=self.get("OutputActivation"), name="Output")(Add_Layer)
        self.m_kerasmodel = tf.keras.Model(inputs=Input_Layer, outputs=Output_Layer)
        self.m_kerasmodel.compile(loss=[self.get("Loss")],
                                  optimizer=tf.keras.optimizers.Nadam(learning_rate=self.get("LearningRate")),
                                  weighted_metrics=self.get("Metrics"))
        self.m_kerasmodel.summary()

    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def DNNTrain(self, X, Y, W, output_path, Fold=""):
        """
        Training a simple densely connected NN. This method serves as the "heavy lifter" for training.
        It should work well for not to complicated inputs/outputs.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """

        print(X)
        print(Y)

        if Fold != "":
            print(Fold)
            Fold = "_Fold" + Fold
        # Definition of Callbacks
        callbacks = []
        if self.get("UseTensorBoard"):
            tb = TensorBoard(log_dir=hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_TensorBoardLog" + Fold,
                             histogram_freq=5)
            callbacks.append(tb)
        if self.get("Patience") is not None and self.get("MinDelta") is not None:
            es = EarlyStopping(monitor='val_loss', min_delta=self.get("MinDelta"), patience=self.get("Patience"), restore_best_weights=True)
            callbacks.append(es)
        seed = np.random.randint(1000, size=1)[0]
        W_train, W_val, _, _ = train_test_split(W, Y, test_size=self.get("ValidationSize"), random_state=seed)
        X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=self.get("ValidationSize"), random_state=seed)

        TrainerMessage("Starting Training...")
        if callbacks:
            history = self.m_kerasmodel.fit(X_train, y_train, epochs=self.get("Epochs"), batch_size=self.get("BatchSize"), verbose=self.get("Verbosity"), sample_weight=W_train, validation_data=(X_val, y_val, W_val), callbacks=callbacks)
        else:
            history = self.m_kerasmodel.fit(X_train, y_train, epochs=self.get("Epochs"), batch_size=self.get("BatchSize"), verbose=self.get("Verbosity"), sample_weight=W_train, validation_data=(X_val, y_val, W_val))

        TrainerDoneMessage("Training done! Writing model to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        self.m_kerasmodel.save(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".h5")
        arch = self.m_kerasmodel.to_json()
        # save the architecture string to a file somehow, the below will work
        TrainerDoneMessage("Training done! Writing model architecture to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + "_Architecture.json")
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + "_Architecture.json", 'w') as arch_file:
            arch_file.write(arch)
        # now save the weights as an HDF5 file
        TrainerDoneMessage("Training done! Writing model weights to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + "_Weights.h5")
        self.m_kerasmodel.save_weights(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + "_Weights.h5")
        TrainerDoneMessage("Writing history to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json", mode='w') as f:
            hist_df.to_json(f)

    def CompileAndTrain(self, X, Y, W, output_path, Fold=""):
        """
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if len(self.get("OutputLabels")) > 2 and self.get("Loss") == "categorical_crossentropy":
            Y = np.array(hf.OneHot(Y, len(self.get("OutputLabels")))).astype(np.float32)
        elif len(self.get("OutputLabels")) > 1:
            Y = np.array(Y.tolist()).astype(np.float32)
        else:
            Y = np.array(Y.tolist()).astype(np.float32)
        if self.get("SmoothingAlpha") is not None:
            Y = self.smooth_labels(Y, K=self.__OutputSize, alpha=self.get("SmoothingAlpha"))
        # Pre-designed paralell DNN
        if self.get("PredesignedModel") == "PDNN":
            self.PDNNCompile()
        # Pre-designed double paralell DNN
        elif self.get("PredesignedModel") == "DPDNN":
            self.DPDNNCompile()
        # Pre-designed paralell DNN with attention
        elif self.get("PredesignedModel") == "PADNN":
            self.PADNNCompile()
        # Pre-designed double paralell DNN with attention
        elif self.get("PredesignedModel") == "DPADNN":
            self.DPADNNCompile()
        # Pre-designed ResDNN
        elif self.get("PredesignedModel") == "ResDNN":
            self.ResDNNCompile()
        # "Normal" densely connected DNN
        else:
            self.DNNCompile()
        self.DNNTrain(X, Y, W, output_path, Fold)

    def Print(self, fn=print):
        """
        Simple print method.
        """
        self.m_options.Print(fn)
