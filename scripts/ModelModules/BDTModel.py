"""
Module for a BDT model
"""
from pickle import dump
import sklearn
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import TrainerMessage, TrainerDoneMessage
from HelperModules.OptionHandler import OptionHandler
from ModelModules.BasicModel import BasicModel
from packaging import version
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss, mean_squared_error, mean_absolute_error


class BDTModel(BasicModel):
    """
    Class for implementing boosted decision trees.
    """

    def __init__(self, group, input_dim):
        super().__init__(group)
        self.m_InputDim = input_dim  # Input dimensionality of the data
        self.m_scikitmodel = None      # Keras model object

        self.m_options.set_description("BDTMODEL")
        self.m_options.register_option("nEstimators", optiondefault=100)
        self.m_options.register_option("MaxDepth", optiondefault=3)
        self.m_options.register_option("MaxFeatures", optiondefault=None)
        self.m_options.register_option("MinSamplesSplit", optiondefault=2)
        self.m_options.register_option("MinSamplesLeaf", optiondefault=1)
        self.m_options.register_option("Patience", optiondefault=None)
        self.m_options.register_option("MinDelta", optiondefault=None)
        self.m_options.register_option("Criterion", optiondefault="friedman_mse")
        self.m_options.register_option("Loss", optiondefault=None)
        self.m_options.register_option("SmoothingAlpha", optiondefault=None)
        self.m_options.register_option("RegressionTarget", optiondefault=None)
        self.m_options.register_option("RegressionTargetLabel", optiondefault=None)

        self.m_options.register_check(optionname="MaxDepth", check="larger", thr=0)
        self.m_options.register_check(optionname="MinSamplesSplit", check="larger", thr=0)
        self.m_options.register_check(optionname="MinSamplesLeaf", check="larger", thr=0)
        self.m_options.register_check(optionname="MinDelta", check="larger", thr=0)
        self.m_options.register_check(optionname="Patience", check="larger", thr=0)
        self.m_options.register_check(optionname="Loss", check="inlist", lst=["deviance","log_loss", "squared_error", "absolute_error"])
        self.m_options.register_check(optionname="Criterion", check="inlist", lst=["friedman_mse", "mse"])
        self.m_options.register_check(optionname="SmoothingAlpha", check="larger", thr=0.5)
        self.m_options.register_check(optionname="SmoothingAlpha", check="smaller", thr=1)
        self.m_options.register_check(optionname="RegressionTarget", check="set", case="Regression-BDT")
        self.m_options.register_check(optionname="RegressionTargetLabel", check="set", case="Regression-BDT")

        for item in self.m_Group:
            self.m_options.set_option(item)
        self.m_options.check_options()

    def BDTLoss(self, y_true, y_pred, sample_weight):
        if self.get("Loss") == "log_loss" or self.get("Loss") == "deviance":
            return log_loss(y_true, y_pred, sample_weight=sample_weight)
        elif self.get("Loss") == "square_error":
            return mean_squared_error(y_true, y_pred, sample_weight=sample_weight)
        elif self.get("Loss") == "square_error":
            return mean_absolute_error(y_true, y_pred, sample_weight=sample_weight)

    def BDTCompile(self):
        """
        Compile a simple BDT model
        """
        if version.parse(sklearn.__version__) >= version.parse("1.1.0") and self.get("Loss") == "deviance":
            loss = "log_loss" # deviance is depricated in sklearn as of 1.3, we replace it starting with v1.1
        elif version.parse(sklearn.__version__) < version.parse("1.1.0") and self.get("Loss") == "log_loss":
            loss = "deviance" # log loss was introduced in 1.1.0
        else:
            loss = self.get("Loss")
        self.m_randomstate = np.random.randint(1000, size=1)[0]
        if self.isRegressionBDT():
            self.m_scikitmodel = GradientBoostingRegressor(n_estimators=self.get("nEstimators"),
                                                           learning_rate=self.get("LearningRate"),
                                                           loss=loss,
                                                           criterion=self.get("Criterion"),
                                                           min_samples_split=self.get("MinSamplesSplit"),
                                                           min_samples_leaf=self.get("MinSamplesLeaf"),
                                                           max_depth=self.get("MaxDepth"),
                                                           max_features=self.get("MaxFeatures"),
                                                           n_iter_no_change=self.get("Patience"),
                                                           validation_fraction=self.get("ValidationSize"),
                                                           tol=self.get("MinDelta"),
                                                           verbose=self.get("Verbosity"),
                                                           random_state=self.m_randomstate)
        if self.isClassificationBDT():
            self.m_scikitmodel = GradientBoostingClassifier(n_estimators=self.get("nEstimators"),
                                                            learning_rate=self.get("LearningRate"),
                                                            loss=loss,
                                                            criterion=self.get("Criterion"),
                                                            min_samples_split=self.get("MinSamplesSplit"),
                                                            min_samples_leaf=self.get("MinSamplesLeaf"),
                                                            max_depth=self.get("MaxDepth"),
                                                            max_features=self.get("MaxFeatures"),
                                                            n_iter_no_change=self.get("Patience"),
                                                            validation_fraction=self.get("ValidationSize"),
                                                            tol=self.get("MinDelta"),
                                                            verbose=self.get("Verbosity"),
                                                            random_state=self.m_randomstate)

    def CompileAndTrain(self, X, Y, W, output_path, Fold=""):
        """
        Training a simple GradientBoosting classifier.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model .joblib files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if Fold != "":
            Fold = "_Fold" + Fold
        if self.get("SmoothingAlpha") is not None:
            Y = self.smooth_labels(Y, K=len(np.unique(Y)), alpha=self.get("SmoothingAlpha"))
        self.BDTCompile()
        _, W_val, _, _ = train_test_split(W, Y, test_size=self.get("ValidationSize"), random_state=self.m_randomstate)
        _, X_val, _, y_val = train_test_split(X, Y, test_size=self.get("ValidationSize"), random_state=self.m_randomstate)
        TrainerMessage("Starting Training...")
        self.m_scikitmodel.fit(X, Y, sample_weight=W)
        TrainerDoneMessage("Training done! Writing model to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".pkl")
        val_loss = []
        for _, y_pred in enumerate(self.m_scikitmodel.staged_predict_proba(X_val)):
            val_loss.append(self.BDTLoss(y_val, y_pred, W_val))
        val_loss = np.array(val_loss)
        history = {"loss": self.m_scikitmodel.train_score_,
                "val_loss": val_loss}
        TrainerDoneMessage("Writing history to " + hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json")
        hist_df = pd.DataFrame(history)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json", mode='w') as f:
            hist_df.to_json(f)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + Fold + ".pkl", 'wb') as f:
            dump(self.m_scikitmodel, f)
