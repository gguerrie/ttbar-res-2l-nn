'''
Module for a homogenous or heterogeneous GNN model
'''
import torch
import json
import pandas as pd
import numpy as np
import HelperModules.HelperFunctions as hf
from torch.nn import Linear, ELU, ReLU, Sigmoid, Tanh, LeakyReLU, Softmax
from torch_geometric.loader import DataLoader
from torch_geometric.nn import Sequential, global_max_pool, global_mean_pool, GCNConv, SAGEConv, GATConv, HeteroConv
from HelperModules.MessageHandler import TrainerMessage, TrainerDoneMessage, ErrorMessage, WarningMessage
from HelperModules.GraphBuilder import GraphBuilder
from ModelModules.BasicModel import BasicModel
from sklearn.model_selection import train_test_split

def get_pytorch_act(act):
    if act == "relu":
        return ReLU(inplace=True)
    if act == "sigmoid":
        return Sigmoid()
    if act == "softmax":
        return Softmax()
    if act == "tanh":
        return Tanh()
    if act == "leaky_relu":
        return LeakyRelu()
    if act == "elu":
        return ELU()

def build_fc(in_nodes, hidden_nodes, hidden_act, out_nodes=None, out_act=None, input_string='x'):
    '''
    Method to create a fully connected (PyTorch) NN.

    Keyword arguments:
    in_nodes (int): Number of input nodes.
    hidden_nodes (list of int): List of integers defining the number of nodes per hidden layer.
    hidden_act (list of string): List of strings containing identifiers for activation functions in the hidden layers.
    out_nodes (int): Number of output nodes.
    out_act (string): String identifier for output activation function.
    input_string (string): String to define the inputs for Sequential_geometric object.

    Returns:
    Sequential('{}'.format(input_string), fc_model) (PyTorch Sequential_geometric object): Fully connected Sequential object.
    '''
    fc_model = []
    prev_nodes = in_nodes
    for nodes, act in zip(hidden_nodes, hidden_act):
        fc_model.append((Linear(prev_nodes, nodes), f'{input_string} -> {input_string}'))
        fc_model.append(get_pytorch_act(act))
        prev_nodes = nodes
    if out_nodes is not None:
        fc_model.append((Linear(prev_nodes, out_nodes), f'{input_string} -> {input_string}'))
    if out_act is not None:
        fc_model.append(get_pytorch_act(out_act))
    return Sequential(f'{input_string}', fc_model)

class GlobalModel(torch.nn.Module):
    '''
    Class for a global (fc) NN for a graph neural network.
    '''
    def __init__(self, global_in_nodes, global_nodes, global_act):
        '''
        Constructur of GlobalModel class.

        Keyword arguments:
        global_in_nodes (int): Number of input nodes.
        global_nodes (list of int): List of integers defining the number of nodes per hidden layer.
        global_act (list of string): List of strings containing identifiers for activation functions in the hidden layers.
        input_string (string): String to define the inputs for Sequential_geometric object.
        '''
        super().__init__()
        self.gl_m = build_fc(global_in_nodes, global_nodes, global_act, input_string='u')

    def forward(self, u):
        '''
        Forward method for global model.

        Keyword arguments:
        u (PyTorch Tensor): [B, F_u], the global feature tensor.

        Returns:
        self.gl_m(u) (PyTorch Tensor): The output of the global model.
        '''
        return self.gl_m(u)

class fcModel(torch.nn.Module):
    '''
    Class for a global (fc) NN for a graph neural network.
    '''
    def __init__(self, in_nodes, hidden_nodes, hidden_act, out_nodes, out_act):
        '''
        Constructur of fcModel class.

        Keyword arguments:
        in_nodes (int): Number of input nodes.
        hidden_nodes (list of int): List of integers defining the number of nodes per hidden layer.
        hidden_act (list of string): List of strings containing identifiers for activation functions in the hidden layers.
        out_nodes (int): Number of output nodes.
        out_act (string): String identifier for output activation function.
        input_string (string): String to define the inputs for Sequential_geometric object.
        '''
        super().__init__()
        self.fc_m = build_fc(in_nodes, hidden_nodes, hidden_act, out_nodes, out_act)

    def forward(self, x):
        '''
        Forward method for global model.

        Keyword arguments:
        x (PyTorch Tensor): [N, F_x], where N is the number of nodes and F_x the number of node features.

        Returns:
        self.fc_m(x) (PyTorch Tensor): The output of the fully connected model.
        '''
        return self.fc_m(x)

class HomoConvModel(torch.nn.Module):
    '''
    Class for a convolutional mode for a graph convolutions.
    '''
    def __init__(self, graph_layers, graph_act, graph_channels):
        '''
        Constructur of HomoConvModel class. Creates a sequential set of graph convolutions.
        The used convolutions are defined via 'graph_layers'.
        Their activation functions are defined via 'graph_act'.

        Keyword arguments:
        graph_layers (list of strings): List of strings containing identifiers for the convolutional layers.
        grap_act (list of strings): List of strings containing the identifiers for the activation functions.
        graph_channels (list of integer): List of Integers defining the hidden channels.
        '''
        super().__init__()
        gr_m = []
        prev_channels = -1
        for layer, act, channels in zip(graph_layers, graph_act, graph_channels):
            if layer == "GCNConv":
                gr_m.append((GCNConv(prev_channels, channels), 'x, edge_index -> x'))
            elif layer == "SAGEConv":
                gr_m.append((SAGEConv((-1, -1), channels), 'x, edge_index -> x'))
            elif layer == "GATConv":
                gr_m.append((GATConv((-1, -1), channels), 'x, edge_index, edge_attr -> x'))
            prev_channels = channels
            gr_m.append(get_pytorch_act(act))
        self.gr_m = Sequential('x, edge_index, edge_attr', gr_m)

    def forward(self, x, edge_index, edge_attr):
        '''
        Forward method for conv model.

        Keyword arguments:
        x (PyTorch Tensor): [N, F_x], where N is the number of nodes and F_x the number of node features.
        edge_index (PyTorch Tensor): [2, E] with max entry N - 1, where E is the number of edges.
        edge_attr (PyTorch Tensor): [E, F_e] with F_e being the edge features.

        Returns:
        self.gr_m(x, edge_index) (PyTorch Tensor): The output of the convolutional model.
        '''
        return self.gr_m(x, edge_index, edge_attr)

class HomoGNNModel(torch.nn.Module):
    '''
    Class for a homogeneous graph NN model. This model combines a global and graph convolutional model and
    puts a fully connected NN on top.
    '''
    def __init__(self, model_metadata):
        '''
        Constructur of HomoGNNModel class.

        Keyword arguments:
        model_metadata (dictionary): Dictionary holding all parameters of the HomoConvModel, GlobalModel and fcModel.
        '''
        super().__init__()
        self.hasglobal = not model_metadata["GlobalInputs"] is None
        self.conv = HomoConvModel(graph_layers=model_metadata["GraphLayers"],
                              graph_act=model_metadata["GraphActivationFunctions"],
                              graph_channels=model_metadata["GraphChannels"])
        if self.hasglobal:
            self.glob = GlobalModel(global_in_nodes=model_metadata["GlobalInputs"],
                                    global_nodes=model_metadata["GlobalNodes"],
                                    global_act=model_metadata["GlobalActivationFunctions"])
            self.fc = fcModel(in_nodes=model_metadata["GlobalNodes"][-1]+model_metadata["GraphChannels"][-1]*2,
                              hidden_nodes=model_metadata["FinalNodes"],
                              hidden_act=model_metadata["FinalActivationFunctions"],
                              out_nodes=model_metadata["OutputSize"],
                              out_act=model_metadata["OutputActivation"])
        else:
            self.fc = fcModel(in_nodes=model_metadata["GraphChannels"][-1]*2,
                              hidden_nodes=model_metadata["FinalNodes"],
                              hidden_act=model_metadata["FinalActivationFunctions"],
                              out_nodes=model_metadata["OutputSize"],
                              out_act=model_metadata["OutputActivation"])

    def forward(self, data):
        '''
        Forward method for homogeneous graph model.

        Keyword arguments:
        data (PyTorch Data object): A data object describing a homogeneous graph. The data object can hold node-level, link-level and graph-level attributes.

        Returns:
        self.fc(out) (PyTorch Tensor): The output of the graph model.
        '''
        x_new = self.conv(data.x.float(), data.edge_index, data.edge_attr)
        x_mean_pool, x_max_pool = global_mean_pool(x_new, data.batch), global_max_pool(x_new, data.batch)
        if self.hasglobal:
            u_new = self.glob(data.u.float())
            out = torch.cat([u_new, x_mean_pool, x_max_pool], dim=1).float()
        else:
            out = torch.cat([x_mean_pool, x_max_pool], dim=1).float()
        return self.fc(out)

class HeteroConvModel(torch.nn.Module):
    '''
    Class for a convolutional mode for a heterogeneous graph convolutions.
    We use the generic wrapper for computing graph convolution on heterogeneous graphs.
    The 'HeteroConv' layer will pass messages from source nodes to target nodes based on the bipartite GNN layer given for a specific edge type.
    The results will be aggregated using the (default) sum function.
    '''
    def __init__(self, graph_layers, graph_act, graph_channels, edge_types):
        '''
        Constructur of HeteroConvModel class. Creates a sequential set of graph convolutions.
        The used convolutions are defined via 'graph_layers'.
        Their activation functions are defined via 'graph_act'.

        Keyword arguments:
        graph_layers (list of strings): List of strings containing identifiers for the convolutional layers.
        grap_act (list of strings): List of strings containing the identifiers for the activation functions.
        graph_channels (list of integer): List of Integers defining the hidden channels.
        edge_types (list of tuple(string, string, string)): List of edge types.
        '''
        super().__init__()
        gr_m = []
        prev_channels = -1
        for layer, act, channels in zip(graph_layers, graph_act, graph_channels):
            if layer == "GCNConv":
                ErrorMessage("GCNConv in heterogeneous models is currently broken. Please use a different convolutional layer.")
                gr_m.append((HeteroConv({
                    edge_type: GCNConv(prev_channels, channels, add_self_loops=False)
                    for edge_type in edge_types
                }), 'x_dict, edge_index_dict -> x_dict'))
            elif layer == "SAGEConv":
                gr_m.append((HeteroConv({
                    edge_type: SAGEConv((-1, -1), channels)
                    for edge_type in edge_types
                }), 'x_dict, edge_index_dict -> x_dict'))
            elif layer == "GATConv":
                gr_m.append((HeteroConv({
                    edge_type: GATConv((-1, -1), channels, add_self_loops=False)
                    for edge_type in edge_types
                }), 'x_dict, edge_index_dict, edge_attr_dict -> x_dict'))
            WarningMessage("Activation functions for heterogeneous graphs have currently no affect on the graph network")
            # gr_m.append(get_pytorch_act(act)) TODO find a way to include activation functions
            prev_channels = channels
        self.gr_m = Sequential('x_dict, edge_index_dict, edge_attr_dict', gr_m)

    def forward(self, x_dict, edge_index_dict, edge_attr):
        '''
        Forward method for graph model.

        Keyword arguments:
        x_dict (dict of PyTorch Tensor): {Node type:[N, F_x]}, where N is the number of nodes and F_x the number of node features.
        edge_index_dict (dict of PyTorch Tensor): {Node type:[2, E]} with max entry N - 1, where E is the number of edges.
        edge_attr_dict (dict of PyTorch Tensor): {Node type:[E, F_e]} with F_e being the edge features.

        Returns:
        self.fc(out) (PyTorch Tensor): The output of the graph model.
        '''
        x_dict = self.gr_m(x_dict, edge_index_dict, edge_attr)
        return x_dict

class HeteroGNNModel(torch.nn.Module):
    '''
    Class for a heterogeneous graph NN model. This model combines a global and graph convolutional model and
    puts a fully connected NN on top.
    '''
    def __init__(self, model_metadata):
        '''
        Constructur of HeteroGNNModel class.

        Keyword arguments:
        model_metadata (dictionary): Dictionary holding all parameters of the HeteroConvModel, GlobalModel and fcModel.
        '''
        super().__init__()
        self.hasglobal = not model_metadata["GlobalInputs"] is None
        self.conv = HeteroConvModel(graph_layers=model_metadata["GraphLayers"],
                                    graph_act=model_metadata["GraphActivationFunctions"],
                                    graph_channels=model_metadata["GraphChannels"],
                                    edge_types=model_metadata["EdgeTypes"])
        if self.hasglobal:
            self.glob = GlobalModel(global_in_nodes=model_metadata["GlobalInputs"],
                                    global_nodes=model_metadata["GlobalNodes"],
                                    global_act=model_metadata["GlobalActivationFunctions"])
            self.fc = fcModel(in_nodes=model_metadata["GlobalNodes"][-1]+model_metadata["GraphChannels"][-1]*model_metadata["NodeTypes"],
                              hidden_nodes=model_metadata["FinalNodes"],
                              hidden_act=model_metadata["FinalActivationFunctions"],
                              out_nodes=model_metadata["OutputSize"],
                              out_act=model_metadata["OutputActivation"])
        else:
            self.fc = fcModel(in_nodes=model_metadata["GraphChannels"][-1]*model_metadata["NodeTypes"],
                              hidden_nodes=model_metadata["FinalNodes"],
                              hidden_act=model_metadata["FinalActivationFunctions"],
                              out_nodes=model_metadata["OutputSize"],
                              out_act=model_metadata["OutputActivation"])

    def forward(self, data):
        '''
        Forward method for graph model.

        Keyword arguments:
        data (Pytorch HeteroData object): A data object describing a heterogeneous graph, holding multiple node and/or edge types in disjunct storage objects.

        Returns:
        self.fc(out) (PyTorch Tensor): The output of the graph model.
        '''
        dataKeys = data.metadata()[0]
        x_dict = self.conv(data.x_dict, data.edge_index_dict, data.edge_attr_dict)
        x_dict = {key: global_mean_pool(x_dict[key], data[key].batch) for key in dataKeys}
        x_dict = torch.cat(list(x_dict.values()), dim=1)
        if self.hasglobal:
            u_new = self.glob(data.u.float())
            out = torch.cat([u_new, x_dict], dim=1).float()
        else:
            out = torch.cat([x_dict], dim=1).float()
        return self.fc(out)

class GNNModel(BasicModel):
    '''
    Class for a graph neural network.
    The class supports the construction of both homogeneous and heterogeneous GNNs.
    '''
    def __init__(self, group, gnodes=None, variables=None):
        super().__init__(group)
        self.m_gnodes = gnodes
        self.m_variables = variables
        self.m_model = None
        self.m_lossfct = None
        self.m_optfct = None
        self.m_options.set_description("GNNMODEL")
        self.m_options.register_option("GraphStructure", optiondefault="homogeneous")
        self.m_options.register_option("RunOnGPU", optiondefault=True)
        self.m_options.register_option("GraphLayers", optiondefault=None, optionislist=True)
        self.m_options.register_option("GraphChannels", optiondefault=None, optionislist=True)
        self.m_options.register_option("GlobalNodes", optiondefault=None, optionislist=True)
        self.m_options.register_option("FinalNodes", optiondefault=None, optionislist=True)
        self.m_options.register_option("GraphActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("GlobalActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("FinalActivationFunctions", optiondefault=None, optionislist=True)
        self.m_options.register_option("OutputSize", optiondefault=None)
        self.m_options.register_option("OutputActivation", optiondefault=None)
        self.m_options.register_option("Epochs", optiondefault=100)
        self.m_options.register_option("BatchSize", optiondefault=32)
        self.m_options.register_option("LearningRate", optiondefault=0.001)
        self.m_options.register_option("ValidationSize", optiondefault=0.25)
        self.m_options.register_option("Optimiser", optiondefault="Nadam")
        self.m_options.register_option("Loss", optiondefault=None)
        self.m_options.register_option("Patience", optiondefault=None)
        self.m_options.register_option("MinDelta", optiondefault=None)

        self.m_options.register_check(optionname="GraphStructure", check="set")
        self.m_options.register_check(optionname="GraphStructure", check="inlist", lst=["homogeneous", "heterogeneous"])
        self.m_options.register_check(optionname="RunOnGPU", check="inlist", lst=[True, False])
        self.m_options.register_check(optionname="GraphLayers", check="set")
        self.m_options.register_check(optionname="GraphLayers", check="is_subset", lst=["GCNConv", "GATConv", "SAGEConv"])
        self.m_options.register_check(optionname="GraphLayers", check="equal_len", l="GraphActivationFunctions")
        self.m_options.register_check(optionname="GraphChannels", check="equal_len", l="GraphActivationFunctions")
        # self.m_options.register_check(optionname="GlobalNodes", check="set")
        # self.m_options.register_check(optionname="GlobalNodes", check="equal_len", l="GlobalActivationFunctions")
        self.m_options.register_check(optionname="FinalNodes", check="set")
        self.m_options.register_check(optionname="FinalNodes", check="equal_len", l="FinalActivationFunctions")
        self.m_options.register_check(optionname="GraphActivationFunctions", check="set")
        self.m_options.register_check(optionname="GraphActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "softmax", "tanh", "elu", "leaky_relu"])
        # self.m_options.register_check(optionname="GlobalActivationFunctions", check="set")
        # self.m_options.register_check(optionname="GlobalActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "softmax", "tanh", "elu", "leaky_relu"])
        self.m_options.register_check(optionname="FinalActivationFunctions", check="set")
        self.m_options.register_check(optionname="FinalActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "softmax", "tanh", "elu", "leaky_relu"])
        self.m_options.register_check(optionname="OutputSize", check="set")
        self.m_options.register_check(optionname="OutputSize", check="larger", thr=0)
        self.m_options.register_check(optionname="OutputLabels", check="equal_len", l="OutputSize")
        self.m_options.register_check(optionname="OutputActivation", check="set")
        self.m_options.register_check(optionname="OutputActivation", check="inlist", lst=["relu", "sigmoid", "softmax", "tanh", "elu", "leaky_relu"])
        self.m_options.register_check(optionname="BatchSize", check="larger", thr=0)
        self.m_options.register_check(optionname="LearningRate", check="larger", thr=0)
        self.m_options.register_check(optionname="ValidationSize", check="larger", thr=0)
        self.m_options.register_check(optionname="Optimiser", check="inlist", lst=["Adadelta", "Adam", "Adamax", "Nadam", "RMSprop"])
        self.m_options.register_check(optionname="Loss", check="set")
        self.m_options.register_check(optionname="Loss", check="inlist", lst=["binary_crossentropy", "categorical_crossentropy"])
        self.m_options.register_check(optionname="Patience", check="larger", thr=0)
        self.m_options.register_check(optionname="MinDelta", check="larger", thr=0)

        for item in self.m_Group:
            self.m_options.set_option(item)
        self.m_options.check_options()

        self.m_device = torch.device('cuda' if torch.cuda.is_available() and self.get("RunOnGPU") else 'cpu')

    def _set_lossfct(self):
        if self.get("Loss") == "binary_crossentropy":
            self.m_lossfct = torch.nn.BCELoss(reduction='none')
        elif self.get("Loss") == "categorical_crossentropy":
            self.m_lossfct = torch.nn.CrossEntropyLoss(reduction='none')

    def _isHomogeneous(self):
        return self.get("GraphStructure") == "homogeneous"

    def _set_optimiser(self):
        '''
        Getter method to return the actual optimiser object
        '''
        optimisers = {"Adadelta": torch.optim.Adadelta(self.m_model.parameters(), lr=self.get("LearningRate")),
                      "Adam": torch.optim.Adam(self.m_model.parameters(), lr=self.get("LearningRate")),
                      "Adamax": torch.optim.Adamax(self.m_model.parameters(), lr=self.get("LearningRate")),
                      "Nadam": torch.optim.NAdam(self.m_model.parameters(), lr=self.get("LearningRate")),
                      "RMSprop": torch.optim.RMSprop(self.m_model.parameters(), lr=self.get("LearningRate"))}
        return optimisers[self.get("Optimiser")]

    def _get_loss(self, out, data, case='binary_crossentropy'):
        '''
        Function to calculate loss.

        Inputs:
        out (PyTorch tensor): Output of the model.
        data (PyTorch Batch object): Batch of graph from which one can extract labels and sample weights
        Returns:
        loss (PyTorch Tensor): The calculated loss on which backpropagation can be performed.
        '''
        if self.get("Loss") == 'binary_crossentropy':
            loss = self.m_lossfct(out, data.y.view(-1, 1).float())
        elif self.get("Loss") == 'categorical_crossentropy':
            loss = self.m_lossfct(out, data.y.long())
        loss = torch.div(
            torch.dot(loss.flatten(), data.w), torch.sum(data.w))
        return loss

    def _train(self, loader):
        '''
        Function to run training epoch.

        Inputs:
        loader (PyTorch loader)
        '''
        self._set_lossfct()
        optimizer = self._set_optimiser()
        self.m_model.train()
        running_loss = 0
        tot = len(loader.dataset)
        for data in loader:  # Iterate in batches over the training dataset.
            data.to(self.m_device) # TO-DO smarter handling of CPU vs GPU usage.
            optimizer.zero_grad() # zero the parameter gradients.
            out = self.m_model(data) # Perform a single forward pass.
            loss = torch.mul(self._get_loss(out, data), len(data)/tot)
            loss.backward()  # Derive gradients.
            optimizer.step()  # Update parameters based on gradients.
            running_loss += loss.item()
        return running_loss

    def _test(self, loader):
        '''
        Function to run testing.

        Inputs:
        loader (PyTorch loader)
        '''

        running_loss = 0
        tot = len(loader.dataset)
        for data in loader:  # Iterate in batches over the validation dataset.
            data.to(self.m_device)
            out = self.m_model(data) # Perform a single forward pass.
            loss = torch.mul(self._get_loss(out, data), len(data)/tot)
            running_loss += loss.item()
        return running_loss

    def _compile(self, nglobal, ntypes, output_path, Fold, edge_types):
        model_metadata = {"GraphStructure": self.get("GraphStructure"),
                          "GraphLayers": self.get("GraphLayers"),
                          "GraphActivationFunctions": self.get("GraphActivationFunctions"),
                          "GraphChannels": self.get("GraphChannels"),
                          "GlobalInputs": nglobal,
                          "NodeTypes": ntypes,
                          "GlobalNodes": self.get("GlobalNodes"),
                          "GlobalActivationFunctions": self.get("GlobalActivationFunctions"),
                          "FinalNodes": self.get("FinalNodes"),
                          "FinalActivationFunctions": self.get("FinalActivationFunctions"),
                          "OutputSize": self.get("OutputSize"),
                          "OutputActivation": self.get("OutputActivation"),
                          "EdgeTypes": edge_types}
        if self._isHomogeneous():
            self.m_model = HomoGNNModel(model_metadata)
        else:
            self.m_model = HeteroGNNModel(model_metadata)
        TrainerMessage(str(self.m_model))
        model_metadata_path = f"%s%s_%s_metadata%s.json"%(hf.ensure_trailing_slash(output_path),
                                                    self.get("Name"),
                                                    self.get("Type"),
                                                    Fold)
        TrainerMessage(f"Writing model metadata to %s"%model_metadata_path)
        with open(model_metadata_path, mode='w') as f:
            json.dump(model_metadata,f)

    def _run_epochs(self, train_loader, val_loader):
        train_loss_lst = []
        val_loss_lst = []
        for epoch in range(1, self.get("Epochs")):
            train_loss = self._train(loader=train_loader)
            val_loss = self._test(val_loader)
            train_loss_lst.append(float(train_loss))
            val_loss_lst.append(float(val_loss))
            TrainerMessage(f'Epoch: {epoch:03d}, Train Loss: {train_loss:.4f}, Val Loss: {val_loss:.4f}')

            if not self.get("MinDelta") is None and not self.get("Patience") is None:
                # Early Stopping
                if epoch == 1:
                    best = val_loss
                    counter = 0
                if val_loss < best - self.get("MinDelta"):
                    best = val_loss
                    counter = 0
                else:
                    counter += 1
                if counter > self.get("Patience"):
                    break
        return {"loss": train_loss_lst,
                "val_loss": val_loss_lst}

    def CompileAndTrain(self, X, Y, W, output_path, Fold=""):
        '''
        Combination of compilation and training for a GNN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        if Fold != "":
            Fold = "_Fold" + Fold
        seed = np.random.randint(1000, size=1)[0]
        xtrain, xval, ytrain, yval = train_test_split(X, Y, test_size=self.get("ValidationSize"), random_state=seed)
        _, _, wtrain, wval = train_test_split(X, W, test_size=self.get("ValidationSize"), random_state=seed)

        TrainerMessage("Creating graphs...")
        xtrain_dict, xval_dict = xtrain.to_dict('records'), xval.to_dict('records')
        train_graphs, val_graphs = [], []
        gb = GraphBuilder(gnodes=self.m_gnodes, variables=self.m_variables)
        if self._isHomogeneous():
            train_graphs = [gb.build_HomGraph(dfrow=r, y=y, w=w) for r, y, w in zip(xtrain_dict, ytrain, wtrain)]
            val_graphs = [gb.build_HomGraph(dfrow=r, y=y, w=w) for r, y, w in zip(xval_dict, yval, wval)]
        else:
            train_graphs = [gb.build_HetGraph(dfrow=r, y=y, w=w) for r, y, w in zip(xtrain_dict, ytrain, wtrain)]
            val_graphs = [gb.build_HetGraph(dfrow=r, y=y, w=w) for r, y, w in zip(xval_dict, yval, wval)]
        TrainerMessage("Graphs are created.")
        train_loader = DataLoader(train_graphs, batch_size=self.get("BatchSize"), shuffle=True)
        val_loader = DataLoader(val_graphs, batch_size=self.get("BatchSize"), shuffle=True)
        self._compile(gb.get_nglobal(),
                      gb.get_ntypes(),
                      output_path,
                      Fold,
                      gb.get_edgetypes())
        self.m_model.to(self.m_device)
        history = self._run_epochs(train_loader, val_loader)
        hist_path = "%s%s_%s_history%s.json"%(hf.ensure_trailing_slash(output_path),
                                             self.get("Name"),
                                             self.get("Type"),
                                             Fold)
        model_path = "%s%s_%s%s.pkl"%(hf.ensure_trailing_slash(output_path),
                                      self.get("Name"),
                                      self.get("Type"),
                                      Fold)
        TrainerDoneMessage("Training done! Writing model to %s"%model_path)
        torch.save(self.m_model.state_dict(), model_path)
        TrainerDoneMessage("Writing history to %s"%hist_path)
        hist_df = pd.DataFrame(history)
        with open(hf.ensure_trailing_slash(output_path) + self.get("Name") + "_" + self.get("Type") + "_history" + Fold + ".json", mode='w') as f:
            hist_df.to_json(f)
